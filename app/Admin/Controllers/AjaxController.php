<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\City;
use App\Property;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function users(Request $request)
    {
        $q = $request->get('q');

        return User::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }
    
    public function properties(Request $request)
    {
        $q = $request->get('q');

        return Property::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }
    
    public function cities(Request $request)
    {
        $q = $request->get('q');
        $q = str_replace("saint", "st", strtolower($q));

        return DB::table("cities")->select(DB::raw('id, CONCAT(nom_commune," ",code_postal) as text'))->where(DB::raw('CONCAT(nom_commune," ",code_postal)'), 'like', "%$q%")->paginate();
    }

    public function countries(Request $request)
    {
        $q = $request->get('q');

        return DB::table("countries")->select(DB::raw('id, nicename as text'))->where('nicename', 'like', "%$q%")->paginate();
    }
}
