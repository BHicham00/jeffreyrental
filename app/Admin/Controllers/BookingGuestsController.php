<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\AdminInfo;
use App\Admin\Extensions\Form\Field\ContractUpload;
use App\Admin\Extensions\Tools\HTMLTool;
use App\BookingGuest;
use App\Country;
use App\Helpers\Mailer;
use App\User;
use App\Company;
use GuzzleHttp\Psr7\MimeType;
use Illuminate\Http\Request;
use Encore\Admin\Show;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Row;
use Encore\Admin\Layout\Column;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class BookingGuestsController extends Controller
{

    use ModelForm;
    use AuthenticatesUsers;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Gestion des clients');
                    $content->description('');

                    $content->body($this->grid());
                });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {

        $user = User::where("id", "=", $id)->first();
        if($user && !$user->company)
        {
            $company = Company::create(['user_id' => $id]);
        }

        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des clients');
                    $content->description('édit du compte client');
                    $content->body($this->form($id)->edit($id));
                });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return redirect()->action('\App\Admin\Controllers\BookingGuestsController@edit', ["id" => $id]);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Gestion des guests');
                    $content->description('');

                    $content->body($this->form());
                });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BookingGuest());
        $user = Admin::user();
        $grid->model()->select('booking_guests.*')
            ->join('bookings', 'bookings.id', '=', 'booking_id')
            ->orderByDesc('bookings.start_at');
        if($user->is_city_manager){
            $users = $user->guests()->get()->pluck('id');
            $grid->model()->whereIn('id', $users);
        }
        $grid->lastname('Nom')->display(function($name){
            return '<a href="'. route('admin.guests.edit', ['bookingguest' => $this->id]) . '">' . $name . '</a>';
        })->sortable();
        $grid->firstname('Prénom')->sortable();
        $grid->column('booking.reference')->display(function($ref){
            return '<a href="'. route('admin.bookings.edit', ['booking' => $this->booking_id]) . '">' . $ref . '</a>';
        })->sortable();
        $grid->column('booking.property', 'Propriété')->display(function($property){
            return '<a href="'. route('admin.properties.edit', ['booking' => $this->booking->property_id]) . '">' . $this->booking->property->name . '</a>';
        })->sortable();
        $grid->column('booking.start_at');
        $grid->column('booking.end_at');
        $grid->email('Email')->sortable();
        $grid->phone('Tel')->sortable();
        $grid->verified("Vérifié")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();

        $grid->actions(function (Grid\Displayers\Actions $actions){
            $actions->disableView();
            $actions->disableDelete();
        });
        $grid->disableCreateButton();
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('lastname', 'Nom');
            $filter->like('firstname', 'Prénom');
            $filter->like('booking.property.name', 'Propriété');
        });
        $grid->export(function(Grid\Exporters\CsvExporter $export){
            $export->originalValue(['verified']);
        });
        $grid->paginate(100);
        return $grid;
    }

    public function identFile($id)
    {
        $guest = BookingGuest::where("id", "=", $id)->first();
        $path = $guest->getIdentFile();
        $filename = explode('/', $guest->ident_file)[1];
        if(file_exists($path))
        {
            return response()->make(file_get_contents($path), 200, [
                'Content-type' => MimeType::fromFilename($filename),
                'Content-Transfer-Encoding' => 'binary',
                'Content-Disposition' => 'filename="' . $filename . '"']);
        }
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(BookingGuest::class, function (Form $form) use ($id)
                {
                    //$form->display('id', 'ID');

                    $form->tab('Guest', function(Form $form)
                    {
                        $form->switch('verified', 'Vérifié ?')->states([
                            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
                        ]);
                        $form->text('lastname', 'Nom')->rules('required');
                        $form->text('firstname', 'Prénom')->rules('required');
                        $form->text('email', 'Email')->rules('email|required');
                        $form->text('phone', 'Téléphone');
                        $form->select('transport_type', 'Type de transport')->options(BookingGuest::getTransportTypes());
                        $form->display('transport_number', 'Numéro de train/vol');
                        $form->datetime('arrival', 'Arrivé');
                        $form->datetime('checkin', 'Checkin');
                        $form->datetime('checkout', 'Checkout');

                    });
                    $form->tab('Identification', function(Form $form) {
                        $form->select('country_id', 'Pays')->options(function ($id)
                        {
                            $country = Country::where('id', $id)->first();;
                            if($country)
                            {
                                return [$country->id => $country->nicename];
                            }
                        })->ajax('/admin/api/countries');
                        $form->select('ident_type')->options(BookingGuest::getIdentTypes());
                        $ident = $form->identFile('ident_file', "Document d'identification")->removable();
                        $ident->options([
                            "initialPreviewDownloadUrl" => "identfile",
                        ])->setPreviewUrl('identfile');
                        $form->text('ident_number');
                        $form->date('ident_expiration');

                        $form->saving(function($form) use ($ident)
                        {
                            $guest = $form->model();
                            /** @var ContractUpload $ident */
                            $ident->name(function($file) use ($guest){
                                return $guest->booking->getIdentFileName($file->guessExtension());
                            });
                            $ident->move($guest->booking->reference);
                        });
                    });

                    if($id)
                    {
                        if(BookingGuest::find($id)->booking->source !== 'Airbnb') {
                            $form->tab('Card', function (Form $form) {
                                $form->select('card_type')->options(BookingGuest::getCardTypes());
                                $form->display('card_holder');
                                $form->display('card_last_digits');
                                $form->display('card_expiration');
                            });
                        }
                        $controller = $this;
                        $form->tools(function(Form\Tools $tools) use ($id, $controller)
                        {
                            $controller->userTools($tools, $id);
                        });
                    }

                    $form->footer(function (Form\Footer $footer)
                    {
                        $footer->checkEditing();
                        $footer->disableCreatingCheck();
                        $footer->disableReset();
                    });

                });

    }
    
    public function userTools($tools, $id){
        $guest = BookingGuest::find($id);
        if($guest) {
            /*$tools->prepend("<div class='btn-group' style='margin-right:5px;'>");
            $tools->prepend("<a class='btn btn-sm btn-primary' target='_blank' href='" . url()->action('\App\Admin\Controllers\BookingGuestsController@logMeAsUser', ["id" => $id]) . "'>");
            $tools->prepend("<i class='fa fa-sign-in'></i><span class='hidden-xs'> Se connecter au compte</span></a></div>");*/

            $sync = new HTMLTool();
            $sync->html = '<a target="_blank" href="/admin/bookings/' . $guest->booking_id . '/edit" class="btn btn-sm btn-primary" title="Réservation"><i class="fa fa-book"></i><span class="hidden-xs"> Réservation</span></a>';
            $tools->append($sync);
        }

        /*$tools->prepend("<div class='btn-group' style='margin-right:5px;'>");
        $tools->prepend("<a class='btn btn-sm btn-primary' href='" . url()->action('\App\Admin\Controllers\UserController@sendWelcomeMail', ["id" => $id]) . "'>");
        $tools->prepend("<i class='fa fa-send'></i><span class='hidden-xs'> Renvoyer le mail de bienvenue</span></a></div>");*/
    }
    
    public function logMeAsUser($id){
        $user = BookingGuest::where('id', '=', $id)->first();
        if($user){
            Auth::guard('bookinguest')->loginUsingId($id);
            return redirect(route('guest.home'));
        }
    }

}
