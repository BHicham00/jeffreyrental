<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Grid\Actions\LinkAction;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\BookingSync\BookingSync;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Property;
use App\Booking;
use App\Helpers\Mailer;
use App\Admin\Extensions\Tools\HTMLTool;
use App\Admin\Extensions\Grid\Actions\SyncBooking;
use App\Admin\Extensions\Grid\Tools\BatchSync;
use App\Admin\Extensions\Grid\Tools\BatchTax;
use App\Admin\Extensions\Grid\Tools\BatchClean;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class BookingsController extends Controller
{

    use ModelForm;

    protected $bookingSync;

    function __construct()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'), //'215b85af3abaed3151b4bc05c05db02f84f821966f36c9617a277b8cf3cdbc7a',
                config('bookingsync.secret'), //'c1f3912e16a72b6c4e017abec3a388b712839f72f84d61c20b2d16e87d8e23e8', 
                route('admin.bookings.index')
        );
    }

    public function index(Request $request)
    {
        if($request->get("code"))
        {
            return redirect("/admin/bookings");
        }
        if($request->get("refresh"))
        {
            try
            {
                $results = $this->synchronizeBookings();
                if($results["status"] != "success")
                {
                    admin_warning($results["results"] . " bookings ont été mis à jour, mais une erreur a interrompu la synchronisation. Veuillez relancer la synchronisation : " . $results["status"]);
                } else
                {
                    admin_success($results["results"] . " bookings ont été mis à jour !");
                }
            } catch(Exception $e)
            {
                admin_error("La liste n'a pas pu être mises à jour, réessayez dans une heure, si le problème persiste appelez votre développeur préféré");
            }
            return redirect("/admin/bookings");
        }

        return Admin::content(function (Content $content)
                {
                    $content->header('Gestion des Réservations');
                    $content->description('');

                    $content->body($this->formCreate());
                    $content->body($this->grid());
                });
    }

    public function incoming(){
        return Admin::content(function (Content $content)
        {
            $grid = $this->incomingGrid();
            $content->header('Arrivés prochaines');
            $content->description('');
            $content->body($grid);
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function incomingGrid()
    {
        $grid = new Grid(new Booking());
        $grid->disableCreateButton();

        $grid->model()->orderBy("start_at", 'desc')
            ->where("status", "Booked")
            ->where("start_at", "<", Carbon::now()->addDays(15))
            ->where("end_at", ">", Carbon::now()->addDays(-10));
        $grid->status("Statut")->display(function($status){
            return "<a href='" . url("/admin/bookings/" . $this->id ."/edit") . "'>$status</a>";
        })->sortable();
        $grid->property()->name('Propriété')->display(function($property){
            return "<a href='" . url("/admin/properties/" . $this->property->id ."/edit") . "'>$property</a>";
        })->sortable();
        $grid->reference('Reference')->sortable();
        $grid->source('Source')->sortable();
        $grid->guest('Client')->display(function($client)
        {
            $client = json_decode($client);
            if(!$client)
            {
                return "Pas d'info. client";
            }
            return $client->firstname . " " . $client->lastname;
        });
        $grid->start_at('Date d\'arrivée')->sortable();
        $grid->end_at('Date de départ')->sortable();
        $grid->amount('Montant')->sortable();

        $grid->need_sync("Synchro?")->display(function($needsync)
        {
            if(!$needsync) {
                $label = "synchro";
                $color = "#2a2";
            }else {
                $label = "non synchro";
                $color = "#b20";
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i></span>";
        })->sortable();
        $grid->column('paid', "Payé ?")->display(function($paid)
        {
            if($paid) {
                $label = "oui";
                $color = "#2a2";
            }else {
                $label = "non";
                $color = "#b20";
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i></span>";
        })->sortable();
        $grid->column('has_guest', "Precheckin ?")->display(function($needsync)
        {
            if($this->bookingguest) {
                $label = "oui";
                $color = "#2a2";
            }else {
                $label = "non";
                $color = "#b20";
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i></span>" . ($this->bookingguest ? ' <a href="'. route('admin.guests.edit', ['bookingguest' => $this->bookingguest->id]) . '">' . $this->bookingguest->firstname . '</a>' : "");
        })->sortable();
        $grid->column('prechekin_sent', "Mail precheckin ?")->display(function($mailed)
        {
            if($mailed) {
                $label = "oui";
                $color = "#2a2";
            }else {
                $label = "non";
                $color = "#b20";
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i></span>";
        })->sortable();
        $grid->column('prechekout_sent', "Mail checkout ?")->display(function($mailed)
        {
            if($mailed) {
                $label = "oui";
                $color = "#2a2";
            }else {
                $label = "non";
                $color = "#b20";
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i></span>";
        })->sortable();

        $grid->actions(function (Grid\Displayers\Actions $actions)
        {
            $actions->add(new SyncBooking());
            $actions->add(new LinkAction("Precheckin", route('admin.bookings.precheckin', ["id" => 'id']), 'precheckin'));
            $actions->add(new LinkAction("Precheckout", route('admin.bookings.precheckout', ["id" => 'id']), 'precheckout'));
            $actions->disableEdit();
            $actions->disableView();
            $actions->disableDelete();
        });
        $grid->tools(function (Grid\Tools $tools)
        {
            /* $reset = new HTMLTool();
              $reset->html = '<a href="resetBookings" class="btn btn-sm btn-danger" title="Tout supprimer"><i class="fa fa-close"></i><span class="hidden-xs"> Tout Supprimer</span></a>';
              $tools->append($reset); */
            /*
            $sync = new HTMLTool();
            $sync->html = '<a href="?refresh=1" class="btn btn-sm btn-primary" title="Vérifier (lourd ne pas faire réguliérement)"><i class="fa fa-refresh"></i><span class="hidden-xs"> Vérifier (lourd ne pas faire réguliérement)</span></a>';
            $tools->append($sync);
            $sync = new HTMLTool();
            $sync->html = '<a href="' . route('admin.bookings.syncGlobal') . '" class="btn btn-sm btn-primary" title="Synchroniser"><i class="fa fa-refresh"></i><span class="hidden-xs"> Synchroniser</span></a>';
            $tools->append($sync);
            */

            $tools->disableRefreshButton();
        });
        $grid->paginate(100);
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->equal('status')->select([Booking::STATUS_BOOKED => Booking::STATUS_BOOKED, Booking::STATUS_UNAVAILABLE => Booking::STATUS_UNAVAILABLE, Booking::STATUS_CANCELED => Booking::STATUS_CANCELED]);
            $filter->equal('paid', 'Payé')->radio(["1" => "payé", "0" => "non payé"]);
            $filter->equal('need_sync', _("Synchro. nécessaire"))->select([0 => "Non", 1 => "Oui"]);
            $filter->like('property.name', 'Propriété');
            $sources = DB::table("bookings")->selectRaw("source")->whereNotNull('source')->groupBy("source")->get()->pluck('source', 'source');
            //$filter->equal('source', 'Source')->multipleSelect($sources);
            $filter->like('source', 'Source');
            $filter->like('guest', 'Client');

            $filter->between('created_at', 'Date de création')->datetime();
            $filter->between('start_at', 'Date d\'arrivée')->datetime();
            $filter->between('end_at', 'Date de fin')->datetime();
            $filter->between('amount', 'Montant (€)');
            $filter->equal('bookingsync_id', 'BookingSync ID');
            $filter->equal('reference', 'Référence');
            $filter->equal('property.keynest_auto', 'Keynest auto')->radio(["1" => "Avec", "0" => "Sans"]);
        });

        $grid->batchActions(function ($batch)
        {
            $batch->add(new BatchSync(trans('admin.batch_sync')));
            $batch->add(new BatchTax(trans('admin.batch_tax')));
            $batch->add(new BatchClean(trans('admin.batch_clean')));
        });
        return $grid;
    }


    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit(Request $request, $id)
    {
        //var_dump($request->all());
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des Réservations');
                    $content->description('détails de la réservation');
                    $form = $this->form($id)->edit($id);
                    $content->body($form);

                    /* $text = new Textarea("link", ["Lien"]);
                      $text->fill(["link" => $form->model()->getLink()]);
                      //$text->disable();
                      $text->setElementClass("full-selection");
                      $content->body($text); */
                });
    }

    public function syncUnsynced(){
        $bookings = Booking::where('need_sync', 1)->get();
        if(!count($bookings)) {
            admin_success("Rien à synchroniser");
            return redirect(url('/admin/bookings/'));
        }
        $ids = $bookings->pluck('id','id');
        try {
            foreach ($ids as $id) {
                $this->synchronize($id, false);
            }
        }catch(Exception $e){
            admin_error($e->getMessage());
        }
        admin_success("Les " . count($ids) . " réservations ont bien été synchronisé");
        return redirect(url('/admin/bookings/'));
    }

    public function syncAjax(Request $request, $ids)
    {
        $clean = $request->get("clean");
        $ids = explode(",", $ids);
        set_time_limit(720);
        foreach($ids as $id)
        {
            $this->synchronize($id, $clean);
        }
        admin_success("Les " . count($ids) . " réservations ont bien été synchronisé");
        echo json_encode(["success" => true, 'message' => "Les " . count($ids) . " réservations ont bien été synchronisé"]);
    }
    
    public function syncTax(Request $request, $ids)
    {
        $ids = explode(",", $ids);
        set_time_limit(720);
        foreach($ids as $id)
        {
            $this->synchronizeTax($id);
        }
        admin_success("Les " . count($ids) . " réservations ont bien été synchronisé");
        echo json_encode(["success" => true, 'message' => "Les " . count($ids) . " réservations ont bien été synchronisé"]);
    }

    public function synchronizeProperty(Request $request, $propertyId)
    {
        $property = Property::where("id", "=", $propertyId)->first();
        $from = new Carbon($request->get("from"));
        $query = 'bookings?include_canceled=true&status=booked,unavailable&rental_id=' . $property->bookingsync_id . '&from=' . $from->format("Ymd");
        $data = $this->bookingSync->getApiData($query);
        if(!$data || !array_key_exists("bookings", $data))
        {
            echo "no booking data";
            valdebug($data);
            die;
        }

        $pages = (int) $data['meta']['X-Total-Pages'];
        $data = $data['bookings'];
        $count = 0;
        $results = [];
        $j = 0;
        for($i = 1; $i <= $pages; $i++)
        {
            if($i != 1)
            {
                $data = $this->bookingSync->getApiData($query . '&page=' . $i);
                if(!$data || !array_key_exists("bookings", $data))
                {
                    echo "no booking data page " . $i;
                    valdebug($data);
                    die;
                }
                $data = $data["bookings"];
            }
            $count += count($data);

            foreach($data as $row)
            {
                $register = Booking::where("reference", "=", $row["reference"])->first();
                if(!$register && $row["canceled_at"] == NULL)
                {
                    $j++;
                    $error = $this->bookingSync->registerBooking($row);
                    if($error)
                    {
                        admin_error($j - 1 . " réservations ont été synchronisées mais le processus a rencontré un erreur, veuillez relancer la synchronisation - ". $row["id"]);
                        return redirect("/admin/properties/" . $propertyId . "/edit?from=" . $from);
                    }
                }else if($register && $row["canceled_at"] != NULL && $register->status != Booking::STATUS_CANCELED){
                    $j++;
                    $register->status = Booking::STATUS_CANCELED;
                    $register->save();
                }else if($register){
                    $start_at = substr($row["start_at"], 0, 10);
                    $end_at = substr($row["end_at"], 0, 10);
                    if($start_at != $register->start_at || $end_at != $register->end_at){
                        $j++;
                        $error = $this->bookingSync->registerBooking($row);
                        if($error)
                        {
                            admin_error($j - 1 . " réservations ont été synchronisées mais le processus a rencontré un erreur, veuillez relancer la synchronisation - ". $row["id"]);
                            return redirect("/admin/properties/" . $propertyId . "/edit?from=" . $from);
                        }
                    }
                }
            }
        }
        admin_success($j . " réservations ont été synchronisées avec succès");
        return redirect("/admin/properties/" . $propertyId . "/edit?from=" . $from);
    }
    
    protected function synchronizeTax($id)
    {
        $booking = Booking::where("id", "=", $id)->first();
        $property = $booking->property;
        $from = new Carbon($booking->start_at);
        $to = new Carbon($booking->end_at);
        //status=booked&rental_id=' . $property->bookingsync_id . '&
        $data = null;
        if($booking->bookingsync_id)
        {
            $data = $this->bookingSync->getApiData('bookings/' . $booking->bookingsync_id . '/?status=booked,unavailable&from=' . $from->format('Ymd'));
        }
        
        if($data && array_key_exists("bookings", $data))
        {
            
            $found = false;
            if(count($data['bookings']) > 0)
            {
                foreach($data['bookings'] as $row)
                {
                    if($row['reference'] == $booking->reference)
                    {
                        $nights = $booking->nights();
                        list ($unit, $total) = $this->bookingSync->calculateTouristTax($row, $nights);
                        if($total) {
                            $booking->tax = $total;
                            $booking->save();
                        }
                        admin_success("la réservation " . $booking->bookingsync_id . " a été mise à jour");
                    }
                }
            }
        }
        return redirect(url('/admin/bookings/' . $id . "/edit"));
    }

    public function synchronize($id, $clean = false)
    {
        $booking = Booking::where("id", "=", $id)->first();
        $property = $booking->property;
        $from = new Carbon($booking->start_at);
        $to = new Carbon($booking->end_at);
        //status=booked&rental_id=' . $property->bookingsync_id . '&
        if($booking->bookingsync_id)
        {
            $data = $this->bookingSync->getApiData('bookings/' . $booking->bookingsync_id . '?status=booked,unavailable&from=' . $from->format('Ymd'));
        } else
        {
            $data = $this->bookingSync->getApiData('bookings/?status=booked,unavailable&rental_id=' . $property->bookingsync_id . '&from=' . $from->format('Ymd') . '&until=' . $to->format('Ymd'));
        }
        //valdebug($data);die;
        if($data && array_key_exists("bookings", $data))
        {
            $found = false;
            if(count($data['bookings']) > 0)
            {
                foreach($data['bookings'] as $row)
                {
                    if($row['reference'] == $booking->reference)
                    {
                        $found = true;
                        if(!$clean){
                            $error = $this->bookingSync->registerBooking($row);
                            if(!$error) {
                                admin_success("la réservation " . $booking->bookingsync_id . " a été mise à jour");
                            }else{
                                admin_error("la réservation " . $booking->bookingsync_id . " n'a pas été mise à jour ---- ". $error);
                            }
                        }
                    }
                }
            }
            if(!$found)
            {
                admin_warning("la réservation " . $booking->bookingsync_id . " n'existe plus");
                $booking->delete();
                return redirect(url('/admin/bookings/'));
            }
        }else{
            admin_error("la réservation " . $booking->bookingsync_id . " n'a pas été mise à jour");
        }
        return redirect(url('/admin/bookings/' . $id . "/edit"));
    }

    protected function synchronizeBookings()
    {
        set_time_limit(720);
        $date = (new Carbon())->startOfMonth();
        $dateEnd = new Carbon();
        $dateEnd->addMonth(2);
        $request = 'bookings?include_canceled=true&status=booked,unavailable&from=' . $date->format("Ymd") . '&until=' . $dateEnd->format("Ymd");
        $data = $this->bookingSync->getApiData($request);
        if(!$data || !array_key_exists("bookings", $data))
        {
            echo "no booking data";
            valdebug($data);
            die;
        }
        //valdebug($data);die;
        $pages = (int) $data['meta']['X-Total-Pages'];
        $data = $data['bookings'];
        $count = 0;
        $results = [];
        $j = 0;
        for($i = 1; $i <= $pages; $i++)
        {
            if($i != 1)
            {
                $data = $this->bookingSync->getApiData($request . '&page=' . $i);
                if(!$data || !array_key_exists("bookings", $data))
                {
                    echo "no booking data page " . $i;
                    valdebug($data);
                    die;
                }
                $data = $data["bookings"];
            }
            $count += count($data);

            foreach($data as $row)
            {
                $property = Property::where("bookingsync_id", "=", $row["links"]["rental"])->first();
                $register = Booking::where("reference", "=", $row["reference"])->first();
                if(!$register && $property && $row["canceled_at"] == NULL)
                {
                    //$error = false;//$this->registerBooking($row);
                    $error = $this->bookingSync->registerBooking($row);
                    if($error){
                        return ["status" => $error, "results" => $j];
                    }else if(Booking::where("reference", "=", $row["reference"])->first()){
                        $j++;
                    }
                }else if($register && $row["canceled_at"] != NULL && $register->status != Booking::STATUS_CANCELED){
                    $j++;
                    $register->status = Booking::STATUS_CANCELED;
                    $register->save();
                }
            }
        }
        //valdebug($j);die;
        return ["status" => "success", "results" => $j];
    }

    public function resetAll()
    {
        Booking::truncate();
        return redirect("/admin/bookings");
    }

    public function show($id)
    {
        return redirect('/admin/bookings/' . $id . "/edit");
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Booking());
        $grid->disableCreateButton();

        $grid->model()->orderBy("start_at", 'desc');
        $grid->status("Statut")->display(function($status){
            return "<a href='" . url("/admin/bookings/" . $this->id ."/edit") . "'>$status</a>";
        })->sortable();
        $grid->property()->name('Propriété')->display(function($property){
            return "<a href='" . url("/admin/properties/" . $this->property->id ."/edit") . "'>$property</a>";
        })->sortable();
        $grid->reference('Reference')->sortable();
        $grid->source('Source')->sortable();
        $grid->guest('Client')->display(function($client)
        {
            $client = json_decode($client);
            if(!$client)
            {
                return "Pas d'info. client";
            }
            return $client->firstname . " " . $client->lastname;
        });
        $grid->created_at('Date de création')->sortable();
        $grid->start_at('Date d\'arrivée')->sortable();
        $grid->end_at('Date de départ')->sortable();
        $grid->amount('Montant')->sortable();
        $grid->tax('T. séjour')->sortable();
        $grid->ota('OTA')->sortable();
        $grid->cleaning('Ménage')->sortable();
        $grid->owner('Virement')->sortable();

        $grid->need_sync("Synchro?")->display(function($needsync)
        {
            if(!$needsync) {
                $label = "synchro";
                $color = "#2a2";
            }else {
                $label = "non synchro";
                $color = "#b20";
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i></span>";
        })->sortable();

        $grid->actions(function (Grid\Displayers\Actions $actions)
        {
            $actions->add(new SyncBooking());
            $actions->disableView();
            $actions->disableDelete();
        });
        $grid->tools(function (Grid\Tools $tools)
        {
            /* $reset = new HTMLTool();
              $reset->html = '<a href="resetBookings" class="btn btn-sm btn-danger" title="Tout supprimer"><i class="fa fa-close"></i><span class="hidden-xs"> Tout Supprimer</span></a>';
              $tools->append($reset); */

            $sync = new HTMLTool();
            $sync->html = '<a href="?refresh=1" class="btn btn-sm btn-primary" title="Vérifier (lourd ne pas faire réguliérement)"><i class="fa fa-refresh"></i><span class="hidden-xs"> Vérifier (lourd ne pas faire réguliérement)</span></a>';
            $tools->append($sync);
            $sync = new HTMLTool();
            $sync->html = '<a href="' . route('admin.bookings.syncGlobal') . '" class="btn btn-sm btn-primary" title="Synchroniser"><i class="fa fa-refresh"></i><span class="hidden-xs"> Synchroniser</span></a>';
            $tools->append($sync);

            $tools->disableRefreshButton();
        });
        $grid->paginate(100);
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->equal('status')->select([Booking::STATUS_BOOKED => Booking::STATUS_BOOKED, Booking::STATUS_UNAVAILABLE => Booking::STATUS_UNAVAILABLE, Booking::STATUS_CANCELED => Booking::STATUS_CANCELED]);
            $filter->equal('paid', 'Payé')->radio(["1" => "payé", "0" => "non payé"]);
            $filter->equal('need_sync', _("Synchro. nécessaire"))->select([0 => "Non", 1 => "Oui"]);
            $filter->like('property.name', 'Propriété');
            $sources = DB::table("bookings")->selectRaw("source")->whereNotNull('source')->groupBy("source")->get()->pluck('source', 'source');
            //$filter->equal('source', 'Source')->multipleSelect($sources);
            $filter->like('source', 'Source');
            $filter->like('guest', 'Client');

            $filter->between('created_at', 'Date de création')->datetime();
            $filter->between('start_at', 'Date d\'arrivée')->datetime();
            $filter->between('end_at', 'Date de fin')->datetime();
            $filter->between('amount', 'Montant (€)');
            $filter->equal('bookingsync_id', 'BookingSync ID');
            $filter->equal('reference', 'Référence');
            $filter->equal('property.keynest_auto', 'Keynest auto')->radio(["1" => "Avec", "0" => "Sans"]);
        });

        $grid->batchActions(function ($batch)
        {
            $batch->add(new BatchSync(trans('admin.batch_sync')));
            $batch->add(new BatchTax(trans('admin.batch_tax')));
            $batch->add(new BatchClean(trans('admin.batch_clean')));
        });
        return $grid;
    }



    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $booking = Booking::where("id", "=", $id)->first();
        $form = new Form(new Booking());
        $client = null;
        if($booking) {
            $client = json_decode($booking->guest);
            if($booking->deposit) {
                $form->switch('deposit.paid', 'Caution Payé')->states([
                    'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                    'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
                ])->disable();
            }
        }
        $form->text('property.name', 'Propriété')->attribute(["disabled" => "disabled"]);
        $form->switch('paid', 'Payé ?')->states([
            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
        ]);
        $form->switch('need_sync', 'Synchro ?')->states([
            'off' => ["value" => 1, "text" => "NON", "color" => "danger"],
            'on' => ["value" => 0, "text" => "OUI", "color" => "success"],
        ])->attribute(["disabled" => true]);
        $form->text('status', 'Statut')->attribute(["disabled" => "disabled"]);
        $form->text('reference', 'Référence')->attribute(["disabled" => "disabled"]);
        $form->text('source', 'Source');
        $form->text('keynest_collection', 'Keynest Collection Code')->attribute(["disabled" => "disabled"]);

        $booking_id = $form->text('bookingsync_id', 'Réservation (cliquez pour la voir)');
        if($booking)
            $booking_id->attribute(["onclick" => "window.open('https://www.bookingsync.com/fr/bookings/$booking->bookingsync_id')"]);
        $form->text('guest.name', 'Client')->attribute(["disabled" => "disabled", "value" => $client ? $client->firstname . " " . $client->lastname : ""]);
        $form->text('guest.email', 'Email de contact')->attribute(["disabled" => "disabled", "value" => $client ? $client->email : ""]);
        $form->text('guest.phone', 'Téléphone du client')->attribute(["disabled" => "disabled", "value" => $client ? $client->phone : ""]);
        $form->text('guest.lang', 'Langue du client')->attribute(["disabled" => "disabled", "value" => $client ? $client->lang : ""]);

        $form->datetime('created_at', 'Date de création')->attribute(["disabled" => "disabled"]);
        $form->date('start_at', 'Arrivée')->attribute(["disabled" => "disabled"]);
        $form->date('end_at', 'Départ')->attribute(["disabled" => "disabled"]);
        if(!$form->isCreating() && $booking)
            $form->number('amount_total', 'Montant Total (€)')->attribute(["disabled" => "disabled"])->default($booking->amount+$booking->cleaning+$booking->tax);
        $form->number('paid_amount', 'Montant Payé (€)')->attribute(["disabled" => "disabled"]);
        $form->number('amount', 'Montant du séjour (€)')->attribute(["disabled" => "disabled"]);
        $form->number('under_adar', 'Sous-montant ADAR (€)')->attribute(["disabled" => "disabled"]);
        $form->number('ota', 'OTA (€)')->attribute(["disabled" => "disabled"]);
        $form->number('profit', 'Com. Jeffrey (€)')->attribute(["disabled" => "disabled"]);
        $form->number('cleaning', 'Ménage (€)')->attribute(["disabled" => "disabled"]);
        $form->number('tax', 'Taxe de séjour (€)')->attribute(["disabled" => "disabled"]);
        $form->number('owner', 'Propriétaire (€)')->attribute(["disabled" => "disabled"]);

        $form->textarea('comment', 'Commentaire Channel')->attribute(["disabled" => "disabled"]);


        $form->disableViewCheck();
        $form->disableReset();
        $form->tools(function(Form\Tools $tools) use ($booking)
        {
            if($booking) {
                if($booking->bookingguest){
                    $sync = new HTMLTool();
                    $sync->html = '<a href="/admin/guests/' . $booking->bookingguest->id . '/edit" class="btn btn-sm btn-primary" title="Fiche Guest"><i class="fa fa-bed"></i><span class="hidden-xs"> Fiche Guest</span></a>';
                    $tools->append($sync);
                }
                if($booking->deposit){
                    $sync = new HTMLTool();
                    $sync->html = '<a href="/admin/deposits/' . $booking->deposit->id . '/edit" class="btn btn-sm btn-primary" title="Caution"><i class="fa fa-bank"></i><span class="hidden-xs"> Caution</span></a>';
                    $tools->append($sync);
                }
                $sync = new HTMLTool();
                $sync->html = '<a href="/admin/properties/' . $booking->property_id . '/edit" class="btn btn-sm btn-primary" title="Fiche propriété"><i class="fa fa-bank"></i><span class="hidden-xs"> Fiche propriété</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="synchronize" class="btn btn-sm btn-primary" title="Mettre à jour"><i class="fa fa-refresh"></i><span class="hidden-xs"> Mettre à jour</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="precheckin" class="btn btn-sm btn-primary" title="Mail precheckin"><i class="fa fa-send"></i><span class="hidden-xs"> Mail Precheckin</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="precheckout" class="btn btn-sm btn-primary" title="Mail precheckout"><i class="fa fa-send"></i><span class="hidden-xs"> Mail Precheckout</span></a>';
                $tools->append($sync);
            }
            $tools->disableView();
        });
        return $form;
    }

    public function precheckin($id){
        try{
            $b = Booking::where("id", "=", $id)->first();
            Mailer::sendGuestInvite($b);
            $b->update(["prechekin_sent" => Carbon::now()]);
            admin_success("mail de precheckin envoyé");
        }catch(\Exception $e) {
            admin_error("Une erreur s'est produite : " . $e->getMessage());
        }
        return redirect(url('/admin/bookings/' . $id . "/edit"));
    }

    public function precheckout($id){
        try {
            $b = Booking::where("id", "=", $id)->first();
            Mailer::sendGuestCheckout($b);
            $b->update(["prechekout_sent" => Carbon::now()]);
            admin_success("mail de precheckout envoyé");
        }catch(\Exception $e) {
            admin_error("Une erreur s'est produite : " . $e->getMessage());
        }
        return redirect(url('/admin/bookings/' . $id . "/edit"));
    }

    public function createSync(Request $request)
    {
        $bId = $request->get("booking_id");
        $booking = Booking::firstOrCreate(["bookingsync_id" => $bId]);
        return $this->synchronize($booking->id);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function formCreate($id = null)
    {
        $form = new \Encore\Admin\Widgets\Form();
        $form->action(url("admin/bookings/createsync"));
        $form->text('booking_id', 'Réservation');
        return $form;
    }
    
    public function reportForm($property, $data)
    {
        $form = new \Encore\Admin\Widgets\Form($data);
        $form->action(url("/admin/properties/".$property->id."/customReport"));
        $form->method('GET');
        $form->date("from", "à Partir du");
        $form->date("to", "Jusqu'au");
        $form->disablePjax();
        $form->disableReset();
        return $form;
    }

}
