<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\AdminHasProperty;
use App\Admin\Extensions\Auth\Database\Administrator;
use App\Invitation;
use App\Property;
use Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Encore\Admin\Show;
use Encore\Admin\Form;
use Encore\Admin\Form\Field\Textarea;
use Encore\Admin\Form\Field\Button;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CityManagerController extends Controller
{

    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public $title = 'Gestion des relations CityManager/propriété';

    public function index()
    {
        $properties = Property::all();
        foreach ($properties as $p) {
            if (!$p->ahp) {
                AdminHasProperty::firstOrCreate([
                    'admin_user_id' => null,
                    'property_id' => $p->id,
                ]);
            }
        }
        return Admin::content(function (Content $content) {
            $content->header('Gestion des relations CityManager/propriété');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Gestion des CityManager');
            $content->description('édition de CityManager');
            $form = $this->form()->edit($id);
            $content->body($form);
        });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Gestion des invitations client');
            $content->description('Vue de l\'invitation');

            $content->body(Admin::show(Invitation::findOrFail($id), function (Show $show) {
                $show->client_id('Client ID');
                $show->token('Token');
                $show->email('Email');
                $show->commission('Commission (%)');
                $show->field('lite', 'Contrat Lite');
                $show->field('bnbkeys', 'Contrat BnbKeys');
                $show->panel()->tools(function ($tools) {
                    $tools->disableEdit();
                });
            }));
            $invit = Invitation::where('id', '=', $id)->first();
            $text = new Textarea("link", ["Lien"]);
            $text->fill(["link" => $invit->getLink()]);
            //$text->disable();
            $text->setElementClass("full-selection");
            $content->body($text);
            $button = new Button('btn-primary', ['Renvoyer l\'invitation']);
            $button->on('click', 'window.top.location = "' . url()->action('\App\Admin\Controllers\InvitationController@send', ["id" => $id]) . '";');
            $content->body($button);

        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return redirect()->action('\App\Admin\Controllers\CityManagerController@index');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AdminHasProperty());
        $managers = Administrator::where('is_city_manager', 1)->get()->pluck('name', 'id')->toArray();
        $managers['1'] = '';
        $grid->admin_user_id('Manager')->sortable()->editable('select', $managers);
        //$grid->token('Token')->sortable();
        $grid->property('Propriété')->name()->display(function ($name) {
            return "<a href='" . url('/admin/managers/' . $this->id . '/edit') . "'>" . $name . "</a>";
        })->sortable();
        $grid->column('city', 'Commune')->display(function ($property) {
            if($this->property) {
                $city = $this->property->city;
                if ($city)
                    return $city->nom_commune . ' - ' . $city->getPostal();
            }
            return $this->property_id;
        });
        $grid->commission('Commission')->editable('text');
        $grid->owned('Premium')/*->display(function($value)
        {
            return "<i style='color:#" . ($value ? "595" : "a55") . "' class='fa " . ($value ? "fa-check" : "fa-close") . "'/>";
        })*/ ->sortable()->switch([
            'on' => ['value' => 1, 'text' => 'oui', 'color' => 'primary'],
            'off' => ['value' => 0, 'text' => 'non', 'color' => 'default'],
        ]);
        $grid->disableCreateButton();
        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('admin_user.name', 'City Manager');
            //$filter->like('property.name', 'Propriété');
            $filter->equal('property_id', 'Propriété')->select()->ajax('/admin/api/properties');
            $filter->notLike('property.name', 'Non présent dans le nom');
            $filter->like('property.city.nom_commune', 'Commune par nom');
            $filter->in('property.city_id', 'Communes')->multipleSelect()->ajax('/admin/api/cities');
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AdminHasProperty::class, function (Form $form) {
            $form->select('admin_user_id', 'City Manager')->options(Administrator::where('is_city_manager', 1)->get()->pluck('name', 'id'));
            $form->text('property.name')->disable();
            $form->number('commission');
            $form->switch('owned', 'Premium');
        });
    }
}
