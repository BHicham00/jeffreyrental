<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\Administrator;
use App\Admin\Extensions\Grid\Actions\Link;
use App\Admin\Extensions\Grid\Actions\SyncBooking;
use App\CmInvoice;
use App\InvoiceMission;
use App\Property;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form\Footer;
use Encore\Admin\Form\Tools;
use Illuminate\Http\Request;
use App\Admin\Extensions\Form\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
use App\Admin\Extensions\Tools\HTMLTool;

class CmInvoicesController extends Controller
{

    use HasResourceActions;

    public function index(Request $request)
    {


        return Admin::content(function (Content $content)
                {
                    $content->header('Gestion des Factures City manager');
                    $content->description('');
                    $content->body($this->grid());
                });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
        {

            $content->header('Gestion des Factures City manager');
            $content->description('créer une facture CM');

            $content->body($this->form());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit(Request $request, $id)
    {
        //var_dump($request->all());
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des Factures City manager');
                    $content->description('détails de la facture');
                    $form = $this->form($id)->edit($id);
                    $content->body($form);

                    /* $text = new Textarea("link", ["Lien"]);
                      $text->fill(["link" => $form->model()->getLink()]);
                      //$text->disable();
                      $text->setElementClass("full-selection");
                      $content->body($text); */
                });
    }

    public function show($id)
    {
        return redirect('/admin/cminvoices/' . $id . "/edit");
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CmInvoice());
        $user = auth('admin')->user();
        if($user->is_city_manager){
            $grid->model()->where('admin_user_id', $user->id);
        }
        $grid->model()->orderBy("start_at", 'desc');
        $grid->label()->display(function ($label) {
            return "<a href='" . url("/admin/cminvoices/" . $this->id . "/edit") . "'>$label</a>";
        })->sortable();
        $manager = $grid->city_manager()->name('City Manager')->sortable();
        if(!$user->is_city_manager) {
            $manager->display(function ($name) {
                return "<a href='" . url("/admin/auth/users/" . $this->admin_user_id . "/edit") . "'>$name</a>";
            });
        }
        $grid->jeffrey('brut jeffrey')->sortable();
        $grid->commission('Commission')->sortable();
        $grid->total_ttc('Total TTC')->sortable();

        $grid->actions(function (Grid\Displayers\Actions $actions)
        {
            $actions->disableView();
            $actions->add(new Link("Télécharger", "cminvoices/".$actions->getKey()."/download", '_blank'));
            //$actions->disableDelete();
        });
        $grid->paginate(100);
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('city_manager.name', 'City Manager');
            //$filter->equal('source', 'Source')->multipleSelect($sources);
            $filter->like('label', 'Label');

            $filter->between('start_at', 'Date d\'arrivée')->date();
            $filter->between('end_at', 'Date de fin')->date();
        });

        $grid->batchActions(function ($batch)
        {
            //$batch->add(new BatchSync(trans('admin.batch_sync')));
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $cmInvoice = CmInvoice::where("id", "=", $id)->first();
        $form = new Form(new CmInvoice());

        $user = auth('admin')->user();
        $form->tab('Général', function(Form $form) use ($user) {
            if (!$form->isCreating()) {
                $form->display('city_manager.name', 'City Manager');
            } else {
                $options = Administrator::where('is_city_manager', 1);
                if($user->is_city_manager){
                    $options->where('id', $user->id);
                 }
                $select = $form->select('admin_user_id', 'City Manager')->options($options->get()->pluck('name', 'id'));
                if($user->is_city_manager){
                    $select->default($user->id);
                }
            }
            if (!$form->isCreating()) {
                $form->display('label', 'Label');
            }
            $form->switch('include_cleanings', 'Inclure les ménages')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);
            $switch = $form->switch('paid', 'Payé ?')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);
            if($user->is_city_manager) {
                $switch->disable();
            }
            $form->date('start_at', 'Date de début');
            $form->date('end_at', 'Date de fin');

            $form->number('static_fee', 'Soutient forfaitaire(€)')->default(0);
            if (!$form->isCreating()) {
                $form->display('jeffrey', 'Brut jeffrey (€)')->attribute(["disabled" => "disabled"]);
                $form->display('commission', 'Commission (€)')->attribute(["disabled" => "disabled"]);
                $form->display('cleaning', 'Reversement ménages (€)')->attribute(["disabled" => "disabled"]);
                $form->display('total_ttc', 'ToTal TTC (€)')->attribute(["disabled" => "disabled"]);
            }
        });
        $form->tab('Missions', function(Form $form) use ($user, $cmInvoice) {
           $form->hasManyThrough('missions',function($form) use ($user, $cmInvoice){
               $options = Property::all();
               if($user->is_city_manager){
                    $options = $user->properties;
               }else if($cmInvoice && $cmInvoice->admin_user_id) {
                   $options = $cmInvoice->city_manager->properties;
               }
               $form->select('property_id', 'Propriété')->options($options->pluck('name', 'id'))->required();
               $form->date('date')->required();
               $form->number('checkin', 'Check-in (€)')->default(0);
               $form->number('checkout', 'Check-out (€)')->default(0);
               $form->number('cleaning', 'Ménage (€)')->default(0);
           });
        });
        $form->tab('Débours', function(Form $form) use ($user, $cmInvoice) {
           $form->hasMany('debours',function($form) use ($user, $cmInvoice){
               $form->date('date')->required();
               $form->text('label', 'Label')->required();
               $form->decimal('value', 'Valeur (€)')->default(0);
           });
        });

        $form->saved(function(Form $form){
            CmInvoice::find($form->model()->id)->calculate();
        });

        $form->disableViewCheck();
        $form->footer(function(Footer $footer){
            $footer->checkEditing();
        });
        $form->tools(function(Tools $tools) use ($user, $cmInvoice)
        {
            if($cmInvoice) {
                if (!$user->is_city_manager) {
                    $sync = new HTMLTool();
                    $sync->html = '<a href="/admin/auth/users/' . $cmInvoice->admin_user_id . '/edit" class="btn btn-sm btn-primary" title="Fiche CM"><i class="fa fa-user"></i><span class="hidden-xs"> Fiche CM</span></a>';
                    $tools->append($sync);
                }
                $sync = new HTMLTool();
                $sync->html = '<a href="synchronize" class="btn btn-sm btn-primary" title="Mettre à jour"><i class="fa fa-refresh"></i><span class="hidden-xs"> Recalculer</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="download" target="_blank" class="btn btn-sm btn-primary" title="Mettre à jour"><i class="fa fa-file"></i><span class="hidden-xs"> Télécharger</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="details" target="_blank" class="btn btn-sm btn-primary" title="Détails"><i class="fa fa-file-code-o"></i><span class="hidden-xs"> Détails</span></a>';
                $tools->append($sync);
            }
            $tools->disableView();
        });
        return $form;
    }

    public function synchronize($id, Request $request)
    {
        $cmInvoice = CmInvoice::find($id);
        $cmInvoice->calculate();
        admin_success('La facture à bien été mise à jour !');
        return redirect('/admin/cminvoices/'.$id.'/edit');
    }
    public function download($id, Request $request)
    {
        $cmInvoice = CmInvoice::find($id);
        $cmInvoice->generateFile();
    }
    public function details($id, Request $request)
    {
        $cmInvoice = CmInvoice::find($id);
        $cmInvoice->getDetailsFile();
    }

}
