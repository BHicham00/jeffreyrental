<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Crons;

class CronLauncherController extends Controller
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Models\CronLauncher';

    /**
     * Index interface.
     *
     * @param Content $content
     *
     * @return Content
     */
    public function index(Request $request, Content $content)
    {
        Admin::disablePjax();
        $running = session('custom_cron_job', (object)['value' => false]);/*Setting::firstOrCreate([
            'key' => 'custom_cron_job',
            'value_type' => 'text'
        ]);*/
        if($running->value) {
            /** @var Carbon $time */
            $time = new Carbon(explode('|', $running->value)[1]);
            $time = $time->floatdiffInMinutes(Carbon::now());
            admin_error('Un tâche Cron "'. explode('|', $running->value)[0] .'" tourne déjà depuis '. floor($time). 'min ' . floor((fmod($time, 1))*60) . 'sec, veuillez attendre qu\'elle soit terminée');
        }
        return $content
            ->title($this->title)
            ->row('<div class="alert alert-warning"><h4><i class="icon fa fa-warning"></i>Permet de lancer individuellement une tâche Cron, attention une utilisation abusive peut ralentir le site voir produire des erreurs</h4></div>')
            ->body($this->listCrons());

    }

    protected function listCrons(){
        $cronPath = app_path('Crons');
        $page = "<div class='form-horizontal'>";
        $running = session('custom_cron_job', (object)['value' => false]);
        $activeCron = $running->value ? explode('|', $running->value)[0] : null;

        foreach(scandir($cronPath) as $file){
            if(is_file($cronPath . DIRECTORY_SEPARATOR . $file)) {
                $file = str_replace('.php', '', $file);
                $class = "App\\Crons\\".$file;
                $page .= '<div class="form-group col-sm-4"><label class="col-sm-8  control-label">' . $class::LABEL . '</label>
                            <div class="col-sm-4">' .
                            adminToolBtn('Lancer', route('admin.cron.launcher.launch', ["cron" => $file]), 'fa-refresh', '_self', $activeCron == $class::LABEL ? 'btn-danger' : 'btn-primary') .
                        '</div></div>';
            }
        }
        $page .= "</div>";
        return $page;
    }

    public function launch($cron, Request $request){
        $class = "App\\Crons\\".$cron;
        $type = $request->get('reward_type');
        $running = session('custom_cron_job', (object)['value' => false]);
        if(class_exists($class) && !$running->value){
            set_time_limit(3600);
            $cronInstance = new $class();
            $running->value = $class::LABEL.'|'.Carbon::now();
            session()->put('custom_cron_job', $running);
            try {
                $cronInstance();
                admin_success('La tâche ' . $cron . ' s\'est bien effectué');
            }catch(\Exception $e){
                admin_error('La tâche ' . $cron . ' a rencontré une erreur : ' . $e->getMessage());
            }
            $running->value = null;
            session()->put('custom_cron_job', $running);
        }else{
            /** @var Carbon $time */
            $time = new Carbon(explode('|', $running->value)[1]);
            $time = $time->floatdiffInMinutes(Carbon::now());
            admin_error('Un tâche Cron "'. explode('|', $running->value)[0] .'" tourne déjà depuis '. floor($time). 'min ' . floor((fmod($time, 1))*60) . 'sec, veuillez attendre qu\'elle soit terminée');
        }
        return redirect(route('admin.cron.launcher'));
    }
}
