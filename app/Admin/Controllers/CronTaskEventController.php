<?php

namespace App\Admin\Controllers;

use App\CronTaskEvent;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class CronTaskEventController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'CronTaskEvent';

    protected $state = [
        0 => 'default',
        1 => 'success',
        2 => 'danger',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CronTaskEvent());

        $grid->model()->orderByDesc('created_at');

//        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('state', 'Etat')
            ->display(function($state) { switch($state) { case 0: return 'En cours';  case 1: return 'Succès';  case 2: return 'Echec'; } })
            ->label($this->state);
        $grid->column('frequency', __('Frequency'));
        $grid->column('errors', __('Errors'))->hide();
        $grid->column('details', __('Details'));
        $grid->column('created_at', "Date")->display(function($date) {
            return Carbon::create($date)->setTimezone('Europe/Paris')->formatLocalized("%d/%m/%Y %R:%S");
        });
//        $grid->column('updated_at', __('Updated at'));

        $grid->actions(function ($actions)
        {
            $actions->disableDelete();
            $actions->disableView();
        });

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->like('name', 'Nom');
            $filter->equal('state')->select([0 => "En cours", 1 => "Succès", 2 => "Echec"]);
            $filter->between('created_at', 'Date')->datetime();
            $filter->like('frequency');
            $filter->where(function ($query) {
                switch ($this->input) {
                    case 'yes':
                        $query->whereNotNull('errors');
                        break;
                    case 'no':
                        $query->whereNull('errors');
                        break;
                }
            }, 'Contient des erreurs', 'errors')->radio([
                '' => 'Tous',
                'yes' => 'Seulement avec erreurs',
                'no' => 'Seulement sans erreurs',
            ]);
        });

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CronTaskEvent());

        $form->text('name', __('Name'));
        $form->select('state', __('State'))->options([0 => "En cours", 1 => "Succès", 2 => "Echec"]);
        $form->text('frequency', __('Frequency'));
        $form->display('errors', __('Errors'));
        $form->text('details', __('Details'));

        return $form;
    }
}
