<?php

namespace App\Admin\Controllers;

use App\BookingSync\BookingSync;
use App\Property;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Admin\Extensions\Grid\FilledGrid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class DailyBookingsController extends Controller
{

    use ModelForm;

    protected $bookingSync;

    public function __construct()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'), //'215b85af3abaed3151b4bc05c05db02f84f821966f36c9617a277b8cf3cdbc7a',
                config('bookingsync.secret'), //'c1f3912e16a72b6c4e017abec3a388b712839f72f84d61c20b2d16e87d8e23e8', 
                url('admin/daily')
        );
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index(Request $request)
    {
        set_time_limit(120);
        return Admin::content(function (Content $content) use ($request)
                {
                    if(!Cache::get("daily_books"))
                    {
                        $dateStart = (new Carbon())->addDays(-3);
                        $data = $this->bookingSync->getApiData('bookings?status=booked&updated_since=' . $dateStart->format('Ymd'));
                        
                        if(!$data || !array_key_exists("bookings", $data))
                        {
                            echo "no booking data";
                            valdebug($data);
                            die;
                        }
                        
                        $bookings = [];
                        
                        $pages = (int) $data['meta']['X-Total-Pages'];
                        $count = 0;
                        $results = [];
                        $j = 0;
                        $sources = [];
                        for($i = 1; $i <= $pages; $i++)
                        {
                            if($i != 1)
                            {
                                $data = $this->bookingSync->getApiData('bookings?status=booked&updated_since=' . $dateStart->format('Ymd') . '&page=' . $i);
                                if(!$data || !array_key_exists("bookings", $data))
                                {
                                    echo "no booking data page " . $i;
                                    valdebug($data);
                                    die;
                                }
                            }
                            
                            foreach($data["bookings"] as $b)
                            {
                                if(!empty($b["links"]["source"]))
                                {
                                    if(!isset($sources[$b["links"]["source"]]))
                                    {
                                        $source = $this->bookingSync->getApiData('sources/' . $b["links"]["source"]);
                                        $b["source"] = $source["sources"][0]["name"];
                                        $sources[$b["links"]["source"]] = $b["source"];
                                    } else
                                    {
                                        $b["source"] = $sources[$b["links"]["source"]];
                                    }
                                }
                                $property = Property::where('bookingsync_id', $b['links']['rental'])->first();
                                unset($b["links"]);
                                $b = json_decode(json_encode($b));
                                $book = new Booking();
                                foreach(Booking::$directFills as $field)
                                {
                                    if(isset($b->$field))
                                    {
                                        $book->$field = $b->$field;
                                    }
                                }
                                if($property){
                                    $book->property_id = $property->id;
                                    $book->property = $property;
                                }
                                /* $book->id = $b->id;
                                  $book->updated_at = new Carbon($b->updated_at);
                                  $book->start_at = new Carbon($b->start_at);
                                  $book->end_at = new Carbon($b->end_at);
                                  $book->reference = $b->reference; */
                                //valdebug($book);die;
                                $bookings[] = $book;
                            }
                        }
                        Cache::add('daily_books', $bookings, 60);
                    } else if(Cache::get('daily_books'))
                    {
                        $bookings = Cache::get('daily_books');
                    }

                    $sort = $request->get("_sort");
                    unset($request['_sort']);
                    uasort($bookings, function($a, $b) use ($sort)
                    {
                        if($sort)
                        {
                            if($sort["type"] == "desc")
                                return $a->{$sort["column"]} < $b->{$sort["column"]};
                            else
                                return $a->{$sort["column"]} > $b->{$sort["column"]};
                        }
                        return $a->created_at < $b->created_at;
                    });
                    $content->header('header');
                    $content->description('description');
                    $content->body($this->grid($bookings));
                });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('header');
                    $content->description('description');

                    $content->body($this->form()->edit($id));
                });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('header');
                    $content->description('description');

                    $content->body($this->form());
                });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($bookings)
    {
        return new FilledGrid(Admin::getModel(Booking::class), function (FilledGrid $grid) use ($bookings)
        {

            $grid->id('ID')->display(function($id)
            {
                return "<a target='_blank' href='https://www.bookingsync.com/fr/bookings/$id'>$id</a>";
            });
            $grid->created_at("Créé le")->sortable();
            $grid->updated_at("Dernière mise à jour")->sortable();
            $grid->property()->name('Propriété')->display(function($property){
                if($this->property)
                    return "<a href='" . url("/admin/properties/" . $this->property->id ."/edit") . "'>$property</a>";
                return 'Inconnu de l\'admin';
            })->sortable();
            $grid->source('Source');
            $grid->final_price('Prix total');
            $grid->paid_amount('Déjà payé');
            $grid->start_at("Début de séjour")->sortable();
            $grid->end_at("Fin de séjour")->sortable();
            $grid->disableActions();
            $grid->disableBatchActions();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->disablePagination();
            $grid->disableExport();
            $grid->disableCreateButton();
            $grid->fillData($bookings);
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(YourModel::class, function (Form $form)
                {

                    $form->display('id', 'ID');

                    $form->display('created_at', 'Created At');
                    $form->display('updated_at', 'Updated At');
                });
    }

}
