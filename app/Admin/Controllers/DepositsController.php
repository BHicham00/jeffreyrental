<?php

namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\BookingSync\BookingSync;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Property;
use App\Deposit;
use App\Helpers\Mailer;
use App\Admin\Extensions\Tools\HTMLTool;
use App\Admin\Extensions\Grid\Actions\ArchiveDeposit;
use App\Admin\Extensions\Grid\Actions\BlockDeposit;
use App\Admin\Extensions\Grid\Actions\MailDeposit;

class DepositsController extends Controller
{

    use ModelForm;

    protected $bookingSync;

    function __construct()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'), //'215b85af3abaed3151b4bc05c05db02f84f821966f36c9617a277b8cf3cdbc7a',
                config('bookingsync.secret'), //'c1f3912e16a72b6c4e017abec3a388b712839f72f84d61c20b2d16e87d8e23e8', 
                route('admin.deposits.index')
        );
    }

    protected function synchronizeDeposits()
    {
        set_time_limit(360);
        Carbon::setLocale("fr");
        $now = new Carbon();
        $formatFrom = $now->addDay(-1)->format("Ymd");
        $now = new Carbon();
        $formatTo = $now->addDay(14)->format("Ymd");
        $deposits = Deposit::all();
        $bookingIds = [];

        foreach($deposits as $d)
        {
            $bookingIds[$d->booking_id] = $d;
        }

        /* $data = $this->bookingSync->getApiData('bookings?id=' . implode(',', $bookingIds));
          $data = $data['bookings'];
          foreach($data as $row)
          {
          $left_to_pay = (float) $row["payment_left_to_collect"];
          foreach($deposits as $d)
          {
          if($row["id"] == $d->booking_id)
          {
          if(count($row["links"]["payments"]) > 0)
          {
          $pay = $this->bookingSync->getApiData('payments/' . $row["links"]["payments"][0]);
          $d->transaction_id = $pay["payments"][0]["transaction_id"];
          $d->order_id = $pay["payments"][0]["order_id"];
          }
          $d->save();
          }
          }
          } */

        $data = $this->bookingSync->getApiData('bookings?status=booked&from=' . $formatFrom . '&until=' . $formatTo);
        $pages = (int) $data['meta']['X-Total-Pages'];
        //valdebug($data);die;
        $data = $data['bookings'];

        $results = array();

        for($i = 1; $i <= $pages; $i++)
        {
            if($i != 1)
                $data = $this->bookingSync->getApiData('bookings?page=' . $i . '&status=booked&from=' . $formatFrom . '&until=' . $formatTo)["bookings"];

            
            foreach($data as $row)
            {
                $booking = array();
                $caution = (float) $row["damage_deposit"];

                $source = null;
                if(!empty($row["links"]["source"]))
                {
                    $sources = $this->bookingSync->getApiData('sources/' . $row["links"]["source"]);
                    $source = $sources["sources"][0]["name"];
                }

                $left_to_pay = (float) $row["payment_left_to_collect"];
                //var_dump("");
                /* if(count($rental) == 0 || (float) $row["paid_amount"] == 0 || $caution == 0 || array_search($row["id"], $bookingIds) !== FALSE || array_search($row["id"], $paidBookingIds) !== FALSE)
                  {
                  continue;
                  } */
                if(!array_key_exists($row["id"], $bookingIds) && $caution > 0 && $source && strtolower($source) !== 'airbnb')
                {
                    //var_dump($row);die;
                    $booking["booking_id"] = $row["id"];
                    $property = Property::where("bookingsync_id", "=", $row["links"]["rental"])->first();
                    if($property)
                    {
                        $rental = $this->bookingSync->getApiData('rentals/' . $row["links"]["rental"]);
                        if($rental && array_key_exists("rentals", $rental) && count($rental["rentals"]))
                        {
                            $rental = $rental["rentals"];

                            $booking["property_id"] = $property->id;
                            $booking["client_id"] = $row["links"]["client"];
                            $client = $this->bookingSync->getApiData('clients/' . $row["links"]["client"]);
                            $client = $client['clients'][0];
                            $booking["client"] = json_encode([
                                "id" => $client["id"],
                                "firstname" => $client["firstname"],
                                "lastname" => $client["lastname"],
                                "email" => count($client["emails"]) ? $client["emails"][0]["email"] : "",
                                "phone" => count($client["phones"]) ? $client["phones"][0]["number"] : "",
                                "lang" => $client["preferred_locale"],
                            ]);
                            if(isset($row["links"]["payments"]) && count($row["links"]["payments"]) > 0)
                            {
                                $pay = $this->bookingSync->getApiData('payments/' . $row["links"]["payments"][0]);
                                $booking["transaction_id"] = $pay["payments"][0]["transaction_id"];
                                $booking["order_id"] = $pay["payments"][0]["order_id"];
                            }
                            $booking["city"] = $rental[0]["city"];
                            $booking["payment_url"] = $row["payment_url"] ?: "";
                            $booking["reference"] = $row["reference"];
                            $booking["amount"] = $caution;
                            $booking["start_at"] = new Carbon($row["start_at"]);
                            $booking["end_at"] = new Carbon($row["end_at"]);
                            $booking["paid"] = false;
                            $booking["token"] = bcrypt($booking["booking_id"] . $client["firstname"] . $client["lastname"] . $booking["amount"]);

                            $deposit = Deposit::where("booking_id", "=", $row["id"])->first();
                            if(!$deposit)
                            {
                                $deposit = Deposit::create($booking);
                            } else
                            {
                                $deposit->fill($booking);
                            }
                            $deposit->save();
                        }
                    }
                }
            }
        }
    }

    public function index(Request $request)
    {
        if($request->get("code"))
        {
            return redirect("/admin/deposits");
        }
        if($request->get("refresh"))
        {
            try
            {
                $this->synchronizeDeposits();
                admin_success("La liste de cautions a été mises à jour !");
            } catch(Exception $e)
            {
                admin_error("La liste de cautions n'a pas pu être mises à jour, réessayez dans une heure, si le problème persiste appelez votre développeur préféré");
            }
            return redirect("/admin/deposits");
        }

        return Admin::content(function (Content $content)
                {
                    $content->header('Gestion des cautions');
                    $content->description('');

                    $content->body($this->formCreate());
                    $content->body($this->grid());
                });
    }

    public function archive($id)
    {
        $deposit = Deposit::where("id", "=", $id)->first();
        $deposit->archived = true;
        $deposit->save();
        echo json_encode(["success" => true, 'message' => "La caution a bien été archivé"]);
    }
    
    public function toggleBlock($id)
    {
        $deposit = Deposit::where("id", "=", $id)->first();
        $deposit->blocked = !$deposit->blocked;
        $deposit->save();
        echo json_encode(["success" => true, 'message' => $deposit->blocked ? "La caution a bien été bloqué" : "La caution a bien été débloqué"]);
    }

    public function sendRequestMail($id)
    {
        $deposit = Deposit::where("id", "=", $id)->first();
        if($deposit)
        {
            Mailer::sendDepositRequest($deposit);
            $deposit->mail_sent = true;
            $deposit->mail_count++;
            $deposit->save();
            admin_success("Mail de caution envoyé!");
        }
        return redirect("/admin/deposits/$id/edit");
    }

    public function jsonMail($id)
    {
        $deposit = Deposit::where("id", "=", $id)->first();
        if($deposit)
        {
            Mailer::sendDepositRequest($deposit);
            $deposit->mail_sent = true;
            $deposit->mail_count++;
            $deposit->save();
            echo json_encode(["success" => true, 'message' => "Mail de caution envoyé!"]);
            die;
        }
        echo json_encode(["success" => false, 'message' => "Erreur à l'envoi du mail!"]);
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit(Request $request, $id)
    {
        //var_dump($request->all());
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des cautions');
                    $content->description('édition de la caution');
                    $form = $this->form($id)->edit($id);
                    $content->body($form);

                    /* $text = new Textarea("link", ["Lien"]);
                      $text->fill(["link" => $form->model()->getLink()]);
                      //$text->disable();
                      $text->setElementClass("full-selection");
                      $content->body($text); */
                });
    }

    public function synchronize($id)
    {
        $deposit = Deposit::where("id", "=", $id)->first();


        $data = $this->bookingSync->getApiData('bookings/' . $deposit->booking_id);
        if(count($data['bookings']) > 0)
        {
            $row = $data['bookings'][0];
            $booking["booking_id"] = $row["id"];
            $rental = $this->bookingSync->getApiData('rentals/' . $row["links"]["rental"])["rentals"];
            $caution = (float) $row["damage_deposit"];
            $left_to_pay = (float) $row["payment_left_to_collect"];
            $property = Property::where("bookingsync_id", "=", $row["links"]["rental"])->first();
            //valdebug($property->name);die;
            if($property)
            {
                $booking["property_id"] = $property->id;
                $booking["client_id"] = $row["links"]["client"];
                $client = $this->bookingSync->getApiData('clients/' . $row["links"]["client"]);
                $client = $client['clients'][0];
                $booking["client"] = json_encode([
                    "id" => $client["id"],
                    "firstname" => $client["firstname"],
                    "lastname" => $client["lastname"],
                    "email" => count($client["emails"]) ? $client["emails"][0]["email"] : "",
                    "phone" => count($client["phones"]) ? $client["phones"][0]["number"] : "",
                    "lang" => $client["preferred_locale"],
                ]);
                if(count($row["links"]["payments"]) > 0)
                {
                    $pay = $this->bookingSync->getApiData('payments/' . $row["links"]["payments"][0]);
                    $booking["transaction_id"] = $pay["payments"][0]["transaction_id"];
                    $booking["order_id"] = $pay["payments"][0]["order_id"];
                }
                $booking["city"] = $rental[0]["city"];
                $booking["payment_url"] = $row["payment_url"] ?: "";
                $booking["reference"] = $row["reference"];
                $booking["amount"] = $caution;
                $booking["start_at"] = new Carbon($row["start_at"]);
                $booking["end_at"] = new Carbon($row["end_at"]);
                $booking["paid"] = false;
                $booking["token"] = bcrypt($booking["booking_id"] . $client["firstname"] . $client["lastname"] . $booking["amount"]);

                $deposit = Deposit::where("booking_id", "=", $row["id"])->first();
                if(!$deposit)
                {
                    $deposit = Deposit::create($booking);
                } else
                {
                    $deposit->fill($booking);
                }
                $deposit->save();
                admin_success('caution '. $deposit->booking_id . ' mise à jour');
            }
        }else{
            admin_warning('la caution '. $deposit->booking_id . ' n\'existe plus, vous pouvez la supprimer si aucune opération n\'est en cours');
        }
        //valdebug(url('/admin/deposits/' . $id . "/edit"));die;
        return redirect(route('admin.deposits.edit', ['deposit' => $id]));
    }

    public function resetAll()
    {
        Deposit::truncate();
        return redirect("/admin/deposits");
    }

    public function show($id)
    {
        return redirect('/admin/deposits/' . $id . "/edit");
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Deposit());
        $grid->disableCreateButton();

        $grid->model()->where('archived', '=', 0)->orderBy("start_at",'desc');
        $grid->property()->name('Propriété')->sortable();
        $grid->amount('Caution dû (€)')->sortable();
        $grid->client('Client')->display(function($client)
        {
            $client = json_decode($client);
            if(!$client){
                return "Erreur récupération client";
            }
            $mail = explode('@',$client->email);
            return $client->firstname . " " . $client->lastname . '<br/>' . (count($mail) > 1 ? $mail[1] : $mail[0]);
        });
        $grid->start_at('Date d\'arrivée')->sortable();
        $grid->end_at('Date de départ')->sortable();
        $grid->actions(function (Grid\Displayers\Actions $actions)
        {
            $actions->disableView();
            $actions->disableDelete();
            $actions->add(new MailDeposit());
            $actions->add(new ArchiveDeposit());
            $actions->add(new BlockDeposit());
        });
        $grid->tools(function (Grid\Tools $tools)
        {
            $reset = new HTMLTool('<a href="resetDeposits" class="btn btn-sm btn-danger" title="Tout supprimer"><i class="fa fa-close"></i><span class="hidden-xs"> Tout Supprimer</span></a>');
            $tools->append($reset);
            $sync = new HTMLTool('<a href="?refresh=1" class="btn btn-sm btn-primary" title="Synchroniser"><i class="fa fa-refresh"></i><span class="hidden-xs"> Synchroniser</span></a>');
            $tools->append($sync);
            $tools->disableRefreshButton();
        });
        $grid->paginate(100);
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('property.name', 'Propriété');
            $filter->like('client', 'Client');
            $filter->between('start_at', 'Date d\'arrivée')->datetime();
            $filter->between('end_at', 'Date de départ')->datetime();
            $filter->gt('amount', 'Valeur (€)');
            $filter->in('paid', 'Payé?')->radio([
                0 => 'non',
                1 => 'oui',
            ]);
            $filter->in('mail_sent', 'Lien envoyé ?')->radio([
                0 => 'non',
                1 => 'oui',
            ]);
            $filter->in('mail_count', 'Mail envoyé 2 fois')->checkbox([
                2 => '',
            ]);
        });
        $grid->paid("Payé")->display(function($paid)
        {
            return "<i style='color:#" . ($paid ? "595" : "a55") . "' class='fa " . ($paid ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->mail_sent("Mail")->display(function($mail)
        {
            return "<i style='color:#" . ($mail ? "595" : "a55") . "' class='fa " . ($mail ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->mail_count("Rappels")->sortable();
        $grid->blocked("Bloqué")->display(function($blocked)
        {
            return "<i style='color:#" . ($blocked ? "a55" : "bbb") . "' class='fa fa-ban'/>";
        })->sortable();
        $grid->status("status")->display(function($status)
        {
            switch($status)
            {
                case "5":
                case "9":
                case "requires_capture":
                case "succeeded":
                    $label = "Autorisé";
                    $color = "#2a2";
                    break;
                case "51":
                case "4":
                case "41":
                case "91":
                    $label = "Autorisation en attente";
                    $color = "#b70";
                    break;
                default:$label = "Aucun paiement";
                    $color = "#000";
                    ;
            }
            return "<span style='color:$color'><i class='fa fa-circle'></i> $label</span>";
        })->sortable();
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $deposit = Deposit::where("id", "=", $id)->first();
        $form = new Form(new Deposit());
        $client = null;
        if($deposit)
        {
            $client = json_decode($deposit->client);
        }
        $form->switch('archived', 'Archivé ?')->states([
            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
        ]);
        $form->switch('blocked', 'Bloqué ?')->states([
            'on' => ["value" => 1, "text" => "OUI", "color" => "danger"],
            'off' => ["value" => 0, "text" => "NON", "color" => "success"]
        ]);

        $form->number('amount', 'Caution (€)')->attribute(["disabled" => "disabled"]);
        $form->switch('paid', 'Payé ?')->states([
            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
        ]);
        $form->switch('mail_sent', 'Mail envoyé ?')->states([
            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
        ]);
        $form->text('reference', 'Référence')->attribute(["disabled" => "disabled"]);
        ;
        $booking_id = $form->text('booking_id', 'Réservation (cliquez pour la voir)');
        if($deposit)
            $booking_id->attribute(["onclick" => "window.open('https://www.bookingsync.com/fr/bookings/$deposit->booking_id')"]);
        $form->text('order_id', 'Merch ID')->attribute(["disabled" => "disabled"]);
        $form->text('transaction_id', 'Transaction ID')->attribute(["disabled" => "disabled"]);
        $form->text('city', 'Ville du séjour')->attribute(["disabled" => "disabled"]);
        $form->text('client.name', 'Client')->attribute(["disabled" => "disabled", "value" => $client ? $client->firstname . " " . $client->lastname : ""]);
        $form->text('client.email', 'Email de contact')->attribute(["disabled" => "disabled", "value" => $client ? $client->email : ""]);
        $form->text('client.phone', 'Téléphone du client')->attribute(["disabled" => "disabled", "value" => $client ? $client->phone : ""]);
        $form->text('client.lang', 'Langue du client')->attribute(["disabled" => "disabled", "value" => $client ? $client->lang : ""]);
        if($deposit)
        {
            $link = route('deposit', ['id' => $deposit->id]) . '?token='.$deposit->token;
            $form->text('payment_link', 'Lien de paiement')->attribute(["value" => $link, "onclick" => "window.open('$link')"]);
        }
        $form->date('start_at', 'Arrivée')->attribute(["disabled" => "disabled"]);
        $form->text('property.name', 'Propriété')->attribute(["disabled" => "disabled"]);

        $form->text('token', 'Token de paiement')->attribute(["disabled" => "disabled"]);
        $pay_id = $form->text('pay_id', 'PayID (Stripe)');
        if($deposit && $deposit->pay_id)
            $pay_id->attribute(["onclick" => "window.open('https://dashboard.stripe.com/acct_1Ez1imJUXIVl1z7c/payments/$deposit->pay_id')"]);
        $form->text('status', 'Status')->attribute(["disabled" => "disabled"]);
        $form->text('PM', 'Moyen de paiement')->attribute(["disabled" => "disabled"]);
        $form->text('card_brand', 'Brand')->attribute(["disabled" => "disabled"]);
        $form->text('card', 'Card')->attribute(["disabled" => "disabled"]);
        $form->text('expire_date', 'Expire Date')->attribute(["disabled" => "disabled"]);
        $form->textarea('comment', 'Commentaire')->placeholder('Commentaire');

        $form->disableViewCheck();
        $form->disableReset();
        $form->tools(function(Form\Tools $tools) use ($deposit)
        {
            if($deposit) {
                $sync = new HTMLTool();
                $sync->html = '<a href="/admin/bookings/' . $deposit->booking->id . '/edit" class="btn btn-sm btn-primary" title="Réservation"><i class="fa fa-book"></i><span class="hidden-xs"> Réservation</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="/admin/properties/' . $deposit->booking->property_id . '/edit" class="btn btn-sm btn-primary" title="Fiche propriété"><i class="fa fa-bank"></i><span class="hidden-xs"> Fiche propriété</span></a>';
                $tools->append($sync);
                $sync = new HTMLTool();
                $sync->html = '<a href="synchronize" class="btn btn-sm btn-primary" title="Mettre à jour"><i class="fa fa-refresh"></i><span class="hidden-xs"> Mettre à jour</span></a>';
                $tools->append($sync);
                $mail = new HTMLTool();
                $mail->html = '<a href="send" class="btn btn-sm btn-warning" title="Envoyer le mail de caution"><i class="fa fa-send"></i><span class="hidden-xs"> Envoyer le mail</span></a>';
                $tools->append($mail);
            }
            $tools->disableView();
        });
        return $form;
    }

    public function createSync(Request $request)
    {
        $bId = $request->get("booking_id");
        $deposit = Deposit::firstOrCreate(["booking_id" => $bId]);
        return $this->synchronize($deposit->id);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function formCreate($id = null)
    {
        $form = new \Encore\Admin\Widgets\Form();
        $form->action(url("admin/deposits/createsync"));
        $form->text('booking_id', 'Réservation');
        return $form;
    }

}
