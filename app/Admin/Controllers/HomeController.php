<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\AdminHasProperty;
use App\Admin\Extensions\Auth\Database\AdminInfo;
use App\Admin\Extensions\Auth\Database\Administrator;
use App\Booking;
use App\City;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Collapse;
use Encore\Admin\Widgets\Form;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;

class HomeController extends Controller
{
    public function index(Request $request)
    {        
        return Admin::content(function (Content $content) use ($request) {
            $time_start = microtime(true);
            $content->header('Dashboard');
            $content->description('Description...');
            $content->row(Dashboard::title());

            $user = Admin::user();
            if($user->is_city_manager){
                AdminInfo::firstOrCreate(['admin_user_id' => $user->id]);
            }
            $statsGlobal = "";
            $users = [];
            if($user->is_city_manager) {
                $users = collect([$user]);
            }else if($user->id == 1){
                $users = Administrator::where('is_city_manager', 1)->get();
            }
            $filters = $request->all();
            $collapse = new Collapse();
            $collapse->add("Filtres", $this->form($filters)->render());
            $content->row($collapse);

            $lava = new Lavacharts();
            $managerFees = AdminHasProperty::all();
            $managerFees = $managerFees->groupBy('property_id')->toArray();
            $Allbookings = Booking::where('status', 'Booked')->where('paid', 1);
            if(array_key_exists("source", $filters) && $filters["source"][0])
            {
                $Allbookings->whereIn('source', $filters["source"]);
            }
            $Allbookings->join('properties', function($join) use ($filters)
            {
                $this->filter($join, $filters);
            });
            $Allbookings = $Allbookings->get();


            $fullyear = array_key_exists("fullyear", $filters) ? $filters["fullyear"] : true;

            foreach($users as $u) {
                $properties = $u->properties()->select('properties.id')->get()->pluck('id');
                $bookings = $Allbookings->whereIn('property_id', $properties);
                $totalCom = 0;

                foreach ($bookings as $booking) {
                    $com = $managerFees[$booking->property_id][0]['commission'];
                    $booking->manager_com = round(($booking->profit / 1.2) * ($com / 100), 2);
                    $totalCom += $booking->manager_com;
                }

                $groupBookings = $bookings->groupBy(function ($val) use ($fullyear) {
                    return Carbon::parse($val->end_at)->format($fullyear ? 'Y-' : 'Y-m');
                });

                /*$time_end = microtime(true);
                $execution_time = ($time_end - $time_start);
                echo '<b>Total Execution Time:</b> '.$execution_time.' Mins'. '<br/>';*/

                /* @var $bookings Collection */
                $datas = [];
                if (count($bookings)) {
                    $dateStart = new Carbon($bookings->first()->start_at);
                    $dateEnd = (new Carbon($bookings->last()->end_at));
                    if(!$fullyear) {
                        while ($dateStart->diffInMonths($dateEnd) > 0) {
                            $datas[$dateStart->format("Y-m")] = [];
                            $dateStart->addMonth();
                        }
                    }else{
                        while ($dateStart->diffInYears($dateEnd) > 0) {
                            $datas[$dateStart->format("Y-")] = [];
                            $dateStart->addYear();
                        }
                    }

                    $datas = array_merge($datas, $groupBookings->toArray());
                    $datas = aggregateBookings($datas, $fullyear, ['manager_com']);
                }

                $collapCharts = new Collapse();

                $columns = [
                    [
                        "title" => "Commissions",
                        "name" => "manager_com",
                        "suffix" => " €"
                    ],
                ];

                bookingsChart($lava, "ColumnChart", $datas, "commissions-".$u->id, $columns, ["#5a5"], false, $fullyear);
                $collapCharts->add("Commissions " . $u->name, "<div id='com-chart-".$u->id."'></div>" . $lava->render('ColumnChart', 'commissions-'.$u->id, 'com-chart-'.$u->id));
                $statsGlobal .= '<h1>Total commissions ' . $u->name . ': ' . $totalCom . ' €</h1>';
                $statsGlobal .= $collapCharts->render();
            }
            $content->body($statsGlobal);
        });
    }


    private function form($data)
    {
        $form = new Form($data);
        $form->action("/admin");
        $form->method("GET");
        $form->hidden("_token")->default(csrf_token());
        $form->radio('fullyear', 'Compacté par année')->options([0 => "non", 1 => "oui"])->default(1);
        $sources = DB::table("bookings")->selectRaw("source")->whereNotNull('source')->groupBy("source")->get();
        $form->multipleSelect('source', 'Sources')->options(function()use($sources){
            $options = [];
            foreach($sources as $s){
                $options[$s->source] = $s->source;
            }
            return $options;
        });
        /*$form->select('groupby', 'Grouper par')->options([
            "end_at" => "date de réservation",
            "created_at" => "date de création"
        ]);*/
        $form->multipleSelect('departement', 'Départments')->options(departements());
        $form->multipleSelect('cities', 'Communes')->options(function($ids){
            if(!$ids)
                $ids = [];
            $cities = City::whereIn('id', $ids)->get();
            $options = [];
            foreach($cities as $city){
                $options[$city->id] = $city->code_postal . ' - ' . $city->nom_commune;
            }
            return $options;
        })->ajax('/admin/api/cities');
        /*$form->multipleSelect('property', 'Propriétés')->options(function()
        {
            $options = [];
            foreach(Property::all() as $p)
            {
                $options[$p->id] = $p->name;
            }
            return $options;
        });*/
        $form->disableReset();
        return $form;
    }

    private function filter($join, $filters)
    {
        $join->on('properties.id', '=', 'property_id');
        if(array_key_exists("cities", $filters) && $filters["cities"][0])
        {
            $join->whereIn('properties.city_id', $filters["cities"]);
        }
        if(array_key_exists("departement", $filters) && $filters["departement"][0])
        {
            $join->whereIn('properties.departement', $filters["departement"]);
        }
        if(array_key_exists("address", $filters) && !empty($filters["address"]))
        {
            $join->where('properties.address', "LIKE", "%" . $filters["address"] . "%");
        }
    }
}
