<?php

namespace App\Admin\Controllers;

use App\Invitation;
use Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Encore\Admin\Show;
use Encore\Admin\Form;
use Encore\Admin\Form\Field\Textarea;
use Encore\Admin\Form\Field\Button;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class InvitationController extends Controller
{

    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content)
                {
                    $content->header('Gestion des invitations client');
                    $content->description('');

                    $content->body($this->grid());
                });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des invitations client');
                    $content->description('édition de l\'invitation client');
                    $form = $this->form()->edit($id);
                    $content->body($form);
                    $text = new Textarea("link", ["Lien"]);
                    $text->fill(["link" => $form->model()->getLink()]);
                    //$text->disable();
                    $text->setElementClass("full-selection");
                    $content->body($text);
                });
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des invitations client');
                    $content->description('Vue de l\'invitation');

                    $content->body(Admin::show(Invitation::findOrFail($id), function(Show $show)
                            {
                                $show->client_id('Client ID');
                                $show->token('Token');
                                $show->email('Email');
                                $show->commission('Commission (%)');
                                $show->field('lite', 'Contrat YELD');
                                //$show->field('bnbkeys', 'Contrat BnbKeys');
                                $show->panel()->tools(function($tools){
                                   $tools->disableEdit(); 
                                });
                            }));
                    $invit = Invitation::where('id', '=', $id)->first();
                    $text = new Textarea("link", ["Lien"]);
                    $text->fill(["link" => $invit->getLink()]);
                    //$text->disable();
                    $text->setElementClass("full-selection");
                    $content->body($text);
                    $button = new Button('btn-primary', ['Renvoyer l\'invitation']);
                    $button->on('click','window.top.location = "'.url()->action('\App\Admin\Controllers\InvitationController@send', ["id" => $id]).'";');
                    $content->body($button);

                });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Gestion des invitations client');
                    $content->description('créer une invitation client');

                    $content->body($this->form());
                });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Invitation());
        $grid->client_id('client ID')->sortable();
        //$grid->token('Token')->sortable();
        $grid->email('Email')->sortable();
        $grid->created_at()->sortable();
        $grid->actions(function ($actions){
            $actions->disableEdit();
        });
        $grid->filter(function ($filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('name', 'name');
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Invitation::class, function (Form $form)
            {
                $clientId = DB::table('users')->selectRaw('MAX(client_id) as max')->first()->max + 1;
                $clientId_invit = DB::table('invitations')->selectRaw('MAX(client_id) as max')->first()->max + 1;
                $clientId = max([$clientId, $clientId_invit]);
                $form->text('client_id', 'Client ID')->rules(function($form){
                    $id = $form->model()->id;
                    $rules = 'unique:users|required';
                    if(!$id) $rules .= '|unique:invitations';
                    return $rules;
                })->fill(["client_id" => $clientId]);
                $form->text('token', 'Token (entrez un texte pour une generation)')->rules(function($form){
                    $id = $form->model()->id;
                    $rules = 'required';
                    if(!$id) $rules .= '|unique:invitations';
                    return $rules;
                });
                $form->text('email', 'Email')->rules(function($form){
                    $id = $form->model()->id;
                    $rules = 'unique:users|email|required';
                    if(!$id) $rules .= '|unique:invitations';
                    return $rules;
                });
                $form->number('commission', 'Commission')->rules('required');
                $form->switch('lite', 'Contrat YELD')->states([
                    'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                    'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
                ]);


                $form->saving(function (Form $form)
                {
                    if($form->token && $form->model()->token != $form->token && $form->model()->token !== bcrypt($form->token))
                    {
                        $form->token = bcrypt($form->token . $form->email);
                    }
                });
                $form->saved(function (Form $form)
                {
                    $email = $form->model()->email;
                    $this->sendInvitation($email, $form->model());
                });
            });
    }

    public function send($id){
        $invit = Invitation::where('id', '=', $id)->first();
        $this->sendInvitation($invit->email, $invit);
        return redirect()->action('\App\Admin\Controllers\InvitationController@show', ["id" => $id]);
    }

    /**
     * @param $email
     * @param Invitation $invite
     */
    protected function sendInvitation($email, $invite){
        Mail::send('emails.invite', [
            'email' => $email,
            'link' => $invite->getLink(),
            'bnbkeys' => $invite->bnbkeys
        ], function ($message) use ($email) {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to($email)->subject("JeffreyHost - demande d'informations");
        });
        admin_success("Invitation envoyée !");
    }

}
