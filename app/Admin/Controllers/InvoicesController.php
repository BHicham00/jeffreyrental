<?php

namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use Encore\Admin\Widgets\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\BookingSync\BookingSync;
use Carbon\Carbon;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\PhpWord;
use Illuminate\Support\Facades\Config;
use App\User;
use App\Property;
use App\Report;
use App\Fee;
use App\ReportHasValue;
use App\Booking;
use Complex\Exception;

class InvoicesController extends Controller
{

    use ModelForm;

    protected $bookingSync;

    function __construct()
    {
        /*$this->bookingSync = new BookingSync(
                config('bookingsync.client'), config('bookingsync.secret'), url('/admin/invoices')
        );*/
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Invoices Creation');
                    $content->description('');
                    $content->body($this->form());
                });
    }

    public function fullReport()
    {
        /*$data = $this->bookingSync->getApiData('bookings?status=booked&from=' . $formatFrom . '&until=' . $formatTo);
        $data = $data['bookings'];
        $results = array();*/
    }
    
    public function create(Request $request)
    {
        set_time_limit(120);
        /* @var $property Property */
        $property = Property::where("id", "=", $request->input('property'))->first();
        if(!$property->bookingsync_id)
        {
            echo 'La propriété n\'est pas relié à bookingsync';
            die;
        }
        $user = $property->user;
        Carbon::setLocale("fr");
        $now = new Carbon();
        $reportId = $request->input('report');
        $report = Report::where("id", "=", $reportId)->first();
        if(!$report)
        {
            $from = new Carbon($request->input('from'));
            $to = new Carbon($request->input('to'));
        } else
        {
            $from = new Carbon($report->start_at);
            $to = new Carbon($report->end_at);
        }
        $clientRef = $user->client_id; //$request->input('client_ref');
        $invoiceNb = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2) . "/" . $clientRef;

        $type = $request->input('type');

        $formatFrom = $from->format("Ymd");
        $formatTo = $to->format("Ymd");

        $jeffreyFees = Fee::where("property_id", "=", $property->id)->where("date", ">=", $from)->where("date", "<=", $to)->get();



        $data = $property->bookings("property_id", "=", $property->id)
                ->where("end_at", ">=", $report->start_at)
                ->where("end_at", "<=", $report->end_at)
                ->where("status", "=", Booking::STATUS_BOOKED)
                ->where("owner", ">", "0")
                ->where("paid", "=", "1")
                ->get();//$this->bookingSync->getApiData('bookings?status=booked&rental_id=' . $property->bookingsync_id . '&from=' . $formatFrom . '&until=' . $formatTo);
        $results = array();

        $totalJeffreyCommission = 0;
        $touristTaxGlobal = 0;
        
        foreach($data as $row)
        {
            $booking = array();

            $booking["start_at"] = new Carbon($row->start_at);
            $booking["end_at"] = new Carbon($row->end_at);
            if($booking["end_at"]->month != $to->month)
            {
                continue;
            }
            $booking["nights"] = $booking["start_at"]->hour(0)->minute(0)->diffInDays($booking["end_at"]->hour(0)->minute(0));

            $booking["commission"] = $row->ota;
            $booking["cleaningFees"] = $row->cleaning;
            $booking["touristTaxTotal"] = $row->tax;
            $touristTaxGlobal += $row->tax;
            $booking["source"] = $row->source;
            $booking["comment"] = $row->comment;
            $matches = array();
            preg_match("/\: (\d{5,})/", $booking["comment"], $matches);
            $booking["reservID"] = "";
            if(!empty($matches))
            {
                $booking["reservID"] = $matches[1];
            }
            $rentalPrice  = $row->amount;
            
            $booking["final_price"] = $row->amount;
            $booking["commission_rate"] = ((float) $row->ota / $row->amount) * 100;
            //$booking["under_price"] = $booking["final_price"] - $booking["commission"];
            $booking["jeffrey"] = $row->profit;
            
            $booking["client_final_price"] = $row->owner;
            $totalJeffreyCommission += $booking["jeffrey"];

            if($booking["end_at"] >= new Carbon("2019-09-01")){
                if(strtolower($booking['source']) == "booking.com" || strtolower($booking['source']) == "airbnb"){
                    $bookingOTA = round(($booking["commission"] - ($booking["cleaningFees"]/($booking["final_price"] + $booking["cleaningFees"])) * $booking["commission"])*100)/100;
                    $booking["commission"] = $bookingOTA;
                    /*if(strtolower($booking['source']) == "airbnb"){
                        $booking["client_final_price"] = $row->amount - $bookingOTA - $row->profit;
                    }*/
                }
            }
            
            array_push($results, $booking);
        }
        /* var_dump($results);
          die;
          /*
          echo $property->bookingsync_id;
          echo $from->toDateTimeString();
          echo $to->toDateTimeString();
         */
        $amount = $totalJeffreyCommission;

        $totalClient = $this->getClientTotal($results, $jeffreyFees, $touristTaxGlobal);

        if($report)
        {
            $this->setDBValue("user_ca", $property, $report, (float) $totalClient);
        }

        //$this->makeInvoice($now, $from, $to, $user, $property, $amount, $clientRef, $invoiceNb);
        $this->makeReport($report, $now, $from, $to, $user, $property, $amount, $clientRef, $invoiceNb, $results, $jeffreyFees, ($type == "report"));
        $this->makeInvoice($now, $from, $to, $user, $property, $amount, $clientRef, $invoiceNb, ($type == "invoice"));
        echo $totalClient;
        die;
    }

    private function setDBValue($label, $property, $report, $value)
    {
        $reportValue = ReportHasValue::firstOrCreate([
                    "label" => $label,
                    "report_id" => $report->id,
                    "property_id" => $property->id,
        ]);
        $reportValue->profit = $value;
        $reportValue->save();
    }

    protected function getClientTotal($bookings, $fees, $touristTax)
    {
        $total_final = 0;
        foreach($bookings as $booking)
        {
            $total_final += $booking["client_final_price"];
        }
        foreach($fees as $fee)
        {
            $total_final -= $fee->value;
        }
        return $total_final;
    }

    protected function makeReport($report, $now, $from, $to, $user, $property, $amount, $clientRef, $invoiceNb, $bookings, $fees, $isAsked)
    {
        $template = "report_template_jeffreyhost.docx";
        if($property->report_tva){
            $template = "report_template_jeffreyhost_TVA.docx";
        }else if($property->lite)
            $template = "report_template_jeffreyhost_lite.docx";
        $invoice = new TemplateProcessor(__DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . $template);
        if(!empty($property->billing))
        {
            $invoice->setValue('address', $property->billing);
        } else
        {
            $invoice->setValue('address', $user->address);
        }
        $invoice->setValue('name', $user->name);
        $invoice->setValue('client_number', $clientRef);
        $invoice->setValue('number', $clientRef . '/' . $invoiceNb);
        $invoice->setValue('property_adress', $property->address);
        $invoice->setValue('from', $from->formatLocalized("%d/%m/%Y"));
        $invoice->setValue('to', $to->formatLocalized("%d/%m/%Y"));
        if($property->report_tva){
            $invoice->setvalue('TVA', $property->user->tva_number);
        }

        $invoice->setValue('date', $now->formatLocalized("%d/%m/%Y"));

        $total_final = 0;
        $total_ota = 0;
        $total_price = 0;
        $total_taxes = 0;
        $total_nights = 0;
        $total_menage = 0;
        $invoice->cloneRow('reserv_dates', count($bookings));
        if($property->report_tva || $property->lite){
            $invoice->cloneRow('menage_dates', count($bookings));
        }
        $i = 1;
        foreach($bookings as $booking)
        {
            $total_ota += $booking["commission"];
            $total_price += $booking["final_price"];
            $total_nights += $booking["nights"];
            $total_final += $booking["client_final_price"];
            $total_taxes += $booking["touristTaxTotal"];

            $invoice->setValue('reserv_dates#' . $i, "du " . $booking["start_at"]->formatLocalized("%d/%m/%Y") . " au " . $booking["end_at"]->formatLocalized("%d/%m/%Y"));
            $invoice->setValue('reserv_id#' . $i, $booking["reservID"]);
            $invoice->setValue('source#' . $i, $booking["source"]);
            $invoice->setValue('commission#' . $i, $booking["commission"]);
            $invoice->setValue('nights#' . $i, $booking["nights"]);
            //$invoice->setValue('tourist_tax#' . $i, $booking["touristTaxTotal"]);
            $invoice->setValue('final_price#' . $i, $booking["final_price"]);
            //$invoice->setValue('under_price#' . $i, $booking["under_price"]);
            $invoice->setValue('jeffrey#' . $i, $booking["jeffrey"]);
            $invoice->setValue('client_final#' . $i, $booking["client_final_price"]);

            if(($property->report_tva || $property->lite)) {
                if ($booking["cleaningFees"] > 0) {
                    $invoice->setValue('menage_dates#' . $i, "du " . $booking["start_at"]->formatLocalized("%d/%m/%Y") . " au " . $booking["end_at"]->formatLocalized("%d/%m/%Y"));
                    $invoice->setValue('menage_id#' . $i, $booking["reservID"]);
                    $invoice->setValue('menage_source#' . $i, $booking["source"]);
                    $cleaning_com = round($booking["cleaningFees"] * ($booking["commission"] / $booking["final_price"]), 2);
                    $invoice->setValue('menage_commission#' . $i, $cleaning_com);
                    $invoice->setValue('menage_price#' . $i, $booking["cleaningFees"]);
                    $invoice->setValue('menage_final#' . $i, $booking["cleaningFees"] - $cleaning_com);
                    $total_menage += $booking["cleaningFees"] - $cleaning_com;
                } else {
                    $invoice->cloneRow('menage_dates#' . $i, 0);
                }
            }

            $i++;
        }
        $i = 1;
        $invoice->cloneRow('deb_name', count($fees));
        $deb_final = 0;
        $deb_norefund = 0;
        foreach($fees as $fee)
        {
            if($fee->value > 0)
            {
                $deb_norefund += $fee->value;
            }
            $deb_final += $fee->value;
            $invoice->setValue('deb_name#' . $i, $fee->info);
            $invoice->setValue('deb_value#' . $i, $fee->value);
            $i++;
        }
        $invoice->setValue('debours_total', $deb_norefund);
        $invoice->setValue('menages_total', $total_menage);
        $invoice->setValue('total_ota', $total_ota);
        $invoice->setValue('total_commission', $amount);
        $invoice->setValue('total_final', $total_final);

        if($property->report_tva) {
            $invoice->setValue('tva_10_label', 'TVA 10%');
            $invoice->setValue('tva_10', $total_final - round(($total_final / 1.1), 2));
            $invoice->setValue('total_final_ht', round(($total_final / 1.1), 2));
        }
        $final_client_ttc = ($property->lite ? $total_final - $deb_final + $total_menage : $total_final - $deb_final);


        $invoice->setValue('total_final_debours', $final_client_ttc);
        $invoice->setValue('total_price', $total_price);
        $invoice->setValue('total_nights', $total_nights);
        //$invoice->setValue('total_taxe', $total_taxes);

        /*
         * possible values :
         * user_ca : virement final du client
         * cleaning : frais de ménage
         * tourist_tax : taxe de séjour
         * nights : nombre de nuités
         * jeffrey : commission jeffrey
         * commission : commission channels
         * brut : total brut
         */
        if($report)
        {
            $this->setDBValue("cleaning", $property, $report, $total_menage);
            $this->setDBValue("tourist_tax", $property, $report, $total_taxes);
            $this->setDBValue("nights", $property, $report, $total_nights);
            $this->setDBValue("jeffrey", $property, $report, $amount);
            $this->setDBValue("commission", $property, $report, $total_ota);
            $this->setDBValue("brut", $property, $report, $total_price);
        }
        

        $invoice->setValue('date', $now->formatLocalized("%d/%m/%Y"));
        $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $filename = $property->getReportFileName($time); //"report_" . $clientRef . "_" . $time . "_" . urlencode($property->name) . ".docx";
        $path = $property->getReportFile($time);
        $invoice->saveAs($path);
        if($isAsked)
        {
            header("Content-Disposition: attachment; filename=" . $filename . "; charset=iso-8859-1");
            echo file_get_contents($path);
            die;
        }
    }

    protected function makeInvoice($now, $from, $to, $user, $property, $amount, $clientRef, $invoiceNb, $isAsked)
    {

        $invoice = new TemplateProcessor(__DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "Facture_template_jeffreyhost.docx");
        if(!empty($property->billing))
        {
            $invoice->setValue('name', $user->name);
            $invoice->setValue('address', $property->billing);
        } else
        {
            $invoice->setValue('name', $user->name);
            $invoice->setValue('address', $user->address);
        }
        $invoice->setValue('client_name', $user->name);
        $invoice->setValue('client_number', $clientRef);
        $invoice->setValue('number', $invoiceNb);
        $invoice->setValue('property_adress', $property->address);

        $invoice->setValue('date', $now->formatLocalized("%d/%m/%Y"));
        $invoice->setValue('from_date', $from->formatLocalized("%d/%m/%Y"));
        $invoice->setValue('to_date', $to->formatLocalized("%d/%m/%Y"));

        //$amount = 510;

        $invoice->setValue('amount', $amount . "€");
        $invoice->setValue('ht_amount', $amount - round(($amount * 10) / 6) / 10 . "€");
        $invoice->setValue('TVA', round(($amount * 10) / 6) / 10 . "€");
        $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $filename = $property->getInvoiceFileName($time); //"report_" . $clientRef . "_" . $time . "_" . urlencode($property->name) . ".docx";
        $path = $property->getInvoiceFile($time);
        $invoice->saveAs($path);
        if($isAsked)
        {
            header("Content-Disposition: attachment; filename=" . $filename . "; charset=iso-8859-1");
            echo file_get_contents($path);
            die;
        }
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form();
        $form->method('GET');
        $form->action("/admin/invoices/create");
        $form->select("property", "Property")->options(function ()
        {
            $properties = Property::all();
            $values = [];
            foreach($properties as $prop)
            {
                $values[$prop->id] = $prop->name;
            }
            return $values;
        });
        $form->select("report", "Report")->options(function ()
        {
            $reports = Report::all();
            $values = [];
            foreach($reports as $report)
            {
                $values[$report->id] = $report->name;
            }
            return $values;
        });
        $form->date("from");
        $form->date("to");
        $form->select("type", "Type de document")->options(array(
            "report" => "Relevé",
            "invoice" => "Facture",
        ));
        $form->hidden("_token")->default(csrf_token());
        $form->disablePjax();
        return $form;
    }

}
