<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Grid\Actions\Link;
use App\Admin\Extensions\Tools\HTMLTool;
use App\City;
use App\Http\Controllers\Controller;
use App\KeynestStore;
use App\Property;
use App\Services\KeynestService;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form\Tools;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use App\Admin\Extensions\Form\Form;
use Encore\Admin\Widgets;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KeynestController extends Controller
{


    public function index(Request $request)
    {

        return Admin::content(function (Content $content) use ($request) {
            $content->header('Keynest Manager');
            $content->description('gestion des clés Keynest');
            $content->body($this->grid());
        });
    }

    public function edit(Request $request, $id)
    {

        return Admin::content(function (Content $content) use ($request, $id) {
            $content->header('Keynest Manager');
            $content->description('gestion des clés Keynest');
            $content->body($this->form($id)->edit($id));
        });
    }

    private function grid()
    {
        $grid = new Grid(new Property());
        $grid->name();
        $grid->keynest_key_id()->sortable();
        $grid->disableCreateButton();
        $grid->actions(function(Grid\Displayers\Actions $actions){
            $actions->disableView();
            $actions->disableDelete();
            if(empty($actions->getAttribute('keynest_key_id'))) {
                $actions->add(new Link('Créer', url()->action('\App\Admin\Controllers\KeynestController@newKey', ["id" => $actions->getKey()])));
            }
        });
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('name', 'Label');
            $filter->like('bookingsync_id', 'Bookingsync ID');
            $filter->like('departement', 'Département')->select(departements());
        });
        return $grid;
    }

    public function testKey(){
        $keynest = new KeynestService();
        $property = Property::limit(1)->first();
        if(!$property->keynest_key_id) {
            $stores = $keynest->listStores('nice');
            if ($stores->Status == "Success") {
                $storeId = $stores->ResponsePacket->StoreList[0]->StoreId;
                $key = $keynest->newKey($property, $storeId);
                dump($key);
                if($key->Status == "Success"){
                    $key = $key->ResponsePacket;
                    $property->keynest_key_id = $key->KeyId;
                    $property->keynest_store_id = $storeId;
                    $property->keynest_dropoff = $key->DropOffCode;
                    $property->keynest_collection = $key->CollectionCode;
                    $property->save();
                }else{
                    dd($key);
                }
            }
        }
        dump($keynest->shareCode($property, "vfran123@gmail.com"));
        dump($keynest->getKey($property->keynest_key_id));
    }

    public function newKey(Request $request, $id){
        $property = Property::find($id);
        if(!empty($_POST) && !$property->keynest_key_id){
            $keynest = new KeynestService();
            $storeId = $request->get('keynest_store_id');
            $key = $keynest->newKey($property, $storeId);
            if($key->Status == "Success"){
                $key = $key->ResponsePacket;
                $property->keynest_key_id = $key->KeyId;
                $property->keynest_store_id = $storeId;
                $property->keynest_dropoff = $key->DropOffCode;
                $property->keynest_collection = $key->CollectionCode;
                $property->save();
                admin_success("Clé Keynest créée avec succès ! " . $property->keynest_store_id);
                return redirect('/admin/keynest/' . $id . '/edit');
            }else{

                admin_error("Erreur à la création de clé - " . print_r($key));
            }
        }
        return Admin::content(function (Content $content) use ($request, $id, $property) {
            $content->header('Keynest Manager');
            $content->description('Création d\'une clé pour la propriété "' . $property->name . '"');
            $content->body((new Widgets\Box())->content(
                "<h1>$property->name</h1>".
                        "<h3>$property->address</h3>"
            ));
            $content->body($this->createKeyForm($property));
        });
    }

    private function createKeyForm($property){
        $keynest = new KeynestService();
        $form = new Widgets\Form();
        $form->action(url()->action('\App\Admin\Controllers\KeynestController@newKey', ["id" => $property->id]));
        $form->select('keynest_store_id', 'Stores')->options(function($storeId) use ($keynest, $property){
            $stores = $keynest->nearestStores($property);
            $options = [];
            foreach($stores->ResponsePacket->StoreList as $s){
                $options[$s->StoreId] = $s->StoreName . ' | ' . $s->StoreStreetAddress;
            }
            return $options;
        });
        return $form;
    }


    private function form($id)
    {
        $property = Property::find($id);
        $keynest = new KeynestService();
        $form = new Form(new Property());
        $form->switch('keynest_auto', 'Gestion des clés autonomes (Keynest)')->states([
            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
        ]);
        $form->display('keynest_key_id', 'KeyID');
        $form->select('keynest_store_id', 'Stores')->options(function($storeId) use ($keynest){
            $stores = null;
            $options = [];
            if($storeId){
                $stores = KeynestStore::where('id', $storeId)->get();
            }
            if($stores) {
                foreach ($stores as $s) {
                    $options[$s->id] = $s->name . ' | ' . $s->address;
                }
            }
            return $options;
        })->disable();
        $form->display('keynest_dropoff', 'Code de dépot');
        $form->display('keynest_collection', 'Code de collection');
        $form->tools(function(Tools $tools) use ($property){
            if(!$property->keynest_key_id){
                $createLink = new HTMLTool();
                $createLink->html = "<div class='btn-group' style='margin-right:5px;'>
                <a class='btn btn-sm btn-success' href='" . url()->action('\App\Admin\Controllers\KeynestController@newKey', ["id" => $property->id]) . "'>
                <i class='fa fa-plus'></i><span class='hidden-xs'> Créer une clé Keynest</span></a></div>";
                $tools->add($createLink);
            }
            $tools->disableView();
            $tools->disableDelete();
        });
        $form->disableReset();
        $form->disableSubmit();
        return $form;
    }
}
