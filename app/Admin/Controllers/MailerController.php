<?php

namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use Encore\Admin\Widgets\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use App\User;
use App\Property;
use Encore\Admin\Widgets\Tab;
use Mail;

class MailerController extends Controller
{

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content)
            {

                $content->header('Mailer');
                $content->description('ENvoit groupé de mail');
                $tabs = new Tab();
                $tabs->add('Choix Clients', $this->form());
                $content->row($tabs);
            });
    }
    
    public function send(Request $request){
        $content = $request->get("mail");
        $subject = $request->get("subject");
        $file = $request->file("attachfile");
        $users = $request->get("users");
        $users = User::whereIn("id", $users)->get();
        foreach($users as $u){
            Mail::send('emails.custom', [ 
                'content' => $content
            ], function (\Illuminate\Mail\Message $message) use ($u, $subject, $file){
                $message->from("noreply@jeffrey-conciergerie.com", "Jeffrey-Conciergerie");
                $email = "domainseasy@gmail.com";
                if(config("app.env") === "production")
                {
                    $email = $u->email;
                }
                $message->to($email)->subject($subject)->bcc("developer@jeffrey-conciergerie.com");
                if($file){
                    $message->attach($file->getPathname(), ["as" => $file->getClientOriginalName(), "mime" => $file->getClientMimeType()]);
                }
            });
        }
        admin_success("Mails envoyés !");
        return redirect("/admin/mailer");
    }
    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form();
        $form->action("/admin/mailer/send");
        $form->attribute("enctype", "multipart/form-data");
        $form->hidden("_token")->default(csrf_token());
        $form->listbox("users", "Clients")->options(function(){
            $returns = [];
            foreach(User::where("archived", "=", "0")->get() as $u){
                $returns[$u->id] = $u->name;
            }
            return $returns;
        });
        $form->text('subject', "Sujet");
        $form->file('attachfile', "Pièce jointe");
        $form->editor('mail', "Mail HTML");
        /*$form->select("type")->options([
            "properties",
            "clients",
        ]);*/
        return $form;
    }

}
