<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\Administrator;
use App\Booking;
use App\Country;
use App\Helpers\Mailer;
use App\KeynestStore;
use App\Services\KeynestService;
use App\User;
use App\City;
use App\Amenity;
use App\Report;
use App\Property;
use App\PropertyDetails;
use App\PropertyAmenity;
use Encore\Admin\Form\Footer;
use Symfony\Component\HttpFoundation\Request;
use Encore\Admin\Show;
use App\Admin\Extensions\Form\Form;
use Encore\Admin\Form\Field\Button;
use Encore\Admin\Grid;
use Encore\Admin\Widgets\Collapse;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\BookingSync\BookingSync;
use Carbon\Carbon;
use App\Admin\Extensions\Tools\HTMLTool;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Admin\Extensions\TemplateCustom;
use PhpOffice\PhpWord\PhpWord;
use Lava;
use Khill\Lavacharts\Lavacharts;
use Khill\Lavacharts\DataTables\DataTable;
use Khill\Lavacharts\Charts\BarChart;
use App\Admin\Extensions\Form\Field\ContractUpload;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class PropertyController extends Controller
{

    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content)
            {

                $content->header('Gestion des propriétés');
                if(isset($_GET['message'])){
                    admin_toastr($_GET['message'], 'success');
                }if(isset($_GET['error'])){
                    admin_toastr($_GET['error'], 'error');
                }
                $content->description('.');
                $content->body($this->grid());
                
            });
    }
    public function console_log($data){
        echo '<script>';
        echo 'console.log('. json_encode( $data ) .')';
        echo '</script>';
      }      

    public function signedContract($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getContractFile();
        $filename = $property->getContractFileName();
        if($property->hasContract())
        {
            attachFile($path, $filename);
        }
    }

    public function annexe($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getAnnexeFile();
        $filename = $property->getAnnexeFileName();
        if($property->hasAnnexe())
        {
            attachFile($path, $filename);
        }
    }

    public function ranking($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getRankingFile();
        $filename = $property->getRankingFileName();
        if($property->hasRanking())
        {
            attachFile($path, $filename);
        }
    }

    public function attestLoc($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getAttestLocFile();
        $filename = $property->getAttestLocFileName();
        if($property->hasAttestLoc())
        {
            attachFile($path, $filename);
        }
    }

    public function inventory($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getInventoryFile();
        $filename = $property->getInventoryFileName();
        if($property->hasInventory())
        {
            attachFile($path, $filename);
        }
    }

    public function assessment($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getAssessmentFile();
        $filename = $property->getAssessmentFileName();
        if($property->hasAssess())
        {
            attachFile($path, $filename);
        }
    }

    public function keysfile($id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getKeysFile();
        $filename = $property->getKeysFileName();
        if($property->hasKeysFile())
        {
            attachFile($path, $filename);
        }
    }

    public function uploadContract(Request $request, $id)
    {
        $property = Property::where("id", "=", $id)->first();
        $path = $property->getContractFolder();
        $filename = $property->getContractFileName();
        if(!$property->hasContract())
        {
            //var_dump($request->file('contract'));die;
            $file = $request->file('contract');
            $file->move($path, $filename);
            Mailer::sendContractSentMail($property);
            admin_success("Contrat uploadé !");
        }
        return redirect()->action('\App\Admin\Controllers\PropertyController@edit', ["id" => $id]);
    }

    public function deleteContract(Request $request, $id)
    {
        $property = Property::where("id", "=", $id)->first();
        if($property->hasContract())
        {
            unlink($property->getContractFile());
            admin_success("Contrat supprimé !");
        }
        return redirect()->action('\App\Admin\Controllers\PropertyController@edit', ["id" => $id]);
    }

    /*public function sendContractMail($id)
    {
        $property = Property::where("id", "=", $id)->first();
        if(!$property->hasContract())
        {
            Mailer::sendUserPropertyRecall($property->getUser(), $property);
            admin_success("Mail de rappel envoyé!");
        }
        return redirect()->action('\App\Admin\Controllers\PropertyController@edit', ["id" => $id]);
    }*/

    public function show($id)
    {
        return redirect()->action('\App\Admin\Controllers\PropertyController@edit', ["id" => $id]);
    }

    protected function checkPropertyStatus($property)
    {
        $amenities = Amenity::all();
        if(count($property->amenities()->get()) < count($amenities))
        {
            foreach($amenities as $a)
            {
                $pa = PropertyAmenity::where("amenity_id", "=", $a->id)->where("property_id", "=", $property->id);
                if(!$pa->first())
                {
                    PropertyAmenity::create([
                        'property_id' => $property->id,
                        'amenity_id' => $a->id,
                        'value' => 0,
                    ]);
                }
            }
        }
        if(!$property->detail)
            $details = PropertyDetails::create(['property_id' => $property->id]);

        $messages = [];
        if(!$property->bookingsync_id || empty($property->bookingsync_id))
        {
            $messages[] = "BookingSync ID manquant";
        }

        /*if(!$property->hasContract())
        {
            $messages[] = "Contrat manquant";
        }*/
        if(!$property->hasAttestLoc())
        {
            $messages[] = "Attestation location manquante";
        }
        if(!$property->hasRanking())
        {
            $messages[] = "Classement manquant";
        }
        if(!$property->hasInventory())
        {
            $messages[] = "Inventaire manquant";
        }
        if(!$property->hasAssess())
        {
            $messages[] = "Etats des lieux manquant";
        }
        if(!$property->hasKeysFile())
        {
            $messages[] = "Remise des clés manquante";
        }

        if(!empty($messages))
            admin_warning(implode(" | ", $messages));
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit(Request $request, $id)
    {
        $property = Property::where("id", "=", $id)->first();
        $property_details = PropertyDetails::where('property_id','=',$id)->first();
        
        if($property)
        {
            $this->checkPropertyStatus($property);
        }
        return Admin::content(function (Content $content) use ($request, $property, $id)
                {

                    $content->header('Gestion de "' . $property->name . '"');
                    $content->description('Dashboard de la propriété');


                    $tab = new \Encore\Admin\Widgets\Tab();
                    // KEYSFORM
                    if(!$property->hasKeysFile())
                    {
                        $collapse = new Collapse();
                        $collapse->add("Générateur de remise des clés", $this->keysForm($request->all())->render());
                        $content->row($collapse);
                    }

                    // STATS
                    $tab->add("Stats", $this->getCharts($property));

                    // EDIT FORM
                    $tab->add("Informations", $this->form($id)->edit($id));

                    // REPORTS
                    $tab->add("Rapports", $this->getReports($property));


                    $content->row($tab);

                    $content->body(Admin::show(Property::findOrFail($id), function(Show $show)
                            {
                                $show->panel()->style('hidden');
                                $show->keylogs('KeyLogs', function (Grid $keylog)
                                {
                                    //$booking->setResource('/admin/bookings');
                                    $keylog->model()->orderBy("date", "desc");
                                    $keylog->date();
                                    $keylog->status();
                                    $keylog->previous_status();
                                    $keylog->booking('Réservation')->display(function($b){
                                        if($b){
                                            return "<a href='" . route('admin.bookings.edit', ['booking' => $b['id']]) . "'>" . $b['bookingsync_id'] . "</a>";
                                        }
                                        return $b;
                                    });
                                    $keylog->actions(function(Grid\Displayers\Actions $actions)
                                    {
                                        $actions->disableView();
                                        $actions->disableDelete();
                                    });
                                    $keylog->tools(function(Grid\Tools $tools)
                                    {
                                        $tools->disableBatchActions();
                                    });
                                });
                                $show->bookings('Réservations', function (Grid $booking)
                                {
                                    $booking->setResource('/admin/bookings');
                                    $booking->model()->orderBy("start_at", "desc");
                                    $booking->status();
                                    $booking->bookingsync_id();
                                    $booking->source()->sortable();
                                    $booking->start_at()->sortable();
                                    $booking->end_at()->sortable();
                                    $booking->paid()->sortable();
                                    $booking->disableCreateButton();
                                    $booking->filter(function(Grid\Filter $filter)
                                    {
                                        $filter->between('start_at', 'Date d\'arrivée')->datetime();
                                        $filter->between('end_at', 'Date de fin')->datetime();
                                    });
                                    $booking->actions(function(Grid\Displayers\Actions $actions)
                                    {
                                        $actions->disableView();
                                        $actions->disableDelete();
                                    });
                                    $booking->tools(function(Grid\Tools $tools)
                                    {
                                        $tools->disableBatchActions();
                                    });
                                });
                            }));
                    $collapse = new Collapse();
                    $collapse->add("Synchroniser les réservation", $this->syncForm($property, $request->all())->render());
                    $collapse->add("Exporter les réservations", $this->reportForm($property, $request->all())->render());
                    $content->row($collapse);
                });
    }

    public function report(Request $request, $id, $reportId)
    {
        $property = Property::where("id", "=", $id)->first();
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $clientRef = $property->getUser()->client_id;
        $time = $from->format("my"); //($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $filename = $property->getReportFileName($time); //"report_" . $clientRef . "_" . $time . "_" . urlencode($property->name) . ".docx";
        $path = $property->getReportFile($time);
        if(file_exists($path))
        {
            attachFile($path, $filename);
        }
    }

    public function invoice(Request $request, $id, $reportId)
    {
        $property = Property::where("id", "=", $id)->first();
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $clientRef = $property->getUser()->client_id;
        $time = $from->format("my"); //($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $filename = $property->getInvoiceFileName($time); //"facture_" . $clientRef . "_" . $time . "_" . $property->id . ".docx";
        $path = $property->getInvoiceFile($time);
        if(file_exists($path))
        {
            attachFile($path, $filename);
        }
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Gestion des propriétés');
                    $content->description('créer');

                    $content->body($this->form());
                });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Property());

        //$grid->id('ID')->sortable();
        $user = Admin::user();
        if($user->is_city_manager){
            $props = $user->properties()->get()->pluck('id');
            $grid->model()->whereIn('id', $props);
        }
        $grid->name('Label')->display(function($name)
        {
            return "<a href='" . url('/admin/properties/' . $this->id) . "'>" . $name . "</a>";
        })->sortable();
        $grid->departement('Departement')->sortable();
        $grid->address("Adresse")->sortable();
        $grid->city()->nom_commune("Ville")->sortable();
        $grid->column("postal", "Code Postal")->display(function(){
            if($this->city){
                return $this->city->getPostal();
            }
            return "";
        })->sortable();
        $grid->country()->nicename("Pays")->sortable();
        $grid->user()->display(function($user)
        {
            return "<a href='" . url('/admin/clients/' . $user["id"]) . "'>" . $user["name"] . "</a>";
        });
        /*$grid->contract("Contrat")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();*/
        $grid->attest_loc("Attestation de location meublé")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->ranking_file("Classement")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->inventory("Inventaire")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->assessment("Etat des lieux")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->keys("Remise clés")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        
        $grid->bookingsync_id("BookingSync ID")->display(function($value)
        {
            if(empty($value)){
                Admin::script("document.querySelectorAll('#syncRentals1').forEach(e=>{
                    e.addEventListener('click', (e)=>{
                                        if(!confirm('VOULEZ-VOUS SYNCHRONISER CETTE PROPRIETE AVEC BOOKINGSYNC')){
                                            e.preventDefault();
                                        }
                            });});
                ");
                return "<a href='".url()->action('\App\Http\Controllers\HomeController@syncRentals', ["id" => $this->id])."' id='syncRentals1'>Synchroniser</a>";

            }else
                return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('name', 'Label');
            $filter->like('bookingsync_id', 'Bookingsync ID');
            $filter->like('departement', 'Département');
            $filter->equal('bookingsync_id','Bookingsync')->select([0 => 'Non synchronisé']);

        });

        $grid->actions(function(Grid\Displayers\Actions $actions)
        {
            $actions->disableView();
            $actions->disableDelete();

        });
        $grid->export(function(Grid\Exporters\CsvExporter $exporter){
            //$exporter->originalValue(['name','user']);
            $exporter->column('name', function($value){
                return utf8_decode(strip_tags($value));
            });
            $exporter->column('address', function($value){
                return utf8_decode(html_entity_decode(preg_replace("/\"|&quot;/", '', $value)));
            });
            $exporter->column('user', function($value){
                return utf8_decode(strip_tags($value));
            });
            $exporter->except(['contract', 'inventory', 'assessment', 'keys', 'bookingsync_id']);
        });

        //$grid->user('User')->sortable();
        $grid->paginate(100);

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $property = Property::where("id", "=", $id)->first();
        $property_details = PropertyDetails::where('property_id','=',$id)->first();
        /* @var $property Property  */
        $form = new Form(new Property());


        $form->tab('Propriété', function (Form $form) use($property)
        {
            $managerInfo = $form->select('admin_user_id', 'City Manager')->options(Administrator::where('is_city_manager', 1)->get()->pluck('name', 'id'))->disable()->placeholder('Aucun Manager');
            if($property && $manager = $property->manager){
                $managerInfo->default($manager->id);
            }

            $form->switch('lite', 'Contrat YELD')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);
            /*$form->switch('bnbkeys', 'Contrat BnBkeys')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);*/
            $form->switch('report_tva', 'Para-hotelier TVA/ménages')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);

            $form->select('user_id', 'Propriétaires')->options(function ($id)
            {
                $user = User::find($id);

                if($user)
                {
                    return [$user->id => $user->name];
                }
            })->ajax('/admin/api/users');

            $form->date('vision', 'Rapport visible à partir de');

            $form->text('name', 'Label')->rules('required');
            $form->text('commission', 'Commission')->rules('required');
            $form->display('created_at', 'Date de création');
            $form->datetime('resiliated_at', 'Date de résiliation');
            $form->decimal('min_price', 'Prix minimum/nuitée');
            $form->select('departement', 'Département')->options(function()
            {
                return departements();
            })->rules('required');
            $form->text('address', 'Adresse')->rules('required');

            $form->select('city_id', '  ')->options(function ($id)
            {
                $city = City::find($id);

                if($city)
                {
                    return [$city->id => addslashes($city->nom_commune)];
                }
            })->ajax('/admin/api/cities');

            $form->select('country_id', 'Pays')->options(function ($id)
            {
                $country = Country::where('id', $id)->first();;
                if($country)
                {
                    return [$country->id => $country->nicename];
                }
            })->ajax('/admin/api/countries');


            $form->text('billing', 'Adresse de facturation');
            $form->text('iban', 'IBAN');
            $form->text('bic', 'BIC');
            $form->text('bookingsync_id', 'Booking Sync ID')->rules('required');
            $form->text('rent_number', 'N° de loueur meublé');
            $form->text('class_number', 'N° de classement');
            // $form->textarea('airbnb', 'Informations airbnb');
            // $form->html('<i class="fa fa-refresh" onclick="splitIntoInputs(this)"></i>');
            // Admin::script("document.querySelector('#airbnb').closest('.form-group').setAttribute('style','display:none;')");
            // $form->text('Liens');
            // $form->ignore('Liens');

            $form->text('Airbnb_link', 'Airbnb');
            $form->text('Booking_link', 'Booking');
            $form->text('Hostenga_link', 'Hostenga Travel');
            $form->text('VRBO_link', 'VRBO/ABRITEL');
            $form->text('VEEPEE_link', 'VEEPEE');
            $form->text('autre1_link', 'Autre');
            $form->text('autre2_link', 'Autre');
            $form->text('autre3_link', 'Autre');
            // $form->html('<a id="addInput" onclick="addInputs()" style="cursor:pointer;">Ajouter</a>');
            // $form->html('<a id="saveInput" onclick="mergeIntoTextArea()" style="cursor:pointer;">Enregistrer</a>');
        });

        $form->tab("Keynest",function($form){
            $form->switch('keynest_auto', 'Gestion des clés autonomes (Keynest)')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);
            $form->switch('keynest_auto_checkout', 'Checkout auto (Keynest)')->states([
                'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
            ]);
            $form->display('keynest_key_id', 'KeyID');
            $form->select('keynest_store_id', 'Stores')->options(function($storeId){
                $stores = null;
                $options = [];
                if($storeId){
                    $stores = KeynestStore::where('id', $storeId)->get();
                }
                if($stores) {
                    foreach ($stores as $s) {
                        $options[$s->id] = $s->name . ' | ' . $s->address;
                    }
                }
                return $options;
            })->disable();
            $form->display('keynest_dropoff', 'Code de dépot');
            $form->display('keynest_collection', 'Code de collection');
        });

        $form->tab('Details d\'accès', function (Form $form)
        {
            $form->hasOne('details', function ($form)
            {
                $form->select('residence', 'Type de résidence')->options(PropertyDetails::getResidTypes());
                $form->select('rental_type', 'Type de Location')->options(PropertyDetails::getRentalTypes());
                $form->select('Management_type', 'Type de gestion de location')->options(PropertyDetails::getManagementTypes());
                $form->text('floor', 'Etage');
                $form->text('floors', 'Nombre d étage');
                $form->text('title', 'Titre');
                $form->text('summary', 'Résume');
                $form->textarea('description', 'Description');
                $form->text('entry', 'Entrée');
                $form->select('access', 'Accès immeuble')->options(PropertyDetails::accessTypes());
                $form->text('access_code', 'Code d\'accès');
                $form->select('access_car', 'Accès garage ou résidence')->options(PropertyDetails::accessCarTypes());
                $form->text('access_car_code', 'Code d\'accès garage');
                $form->text('floor', 'Etage');
                $form->radio('elevator', 'Ascenseur')->options(["1" => "Oui", "0" => "Non"]);
                $form->text('door', 'Indication du logement (n° de porte, autre)');
                $form->number('capacity', 'Capacité d\'accueil');
                $form->number('surface', 'Surface (m²)');
                $form->text('parking', 'Place de parking ou garage');
                $form->text('wifi_login', 'Wifi Login');
                $form->text('wifi_pass', 'Wifi Pass');
                $form->select('water', 'Robinet d\'arrivée d\'eau générale')->options([
                    'outside' => 'Commun',
                    'inside' => 'dans l\'appartement',
                ]);
                $form->textarea('electricity', 'Tableau électrique');
                $form->textarea('syndic', 'Information du syndicat');

                $form->select('arriving_time','Heure d\'arrivée')->options(PropertyDetails::getArrivingTimeTypes());
                $form->select('arriving_time_max','Heure d\'arrivée au plus tard')->options(PropertyDetails::getArrivingTimeTypesMax());
                $form->select('begin_time','Heure de depart')->options(PropertyDetails::getBeginTimeTypes());

                //equipments
                $form->number('rooms', 'Nombre de chambres');
                $form->number('bathrooms', 'Nombre de salle de bains');
                $form->number('wc', 'Nombre de toilettes');
                $form->number('bathtubs', 'Nombre de baignoires');
                $form->number('showers', 'Nombre de douches');
                $form->number('k140', 'Lit(s) double(s) 140');
                $form->number('k160', 'Lit(s) double(s) 160');
                $form->number('k180', 'Lit(s) double(s) 180');
                $form->number('k200', 'Lit(s) double(s) 200');
                $form->number('beds', 'Lit(s) Simple(s)');
                $form->number('bunkbeds', 'Lit(s) superposé(s)');
                $form->number('couchbeds', 'Canapé(s) lit(s)');
                $form->number('covers', 'Couette(s)');
                $form->number('square_pillows', 'Oreiller(s) carré(s)');
                $form->number('rect_pillows', 'Oreiller(s) rectangulaire(s)');
            });
        });

        $form->tab('Documents', function (Form $form) use ($property, $id)
        {
            /*$contract = $form->contractFile('contract', "Contrat (.pdf)")->rules('mimes:pdf')->removable();
            /* @var $contract ContractUpload */
            /*$contract->options([
                "initialPreviewDownloadUrl" => "signedContract",
            ])->setPreviewUrl(asset("images/file-icon.png")); //asset("images/file-icon.png"));
            */
            $attest = $form->contractFile('attest_loc', "Attestation location (.pdf)")->rules('mimes:pdf')->removable();
            $attest->options([
                "initialPreviewDownloadUrl" => "attestloc",
            ])->setPreviewUrl(asset("images/file-icon.png"));

            $ranking = $form->contractFile('ranking_file', "Décision de classement (.pdf)")->rules('mimes:pdf')->removable();
            $ranking->options([
                "initialPreviewDownloadUrl" => "ranking",
            ])->setPreviewUrl(asset("images/file-icon.png"));

            $annexe = $form->contractFile('annexe', "Annexe (.pdf)")->rules('mimes:pdf')->removable();
            $annexe->options([
                "initialPreviewDownloadUrl" => "annexe",
            ])->setPreviewUrl(asset("images/file-icon.png"));

            $invent = $form->contractFile('inventory', "Inventaire (.pdf)")->rules('mimes:pdf')->removable();
            $invent->options([
                "initialPreviewDownloadUrl" => "inventory",
            ])->setPreviewUrl(asset("images/file-icon.png"));

            $assess = $form->contractFile('assessment', "Etat des lieux (.pdf)")->rules('mimes:pdf')->removable();
            $assess->options([
                "initialPreviewDownloadUrl" => "assessment"
            ])->setPreviewUrl(asset("images/file-icon.png"));

            $keys = $form->contractFile('keys', "Remise des clés (.docx)")->rules('mimes:docx')->removable();
            $keys->options([
                "initialPreviewDownloadUrl" => "keysfile"
            ])->setPreviewUrl(asset("images/file-icon.png"));

            $form->saving(function($form) use ($attest, $ranking, $invent, $assess, $keys, $annexe)
            {
                $property = $form->model();
                $attest->move($property->getUploadFolder(), $property->getAttestLocFileName());
                $ranking->move($property->getUploadFolder(), $property->getRankingFileName());
                $annexe->move($property->getUploadFolder(), $property->getAnnexeFileName());
                $invent->move($property->getUploadFolder(), $property->getInventoryFileName());
                $assess->move($property->getUploadFolder(), $property->getAssessmentFileName());
                $keys->move($property->getUploadFolder(), $property->getKeysFileName());
            });
        });

        $form->tab('Equipements', function (Form $form)use($property)
        {
            if($property)
            {
                $amen = $property->amenities()->get();
                $options = [];
                $goodStuff = ['amenities_values' => []];
                foreach($amen as $pa)
                {
                    $a = $pa->amenity;
                    if($pa->value)
                    {
                        $goodStuff['amenities_values'][$a->id] = $a->id;
                    }
                    $options[$a->id] =  __($a->title);
                }
                // $form->checkbox('amenities', 'Equipement')->attribute(["class" => "amenities"])->options($options);
                $form->checkbox('amenities_values', 'Equipement')->options($options)->fill($goodStuff);
                // $listbox->options($options)->attribute(['class' => 'amenities'])->fill($goodStuff);
                // $form->checkbox('amenities_values', 'Equipements')->options($options)->attribute(['class' => 'amenities'])->fill($goodStuff);
        
            }
            $form->saving(function($form)
            {
                // dd($form->amenities_values);
                $property = $form->model();
                // dd($form->amenities_values);
                // if(empty($form->amenities_values))
                // {
                    foreach($property->amenities()->get() as $amen)
                    {
                        // echo $amen;
                        $amen->value = array_search($amen->amenity_id, $form->amenities_values) !== FALSE;
                        $amen->save();
                    }
                // }
            });
        });

        $form->tab('Debours', function (Form $form)
        {

            $form->hasMany('fees', function ($form)
            {
                $form->date('date');
                $form->text('info');
                $form->number('value');
            });
        });


        $form->footer(function(\Encore\Admin\Form\Footer $footer) use ($property){
            // if(!empty($property->bookingsync_id)){
            //     $footer->disableSubmit(); 
            // }
            $footer->checkEditing();
            $footer->disableCreatingCheck();
            $footer->disableReset();
        });
        $form->tools(function(\Encore\Admin\Form\Tools $tools) use ($property, $id)
        {
            $tools->disableView();
           
            if($property)
            {
                /*if(!$property->hasContract())
                {
                    $contractMail = "<div class='btn-group' style='margin-right:5px;'>
                        <a class='btn btn-sm btn-primary' href='" . url()->action('\App\Admin\Controllers\PropertyController@sendContractMail', ["id" => $id]) . "'>
                        <i class='fa fa-send'></i><span class='hidden-xs'> Renvoyer le mail de contrat</span></a></div>";
                    $tools->add($contractMail);
                }*/

                $userLink = new HTMLTool();
                $userLink->html = "<div class='btn-group' style='margin-right:5px;'>
                <a class='btn btn-sm btn-primary' href='" . url()->action('\App\Admin\Controllers\UserController@show', ["id" => $property->getUser()->id]) . "'>
                <i class='fa fa-eye'></i><span class='hidden-xs'> Voir fiche proriétaire</span></a></div>";
                $tools->add($userLink);
            }
            try {
                    $userLink = new HTMLTool();
                    $userLink->html = "<div class='btn-group' style='margin-right:5px;'>
                    <a id='syncRentals' class='btn btn-sm btn-primary' href = '".url()->action('\App\Http\Controllers\HomeController@syncRentals', ["id" => $property->id])."'>
                    <i class='fa fa-refresh'></i><span class='hidden-xs'> Synchroniser</span></a></div>";
    
                    Admin::script("var btn= document.getElementById('syncRentals');
    
                    btn.addEventListener('click', (e)=>{
                         if(!confirm('VOULEZ-VOUS SYNCHRONISER CETTE PROPRIETE AVEC BOOKINGSYNC')){
                            e.preventDefault();
                         }
                    });");
                    $tools->add($userLink);
                
            } catch (\Throwable $th) {
                
            }
           
        });

        $form->disableViewCheck();
        $form->saving(function(Form $form){
            $form->unsetInput('admin_user_id');
        });

        return $form;
    }

    public function keysForm($data)
    {
        $form = new \Encore\Admin\Widgets\Form($data);
        $form->action("createKeyDoc");
        $form->number("nbKey", "Nombre de clés")->attribute(["value" => 1]);
        $form->number("nbBadge", "Nombre de badges")->attribute(["value" => 0]);
        $form->number("nbRemote", "Nombre de télécommandes")->attribute(["value" => 0]);
        $form->file("keys", "Photos clés (format paysage, .jpg préféré)")->rules('mimes:jpeg');
        //$form->disablePjax();
        $form->disableReset();
        return $form;
    }

    public function syncForm($property, $data)
    {
        $form = new \Encore\Admin\Widgets\Form($data);
        $form->action(url("/admin/bookings/synchronizeProperty/" . $property->id));
        $form->method('GET');
        $form->date("from", "à Partir du");
        //$form->disablePjax();
        $form->disableReset();
        return $form;
    }

    public function reportForm($property, $data)
    {
        $form = new \Encore\Admin\Widgets\Form($data);
        $form->action(url("/admin/properties/" . $property->id . "/customReport"));
        $form->method('GET');
        $form->date("from", "à Partir du");
        //$form->disablePjax();
        $form->disableReset();
        return $form;
    }

    public function createKeyDoc(Request $request, $id)
    {

        $property = Property::where("id", "=", $id)->first();
        $file = $request->file("keys");
        $nbKey = $request->get("nbKey");
        $nbBadge = $request->get("nbBadge");
        $nbRemote = $request->get("nbRemote");
        $keyDoc = new TemplateCustom(__DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "jeffreyhost_keys_template.docx");
        $now = new Carbon();

        $msg = "";
        if($nbKey)
            $msg .= $nbKey . " clé(s), ";
        if($nbBadge)
            $msg .= $nbBadge . " badge(s), ";
        if($nbRemote)
            $msg .= $nbRemote . " télécommande(s), ";
        //valdebug($file->getPathName());die;
        $keyDoc->save_image('image1.jpeg', $file->getPathName(), $keyDoc);
        $keyDoc->setValue("name", $property->getUser()->name);
        $keyDoc->setValue("address", $property->address);
        $keyDoc->setValue("date", $now->format("d/m/Y"));
        $keyDoc->setValue("msg", $msg);
        $keyDoc->saveAs($property->getKeysFile());
        $property->keys = $property->getUploadFolder() . "/" . $property->getKeysFileName();
        $property->save();
        //$this->keysfile($id);

        return redirect()->action('\App\Admin\Controllers\PropertyController@edit', ["id" => $id]);
    }

    protected function getReports($property)
    {
        $reports = $property->getReports();
        $bookings = $property->bookings()->whereNotNull("bookingsync_id")->orderBy("end_at", "asc")->get();
        uasort($reports, function($a, $b)
        {
            return $a->start_at < $b->start_at;
        });
        setlocale(LC_ALL, "fr");
        $collapse = new Collapse();
        $fullstats = [
            "owner" => 0,
            "profit" => 0,
        ];
        foreach($bookings as $b)
        {
            $fullstats["owner"] += $b->owner;
            $fullstats["profit"] += $b->profit;
        }
        $html = '<h4>Virement : ' . $fullstats["owner"] . ' €</h4>'
                . '<h4>Commission jeffrey : ' . $fullstats["profit"] . ' €</h4>';
        $collapse->add("Total", $html);
        foreach($reports as $r)
        {
            $from = new Carbon($r->start_at);
            $time = $from->format('my');
            if($property->hasReport($time))
            {
                $ca = $r->values()->where("property_id", "=", $property->id)->where("label", "=", "user_ca")->first()->profit;
                $month = utf8_encode(ucfirst($from->formatLocalized("%B")));
                $year = utf8_encode($from->formatLocalized("%Y"));
                $html = //'<h3>Rapport de '.$month.' '.$year.'</h3>
                        '<h4>Virement : ' . $ca . ' €</h4>
                        <a class="reports-property btn btn-primary" target="_blank" href="' . url("admin/properties/" . $property->id . "/report/" . $r->id) . '">Rapport</a>
                        <a class="reports-property btn btn-success" target="_blank" href="' . url("admin/properties/" . $property->id . "/invoice/" . $r->id) . '">Facture</a>
                        ';
                $collapse->add($month . " " . $year, $html);
            }
        }
        return $collapse;
    }

    protected function getCharts($property)
    {
        // STATS
        /* @var $property Property */
        $bookings = $property->bookings()->where("end_at", "<=", (new Carbon())->endOfMonth())->where("status", "=", Booking::STATUS_BOOKED)->whereNotNull("bookingsync_id")->orderBy("end_at", "asc")->get();
        /* @var $bookings \Illuminate\Database\Eloquent\Collection */


        $datas = [];
        $daysInMonth = [];
        if(count($bookings))
        {
            $dateStart = new Carbon($bookings[0]->end_at);
            $dateEnd = new Carbon($bookings[count($bookings) - 1]->end_at);
            while($dateStart->diffInMonths($dateEnd) > 0)
            {
                $datas[$dateStart->format("Y-m")] = [];
                $dateStart->addMonth();
            }
        }
        $groupBookings = $bookings->groupBy(function($val)
        {
            return Carbon::parse($val->end_at)->format('Y-m');
        });


        $datas = array_merge($datas, $groupBookings->toArray());
        $datas = aggregateBookings($datas);
        //valdebug($datas);die;
        $lava = new Lavacharts();
        $columns = [
            [
                "title" => "Revenues propio",
                "name" => "owner",
                "suffix" => " €"
            ],
        ];

        bookingsChart($lava, "ColumnChart", $datas, "Stats", $columns, ["#5A5", "#5b5"]);
        $columns = [
            [
                "title" => "Nuités",
                "name" => "days",
                "suffix" => " nuités"
            ],
            [
                "title" => "Réservations",
                "name" => "count",
                "suffix" => " réservations"
            ],
                /* [
                  "title" => "Jour occupés",
                  "name" => "days",
                  "suffix" => " jours"
                  ],
                  [
                  "title" => "Moyenne de séjour",
                  "name" => "avgNights",
                  "suffix" => " jours"
                  ], */
        ];
        bookingsChart($lava, "ColumnChart", $datas, "Nuités", $columns, ["#08F", "#55A", "#cb5fd3", "#ff7f00"], true);
        $columns = [
            [
                "title" => "Taux d'occupations",
                "name" => "occupied",
                "suffix" => "%"
            ],
        ];
        $global = bookingsChart($lava, "ColumnChart", $datas, "Taux", $columns, ["#ff7f00"]);
        $columns = [
            [
                "title" => "Moyenne de séjour",
                "name" => "avgNights",
                "suffix" => " jours"
            ],
        ];
        $global = bookingsChart($lava, "ColumnChart", $datas, "Moyenne", $columns, ["#08F", "#55A", "#cb5fd3", "#ff7f00"]);

        $statsGlobal = "<h4>Taux d'occupation : " . $global["occupied"] . "%</h4>";
        $statsGlobal .= "<h4>Durée moyenne de séjour : " . $global["avgNights"] . "</h4>";
        $statsGlobal .= "<div class='row'>";
        $statsGlobal .= "<div class='col-md-6 min-chart' id='stats-chart'></div>" . $lava->render('ColumnChart', 'Stats', 'stats-chart');
        $statsGlobal .= "<div class='col-md-6 min-chart' id='taux-chart'></div>" . $lava->render('ColumnChart', 'Taux', 'taux-chart');
        $statsGlobal .= "<div class='col-md-6 min-chart' id='moyenne-chart'></div>" . $lava->render('ColumnChart', 'Moyenne', 'moyenne-chart');
        $statsGlobal .= "<div class='col-md-6 min-chart' id='reservations-chart'></div>" . $lava->render('ColumnChart', 'Nuités', 'reservations-chart');
        $statsGlobal .= "</div>";
        return $statsGlobal;
    }

    public function customReport(Request $request, $id)
    {
        $property = Property::where("id", "=", $id)->first();
        $from = null;
        if($request->get("from"))
        {
            $from = new Carbon($request->get("from"));
        }
        /* @var $report Report */
        /* $this->syncReport($request);
          echo "success";
          return; */
        if($property)
        {
            $datas = [
                [
                    "Rapport " . $property->name
                ],
                [
                    "Nom du client",
                    "Date de réservation",
                    "début de la location",
                    "fin de la location",
                    "Référence réservation",
                    "Propriété",
                    "Source",
                    "Montant",
                    "Sous-montant ADAR",
                    "OTA",
                    "Jeffrey Com.",
                    "Virement propriétaire",
                    "Ménages",
                    "Taxes de séjour",
                ]
            ];

            $bookings = $property->bookings()->where("status", "=", Booking::STATUS_BOOKED)->orderBy("start_at", "asc");
            if($from)
            {
                $bookings = $bookings->where("end_at", ">=", $from);
            }
            $bookings = $bookings->get();

            $i = 2;
            foreach($bookings as $b)
            {
                $i++;

                $guest = json_decode($b->guest);
                $data = [
                    $guest ? $guest->firstname . " " . $guest->lastname : "nom inconnue",
                    (new Carbon($b->created_at))->format("d-m-Y"),
                    (new Carbon($b->start_at))->format("d-m-Y"),
                    (new Carbon($b->end_at))->format("d-m-Y"),
                    $b->reference,
                    $b->property->name,
                    $b->source,
                    $b->amount,
                    $b->under_adar,
                    $b->ota,
                    $b->profit,
                    $b->owner,
                    $b->cleaning,
                    $b->tax,
                ];
                $datas[] = $data;
            }
            $datas[] = [];
            $datas[] = [
                "Totaux", "", "", "", "", "",
                "",
                "=SUM(H3:H" . (count($bookings) + 2) . ")",
                "=SUM(I3:I" . (count($bookings) + 2) . ")",
                "=SUM(J3:J" . (count($bookings) + 2) . ")",
                "=SUM(K3:K" . (count($bookings) + 2) . ")",
                "=SUM(L3:L" . (count($bookings) + 2) . ")",
                "=SUM(M3:M" . (count($bookings) + 2) . ")",
                "=SUM(N3:N" . (count($bookings) + 2) . ")",
            ];
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->fromArray($datas, null, "A1");
            $sheet->mergeCells('A1:M1');

            $style = [
                'font' => [
                    'bold' => 1
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ],
            ];
            $spreadsheet->getActiveSheet()->getStyle('A1:L1')->applyFromArray($style);

            $columns = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"];
            foreach($columns as $c)
            {
                $sheet->getColumnDimension($c)->setAutoSize(true);
                $sheet->getStyle($c . '2')->applyFromArray($style);
            }
            $spreadsheet->createSheet();
            $writer = new Xlsx($spreadsheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="rapport_' . $property->name . '.xlsx"');
            $writer->save("php://output");
            return redirect("/admin/properties/" . $id . "/edit");
        }
    }

}
