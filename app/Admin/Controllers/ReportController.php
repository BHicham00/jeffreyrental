<?php

namespace App\Admin\Controllers;

use App\User;
use App\Property;
use App\Report;
use App\Booking;
use App\Fee;
use App\City;
use Symfony\Component\HttpFoundation\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\BookingSync\BookingSync;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls\Worksheet;
use ZipArchive;
use App\Admin\Extensions\Grid\Actions\Link;

class ReportController extends Controller
{

    use ModelForm;

    function __construct()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'),
                config('bookingsync.secret'),
                route('admin.reports.index')
        );
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index(Request $request)
    {
        if($request->get("code"))
        {
            return redirect("/admin/reports");
        }
        return Admin::content(function (Content $content)
                {

                    $content->header('Clients Management');
                    $content->description('');

                    $content->body($this->grid());
                });
    }

    public function show($id){
        return $this->edit($id);
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Report Management');
                    $content->description('edit the report');
                    $content->body($this->form($id)->edit($id));
                });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Report Management');
                    $content->description('create a Report');

                    $content->body($this->form());
                });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Report());
        $grid->id('ID')->sortable();
        $grid->name('Name')->sortable();
        $grid->start_at('Start at')->sortable();
        $grid->end_at('End at')->sortable();

        $grid->filter(function ($filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('name', 'name');
        });
        $grid->actions(function(Grid\Displayers\Actions $actions)
        {
            $actions->disableView();
            $actions->disableDelete();
            $actions->add(new Link('Rapports Archive', route("admin.reports.allreports")."?report=" . $actions->getKey(), '_blank'));
            $actions->add(new Link('Rapport', route("admin.reports.export")."?report=" . $actions->getKey(), '_blank'));
            $actions->add(new Link('Réservations', route("admin.reports.globalreport")."?report=" . $actions->getKey(), '_blank'));
            $actions->add(new Link('Taxe de séjour', route("admin.reports.taxreports")."?report=" . $actions->getKey(), '_blank'));
            //$actions->prepend('<a title="Rapports Archive" target="_blank" href="' . url("/admin/reports/export/allreports?report=" . $actions->getKey()) . '"><i class="fa fa-archive"></i></a> | ');
            //$actions->prepend('<a title="Rapport" target="_blank" href="' . url("/admin/reports/export?report=" . $actions->getKey()) . '"><i class="fa fa-eye"></i></a> | ');
            //
            //$actions->prepend('<a class="globalsync" title="Sync Month" href="' . url("/admin/reports/syncReport?report=" . $actions->getKey()) . '"><i class="fa fa-refresh"></i></a> | ');

            //$actions->prepend('<a title="ADAR" target="_blank" href="' . url("/admin/reports/adar?report=" . $actions->getKey()) . '"><i class="fa fa-dollar"></i></a> | ');
            //$actions->prepend('<a title="global" target="_blank" href="' . url("/admin/reports/global?report=" . $actions->getKey()) . '"><i class="fa fa-globe"></i></a> | ');
            //$actions->prepend('<a title="tax" target="_blank" href="' . url("/admin/reports/tax?report=" . $actions->getKey()) . '"><i class="fa fa-bank"></i></a> | ');
            //$actions->prepend('<a title="diffReport" target="_blank" href="' . url("/admin/reports/diffReport?report=" . $actions->getKey()) . '"><i class="fa fa-adjust"></i></a> | ');
        });
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Report::class, function (Form $form) use ($id)
                {
                    $form->tab('Report', function ($form)
                    {
                        $form->hidden('id')->attribute(["id" => "report_id"]);
                        $form->text('name', 'Label')->rules('required');
                        $form->date('start_at')->rules('required');
                        $form->date('end_at')->rules('required');
                    });
                    if($id)
                    {
                        $form->tab('Property List', function (Form $form) use ($id)
                        {
                            $properties = Property::all();

                            /* @var $report Report */
                            $report = Report::where("id", "=", $id)->first();
                            /* @var $values Illuminate\Database\Eloquent\Relations\HasMany */
                            /* $values = $report->values()->where('label', '=', 'user_ca'); */
                            foreach($properties as $property)
                            {
                                if($property->bookingsync_id)
                                {
                                    $value = \App\ReportHasValue::firstOrCreate([
                                                'label' => 'user_ca',
                                                'report_id' => $report->id,
                                                'property_id' => $property->id,
                                    ]);
                                }
                            }
                            $form->hasMany('uservalues', function (Form\NestedForm $form)
                            {
                                $form->select('property_id', 'Property')->options(function ($id)
                                {
                                    $property = Property::find($id);

                                    if($property)
                                    {
                                        return [$property->id => $property->name];
                                    }
                                })->config('dropdownCssClass', 'select2-hidden')->attribute(["disabled" => "disabled"]);
                                $form->number('profit');
                            });
                        });
                    }
                });
    }

    public function getReport(Request $request)
    {
        $property = Property::where("id", "=", $request->input('property'))->first();
        $user = $property->user;
        Carbon::setLocale("fr");
        $now = new Carbon();
        $reportId = $request->input('report');
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $clientRef = $user->client_id;
        $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $filename = $property->getReportFileName($time); //"report_" . $clientRef . "_" . $time . "_" . urlencode($property->name) . ".docx";
        $path = $property->getReportFile($time);
        if(file_exists($path))
        {
            header("Content-Disposition: attachment; filename=" . $filename . "; charset=iso-8859-1");
            echo file_get_contents($path);
            die;
        } else
        {
            return redirect('admin/invoices/create?property=' . $property->id . '&report=' . $report->id . '&type=report');
        }
    }

    public function getInvoice(Request $request)
    {
        $property = Property::where("id", "=", $request->input('property'))->first();
        $user = $property->user;
        Carbon::setLocale("fr");
        $now = new Carbon();
        $reportId = $request->input('report');
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $clientRef = $user->client_id;
        $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $filename = $property->getInvoiceFileName($time); //"report_" . $clientRef . "_" . $time . "_" . urlencode($property->name) . ".docx";
        $path = $property->getInvoiceFile($time);
        if(file_exists($path))
        {
            header("Content-Disposition: attachment; filename=" . $filename . "; charset=iso-8859-1");
            echo file_get_contents($path);
            die;
        } else
        {
            return redirect('admin/invoices/create?property=' . $property->id . '&report=' . $report->id . '&type=invoice');
        }
    }

    public function getAllInvoices(Request $request)
    {
        $reportId = $request->input('report');
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $properties = $report->uservalues()->get();
        $zip = new ZipArchive();
        $zipName = __DIR__ . DIRECTORY_SEPARATOR . "archives" . DIRECTORY_SEPARATOR . "factures_" . $time . ".zip";
        if($zip->open($zipName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) !== TRUE)
        {
            die("Could not open archive");
        }
        foreach($properties as $value)
        {
            if($value->profit > 0)
            {
                $property = $value->property;
                $user = $property->user;
                $clientRef = $user->client_id;
                $filename = $property->getInvoiceFileName($time);
                $path = __DIR__ . DIRECTORY_SEPARATOR . "factures" . DIRECTORY_SEPARATOR . $filename;
                if(file_exists($path))
                {
                    $zip->addFile($path, $filename) or die("error on file " . $filename);
                }
            }
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="' . basename($zipName) . '"');
        header('Content-Length: ' . filesize($zipName));
        readfile($zipName);
    }

    public function getAllReports(Request $request)
    {
        $reportId = $request->input('report');
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
        $properties = $report->uservalues()->get();
        $zip = new ZipArchive();
        $zipName = __DIR__ . DIRECTORY_SEPARATOR . "archives" . DIRECTORY_SEPARATOR . "rapports_" . $time . ".zip";
        if($zip->open($zipName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) !== TRUE)
        {
            die("Could not open archive");
        }
        foreach($properties as $value)
        {
            if($value->profit > 0)
            {
                $property = $value->property;
                $user = $property->user;
                $clientRef = $user->client_id;
                $filename = $property->getReportFileName($time);
                $path = __DIR__ . DIRECTORY_SEPARATOR . "reports" . DIRECTORY_SEPARATOR . $filename;
                if(file_exists($path))
                {
                    $zip->addFile($path, $filename) or die("error on file " . $filename);
                }
            }
        }
        /* header('Content-Type: application/zip');
          header('Content-Disposition: attachment; filename="' . basename($zipName) . '"');
          header('Content-Length: ' . filesize($zipName));
          readfile($zipName); */
        attachFile($zipName, basename($zipName));
    }

    public function export(Request $request)
    {
        $reportId = $request->get("report");
        /* @var $report Report */
        $report = Report::where("id", "=", $reportId)->first();
        if($report)
        {
            $from = new Carbon($report->start_at);
            $date = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
            $dateFormat = ($from->month > 9 ? $from->month : "0" . $from->month) . '/' . substr($from->year, 2);

            $datas = [
                [
                    "ID Client",
                    "Nom",
                    "Etablissement",
                    "%",
                    "IBAN",
                    "BIC",
                    'Virement Propiétaire',
                    'Frais de ménage encaissés',
                    'Taxes de séjour',
                    'Nuités',
                    'Commission Jeffrey',
                    'Commission Channels',
                    'Total Brut',
                ]
            ];
            $properties = $report->getReportValuesByProperty();
            foreach($properties as $id => $values)
            {
                $property = Property::where("id", "=", $id)->withTrashed()->first(); //$value->property;
                $user = $property->user;
                if($user && !$user->archived)
                {
                    $data = [
                        $user->client_id,
                        $user->name,
                        $property->name,
                        $property->commission . '%',
                        $property->iban,
                        $property->bic,
                        $values["user_ca"],
                        $values["cleaning"],
                        $values["tourist_tax"],
                        $values["nights"],
                        $values["jeffrey"],
                        $values["commission"],
                        $values["brut"]
                    ];
                    $datas[] = $data;
                }
            }
            $datasTotal = [];
            $datasTotal[] = [
                "Total Virement propriétaires",
                "Total Frais Ménages",
                "Total Taxes de séjour",
                "Total Nuités",
            ];
            $datasTotal[] = [
                "=SUM(Worksheet!G1:G1000)",
                "=SUM(Worksheet!H1:H1000)",
                "=SUM(Worksheet!I1:I1000)",
                "=SUM(Worksheet!J1:J1000)",
            ];
            $datasTotal[] = [
                "Total Commission Jeffrey",
                "Total Commission Channels",
                "Total des Bruts",
            ];
            $datasTotal[] = [
                "=SUM(Worksheet!K1:K1000)",
                "=SUM(Worksheet!L1:L1000)",
                "=SUM(Worksheet!M1:M1000)",
            ];
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $spreadsheet->getActiveSheet()->fromArray($datas, null, "A1");
            $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(1);
            $spreadsheet->getActiveSheet()->fromArray($datasTotal, null, "A1");
            $spreadsheet->setActiveSheetIndex(0);
            $writer = new Xlsx($spreadsheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $report->name . '.xlsx"');
            $writer->save("php://output");
        }
    }

    public function adar(Request $request)
    {
        $reportId = $request->get("report");
        /* @var $report Report */
        $report = Report::where("id", "=", $reportId)->first();
        if($report)
        {
            $from = new Carbon($report->start_at);
            $dateFormat = ($from->month > 9 ? $from->month : "0" . $from->month) . '-' . substr($from->year, 2);
            $datas = [
                [
                    "Rapport ADAR " . $dateFormat
                ],
                [
                    "Nom du client",
                    "Date de réservation",
                    "début de la location",
                    "fin de la location",
                    "Référence réservation",
                    "Propriété",
                    "Montant total",
                    "Prime d'assurance ADAR 2,5%",
                ]
            ];

            $bookings = Booking::where("created_at", ">=", $report->start_at)
                    ->where("created_at", "<=", $report->end_at)
                    ->where("paid", "=", "1")
                    ->where("status", "=", Booking::STATUS_BOOKED)
                    ->where("owner", ">", "0")
                    ->orderBy("created_at", "asc")
                    ->get();
            //$bookings = Booking::where("created_at", ">=", "2019-06-16")->where("created_at", "<=", "2019-07-01")->where("paid", "=", "1")->orderBy("created_at","asc")->get();
            $totalADAR = 0;
            $i = 2;
            foreach($bookings as $b)
            {
                $i++;
                $totalADAR += ($b->amount * 0.025);

                $guest = json_decode($b->guest);
                $data = [
                    $guest ? $guest->firstname . " " . $guest->lastname : "nom inconnue",
                    (new Carbon($b->created_at))->format("d-m-Y"),
                    (new Carbon($b->start_at))->format("d-m-Y"),
                    (new Carbon($b->end_at))->format("d-m-Y"),
                    $b->reference,
                    $b->property->name,
                    $b->amount,
                    //($b->amount * 0.025),
                    "=G$i*0.025"
                ];
                $datas[] = $data;
            }
            $datas[] = [];
            $datas[] = [
                "", "", "", "", "", "",
                "Total ADAR", "=SUM(H3:H" . (count($bookings) + 2) . ")"
            ];
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->fromArray($datas, null, "A1");
            $sheet->mergeCells('A1:H1');

            $style = [
                'font' => [
                    'bold' => 1
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ],
            ];
            $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($style);

            $columns = ["A", "B", "C", "D", "E", "F", "G", "H"];
            foreach($columns as $c)
            {
                $sheet->getColumnDimension($c)->setAutoSize(true);
                $sheet->getStyle($c . '2')->applyFromArray($style);
            }
            $spreadsheet->createSheet();
            $writer = new Xlsx($spreadsheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Jeffrey-Conciergerie_ADAR_' . $dateFormat . '.xlsx"');
            $writer->save("php://output");
        }
    }

    public function diffReport(Request $request)
    {
        Carbon::setLocale("fr");
        $now = new Carbon();
        $reportId = $request->input('report');
        $report = Report::where("id", "=", $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $reportValues = $report->uservalues()->get();
        valdebug($reportValues);
        die;
        $results = [];
        $diff = 0;
        foreach($reportValues as $value)
        {
            $property = $value->property;
            $fees = Fee::where("property_id", "=", $property->id)->where("date", ">=", $from)->where("date", "<=", $to)->get();
            $total_fees = 0;
            foreach($fees as $fee)
            {
                $total_fees += $fee->value;
            }

            $results[$property->id] = [
                "property" => $property->name,
                "old_user_ca" => $value->profit,
            ];
            $bookings = Booking::where("end_at", ">=", $report->start_at)
                            ->where("end_at", "<=", $report->end_at)
                            ->where("property_id", "=", $property->id)
                            ->whereNotNull("bookingsync_id")->get();
            $newProfit = 0;
            foreach($bookings as $booking)
            {
                if($booking->source == "Booking.com" && !$booking->ota)
                    continue;
                $newProfit += $booking->owner;
            }
            $results[$property->id]["new_user_ca"] = round(($newProfit - $total_fees) * 100) / 100;
            $diff += $value->profit - $newProfit;
        }


        $dateFormat = ($from->month > 9 ? $from->month : "0" . $from->month) . '-' . substr($from->year, 2);
        $datas = [
            [
                "Rapport Différentiel " . $dateFormat
            ],
            [
                "Propriété",
                "Ancien relevé",
                "Nouveau relevé",
                "Différence",
            ]
        ];
        $i = 2;
        foreach($results as $data)
        {
            $i++;
            $data = [
                $data["property"],
                $data["old_user_ca"],
                $data["new_user_ca"],
                "=B" . $i . "-C" . $i,
            ];
            $datas[] = $data;
        }
        $datas[] = [];
        $datas[] = [
            "Totaux",
            "=SUM(B3:B" . (count($results) + 2) . ")",
            "=SUM(C3:C" . (count($results) + 2) . ")",
            "=B" . (count($results) + 4) . "-C" . (count($results) + 4),
        ];
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($datas, null, "A1");
        $sheet->mergeCells('A1:D1');
        $style = [
            'font' => [
                'bold' => 1
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                ],
            ],
        ];
        $spreadsheet->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style);

        $columns = ["A", "B", "C", "D"];
        foreach($columns as $c)
        {
            $sheet->getColumnDimension($c)->setAutoSize(true);
            $sheet->getStyle($c . '2')->applyFromArray($style);
        }
        $spreadsheet->createSheet();
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Jeffrey-Conciergerie_DIFFERENTIEL_' . $dateFormat . '.xlsx"');
        $writer->save("php://output");
    }

    public function globalReport(Request $request)
    {
        $reportId = $request->get("report");
        /* @var $report Report */
        /* $this->syncReport($request);
          echo "success";
          return; */
        $report = Report::where("id", "=", $reportId)->first();
        if($report)
        {
            $from = new Carbon($report->start_at);
            $to = new Carbon($report->end_at);
            $dateFormat = ($from->month > 9 ? $from->month : "0" . $from->month) . '-' . substr($from->year, 2);
            $dateFormatTo = ($to->month > 9 ? $to->month : "0" . $to->month) . '-' . substr($to->year, 2);
            $datas = [
                [
                    "Rapport GLOBAL " . $dateFormat . " à " . $dateFormatTo
                ],
                [
                    "Nom du client",
                    "Date de réservation",
                    "début de la location",
                    "fin de la location",
                    "Référence réservation",
                    "Propriété",
                    "Source",
                    "Montant",
                    "Sous-montant ADAR",
                    "OTA",
                    "Jeffrey Com.",
                    "Virement propriétaire",
                    "Ménages",
                    "Taxes de séjour",
                ]
            ];

            $bookings = Booking::where("end_at", ">=", $report->start_at)
                    ->where("end_at", "<=", $report->end_at)
                    ->where("owner", ">", "0")
                    ->where("status","=", Booking::STATUS_BOOKED)
                    ->where("paid", "=", "1")
                    ->orderBy("created_at", "asc")->get();

            $i = 2;
            foreach($bookings as $b)
            {
                $i++;

                $guest = json_decode($b->guest);
                $data = [
                    $guest ? $guest->firstname . " " . $guest->lastname : "nom inconnue",
                    (new Carbon($b->created_at))->format("d-m-Y"),
                    (new Carbon($b->start_at))->format("d-m-Y"),
                    (new Carbon($b->end_at))->format("d-m-Y"),
                    $b->reference,
                    $b->property->name,
                    $b->source,
                    $b->amount,
                    $b->under_adar,
                    $b->ota,
                    $b->profit,
                    $b->owner,
                    $b->cleaning,
                    $b->tax,
                ];
                $datas[] = $data;
            }
            $datas[] = [];
            $datas[] = [
                "Totaux", "", "", "", "", "",
                "",
                "=SUM(H3:H" . (count($bookings) + 2) . ")",
                "=SUM(I3:I" . (count($bookings) + 2) . ")",
                "=SUM(J3:J" . (count($bookings) + 2) . ")",
                "=SUM(K3:K" . (count($bookings) + 2) . ")",
                "=SUM(L3:L" . (count($bookings) + 2) . ")",
                "=SUM(M3:M" . (count($bookings) + 2) . ")",
                "=SUM(N3:N" . (count($bookings) + 2) . ")",
            ];
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->fromArray($datas, null, "A1");
            $sheet->mergeCells('A1:M1');

            $style = [
                'font' => [
                    'bold' => 1
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ],
            ];
            $spreadsheet->getActiveSheet()->getStyle('A1:L1')->applyFromArray($style);

            $columns = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"];
            foreach($columns as $c)
            {
                $sheet->getColumnDimension($c)->setAutoSize(true);
                $sheet->getStyle($c . '2')->applyFromArray($style);
            }
            $spreadsheet->createSheet();
            $writer = new Xlsx($spreadsheet);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Jeffrey-Conciergerie_GLOBAL_' . $dateFormat . '.xlsx"');
            $writer->save("php://output");
        }
    }
    
    
    public function taxReports(Request $request)
    {
        $reportId = $request->get("report");
        /* @var $report Report */
        /* $this->syncReport($request);
          echo "success";
          return; */
        $report = Report::where("id", "=", $reportId)->first();
        if($report)
        {
            $from = new Carbon($report->start_at);
            $to = new Carbon($report->end_at);
            $dateFormat = ($from->month > 9 ? $from->month : "0" . $from->month) . '-' . substr($from->year, 2);
            $dateFormatTo = ($to->month > 9 ? $to->month : "0" . $to->month) . '-' . substr($to->year, 2);
            
            $zip = new ZipArchive();
            $zipName = __DIR__ . DIRECTORY_SEPARATOR . "archives" . DIRECTORY_SEPARATOR . "tax_" . $dateFormat . ".zip";
            if($zip->open($zipName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) !== TRUE)
            {
                die("Could not open archive");
            }
            
            $groupedBookings = Booking::selectRaw('city_id, bookings.*')->where("end_at", ">=", $report->start_at)
                    ->where("end_at", "<=", $report->end_at)->where("owner", ">", "0")
                    ->where("paid", "=", "1")
                    ->where("status", "=", Booking::STATUS_BOOKED)
                    ->join("properties", "properties.id", "=", "bookings.property_id")
                    ->orderBy("bookings.created_at", "asc")->get()->groupBy("city_id");
            $filesToUnlink = [];
            $totalDatas = [
                [
                    "Rapport Taxe de séjours " . $dateFormat . " à " . $dateFormatTo . " - Toutes Communes"
                ],
                [
                    "Commune",
                    "total taxes",
                ]
            ];

            foreach($groupedBookings as $city_id => $bookings){
                $city = City::find($city_id);
                $totalTax = 0;
                if(!$city){
                    $cityLabel = "Non classé";
                }else{
                    $cityLabel = $city->nom_commune . "-" . $city->getPostal();
                }
                $datas = [
                    [
                        "Rapport Taxe de séjours " . $dateFormat . " à " . $dateFormatTo . " - Commune de ". $cityLabel
                    ],
                    [
                        "Propriétaire",
                        "N° loueur meublé",
                        "Propriété",
                        "Adresse",
                        "Date de réservation",
                        "Source",
                        "Nbs nuités",
                        "Taxe de séjour",
                    ]
                ];

                $i = 2;
                foreach($bookings as $b)
                {
                    $i++;
                    $totalTax += strstr(strtolower($b->source), "airbnb") ? 0 : (float)$b->tax;
                    $start = new Carbon($b->start_at);
                    $end = new Carbon($b->end_at);
                    $nights = $start->hour(0)->minute(0)->diffInDays($end->hour(0)->minute(0));
                    $data = [
                        $b->property->getUser()->name,
                        $b->property->rent_number." ",
                        $b->property->name,
                        $b->property->address,
                        "du " . $start->format("d-m-Y") . " au " . $end->format("d-m-Y"),
                        $b->source,
                        $nights,
                        strstr(strtolower($b->source), "airbnb") ? 0 : $b->tax,
                    ];
                    $datas[] = $data;
                }
                $datas[] = [];
                $datas[] = [
                    "Total",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "=SUM(H3:H" . (count($bookings) + 2) . ")",
                ];
                $totalDatas[] = [$cityLabel, $totalTax];
                $spreadsheet = new Spreadsheet();
                $spreadsheet->setActiveSheetIndex(0);
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->fromArray($datas, null, "A1");
                $sheet->mergeCells('A1:F1');

                $style = [
                    'font' => [
                        'bold' => 1
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        ],
                    ],
                ];
                $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($style);

                $columns = ["A", "B", "C", "D", "E", "F", "G", "H"];
                foreach($columns as $c)
                {
                    $sheet->getColumnDimension($c)->setAutoSize(true);
                    $sheet->getStyle($c . '2')->applyFromArray($style);
                }
                $spreadsheet->createSheet();
                $writer = new Xlsx($spreadsheet);
                /*header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="Jeffrey-Conciergerie_tax_' . $dateFormat . '.xlsx"');*/
                $filename = $cityLabel . "_taxe_sejour_" . $dateFormat . ".xlsx";
                $path = tempnam(sys_get_temp_dir(), $cityLabel . "_taxe_sejour_" . $dateFormat);
                $writer->save($path);
                $zip->addFile($path, $filename) or die("error on file " . $filename);
                $filesToUnlink[] = $path;
                //sleep(1);
            }

            //final total file
            $totalDatas[] = [];
            $totalDatas[] = [
                "Total",
                "=SUM(B3:B" . (count($groupedBookings) + 2) . ")",
            ];
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->fromArray($totalDatas, null, "A1");
            $sheet->mergeCells('A1:B1');

            $style = [
                'font' => [
                    'bold' => 1
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ],
            ];
            $spreadsheet->getActiveSheet()->getStyle('A1:B1')->applyFromArray($style);

            $columns = ["A", "B"];
            foreach($columns as $c)
            {
                $sheet->getColumnDimension($c)->setAutoSize(true);
                $sheet->getStyle($c . '2')->applyFromArray($style);
            }
            $spreadsheet->createSheet();
            $writer = new Xlsx($spreadsheet);
            $filename = "total_taxes_sejour_" . $dateFormat . ".xlsx";
            $path = storage_path() . "/total_taxes_sejour_" . $dateFormat . ".xlsx";
            $writer->save($path);
            $zip->addFile($path, basename($path)) or die("error on file total_taxes_sejour_" . $dateFormat . ".xlsx");
            $filesToUnlink[] = $path;



            $zip->close();
            //sleep(1);
            foreach($filesToUnlink as $path){
                unlink($path);
            }
            attachFile($zipName, basename($zipName));
        }
    }

}
