<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\Administrator;
use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Form;
use Lava;
use Khill\Lavacharts\Lavacharts;
use Khill\Lavacharts\DataTables\DataTable;
use Khill\Lavacharts\Charts\BarChart;
use Encore\Admin\Widgets\Collapse;
use Carbon\Carbon;
use App\Property;
use App\Booking;
use App\Report;
use App\StatSearch;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{

    public function index(Request $request)
    {
        return Admin::content(function (Content $content) use ($request)
                {

                    $content->header('Statistiques');
                    $content->description('Vos chiffres');
                    $filters = $request->all();
                    $searchId = $request->get("search");
                    if($searchId)
                    {
                        $statSearch = StatSearch::where("id", "=", $searchId)->first();
                        if($statSearch)
                        {
                            //dd($statSearch);
                            $filters = $statSearch->getFilters();
                        }
                    } else if(array_key_exists("label", $filters) && !empty($filters["label"]))
                    {
                        $label = $filters["label"];
                        unset($filters["label"]);
                        $statSearch = StatSearch::create(["label" => $label, "filters" => json_encode($filters)]);
                    }

                    $collapse = new Collapse();
                    $collapse->add("Recherches préenregistrés", $this->searchSelect($filters)->render());
                    $collapse->add("Filtres", $this->form($filters)->render());
                    $content->body($collapse);

                    //var_dump($filters);

                    $reports = Report::all();

                    //$properties = Properties::all();
                    $fullyear = array_key_exists("fullyear", $filters) ? $filters["fullyear"] : false;
                    
                    $bookings = Booking::select('bookings.*')->where("status", "=", Booking::STATUS_BOOKED);

                    if(!array_key_exists('start_at', $filters) || empty($filters['start_at']))
                        $bookings->where("start_at", ">=", (new Carbon())->addMonths(-12)->startOfMonth());
                    else
                        $bookings->whereBetween("end_at", [$filters['start_at'], $filters['end_at']]);

                    $bookings->orderBy("start_at", "asc");
                    if(array_key_exists("property", $filters) && $filters["property"][0])
                    {
                        $bookings->whereIn('property_id', $filters["property"]);
                    }
                    if(array_key_exists("source", $filters) && $filters["source"][0])
                    {
                        $bookings->whereIn('source', $filters["source"]);
                    }
                    $bookings->join('properties', function($join) use ($filters)
                    {
                        $this->filter($join, $filters);
                    });
                    if(array_key_exists("city_managers", $filters) && $filters["city_managers"][0])
                    {
                        $bookings->join('city_manager_has_properties as cmhp', function($join) use ($filters){
                                $join->on('cmhp.property_id', '=', 'bookings.property_id');
                                $join->whereIn('cmhp.admin_user_id', $filters["city_managers"]);
                        });
                    }
                    $bookings = $bookings->get();
                    /* @var $bookings \Illuminate\Database\Eloquent\Collection */
                    
                    $datas = [];
                    $results = [];
                    $groupby = isset($filters["groupby"]) ? $filters["groupby"] : 'end_at';
                    $groupBookings = $bookings->groupBy(function($val)use($fullyear, $groupby)
                    {
                        if($fullyear)
                            return Carbon::parse($val->$groupby)->format('Y-');
                        else
                            return Carbon::parse($val->$groupby)->format('Y-m');
                    });
                    if(count($bookings))
                    {
                        $dateStart = new Carbon($bookings[0]->start_at);
                        $dateEnd = (new Carbon($bookings[count($bookings) - 1]->end_at));
                        if($fullyear){
                            $datas[$dateStart->format("Y-")] = [];
                            while($dateStart->diffInYears($dateEnd) > 0)
                            {
                                $dateStart->addYear();
                                if($dateStart->diffInYears($dateEnd) > 0)
                                    $datas[$dateStart->format("Y-")] = [];
                            }
                        }else{
                            while($dateStart->diffInMonths($dateEnd) > 0)
                            {
                                $datas[$dateStart->format("Y-m")] = [];
                                $dateStart->addMonth();
                            }
                        }
                    }
                    $datas = array_merge($datas, $groupBookings->toArray());
                    
                    
                    foreach($datas as $month => $data){
                        if(!array_key_exists($month, $results))
                        {
                            $results[$month] = [
                                "properties" => [],
                            ];
                        }
                        if(array_key_exists($month, $groupBookings)){
                            foreach($groupBookings[$month] as $value)
                            {
                                $p = $value->property;
                                if(!array_key_exists($p->name, $results[$month]["properties"]))
                                {
                                    $results[$month]["properties"][$p->name] = [
                                        'property' => $p,
                                        'profit' => $value->owner
                                    ];
                                } else
                                {
                                    $results[$month]["properties"][$p->name]['profit'] += $value->owner;
                                }
                            }
                        }
                    }
                    
                    $datas = aggregateBookings($datas, $fullyear);
                    $statsGlobal = "";

                    $lava = new Lavacharts();
                    $collapCharts = new Collapse();
                    
                    $columns = [
                        [
                            "title" => "Commissions jeffrey",
                            "name" => "profit",
                            "suffix" => " €"
                        ],
                    ];
                    bookingsChart($lava, "ColumnChart", $datas, "Commissions", $columns, ["#5a5"], false, $fullyear);
                    $collapCharts->add("Commissions Jeffrey","<div id='com-chart'></div>" . $lava->render('ColumnChart', 'Commissions', 'com-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    $columns = [
                        [
                            "title" => "Brut Payé",
                            "name" => "paid_amount",
                            "suffix" => " €"
                        ],
                        [
                            "title" => "Payment à venir",
                            "name" => "paymenttocome",
                            "suffix" => " €"
                        ],
                        /*[
                            "title" => "OTA",
                            "name" => "ota",
                            "suffix" => " €"
                        ],
                        [
                            "title" => "Virements",
                            "name" => "owner",
                            "suffix" => " €"
                        ]*/
                    ];
                    $colors = [
                        "#ba5",
                        "#0af",
                    ];
                    bookingsChart($lava, "ComboChart", $datas, "Full", $columns, $colors, true, $fullyear);
                    $collapCharts->add("Tableaux Récapitulatif","<div id='all-chart'></div>" . $lava->render('ComboChart', 'Full', 'all-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    $columns = [
                        [
                            "title" => "OTA",
                            "name" => "ota",
                            "suffix" => " €"
                        ],
                        [
                            "title" => "Virements",
                            "name" => "owner",
                            "suffix" => " €"
                        ]
                    ];
                    $colors = [
                        "#f55",
                        "#55b",
                    ];
                    bookingsChart($lava, "ComboChart", $datas, "OTA", $columns, $colors, true, $fullyear);
                    $collapCharts->add("Tableaux OTA et Propriétaires","<div id='moneyreverse-chart'></div>" . $lava->render('ComboChart', 'OTA', 'moneyreverse-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    /*$columns = [
                        [
                            "title" => "Taux d'occupations",
                            "name" => "occupied",
                            "suffix" => "%"
                        ],
                    ];
                    $global = bookingsChart($lava, "ColumnChart", $datas, "Taux", $columns, ["#ff7f00"]);
                    $statsGlobal .= "<h2>Taux d'occupations</h2><div id='occup-chart'></div>" . $lava->render('ColumnChart', 'Taux', 'occup-chart');
                    */
                    $columns = [
                        [
                            "title" => "Nuités",
                            "name" => "nights",
                            "suffix" => " nuités"
                        ],
                        [
                            "title" => "Réservations",
                            "name" => "count",
                            "suffix" => " réservations"
                        ],
                    ];
                    $colors = [
                        "#0af",
                        "#55b",
                    ];
                    bookingsChart($lava, "ComboChart", $datas, "nights", $columns, $colors, false, $fullyear);
                    $collapCharts->add("Réservations & Nuités","<div id='nights-chart'></div>" . $lava->render('ComboChart', 'nights', 'nights-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    
                    $columns = [
                        [
                            "title" => "Moyenne de séjour",
                            "name" => "avgNights",
                            "suffix" => " jours"
                        ],
                    ];
                    $global = bookingsChart($lava, "ColumnChart", $datas, "Moyenne", $columns, ["#08F", "#55A", "#cb5fd3", "#ff7f00"], false, $fullyear);
                    $collapCharts->add("Moyenne de séjour","<div id='avg-chart'></div>" . $lava->render('ColumnChart', 'Moyenne', 'avg-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    
                    $columns = [
                        [
                            "title" => "Taxes de séjour",
                            "name" => "tax",
                            "suffix" => " €"
                        ],
                    ];
                    bookingsChart($lava, "ColumnChart", $datas, "Taxes", $columns, $colors, false, $fullyear);
                    $collapCharts->add("Taxes de séjour","<div id='tax-chart'></div>" . $lava->render('ColumnChart', 'Taxes', 'tax-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    
                    $columns = [
                        [
                            "title" => "Frais de ménages",
                            "name" => "cleaning",
                            "suffix" => " €"
                        ],
                    ];
                    bookingsChart($lava, "ColumnChart", $datas, "Cleaning", $columns, $colors, false, $fullyear);
                    $collapCharts->add("Frais de ménage","<div id='cleaning-chart'></div>" . $lava->render('ColumnChart', 'Cleaning', 'cleaning-chart'));
                    $statsGlobal .= $collapCharts->render();
                    $collapCharts = new Collapse();
                    
                    $tab = new Tab();
                    $tab->add("Stats global", $statsGlobal);
                    $content->body($tab);
                    
                    krsort($datas);
                    $collap = new Collapse();
                    foreach($datas as $time => $data)
                    {
                        usort($data["properties"], function($a, $b)
                        {
                            return $a["profit"] < $b["profit"];
                        });
                        $highscores = "<table width='100%'>";
                        $i = 1;
                        foreach($data["properties"] as $values)
                        {
                            $property = Property::find($values["id"]);
                            
                            if($property && $i <= 10)
                            {
                                $highscores .= "<tr><td>$i.</td><td><a href='" . url("admin/properties/" . $property->id) . "'>" . $property->name . " :</a></td><td>" . $values["profit"] . " €</td></tr>";
                            }
                            $i++;
                        }
                        $highscores .= "</table>";
                        if(!$fullyear){
                            $time = new Carbon($time . '-01');
                            $collap->add($time->format("M Y"), $highscores);
                        }else{
                            $collap->add(substr($time,0,4), $highscores);
                        }
                    }
                    $tab->add("Classements propriétés", $collap);
                });
    }

    private function filter($join, $filters)
    {
        $join->on('properties.id', '=', 'property_id');
        if(array_key_exists("cities", $filters) && $filters["cities"][0])
        {
            $join->whereIn('properties.city_id', $filters["cities"]);
        }
        if(array_key_exists("departement", $filters) && $filters["departement"][0])
        {
            $join->whereIn('properties.departement', $filters["departement"]);
        }
        if(array_key_exists("address", $filters) && !empty($filters["address"]))
        {
            $join->where('properties.address', "LIKE", "%" . $filters["address"] . "%");
        }

    }

    private function searchSelect($data)
    {
        $form = new Form($data);
        $form->method("GET");
        $form->select('search', 'Recherches')->options(function()
        {
            $searches = [];
            foreach(StatSearch::all() as $s)
            {
                $searches[$s->id] = $s->label;
            }
            return $searches;
        });
        $form->disableReset();
        return $form;
    }

    private function form($data)
    {
        $form = new Form($data);
        $form->action("/admin/stats");
        $form->method("GET");
        $form->attribute("id", "searchValues");
        $form->hidden("_token")->default(csrf_token());
        $form->date('start_at', 'A partir de');
        $form->date('end_at', 'Jusqu\'à');
        $form->text('address', 'Adresse (code postal)');
        $form->radio('fullyear', 'Compacté par année')->options([0 => "non", 1 => "oui"]);
        $sources = DB::table("bookings")->selectRaw("source")->whereNotNull('source')->groupBy("source")->get();
        $form->multipleSelect('source', 'Sources')->options(function()use($sources){
            $options = [];
            foreach($sources as $s){
                $options[$s->source] = $s->source;
            }
            return $options;
        });
        $form->select('groupby', 'Grouper par')->options([
            "end_at" => "date de réservation",
            "created_at" => "date de création"
        ]);
        $form->multipleSelect('city_managers', 'City Manager')->options(Administrator::where('is_city_manager', 1)->pluck('name', 'id'));
        $form->multipleSelect('departement', 'Départments')->options(departements());
        $form->multipleSelect('cities', 'Communes')->options(function($ids){
            if(!$ids)
                $ids = [];
            $cities = City::whereIn('id', $ids)->get();
            $options = [];
            foreach($cities as $city){
                $options[$city->id] = $city->code_postal . ' - ' . $city->nom_commune;
            }
            return $options;
        })->ajax('/admin/api/cities');
        $form->multipleSelect('property', 'Propriétés')->options(function()
        {
            $options = [];
            foreach(Property::all() as $p)
            {
                $options[$p->id] = $p->name;
            }
            return $options;
        });
        $form->text('label', 'Nom de la recherche(si vous l\'enregistrer)');
        $form->disableReset();
        //$form->();
        return $form;
    }

}
