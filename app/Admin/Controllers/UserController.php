<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Auth\Database\AdminInfo;
use App\Helpers\Mailer;
use App\User;
use App\Company;
use Illuminate\Http\Request;
use Encore\Admin\Show;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Row;
use Encore\Admin\Layout\Column;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class UserController extends Controller
{

    use ModelForm;
    use AuthenticatesUsers;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Gestion des clients');
                    $content->description('');

                    $content->body($this->grid());
                });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {

        $user = User::where("id", "=", $id)->first();
        if($user && !$user->company)
        {
            $company = Company::create(['user_id' => $id]);
        }

        return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des clients');
                    $content->description('édit du compte client');
                    $content->body($this->form($id)->edit($id));
                    $content->body(Admin::show(User::findOrFail($id), function(Show $show)
                            {
                                $show->panel()->style('hidden');
                                $show->properties('Propriétés', function ($property)
                                {
                                    $property->setResource('/admin/properties');
                                    $property->name();
                                    $property->address();
                                });
                            }));
                });
    }

    public function sendRentMail($id)
    {
        $user = User::where("id", "=", $id)->first();
        Mailer::sendRentMail($user);
        admin_success("Mail location envoyé !");
        return redirect()->action('\App\Admin\Controllers\UserController@show', ["id" => $id]);
    }

    public function sendWelcomeMail($id)
    {
        $user = User::where("id", "=", $id)->first();
        Mailer::sendConfirmation($user);
        admin_success("Mail de bienvenue envoyé !");
        return redirect()->action('\App\Admin\Controllers\UserController@show', ["id" => $id]);
    }

    /**
     * Show interface.
     *
     * @param $id
     * @return Content
     */
    public function show($id)
    {
        return redirect()->action('\App\Admin\Controllers\UserController@edit', ["id" => $id]);
        /*return Admin::content(function (Content $content) use ($id)
                {

                    $content->header('Gestion des clients');
                    $content->description('edit the client account');

                    $content->body(Admin::show(User::findOrFail($id), function(Show $show) use($id)
                            {
                                $show->field('archived', 'Archivé')->badge($show->getModel()->archived? "red" : "green");
                                $show->client_id('Client ID');
                                $show->name('Nom');
                                $show->email('Email');
                                $show->phone('Téléphone');
                                $show->address('Addresse');
                                if($show->getModel()->company)
                                {
                                    $show->company('Société', function($company)
                                    {
                                        $company->name();
                                        $company->company_number();
                                        $company->address();
                                        $company->capital();
                                        $company->panel()->tools(function($tools)
                                        {
                                            $tools->disableEdit();
                                            $tools->disableList();
                                            $tools->disableDelete();
                                        });
                                    });
                                }
                                $show->properties('Propriétés', function ($property)
                                {

                                    $property->setResource('/admin/properties');

                                    $property->id();
                                    $property->name();
                                    $property->address();
                                });
                                
                                $controller = $this;
                                $show->panel()->tools(function(Show\Tools $tools) use ($id, $controller)
                                {
                                    $controller->userTools($tools, $id);
                                });
                            }));
                });*/
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content)
                {

                    $content->header('Gestion des clients');
                    $content->description('créer un compte client');

                    $content->body($this->form());
                });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $user = Admin::user();
        if($user->is_city_manager){
            $users = $user->users()->get()->pluck('id');
            $grid->model()->whereIn('id', $users);
        }
        $grid->model()->where('archived', '=', 0);
        $grid->client_id('ID Client')->sortable();
        $grid->name('Nom')->display(function($name){
            return "<a href='" . url('/admin/clients/' . $this->id) . "'>" . $name . "</a>";
        })->sortable();
        $grid->email('Email')->sortable();
        $grid->cgv("CGV")->display(function($value)
        {
            return "<i style='color:#" . (!empty($value) ? "595" : "a55") . "' class='fa " . (!empty($value) ? "fa-check" : "fa-close") . "'/>";
        })->sortable();
        $grid->phone('Tel')->sortable();
        /* $grid->created_at();
          $grid->updated_at(); */
        $grid->actions(function (Grid\Displayers\Actions $actions){
            $actions->disableView();
            $actions->disableDelete();
        });
        $grid->filter(function (Grid\Filter $filter)
        {
            $filter->disableIdFilter();
            // Add a column filter
            $filter->like('name', 'name');
        });
        $grid->export(function(Grid\Exporters\CsvExporter $export){
            $export->originalValue(['name']);
        });
        $grid->paginate(100);
        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(User::class, function (Form $form) use ($id)
                {
                    //$form->display('id', 'ID');

                    $form->tab('Client', function($form)
                    {
                        $form->switch('archived', 'Archivé ?')->states([
                            'on' => ["value" => 1, "text" => "OUI", "color" => "danger"],
                            'off' => ["value" => 0, "text" => "NON", "color" => "success"]
                        ]);
                        $form->switch('bnbkeys', 'Contrat BnBkeys')->states([
                            'on' => ["value" => 1, "text" => "OUI", "color" => "success"],
                            'off' => ["value" => 0, "text" => "NON", "color" => "danger"]
                        ]);
                        $form->text('client_id', 'Client ID')->rules('required');
                        $form->text('name', 'Nom')->rules('required');
                        $form->text('email', 'Email')->rules(function ($form)
                        {
                            // If it is not an edit state, add field unique verification
                            if(!$form->model()->id)
                            {
                                return 'unique:users|email|required';
                            } else
                            {
                                return 'email|required';
                            }
                        });
                        $form->text('phone', 'Téléphone');
                        $form->textarea('address', 'Addresse');
                        $form->text('tva_number', 'Numéro de TVA');
                        $form->password('password', 'Mot de passe')->rules(function ($form)
                        {
                            // If it is not an edit state, add field unique verification
                            if(!$form->model()->id)
                            {
                                return 'required';
                            }
                            return '';
                        });
                    });
                    $form->tab('Société', function ($form)
                    {
                        $form->hasOne('companies', function ($form)
                        {
                            $form->text('name', 'Nom de la société');
                            $form->text('company_number', 'N° d\'immatriculation');
                            $form->textarea('address', 'Adresse');
                            $form->number('capital', 'Capital (€)');
                        });
                    });

                    if($id)
                    {
                        $controller = $this;
                        $form->tools(function(Form\Tools $tools) use ($id, $controller)
                        {
                            $controller->userTools($tools, $id);
                        });
                    }

                    $form->saving(function (Form $form)
                    {
                        if($form->password && $form->model()->password != $form->password && $form->model()->password !== bcrypt($form->password))
                        {
                            $form->password = bcrypt($form->password);
                        } else if(empty($form->password))
                        {
                            $form->password = $form->model()->password;
                        }
                    });

                    $form->saved(function(Form $form){
                        foreach($form->companies as $info){
                            $inf = Company::find($info["id"]);
                            foreach(['name', 'company_number', 'address', 'capital'] as $field){
                                if($info[$field] === null){
                                    $inf->$field = null;
                                }
                            }
                            $inf->save();
                        }
                    });

                    $form->footer(function (Form\Footer $footer)
                    {
                        $footer->checkEditing();
                        $footer->disableCreatingCheck();
                        $footer->disableReset();
                    });
                });
    }
    
    public function userTools($tools, $id){
        $tools->prepend("<div class='btn-group' style='margin-right:5px;'>");
        $tools->prepend("<a class='btn btn-sm btn-primary' target='_blank' href='" . url()->action('\App\Admin\Controllers\UserController@logMeAsUser', ["id" => $id]) . "'>");
        $tools->prepend("<i class='fa fa-sign-in'></i><span class='hidden-xs'> Se connecter au compte</span></a></div>");

        $tools->prepend("<div class='btn-group' style='margin-right:5px;'>");
        $tools->prepend("<a class='btn btn-sm btn-primary' href='" . url()->action('\App\Admin\Controllers\UserController@sendRentMail', ["id" => $id]) . "'>");
        $tools->prepend("<i class='fa fa-send'></i><span class='hidden-xs'> Renvoyer le mail de Location meublés</span></a></div>");

        $tools->prepend("<div class='btn-group' style='margin-right:5px;'>");
        $tools->prepend("<a class='btn btn-sm btn-primary' href='" . url()->action('\App\Admin\Controllers\UserController@sendWelcomeMail', ["id" => $id]) . "'>");
        $tools->prepend("<i class='fa fa-send'></i><span class='hidden-xs'> Renvoyer le mail de bienvenue</span></a></div>");
    }
    
    public function logMeAsUser($id){
        $user = User::where('id', '=', $id)->first();
        if($user){
            $this->guard()->loginUsingId($id);
            return redirect('/account');
        }
    }

}
