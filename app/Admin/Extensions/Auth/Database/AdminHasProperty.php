<?php
namespace App\Admin\Extensions\Auth\Database;

use App\Admin\Extensions\Auth\Database\Administrator;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class AdminHasProperty extends Model
{
    protected $table = 'city_manager_has_properties';

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_user_id', 'property_id', 'commission', 'owned'
    ];
    
    public function property(){
        return $this->belongsTo("App\Property");
    }
    public function admin_user(){
        return $this->belongsTo(Administrator::class);
    }
}
