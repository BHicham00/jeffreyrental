<?php
namespace App\Admin\Extensions\Auth\Database;

use App\Admin\Extensions\Auth\Database\Administrator;
use App\City;
use App\Country;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class AdminInfo extends Model
{
    protected $table = 'city_manager_infos';

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_user_id', 'email', 'email_pro', 'firstname', 'lastname', 'phone', 'company', 'address', 'city_id', 'country_id', 'siret', 'iban', 'bic', 'tva'
    ];

    public function city(){
        return $this->belongsTo(City::class);
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
}
