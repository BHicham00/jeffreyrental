<?php

namespace App\Admin\Extensions\Auth\Database;

use App\BookingGuest;
use App\CmInvoice;
use App\Property;
use App\User;
use Encore\Admin\Auth\Database\Administrator as Admin;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

/**
 * Class Administrator.
 *
 * @property Role[] $roles
 */
class Administrator extends Admin
{

    protected $fillable = ['username', 'password', 'name', 'avatar', 'is_city_manager'];

    public function properties(){
        return $this->hasManyThrough(Property::class, AdminHasProperty::class, 'admin_user_id', 'id', 'id', 'property_id');
    }
    public function users(){
        return User::select('users.*')->leftJoin('properties as p', 'p.user_id', '=', 'users.id')->whereIn('p.id', $this->properties()->get()->pluck('id'));
    }
    public function guest(){
        return BookingGuest::select('booking_guests.*')->leftJoin('bookings as b', 'b.id', '=', 'booking_guests.booking_id')->leftJoin('properties as p', 'p.id', '=', 'b.property_id')->whereIn('p.id', $this->properties()->get()->pluck('id'));
    }

    public function invoices(){
        return $this->hasMany(CmInvoice::class, 'admin_user_id');
    }

    public function infos(){
        return $this->hasMany(AdminInfo::class, 'admin_user_id');
    }
    public function info(){
        return $this->hasOne(AdminInfo::class, 'admin_user_id');
    }

}
