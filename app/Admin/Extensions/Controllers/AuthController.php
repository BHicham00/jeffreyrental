<?php

namespace App\Admin\Extensions\Controllers;

use App\Admin\Extensions\Auth\Database\AdminInfo;
use App\City;
use App\Country;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Encore\Admin\Controllers\AuthController as Controller;

class AuthController extends Controller
{
    protected function settingForm()
    {
        $class = config('admin.database.users_model');

        $form = new Form(new $class());
        $form->tab('Account', function($form){
            $form->display('username', trans('admin.username'));
            $form->text('name', trans('admin.name'))->rules('required');
            $form->image('avatar', trans('admin.avatar'));
            $form->password('password', trans('admin.password'))->rules('confirmed|required');
            $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
                ->default(function ($form) {
                    return $form->model()->password;
                });
        });
        $user = auth('admin')->user();
        if($user && $user->is_city_manager) {
            $form->tab('Informations', function (Form $form) {

                $form->hasOne('infos', function (Form\NestedForm $form) {
                    $form->text('firstname', 'Prénom');
                    $form->text('lastname', 'Nom');
                    $form->email('email_pro', 'Email de contact client');
                    $form->email('email', 'Email');
                    $form->text('phone', 'Téléphone');
                    $form->text('address', 'Adresse');
                    $form->select('city_id', 'Ville')->options(function($cityId){
                        $options = [];
                        $city = City::find($cityId);
                        if($city){
                            $options[$city->id] = $city->nom_commune. ' ' . $city->code_postal;
                        }
                        return $options;
                    })->ajax('/admin/api/cities');

                    $form->select('country_id', 'Pays')->options(function($countryId){
                        $options = [];
                        $country = Country::find($countryId);
                        if($country){
                            $options[$country->id] = $country->nicename;
                        }
                        return $options;
                    })->ajax('/admin/api/countries');
                    $form->text('company', 'Entreprise')->help('vide si pas de dénomination');
                    $form->text('siret', 'Siret');
                    $form->text('iban', 'IBAN');
                    $form->text('bic', 'BIC');
                    $form->text('tva', 'N° TVA')->rules('nullable');
                });

            });
        }

        $form->setAction(admin_url('auth/setting'));

        $form->ignore(['password_confirmation']);

        $form->saving(function (Form $form) {
            if ($form->password && $form->model()->password != $form->password) {
                $form->password = bcrypt($form->password);
            }
        });


        $form->saved(function (Form $form) use ($user) {
            if($user && $user->is_city_manager){
                foreach($form->infos as $info){
                    $inf = AdminInfo::find($info["id"]);
                    foreach(['company', 'tva'] as $field){
                        if($info[$field] === null){
                            $inf->$field = null;
                        }
                    }
                    $inf->save();
                }
            }
            admin_toastr(trans('admin.update_succeeded'));

            return redirect(admin_url('auth/setting'));
        });

        return $form;
    }
}
