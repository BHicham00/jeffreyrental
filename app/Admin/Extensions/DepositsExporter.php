<?php

Namespace App\Admin\Extensions;

Use Encore\Admin\Grid\Exporters\ExcelExporter;
use Carbon\Carbon;

class DepositsExporter extends ExcelExporter
{
    protected $fileName = 'listes_cautions.xlsx';

    protected $columns = [
        'id' => 'ID',
        'title' => 'title',
        'content' => 'content',
    ];
    
    public function __constructor(){
        $this->filename = 'listes_cautions_'. (new Carbon())->format('ymd') . '.xlsx';
    }
}