<?php

namespace App\Admin\Extensions\Form\Field;

use Encore\Admin\Form\Field;
/**
 * Class HasOne.
 */
class HasOne extends Field\HasMany
{
    public $isUnique = true;
    
    /**
     * Get the view variables of this field.
     *
     * @return array
     */
    public function variables() : array
    {
        return array_merge($this->variables, [
            'id'          => $this->id,
            'name'        => $this->elementName ?: $this->formatName($this->column),
            'help'        => $this->help,
            'class'       => $this->getElementClassString(),
            'value'       => $this->value(),
            'label'       => $this->label,
            'viewClass'   => $this->getViewElementClasses(),
            'column'      => $this->column,
            'errorKey'    => $this->getErrorKey(),
            'attributes'  => $this->formatAttributes(),
            'placeholder' => $this->getPlaceholder(),
            'isUnique' => $this->isUnique,
        ]);
    }
}
