<?php

namespace App\Admin\Extensions\Form\Field;
use Encore\Admin\Form\Field\File;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContractUpload
 *
 * @author Administrateur
 */
class IdentUpload extends File
{
    
    protected $previewUrl = "";
    
    public function setPreviewUrl($url){
        $this->previewUrl = $url;
    }
    
    protected function initStorage()
    {
        $this->disk(config('admin.guestfiles.disk'));
    }
    public function defaultDirectory()
    {
        $this->disk(config('admin.guestfiles.directory.default'));
    }
    /**
     * If name already exists, rename it.
     *
     * @param $file
     *
     * @return void
     */
    public function renameIfExists(UploadedFile $file)
    {
        if ($this->storage->exists("{$this->getDirectory()}/$this->name")) {
            //echo "EXISTS";
            $this->storage->delete("{$this->getDirectory()}/$this->name");
            //valdebug($this->storage->exists("{$this->getDirectory()}/$this->name"));die;
            //$this->name = $this->generateUniqueName($file);
        }
    }
    
    protected function uploadAndDeleteOriginal(UploadedFile $file)
    {
        $this->renameIfExists($file);

        $path = null;
        
        $this->destroy();

        if (!is_null($this->storagePermission)) {
            $path = $this->storage->putFileAs($this->getDirectory(), $file, $this->name, $this->storagePermission);
        } else {
            $path = $this->storage->putFileAs($this->getDirectory(), $file, $this->name);
        }

        return $path;
    }
    
    /**
     * @return array
     */
    protected function initialPreviewConfig()
    {
        $config = ['caption' => basename($this->value), 'key' => 0];

        $config = array_merge($config);//, $this->guessPreviewType($this->value));

        return [$config];
    }
    
    /**
     * Get file visit url.
     *
     * @param $path
     *
     * @return string
     */
    public function objectUrl($path)
    {
        if (URL::isValidUrl($path)) {
            return $path;
        }
        
        if(!empty($this->previewUrl)){
            return $this->previewUrl;
        }

        if ($this->storage) {
            return $this->storage->url($path);
        }

        return $this->previewUrl;
    }
}
