<?php

namespace App\Admin\Extensions\Grid\Actions;

use Encore\Admin\Admin;

class ArchiveDeposit extends \Encore\Admin\Actions\RowAction
{
    public $name = "Archiver";

    protected function script()
    {
        $url = url("/admin/deposits/");
        return <<<SCRIPT

$('.grid-check-row').on('click', function () {
    
    var id = $(this).data('id');
    var row = $(this).parent().parent();
        swal({
        title: "êtes vous sure de vouloir archivé cette caution ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirmer",
        showLoaderOnConfirm: true,
        cancelButtonText: "Annuler",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.get("$url/"+id+"/archive", function(data){
                    data = JSON.parse(data);
                    if(data.success){
                        $.pjax.reload('#pjax-container');
                        resolve(data);
                    }
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object') {
            if (data.success) {
                swal(data.message, '', 'success');
            } else {
                swal(data.message, '', 'error');
            }
        }
    });
    
});

SCRIPT;
    }

    public function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-danger fa fa-file-archive-o grid-check-row' style='color:#FFF; title='Archiver' data-id='{$this->getKey()}'></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}