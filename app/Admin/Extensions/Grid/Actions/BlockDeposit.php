<?php

namespace App\Admin\Extensions\Grid\Actions;

use Encore\Admin\Admin;
use App\Deposit;

class BlockDeposit extends \Encore\Admin\Actions\RowAction
{
    public $name = "Bloquer";

    protected function script()
    {
        $msg = "êtes vous sure de vouloir débloqué/bloqué cette caution ?";
        $url = url("/admin/deposits/");
        return <<<SCRIPT

$('.grid-check-row').on('click', function () {
    
    var id = $(this).data('id');
    var row = $(this).parent().parent();
        swal({
        title: "$msg",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirmer",
        showLoaderOnConfirm: true,
        cancelButtonText: "Annuler",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.get("$url/"+id+"/block", function(data){
                    data = JSON.parse(data);
                    if(data.success){
                        $.pjax.reload('#pjax-container');
                        resolve(data);
                    }
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object') {
            if (data.success) {
                swal(data.message, '', 'success');
            } else {
                swal(data.message, '', 'error');
            }
        }
    });
    
});

SCRIPT;
    }

    public function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-default fa fa-ban grid-check-row' title='block' style='color:#F00' data-id='{$this->getKey()}'></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}