<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Admin\Extensions\Grid\Actions;

use Encore\Admin\Actions\RowAction;

/**
 * Description of Link
 *
 * @author Administrateur
 */
class Link extends RowAction
{
    public $name = "";
    public $link = "";
    public $target = "";
    
    public function __construct($name, $link, $target = null)
    {
        parent::__construct();
        $this->name = $name;
        $this->link = $link;
        $this->target = $target;
    }
    
    public function href(){
        return $this->link;
    }
    
    public function render()
    {
        if ($href = $this->href()) {
            return "<a target='" . $this->target . "' href='{$href}'>{$this->name()}</a>";
        }

        $this->addScript();

        $attributes = $this->formatAttributes();

        return sprintf(
            "<a data-_key='%s' href='javascript:void(0);' class='%s' {$attributes}>%s</a>",
            $this->getKey(),
            $this->getElementClass(),
            $this->asColumn ? $this->display($this->row($this->column->getName())) : $this->name()
        );
    }
}
