<?php

namespace App\Admin\Extensions\Grid\Actions;

use Encore\Admin\Admin;
use Encore\Admin\Actions\RowAction;

class LinkAction extends RowAction
{
    public $name = "Synchroniser";
    protected $url = "";
    protected $scriptID = "";

    public function __construct($name, $url, $scriptID)
    {
        $this->name = $name;
        $this->url = $url;
        $this->scriptID = $scriptID;
    }

    protected function script()
    {
        $url = $this->url;
        $scriptId = $this->scriptID;
        return <<<SCRIPT

$('.grid-$scriptId-row').on('click', function () {
    
    var id = $(this).data('id');
    var row = $(this).parent().parent();
        swal({
        title: "êtes vous sure de vouloir effectuer cette action ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirmer",
        showLoaderOnConfirm: true,
        cancelButtonText: "Annuler",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.get(("$url").replace("id", id), function(data){
                    $.pjax.reload('#pjax-container');
                    resolve(data);
                }).fail(function(){
                    alert('Temps d\'exécution maximum dépassé, rechargez la page');
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object') {
            if (data.success) {
                swal(data.message, '', 'success');
            } else {
                swal(data.message, '', 'error');
            }
        }
    });
    
});

SCRIPT;
    }

    public function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-default fa fa-send grid-{$this->scriptID}-row' title='Synchroniser' data-id='{$this->getKey()}'>{$this->name}</a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}
