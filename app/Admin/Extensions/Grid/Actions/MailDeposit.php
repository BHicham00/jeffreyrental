<?php

namespace App\Admin\Extensions\Grid\Actions;

use Encore\Admin\Admin;

class MailDeposit extends \Encore\Admin\Actions\RowAction
{
    public $name = "Envoyer mail";

    protected function script()
    {
        $url = url("/admin/deposits/");
        return <<<SCRIPT

$('.grid-mail-row').on('click', function () {
    // Your code.
    var id = $(this).data('id');
    var row = $(this).parent().parent();
    swal({
        title: "Êtes vous sure de vouloir envoyé ce mail ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirmer",
        showLoaderOnConfirm: true,
        cancelButtonText: "Annuler",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.get("$url/"+id+"/jsonMail", function(data){
                    data = JSON.parse(data);
                    if(data.success){
                        $.pjax.reload('#pjax-container');
                
                        resolve(data);
                    }
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object') {
            if (data.success) {
                swal(data.message, '', 'success');
            } else {
                swal(data.message, '', 'error');
            }
        }
    });
});

SCRIPT;
    }

    public function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-primary fa fa-send grid-mail-row' style='color:#FFF;' title='Envoyer mail' data-id='{$this->getKey()}'></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}