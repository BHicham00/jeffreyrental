<?php

namespace App\Admin\Extensions\Grid\Actions;

use Encore\Admin\Admin;
use Encore\Admin\Actions\RowAction;

class SyncBooking extends RowAction
{
    public $name = "Synchroniser";


    protected function script()
    {
        $url = url("/admin/bookings/");
        return <<<SCRIPT

$('.grid-check-row').on('click', function () {
    
    var id = $(this).data('id');
    var row = $(this).parent().parent();
        swal({
        title: "êtes vous sure de vouloir synchronisé cette réservation ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirmer",
        showLoaderOnConfirm: true,
        cancelButtonText: "Annuler",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.get("$url/"+id+"/sync", function(data){
                    data = JSON.parse(data);
                    if(data.success){
                        $.pjax.reload('#pjax-container');
                        resolve(data);
                    }
                }).fail(function(){
                    alert('Temps d\'exécution maximum dépassé, rechargez la page');
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object') {
            if (data.success) {
                swal(data.message, '', 'success');
            } else {
                swal(data.message, '', 'error');
            }
        }
    });
    
});

SCRIPT;
    }

    public function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-default fa fa-refresh grid-check-row' style='color:#5C5' title='Synchroniser' data-id='{$this->getKey()}'></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}
