<?php
namespace App\Admin\Extensions\Grid;

use Encore\Admin\Grid;
use Encore\Admin\Grid\Column;
use Illuminate\Support\Collection;

/**
 * Description of FilledGrid
 *
 * @author Administrateur
 */
class FilledGrid extends Grid
{
    
    private $_collection = null;
    
    public function fillData(Array $data = array()){
        $this->_collection = new Collection($data);
    }
    
    public function build()
    {
        if ($this->builded) {
            return;
        }
        
        $this->applyQuery();
        $collection = $this->applyFilter(false);

        $this->addDefaultColumns();

        Column::setOriginalGridModels($collection);
        if(!$this->_collection){
            $data = $collection->toArray();
        }else{
            Column::setOriginalGridModels($this->_collection);
            $data = $this->_collection->toArray();
        }

        $this->columns->map(function (Column $column) use (&$data) {
            $data = $column->fill($data);

            $this->columnNames[] = $column->getName();
        });

        $this->buildRows($data);

        $this->builded = true;
    }
}
