<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Admin\Extensions;
 

/**
 * Description of TemplateCustom
 *
 * @author Administrateur
 */
class TemplateCustom extends \PhpOffice\PhpWord\TemplateProcessor
{
    /**
     * ZipArchive object.
     *
     * @var \PhpOffice\PhpWord\Shared\ZipArchive
     */
    protected $zipClass;
    
    public function save_image($id,$filepath) { 
        if(file_exists($filepath))
        {
            $this->zipClass->deleteName('word/media/'.$id);          
            $this->zipClass->addFile ($filepath,'word/media/'.$id);
            list($width, $height, $type, $attr) = getimagesize($filepath);
            $this->setValue($id.'::width', $width . "px");
            $this->setValue($id.'::height', $height . "px");
        }   
    }
}
