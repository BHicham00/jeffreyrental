<?php
namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;
/**
 * Description of HTMLTool
 *
 * @author Administrateur
 */
class HTMLTool extends AbstractTool
{
    public $html = "";
    public $script = "";
    
    public function __construct($html = "", $script = "")
    {
        $this->html = $html;
        $this->script = $script;
    }
    
    protected function script()
    {
        return <<<EOT
            $this->script
EOT;
    }

    public function render()
    {
        Admin::script($this->script());
        return $this->html;
    }
}
