<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

//Encore\Admin\Form::forget(['map', 'editor']);
Encore\Admin\Form::extend('hasManyThrough', App\Admin\Extensions\Form\Field\HasManyThrough::class);
Encore\Admin\Form::extend('hasOne', App\Admin\Extensions\Form\Field\HasOne::class);
Encore\Admin\Form::extend('contractFile', App\Admin\Extensions\Form\Field\ContractUpload::class);
Encore\Admin\Form::extend('identFile', App\Admin\Extensions\Form\Field\IdentUpload::class);
Encore\Admin\Form::extend('editor', Encore\Admin\Form\Field\Editor::class);
Encore\Admin\Grid\Filter::extend('notLike', \App\Admin\Extensions\Grid\Filter\NotLike::class);
