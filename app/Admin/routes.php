<?php

use Illuminate\Routing\Router;


Route::group([
    'domain'        => config("app.owners_sub").".".config("app.root"),
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => 'admin.',
], function (Router $router) {
    $router->resource('/auth/users', '\App\Admin\Extensions\Controllers\UserController');

    $router->get('/', 'HomeController@index')->name('home');
    $router->get('/stats', 'StatsController@index')->name('stats');
    $router->post('/stats', 'StatsController@index')->name('stats');

    $router->get('/clients/{id}/rentmail', '\App\Admin\Controllers\UserController@sendRentMail')->name('clients.rentmail');
    $router->get('/clients/{id}/welcomemail', '\App\Admin\Controllers\UserController@sendWelcomeMail')->name('clients.welcomemail');
    $router->get('/clients/{id}/login', '\App\Admin\Controllers\UserController@logMeAsUser')->name('clients.logmein');

    $router->get('/keynest/{id}/newKey', '\App\Admin\Controllers\KeynestController@newKey')->name('keynest.newKey');
    $router->post('/keynest/{id}/newKey', '\App\Admin\Controllers\KeynestController@newKey')->name('keynest.newKey');

    $router->get('/api/users', 'AjaxController@users')->name('api.users');
    $router->post('/api/users', 'AjaxController@users')->name('api.users');
    $router->get('/api/properties', 'AjaxController@properties')->name('api.properties');
    $router->post('/api/properties', 'AjaxController@properties')->name('api.properties');
    $router->get('/api/cities', 'AjaxController@cities')->name('api.cities');
    $router->post('/api/cities', 'AjaxController@cities')->name('api.cities');
    $router->get('/api/countries', 'AjaxController@countries')->name('api.countries');
    $router->post('/api/countries', 'AjaxController@countries')->name('api.countries');

    $router->get('/properties/{id}/invoice/{reportId}', 'PropertyController@invoice')->name('properties.invoice');
    $router->get('/properties/{id}/report/{reportId}', 'PropertyController@report')->name('properties.report');
    $router->get('/properties/{id}/customReport', 'PropertyController@customReport')->name('properties.customreport');

    $router->get('/properties/{id}/signedContract', 'PropertyController@signedContract')->name('properties.contract');
    $router->get('/properties/{id}/inventory', 'PropertyController@inventory')->name('properties.inventory');
    $router->get('/properties/{id}/annexe', 'PropertyController@annexe')->name('properties.annexe');
    $router->get('/properties/{id}/assessment', 'PropertyController@assessment')->name('properties.assessment');
    $router->get('/properties/{id}/keysfile', 'PropertyController@keysfile')->name('properties.keysfile');
    $router->get('/properties/{id}/ranking', 'PropertyController@ranking')->name('properties.ranking');
    $router->get('/properties/{id}/attestloc', 'PropertyController@attestLoc')->name('properties.attestloc');
    //$router->get('/properties/{id}/sendMail', 'PropertyController@sendContractMail')->name('properties.sendmail');
    $router->post('/properties/{id}/createKeyDoc', 'PropertyController@createKeyDoc')->name('properties.createkeydoc');


    $router->get('/managers/all', 'CityManagerController@editAll')->name('managers.all')->name('managers.editall');
    $router->post('/managers/all', 'CityManagerController@editAll')->name('managers.all')->name('managers.editall');

    $router->get('/resetDeposits', 'DepositsController@resetAll')->name('deposits.resetall');
    $router->post('/deposits/createsync', 'DepositsController@createSync')->name('deposits.createsync');
    $router->get('/deposits/{id}/synchronize', 'DepositsController@synchronize')->name('deposits.synchronize');
    $router->get('/deposits/{id}/block', 'DepositsController@toggleBlock')->name('deposits.toggleblock');
    $router->get('/deposits/{id}/archive', 'DepositsController@archive')->name('deposits.archive');
    $router->get('/deposits/{id}/send', 'DepositsController@sendRequestMail')->name('deposits.sendrequestmail');
    $router->get('/deposits/{id}/jsonMail', 'DepositsController@jsonMail')->name('deposits.jsonmail');

    $router->get('/bookings/synchronizeProperty/{propertyId}/', 'BookingsController@synchronizeProperty')->name('bookings.syncproperty');
    $router->post('/bookings/createsync', 'BookingsController@createSync')->name('bookings.createsync');
    $router->get('/bookings/syncGlobal', 'BookingsController@syncUnsynced')->name('bookings.syncGlobal');
    $router->get('/bookings/{id}/sync', 'BookingsController@syncAjax')->name('bookings.syncajax');
    $router->get('/bookings/{id}/syncTax', 'BookingsController@syncTax')->name('bookings.synctax');
    $router->get('/bookings/{id}/synchronize', 'BookingsController@synchronize')->name('bookings.sync');
    $router->get('/bookings/{id}/precheckin', 'BookingsController@precheckin')->name('bookings.precheckin');
    $router->get('/bookings/{id}/precheckout', 'BookingsController@precheckout')->name('bookings.precheckout');
    $router->get('/bookings/incoming', 'BookingsController@incoming')->name('bookings.sync');


    $router->get('/cminvoices/{id}/synchronize', 'CmInvoicesController@synchronize')->name('cminvoices.sync');
    $router->get('/cminvoices/{id}/download', 'CmInvoicesController@download')->name('cminvoices.download');
    $router->get('/cminvoices/{id}/details', 'CmInvoicesController@details')->name('cminvoices.details');
    

    
    $router->get('/invitations/{id}/send', 'InvitationController@send')->name('invitations.send');
    
    
    $router->get('/mailer', 'MailerController@index')->name('mailer.index');
    $router->post('/mailer/send', 'MailerController@send')->name('mailer.send');
    
    $router->post('/import/put', 'ImportController@put')->name('import.put');
    $router->post('/import/addUser', 'ImportController@addUser')->name('import.adduser');
    
    
    $router->get('/reports/export/invoice', 'ReportController@getInvoice')->name('reports.invoice');
    $router->get('/reports/export/allinvoices', 'ReportController@getAllInvoices')->name('reports.allinvoices');
    $router->get('/reports/export/report', 'ReportController@getReport')->name('reports.getreport');
    $router->get('/reports/export/allreports', 'ReportController@getAllReports')->name('reports.allreports');
    $router->get('/reports/export', 'ReportController@export')->name('reports.export');
    $router->get('/reports/adar', 'ReportController@adar')->name('reports.adar');
    $router->get('/reports/global', 'ReportController@globalReport')->name('reports.globalreport');
    $router->get('/reports/tax', 'ReportController@taxReports')->name('reports.taxreports');
    //$router->get('/reports/diffReport', 'ReportController@diffReport')->name('reports.');
    //$router->get('/reports/syncReport', 'ReportController@syncReport')->name('reports.');
    //$router->get('/reports/syncADAR', 'ReportController@syncADAR')->name('reports.');

    $router->get('/guests/{id}/identfile', 'BookingGuestsController@identFile')->name('guests.identfile');
    $router->get('/guests/{id}/login', '\App\Admin\Controllers\BookingGuestsController@logMeAsUser')->name('clients.logmein');

    $router->resources([
        'cron-task-events' => CronTaskEventController::class,
        'properties' => PropertyController::class,
        'keynest' => KeynestController::class,
        'clients' => UserController::class,
        'guests' => BookingGuestsController::class,
        'managers' => CityManagerController::class,
        'deposits' => DepositsController::class,
        'bookings' => BookingsController::class,
        'cminvoices' => CmInvoicesController::class,
        'invoices' => InvoicesController::class,
        'daily' => DailyBookingsController::class,
        'invitations' => InvitationController::class,
        'import' => ImportController::class,
        'reports' => ReportController::class,
    ]);

    $router->get('/cron-launcher', 'CronLauncherController@index')->name('cron.launcher');
    $router->get('/cron-launcher/launch/{cron}', 'CronLauncherController@launch')->name('cron.launcher.launch');
});

Admin::registerAuthRoutes();