<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class Booking extends Model
{

    public $timestamps = false;

    const STATUS_BOOKED = "Booked";
    const STATUS_CANCELED = "Canceled";
    const STATUS_UNAVAILABLE = "Unavailable";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_at',
        'start_at',
        'end_at',
        'reference',
        'guest',
        'source',
        'property_id',
        'amount', 
        'paid_amount',
        'paid',
        'ota',
        'tax',
        'under_adar',
        'cleaning',
        'profit',
        'owner',
        'bookingsync_id',
        'comment',
        'status',
        'balance_due_at',
        'downpayment',
        'need_sync',
        'keynest_collection',
        'prechekin_sent',
        'prechekout_sent',
    ];
    
    static public $directFills = [
        'id',
        'created_at',
        'start_at',
        'end_at',
        'updated_at',
        'reference',
        'status',
        'source',
        'final_price',
        'downpayment',
        'paid_amount',
    ];
    
    public function nights(){
        return (new Carbon($this->start_at))->hour(0)->minute(0)->diffInDays((new Carbon($this->end_at))->hour(0)->minute(0));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function deposit(){
        return $this->hasOne(Deposit::class, 'booking_id', 'bookingsync_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bookingguest(){
        return $this->hasOne(BookingGuest::class);
    }

    public function getFileFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "bookings" . DIRECTORY_SEPARATOR . $this->reference . DIRECTORY_SEPARATOR;
        if(!is_dir($path)){
            mkdir($path);
        }
        return $path;
    }

    public function getUploadFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "bookings" . DIRECTORY_SEPARATOR . $this->reference . DIRECTORY_SEPARATOR;
        $directory = $this->reference;
        if(!is_dir($path)){
            mkdir($path);
        }
        return $directory;
    }

    public function getIdentFileName($ext){
        return "ident_" . $this->bookingsync_id . '.' . $ext;
    }

}
