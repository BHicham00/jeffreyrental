<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class BookingGuest extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $timestamps = false;
    
    protected $fillable = [
        'booking_id',
        'firstname',
        'lastname',
        'email',
        'phone',
        'transport_type',
        'transport_number',
        'arrival',
        'ident_type',
        'ident_number',
        'ident_expiration',
        'ident_file',
        'country_id',
        'checkin',
        'checkout',
        'one_time_pass',
        'card_type',
        'card_holder',
        'card_last_digits',
        'card_expiration',
        'verified'
    ];

    protected $hidden = [
        'one_time_pass'
    ];

    public static function getCardTypes(){
        return [
            __("VISA"),
            __("MASTERCARD"),
            __("AMEX"),
            __("AUTRES"),
        ];
    }
    public static function getIdentTypes(){
        return [
            __("Carte d'identité"),
            __("Passeport"),
        ];
    }

    public static function getTransportTypes(){
        return [
            __("Voiture"),
            __("Train"),
            __("Avion"),
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking(){
        return $this->belongsTo(Booking::class);
    }

    public function getAuthPassword()
    {
        return $this->one_time_pass;
    }

    public function getIdentType(){
        return self::getIdentTypes()[$this->ident_type];
    }

    public function getTransportType(){
        return self::getTransportTypes()[$this->transport_type];
    }

    public function uploadFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "bookings" . DIRECTORY_SEPARATOR;
        return $path;
    }

    public function getIdentFile(){
        return $this->uploadFolder() . $this->ident_file;
    }
}
