<?php

namespace App\BookingSync;

use App\Deposit;
use App\Services\KeynestService;
use Illuminate\Support\Facades\Session;
use League\OAuth2\Client\Exception\IDPException;
use stdClass;
use League\OAuth2\Client\Token\AccessToken;
use Bookingsync\OAuth2\Client\Provider\Bookingsync as BookingSyncOAuth;
use App\Booking;
use App\Property;
use App\PropertyAmenity;
use Carbon\Carbon;
use PhpParser\Node\Stmt\TryCatch;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BookingSync
 *
 * @author Administrateur
 */
class BookingSync
{

    protected $clientID = '';
    protected $clientSecret = '';
    protected $URL = 'https://www.bookingsync.com/api/v3/';
    protected $oauthProvider;
    protected $token;
    protected $user;

    function __construct($clientID, $clientSecret, $redirectURI, $clientCredentials = false)
    {
        session_start();
        $this->clientID = $clientID;
        $this->clientSecret = $clientSecret;
        $params = [
            'clientId' => $this->clientID,
            'clientSecret' => $this->clientSecret,
        ];
        if ($clientCredentials) {
            $params['grant_type'] = 'client_credentials';
            
        } else {
            $params['redirectUri'] = $redirectURI;
            $params['scopes'] = [
                'public',
                'bookings_read',
                'bookings_write_owned',
                'bookings_write',
                'clients_read',
                'inquiries_read',
                'payments_read',
                'rates_read',
                'rentals_read',
                'rentals_write'
            ];
        }

        $this->oauthProvider = new BookingSyncOAuth($params);
        if ($clientCredentials) {
            $this->token = $this->oauthProvider->getAccessToken('client_credentials', $params);
            // dd($this->token);
        } 
        else
        {
            if(!array_key_exists("bsync_token", $_SESSION) || !array_key_exists("bsync_expire", $_SESSION) || $_SESSION["bsync_expire"] <= time())
            {
                if(!isset($_GET['code']))
                {

                    // If we don't have an authorization code then get one
                    try {
                        $authUrl = $this->oauthProvider->getAuthorizationUrl();
                    }catch(IDPException $e){
                        admin_warning('Too many request Bookingsync');
                    }
                    $_SESSION['oauth2state'] = $this->oauthProvider->state;
                    header('Location: ' . $authUrl);
                    exit;
                    // Check given state against previously stored one to mitigate CSRF attack
                } elseif(empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state']))
                {
                    unset($_SESSION['oauth2state']);
                    exit('Invalid state');
                } else {
                    try{
                        // Try to get an access token (using the authorization code grant)
                        $this->token = $this->oauthProvider->getAccessToken('authorization_code', [
                            'code' => $_GET['code']
                        ]);
                        $_SESSION["bsync_token"] = $this->token->accessToken;
                        $_SESSION["bsync_expire"] = $this->token->expires;
                        $_SESSION["bsync_refresh"] = $this->token->refreshToken;
                    }catch(IDPException $e){
                        admin_warning('Too many request Bookingsync');
                    }
                }
            } else {

                $this->token = new AccessToken([
                    "access_token" => $_SESSION["bsync_token"]
                ]);
                $this->token->expires = $_SESSION["bsync_expire"];
                $this->token->refreshToken = !empty($_SESSION["bsync_refresh"]) ? $_SESSION["bsync_refresh"] : "";
            }
            // dd($this->token);
        }
    }

    public function destroyToken()
    {
        session_destroy();
    }

    public function getApiData($query)
    {
        $request = new RestRequest('https://www.bookingsync.com/api/v3/' . $query, 'GET');
        $request->setAccessToken($this->token->accessToken);
        $request->execute();
        return $request->getDecodedResponse();
    }

    public function updateBooking($id, $data)
    {
        $request = new RestRequest('https://www.bookingsync.com/api/v3/bookings/' . $id, 'PUT', ["bookings" => [
            $data
        ]]);
        $request->setAccessToken($this->token->accessToken);
        $request->execute();
        return $request->getDecodedResponse();
    }


    public function calculateTouristTax($row, $nights, $source = null)
    {
        $touristTaxTotal = 0;
        $touristTax = 0;
        $cleaningFees = 0;
        $rental = $this->getApiData('rentals/' . $row["links"]["rental"]);
        if ($rental && empty($rental["rentals"])) {
            return false;
        } else if (!$rental) {
            return "erreur - connection failed";
        }
        $rental = $rental["rentals"][0];
        $city = $rental["city"];
        if (!empty($rental["links"]["rentals_fees"])) {
            $fees = $this->getApiData('rentals_fees?id=' . implode(",", $rental["links"]["rentals_fees"]));
            if (!$fees || !array_key_exists("rentals_fees", $fees)) {
                return 'erreur - no rentals fees 2';
            }
            foreach ($fees["rentals_fees"] as $fee) {
                $finalfees[] = $fee["links"]["fee"];
            }
        } else {
            /*return 'erreur - no rentals fees ' . $row["id"] . " - " . valdebug($rental["links"]["rentals_fees"]);*/
            //valdebug($row);
        }
        if (!empty($finalfees)) {
            $fees = $this->getApiData('fees?id=' . implode(",", $finalfees));

            if (!$fees || !array_key_exists("fees", $fees)) {
                return "erreur - no fees data";
            }
            $fees = $fees["fees"];
            $expedia =  (strpos($source, 'expedia') !== false || $source == 'alper@hemenkiralik.com');
            foreach ($fees as $fee) {
                if ($expedia && strpos(strtolower($fee["kind"]), "cleaning") !== false) {
                    $cleaningFees = (float)$fee["rate"];
                }

                if (strpos(strtolower($fee["kind"]), "city_tax") !== false) {

                    $adults = (int)$row["adults"];
                    $children = (int)$row["children"];
                    $occupants = $adults + $children;
                    if (strpos(strtolower($fee["rate_kind"]), "percentage") !== false) {
                        $noTVA = (float)$row["final_rental_price"] / 1.2;
                        if (!$occupants) $occupants = 1;
                        $nightRate = ($noTVA / $nights) / $occupants;

                        $touristTax = ($nightRate / 100) * (float)$fee["rate"];
                        $touristTax = (round($touristTax * 100) / 100);
                        if (!empty($fee["maximum_possible_rate"]) && $touristTax > (float)$fee["maximum_possible_rate"]) {
                            $touristTax = (float)$fee["maximum_possible_rate"];
                        }
                    } else if (strpos(strtolower($fee["rate_kind"]), "fixed") !== false) {
                        $touristTax = (float)$fee["rate"];

                    }
                    $touristTaxTotal = (round($touristTax * $adults * $nights * 100) / 100);
                }
            }
        }


        return [$city, $touristTax, $touristTaxTotal, $cleaningFees];
    }
    public function addRental($data,$id){
        $request = new RestRequest('https://www.bookingsync.com/api/v3/rentals',
        'POST',["rentals" => [
            $data
        ]]);
        $request->setAccessToken($this->token->accessToken);
        $request->execute();
        return $request->getDecodedResponse();
    }
    public function updateRental($data,$bookingSyncId){
        $request = new RestRequest('https://www.bookingsync.com/api/v3/rentals/'.$bookingSyncId,
        'PUT',["rentals" => [
            $data
        ]]);
        // dd($request);
        $request->setAccessToken($this->token->accessToken);
        $request->execute();
        return $request->getDecodedResponse();
    }
    public function registerBooking($row, $needSync = false)
    {
        $booking = array();
        $booking["start_at"] = new Carbon($row["start_at"]);
        $booking["end_at"] = new Carbon($row["end_at"]);
        $booking["balance_due_at"] = new Carbon($row["balance_due_at"]);
        $register = Booking::where("reference", "=", $row["reference"])->first();
        $property = Property::where("bookingsync_id", "=", $row["links"]["rental"])->first();

        $rentalPrice = $register != null ? $register->amount : (float)$row["final_rental_price"];
        $booking["final_price"] = $register != null ? $register->final_price : 0;
        $booking["source"] = $register != null ? $register->source : "";
        $booking["commission"] = $register != null ? $register->ota : 0;
        $booking["touristTaxTotal"] = $register != null ? $register->tax : 0;
        $booking["jeffrey"] = $register != null ? $register->profit : 0;
        $booking["cleaningFees"] = $register != null ? $register->cleaningFees : 0;
        $booking["client_final_price"] = $register != null ? $register->owner : 0;
        $booking["client"] = $register != null ? $register->guest : 0;
        $booking["comment"] = $register != null ? $register->comment : 0;;
        $expedia = false;
        if ($property) {
            $caution = (float) $row["damage_deposit"];
            $booking["nights"] = $booking["start_at"]->hour(0)->minute(0)->diffInDays($booking["end_at"]->hour(0)->minute(0));
            if ($row["status"] != Booking::STATUS_UNAVAILABLE) {
                $booking["commission"] = floor((float)$row["commission"] * 100) / 100;
                if (!$needSync) {
                    $touristTax = 0;
                    $touristTaxTotal = 0;
                    $cleaningFees = 0;
                    $source = null;
                    if (!empty($row["links"]["source"])) {
                        $fee = $this->getApiData('sources/' . $row["links"]["source"]);
                        $source = $booking["source"] = $fee["sources"][0]["name"];
                    }
                    if (!empty($row["links"]["rental"])) {
                        list ($city, $touristTax, $touristTaxTotal, $cleaningFees) = $this->calculateTouristTax($row, $booking["nights"], strtolower($booking["source"]));
                    }

                    if (!empty($row["links"]["bookings_fees"])) {
                        $fees = $this->getApiData('bookings_fees?id=' . implode(",", $row["links"]["bookings_fees"]));
                        if (!$fees || !array_key_exists("bookings_fees", $fees)) {
                            return "erreur - no booking fees data";
                            //valdebug($row);
                        }
                        foreach ($fees["bookings_fees"] as $fee) {
                            //valdebug($fee);
                            foreach ($fee["name"] as $name) {
                                if (empty($booking["commission"]) && strpos(strtolower($name), "comission") !== false) {
                                    $booking["commission"] = (float)$fee["price"];
                                }
                                if (strpos(strtolower($name), "frais de") !== false || strpos(strtolower($name), "clean") !== false) {
                                    $cleaningFees = (float)$fee["price"];
                                }
                            }
                        }
                    }
                    $booking["comment"] = "";
                    if (!empty($row["links"]["booking_comments"])) {
                        foreach ($row["links"]["booking_comments"] as $cId) {
                            $comments = $this->getApiData('booking_comments/' . $cId);
                            if (!$comments || !array_key_exists("booking_comments", $comments)) {
                                return "erreur - no comment data";
                                //valdebug($row);
                            }
                            $booking["comment"] .= $comments["booking_comments"][0]["content"];
                        }
                    }

                    if ($cleaningFees != 0)
                        $booking["cleaningFees"] = $cleaningFees;
                    if ($touristTax != 0) {
                        $booking["touristTax"] = $touristTax;
                        $booking["touristTaxTotal"] = $touristTaxTotal;
                    }

                    $client = $this->getApiData('clients/' . $row["links"]["client"]);
                    $client = $client['clients'][0];
                    $booking["client"] = json_encode([
                        "id" => $client["id"],
                        "firstname" => $client["firstname"],
                        "lastname" => $client["lastname"],
                        "email" => (isset($client["emails"]) && count($client["emails"])) ? $client["emails"][0]["email"] : "",
                        "phone" => (isset($client["phones"]) && count($client["phones"])) ? $client["phones"][0]["number"] : "",
                        "lang" => $client["preferred_locale"],
                    ]);

                    $rentalPrice = (float)$row["final_rental_price"];
                    switch (strtolower($booking['source'])) {
                        case 'homeaway':
                        case 'abritel':
                            $booking["commission"] = floor($rentalPrice * 5) / 100;
                            break;
                        case strpos(strtolower($booking['source']), 'expedia') === false ? false : true :
                        case 'expedia@rentalsunited.com':
                        case 'alper@hemenkiralik.com':
                            if($booking['source'] !== 'expedia@rentalsunited.com' && $booking['source'] !== 'alper@hemenkiralik.com') {
                                $expedia = true;
                                $rentalPrice = $rentalPrice - $cleaningFees - $touristTaxTotal;
                            }
                            $booking["commission"] = floor($rentalPrice * 17) / 100;
                            break;
                        case 'airbnb':
                            $matches = [];
                            preg_match("/Rental price\: €(.*)\n/", $booking["comment"], $matches);
                            if (!empty($matches)) {
                                $rentalPrice = (float)str_replace(",", "", $matches[1]);
                            }
                            preg_match("/Cleaning fee\: €(.*)\n/", $booking["comment"], $matches);
                            if (!empty($matches)) {
                                $booking["cleaningFees"] = (float)$matches[1];
                            }
                            //$rentalPrice = $rentalPrice - $booking["cleaningFees"];
                            break;
                    }
                }
                $booking["final_price"] = $rentalPrice;
                $booking["jeffrey"] = floor($booking["final_price"] * ($property->commission / 100) * 100) / 100;


                // contract lite
                $liteMin = floor(200 * ($property->commission / 100) * 100) / 100;
                if ($property->lite && $booking["jeffrey"] < $liteMin) {
                    $booking["jeffrey"] = $liteMin;
                }
                if ($property->bnbkeys && $booking["jeffrey"] < 40) {
                    $booking["jeffrey"] = 40;
                }

                // contract lite
                $booking["client_final_price"] = $booking["final_price"] - $booking["commission"] - $booking["jeffrey"];
                if ((strtolower($booking['source']) == "booking.com" || strtolower($booking['source']) == "airbnb") && $booking["final_price"] > 0) {
                    $bookingOTA = $booking["commission"] - ($booking["cleaningFees"] / ($booking["final_price"] + $booking["cleaningFees"])) * $booking["commission"];
                    $booking["client_final_price"] = $booking["final_price"] - $bookingOTA - $booking["jeffrey"];
                }

            }

            $paid = $row["paid_amount"] >= $row["final_price"];
            if($expedia && !$paid){
                $paid = $row["authorized_amount"] >= $row["final_price"];
                $row["paid_amount"] = $row["authorized_amount"];
            }

            if (!$register) {

                $register = Booking::create([
                    "property_id" => $property->id,
                    "created_at" => new Carbon($row["created_at"]),
                    "start_at" => $booking["start_at"],
                    "end_at" => $booking["end_at"],
                    "reference" => $row["reference"],
                    "source" => $booking["source"],
                    "amount" => $rentalPrice,
                    "paid_amount" => (float)$row["paid_amount"],
                    "under_adar" => $booking["final_price"],
                    "ota" => $booking["commission"],
                    "tax" => $booking["touristTaxTotal"],
                    "profit" => $booking["jeffrey"],
                    "cleaning" => $booking["cleaningFees"],
                    "owner" => $booking["client_final_price"],
                    "guest" => $booking["client"],
                    "paid" => $paid,
                    "bookingsync_id" => $row["id"],
                    "comment" => $booking["comment"],
                    "status" => $row["status"],
                    "balance_due_at" => $booking["balance_due_at"],
                    "need_sync" => $needSync
                ]);
            } else {
                $register->fill([
                    "amount" => $rentalPrice,
                    "source" => $booking["source"],
                    "paid_amount" => (float)$row["paid_amount"],
                    "start_at" => $booking["start_at"],
                    "end_at" => $booking["end_at"],
                    "under_adar" => $booking["final_price"],
                    "ota" => $booking["commission"],
                    "tax" => $booking["touristTaxTotal"],
                    "profit" => $booking["jeffrey"],
                    "cleaning" => $booking["cleaningFees"],
                    "owner" => $booking["client_final_price"],
                    "bookingsync_id" => $row["id"],
                    "comment" => $booking["comment"],
                    "guest" => $booking["client"],
                    "paid" => $paid,
                    "status" => $row["status"],
                    "balance_due_at" => $booking["balance_due_at"],
                    "need_sync" => $needSync
                ]);
                $register->save();
            }
            if($property->keynest_key_id && $property->keynest_auto){
                if(!$register->keynest_collection && $register->paid && $register->status == Booking::STATUS_BOOKED){
                    $keynest = new KeynestService();
                    $response = $keynest->createCollectionCode($property, $register->start_at);
                    if(isset($response->ResponsePacket)) {
                        $register->keynest_collection = $response->ResponsePacket->CollectionCode;
                        $register->save();
                    }
                }
            }

            if(!$needSync && $row["canceled_at"] === NULL && $caution > 0 && $source && strtolower($source) !== 'airbnb')
            {
                $depositDB = Deposit::where("booking_id", "=", $row["id"])->first();
                $deposit = [];
                if(!$depositDB){
                    $deposit["booking_id"] = $row["id"];
                    $booking["paid"] = false;
                }else{
                    $deposit["booking_id"] = $depositDB->booking_id;
                }
                $deposit["property_id"] = $property->id;
                $deposit["client_id"] = $row["links"]["client"];
                $deposit["client"] = $booking["client"];
                $deposit["city"] = $city;
                $deposit["payment_url"] = $row["payment_url"] ?: "";
                $deposit["reference"] = $row["reference"];
                $deposit["amount"] = $caution;
                $deposit["start_at"] = new Carbon($row["start_at"]);
                $deposit["end_at"] = new Carbon($row["end_at"]);
                $deposit["token"] = bcrypt($deposit["booking_id"] . $client["firstname"] . $client["lastname"] . $deposit["amount"]);
                if(!$depositDB)
                {
                    $depositDB = Deposit::create($deposit);
                } else
                {
                    $depositDB->fill($deposit);
                }
                $depositDB->save();
            }

            if ($row["canceled_at"] !== NULL) {
                $deposit = Deposit::where('booking_id', $row["id"])->first();
                if ($deposit)
                    $deposit->delete();
            }
        } else {
            return 'no related property';
        }
        return false;
    }

}
