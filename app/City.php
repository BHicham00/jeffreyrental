<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class City extends Model
{
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_commune'
    ];
    
    public function getPostal(){
        $postal = $this->code_postal;
        while(strlen($postal) < 5){
            $postal = "0".$postal;
        }
        return $postal;
    }
}
