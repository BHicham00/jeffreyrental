<?php

namespace App;

use App\Admin\Extensions\Auth\Database\Administrator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpWord\TemplateProcessor;

class CmInvoice extends Model
{
    //public $timestamps = false;

    protected $fillable = [
        'admin_user_id',
        'label',
        'static_fee',
        'jeffrey',
        'commission',
        'cleaning',
        'total_ttc',
        'paid',
        'include_cleanings',
        'start_at',
        'end_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city_manager()
    {
        return $this->belongsTo(Administrator::class, 'admin_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function missions()
    {
        return $this->hasManyThrough(InvoiceMission::class, CmInvoiceHasMissions::class, 'cm_invoice_id', 'id', 'id', 'invoice_mission_id')->select('invoice_missions.*');
    }

    public function debours()
    {
        return $this->hasMany(InvoiceDebour::class, 'invoice_id');
    }

    /**
     * @throws \Exception
     */
    public function calculate(){
        $this->total_ttc = 0;
        $this->commission = 0;
        $this->jeffrey = 0;
        $tva = !empty($this->city_manager->infos->first()->tva);
        $cleaning = 0;
        foreach($this->city_manager->properties as $p){
            $com = $p->ahp->commission;
            $bookings = $p->bookings()
                ->where('paid',1)
                ->where('status', 'Booked')
                ->where('end_at', '<=', $this->end_at)
                ->where('end_at', '>=', $this->start_at)->get();
            foreach($bookings as $b){
                $this->jeffrey += ($b->profit / 1.2);
                $this->commission += ($b->profit / 1.2) * ($com / 100);
                if($this->include_cleanings){
                    $cleaning += ($b->cleaning)*0.83;
                }
            }
        }
        $missions_total = 0;
        foreach($this->missions as $m){
            $missions_total += $m->getTotal();
        }
        $debours_total = 0;
        foreach($this->debours as $d){
            $debours_total += $d->value;
        }

        $this->jeffrey = round($this->jeffrey, 2);
        $this->commission = round($this->commission, 2);
        $this->cleaning = round($cleaning,2);
        $this->total_ttc = round(($this->commission + $missions_total + $this->static_fee + $debours_total + $cleaning) * ($tva ? 1.2 : 1),2);
        $this->label = $this->city_manager->name . " - " . (new Carbon($this->start_at))->format("m/Y");


        $this->save();
    }

    /**
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generateFile(){
        setlocale(LC_TIME, 'fr');
        Carbon::setLocale('fr');
        $start_at = (new Carbon($this->start_at));
        $invoice = new TemplateProcessor(__DIR__ . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "facture_city.docx");
        $info = $this->city_manager->info;
        $invoice->setValue('name', empty($info->company) ? ($info->firstname . ' ' . $info->lastname) : $info->company);
        $invoice->setValue('address', $info->address);
        $invoice->setValue('city', $info->city ? $info->city->getPostal() . " " . $info->city->nom_commune : 'Ville non renseignée');
        $invoice->setValue('country', $info->country  ? $info->country->nicename : 'Pays non renseigné');
        $invoice->setValue('tel', $info->phone);
        $invoice->setValue('siret', $info->siret);
        $invoice->setValue('tva', $info->tva);
        $initials = $this->city_manager->info->firstname[0].$this->city_manager->info->lastname[0];
        $invoice_num = $start_at->format("Y-m").'-'.$initials.$this->admin_user_id;
        $invoice->setValue('invoice_num', $invoice_num);
        $invoice->setValue('date', (new Carbon($this->updated_at))->format("d/m/Y"));
        $nb_properties = count($this->city_manager->properties);
        $invoice->setValue('city_manager_name', $this->city_manager->name);
        $invoice->setValue('nb_properties', $nb_properties);
        $invoice->setValue('static_fee', $this->static_fee);
        $invoice->setValue('mois', $start_at->formatLocalized('%B %Y'));
        $invoice->setValue('ca_jeffrey', $this->jeffrey);
        $invoice->setValue('ca_city', $this->commission);

        $i = 1;
        $missions_total = 0;
        $missionlines = count($this->missions);

        $missions = $this->missions;
        $invoice->setValue('cleaning_fees', $this->cleaning);
        $invoice->cloneRow('mission_property', $missionlines);

        foreach($missions as $m){
            $invoice->setValue('mission_property#' . $i, $m->property->name);
            $invoice->setValue('mission_date#' . $i, (new Carbon($m->date))->format("d/m/Y"));
            $invoice->setValue('mission_chekin#' . $i, $m->checkin);
            $invoice->setValue('mission_chekout#' . $i, $m->checkout);
            $invoice->setValue('mission_cleaning#' . $i, $m->cleaning);
            $invoice->setValue('mission_debours#' . $i, $m->debours);
            $invoice->setValue('mission_total#' . $i, $m->getTotal());
            $missions_total += $m->getTotal();
            $i++;
        }
        $invoice->setValue('missions_total', $missions_total);

        $debours_total = 0;
        $invoice->cloneRow('debours_date', count($this->debours));
        $i = 1;
        foreach($this->debours as $d){
            $invoice->setValue('debours_date#' . $i, (new Carbon($d->date))->format("d/m/Y"));
            $invoice->setValue('debours_label#' . $i, $d->label);
            $invoice->setValue('debours_value#' . $i, $d->value);
            $debours_total += $d->value;
            $i++;
        }
        $invoice->setValue('total_debours', $debours_total);

        $subtotal = $missions_total + $this->commission + $this->static_fee + $debours_total + $this->cleaning;
        $invoice->setValue('sub_total', $subtotal);
        $invoice->setValue('tva_total', empty($info->tva) ? 0 : round($subtotal*0.2, 2));
        $invoice->setValue('total_ttc', $this->total_ttc . " €");
        $invoice->setValue('iban', $info->iban);
        $invoice->setValue('bic', $info->bic);
        $invoice->setValue('no_tva', empty($info->tva) ? "TVA non applicable, art. 293B du CGI" : "");

        $filename = $invoice_num;
        $path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $filename .'.docx';
        $pathPdf = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $filename .'.pdf';
        $invoice->saveAs($path);
        convertToPdf($path, sys_get_temp_dir());
        attachFile($pathPdf, $filename .'.pdf');
        unlink($pathPdf);
    }

    public function getDetailsFile(){
        $file = tempnam(sys_get_temp_dir(), 'export_');
        $handle = fopen($file, 'w+');
        $fields = ["Propriété", "booking_id", "dates de séjour", "CA jeffrey (HT)", "Commission CM (HT)", "Frais de ménage reversé"];
        fputs($handle, utf8_decode(join(',', $fields)."\n"));
        foreach($this->city_manager->properties as $p){
            $com = $p->ahp->commission;
            $bookings = $p->bookings()
                ->where('paid',1)
                ->where('status', 'Booked')
                ->orderBy('end_at')
                ->where('end_at', '<=', $this->end_at)
                ->where('end_at', '>=', $this->start_at)->get();
            foreach($bookings as $b){
                $jeffrey = round(($b->profit / 1.2),2);
                $commission = round(($b->profit / 1.2) * ($com / 100),2);
                $cleaning = 0;
                if($this->include_cleanings){
                    $cleaning = round(($b->cleaning)*0.83,2);
                }
                $dates = 'du ' . (new Carbon($b->start_at))->format('d/m/Y') . ' au ' . (new Carbon($b->end_at))->format('d/m/Y');
                $fields = [$b->property->name, $b->bookingsync_id, $dates, $jeffrey, $commission, $cleaning];
                fputs($handle, utf8_decode(join(',', $fields)."\n"));
            }
        }
        $initials = $this->city_manager->info->firstname[0].$this->city_manager->info->lastname[0];
        $start_at = new Carbon($this->start_at);
        $filename = $start_at->format("Y-m").'-'.$initials.$this->admin_user_id . '.csv';
        fclose($handle);
        attachFile($file, $filename);
    }
}
