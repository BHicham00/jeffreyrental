<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmInvoiceHasMissions extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'cm_invoice_id',
        'invoice_mission_id',
    ];
}
