<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Property;
use App\City;

class FillPropertyLocation extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locations:property';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'refill property locations fields';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $properties = Property::all();
        //$url = 'https://datanova.legroupe.laposte.fr/api/records/1.0/search/?dataset=laposte_hexasmal&facet=nom_de_la_commune&refine.code_postal=';
        
        foreach($properties as $p){
            preg_match("/([0-9]{5,5})/", $p->address, $matches);
            preg_match("/[0-9]{5,5} ([A-Z,a-z]+)/", $p->address, $matches2);
            if(count($matches)){
                $postal = $matches[0];
                $cityName = $matches2[1];
                $cityName = str_replace("saint", "st", strtolower($cityName));
                $city = City::where("code_postal", "=", $postal)->where("nom_commune", "LIKE", "$cityName%")->first();
                //$res = json_decode(curlGetContent($url.$matches[0])["content"]);
                if($city){
                    $p->city_id = $city->id;
                    $p->country = "FRANCE";
                    $p->save();
                    echo "FOUND --- $p->address --- $city->nom_commune\n";
                }else {
                    $city = City::where("code_postal", "=", $postal)->first();
                    if($city){
                        $p->city_id = $city->id;
                        $p->country = "FRANCE";
                        $p->save();
                        echo "FOUND --- $p->address --- $city->nom_commune\n";
                    }else{
                        
                        echo "ERREUR --- $p->address --- $p->name\n";
                        echo "address $postal --- $cityName\n";
                    }
                }
            }else{
                echo "ERREUR --- $p->address --- $p->name\n";
                echo "address $postal --- $cityName\n";
            }
        }
    }

}
