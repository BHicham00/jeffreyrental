<?php

namespace App\Console\Commands;

use App\Booking;
use App\BookingSync\BookingSync;
use App\KeynestStore;
use App\Services\KeynestService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Property;
use App\Helpers\Mailer;

class GetKeynestStores extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'keynest:stores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'retrieves keynest stores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $keynest = new KeynestService();
        $stores = $keynest->listStores()->ResponsePacket->StoreList;
        foreach($stores as $store){

            /** @var KeynestStore $kstore **/
            $kstore = KeynestStore::find($store->StoreId);
            if(!$kstore) {
                $kstore = KeynestStore::firstOrCreate([
                    'id' => $store->StoreId,
                    'name' => $store->StoreName,
                    'lon' => $store->Longitude,
                    'lat' => $store->Latitude
                ]);
            }
            $kstore->fill([
                'name' => $store->StoreName,
                'time' => $store->StoreTime,
                'description' => $store->StoreDescription,
                'address' => $store->StoreStreetAddress,
                'lon' => $store->Longitude,
                'lat' => $store->Latitude,
                'open_details' => $store->StoreOpeningTimingsDetails,
                'close_details' => $store->CloseDates,
            ]);
            $kstore->save();
            dump($kstore->id);
        }
    }

}
