<?php

namespace App\Console\Commands;

use App\Booking;
use App\BookingSync\BookingSync;
use App\Services\KeynestService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Property;
use App\Helpers\Mailer;

class LinkPropertiesWithKeys extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'keynest:keys';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'relink properties and keynest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $keynest = new KeynestService();
        $keys = $keynest->listAllKeys()->ResponsePacket->KeyList;
        \DB::table('properties')->update([
            'keynest_key_id' => null,
            //'keynest_store_id' => null,
        ]);
        foreach($keys as $key){
            if($key->PropertyID){
                $property = Property::where('bookingsync_id', $key->PropertyID)->first();
                if($property){
                    $property->keynest_key_id = $key->KeyId;
                    dump($property->keynest_key_id);
                    if(isset($key->CurrentOrLastStoreID)){
                        $property->keynest_store_id = $key->CurrentOrLastStoreID;
                    }
                    $storeId = null;
                    $keyStatus = $keynest->getKey($key->KeyId)->ResponsePacket;
                    try {
                        $storeId = $keyStatus->Store->Id;
                    }catch( \Exception $e){}
                    if($storeId){
                        $property->keynest_store_id = $storeId;
                        dump($storeId);
                    }else{
                        $keyStatus = $keynest->getDropOff($key->KeyId)->ResponsePacket;
                        try {
                            $storeId = $keyStatus->Store->Id;
                        }catch( \Exception $e){}
                        if($storeId) {
                            $property->keynest_store_id = $storeId;
                            dump($storeId);
                        }else{
                            dump($keyStatus);
                        }
                    }
                    $dropoff = $keynest->getDropOff($key->KeyId);
                    $property->keynest_dropoff = $dropoff->ResponsePacket->DropOffCode;
                    $collections = $keynest->getValidCodes($key->KeyId);
                    $codes = collect($collections->ResponsePacket->ValidCodes);
                    $code = $codes->where("IsPermanent", true)->first();
                    if($code){
                        $property->keynest_collection = $code->Code;
                        dump($property->keynest_collection);
                    }else{
                        $code = $keynest->createCollectionCode($property, null, true)->ResponsePacket->CollectionCode;
                        $property->keynest_collection = $code;
                        dump($property->keynest_collection);
                    }
                    $property->save();
                }
            }
        }
    }

}
