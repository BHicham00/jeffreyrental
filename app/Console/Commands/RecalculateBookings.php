<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Booking;

class RecalculateBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recal:bookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate bookings from 01/09/2019';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookings = Booking::where("end_at", ">", "2019-08-31")->where("status", "=", Booking::STATUS_BOOKED)->get();
        $totalDiff = 0;
        foreach($bookings as $book){
            $property = $book->property;
            echo $book->profit . " - " . $book->owner." - " . $book->cleaning . "\n";
            $com = $property->commission;
            $book->profit = round($book->amount * $com) / 100;
            $liteMin = floor(200 * ($property->commission / 100) * 100) / 100;
            if($property->lite && $book->profit < $liteMin)
            {
                $book->profit = $liteMin;
            }
            $book->owner = $book->amount - $book->profit - $book->ota;
            $diff = 0;
            if(strtolower($book->source) == "booking.com" && $book->amount > 0 && $book->cleaning > 0){
                $bookingOTA = round(($book->ota - (($book->cleaning/($book->amount + $book->cleaning)) * $book->ota))*100)/100;
                echo $book->ota . "\n";
                echo $bookingOTA . "\n";
                $recal = $book->amount - $bookingOTA - $book->profit;
                $diff = $book->owner - $recal;
                $book->owner = $recal;
            }
            $totalDiff += $diff;
            echo $book->profit . " - " . $book->owner." - " . $diff . "\n___________________________\n";
            $book->save();
        }
        echo "Total diff = " . $totalDiff;
    }
}
