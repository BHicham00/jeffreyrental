<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Property;

class RevertFilenames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'revert:filenames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'revert files default names';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $properties = Property::all();
        foreach($properties as $p){
            /* @var $p Property */
            echo "reverting property " . $p->name . "\n";
            if($p->hasContract()){
                rename($p->getContractFile(), $p->getOldContractFile());
                $p->contract = $p->getUploadFolder().'/'.$p->getOldContractFileName();
                echo "\t-contract\n";
            }
            if($p->hasInventory()){
                rename($p->getInventoryFile(), $p->getOldInventoryFile());
                $p->inventory = $p->getUploadFolder().'/'.$p->getOldInventoryFileName();
                echo "\t-inventory\n";
            }
            if($p->hasAssess()){
                rename($p->getAssessmentFile(), $p->getOldAssessmentFile());
                $p->assessment = $p->getUploadFolder().'/'.$p->getOldAssessmentFileName();
                echo "\t-assessment\n";
            }
            if($p->hasKeysFile()){
                rename($p->getKeysFile(), $p->getOldKeysFile());
                $p->keys = $p->getUploadFolder().'/'.$p->getOldKeysFileName();
                echo "\t-keys\n";
            }
            echo "\n";
            $p->save();
        }
    }
}
