<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BookingSync\BookingSync;
use App\Deposit;
use App\Property;
use App\Amenity;
use Illuminate\Support\Facades\DB;

class SynchronizeAmenities extends Command
{

    /**
     * The bookingsync api object
     *
     * @var BookingSync
     */
    protected $bookingSync;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:amenities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'synchronize amenities with bookingsync';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'), //'215b85af3abaed3151b4bc05c05db02f84f821966f36c9617a277b8cf3cdbc7a',
                config('bookingsync.secret'), //'c1f3912e16a72b6c4e017abec3a388b712839f72f84d61c20b2d16e87d8e23e8', 
                "", true
        );

        set_time_limit(180);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Amenity::truncate();
        
        $data = $this->bookingSync->getApiData('amenities');
        $pages = (int) $data['meta']['X-Total-Pages'];
        $data = $data['amenities'];
        $count = 0;
        for($i = 1; $i <= $pages; $i++)
        {
            if($i != 1)
            {
                $data = $this->bookingSync->getApiData('amenities?page=' . $i)['amenities'];
            }
            foreach($data as $row)
            {
                $fill = $row;
                unset($fill["created_at"]);
                unset($fill["updated_at"]);
                unset($fill["canceled_at"]);
                $fill['title'] = $row['title']['fr'];
                $amenity = Amenity::firstOrCreate($fill);
                echo $row['title']['fr'] . " \n";
            }
            $count += count($data);
        }
        echo "\n" . $count . " \n";
        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
