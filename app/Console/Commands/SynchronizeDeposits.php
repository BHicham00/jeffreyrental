<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BookingSync\BookingSync;
use App\Deposit;
use App\Property;
use Carbon\Carbon;

class SynchronizeDeposits extends Command
{
    /**
     * The bookingsync api object
     *
     * @var BookingSync
     */
    protected $bookingSync;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:deposits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'synchronize deposits with bookingsync';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'),//'215b85af3abaed3151b4bc05c05db02f84f821966f36c9617a277b8cf3cdbc7a',
                config('bookingsync.secret'),//'c1f3912e16a72b6c4e017abec3a388b712839f72f84d61c20b2d16e87d8e23e8', 
                url('/admin/deposits'), true
        );
        
        set_time_limit(180);
        Carbon::setLocale("fr");
        $now = new Carbon();
        $formatFrom = $now->format("Ymd");
        $now = new Carbon();
        $formatTo = $now->addDay(14)->format("Ymd");
        $deposits = Deposit::all();
        $bookingIds = [];
        $paidBookingIds = [];

        foreach($deposits as $d)
        {
            if($d->paid)
            {
                $paidBookingIds[] = $d->booking_id;
            } else
            {
                $bookingIds[] = $d->booking_id;
            }
        }

        /* $data = $this->bookingSync->getApiData('bookings?id=' . implode(',', $bookingIds));
          $data = $data['bookings'];
          foreach($data as $row)
          {
          $left_to_pay = (float) $row["payment_left_to_collect"];
          foreach($deposits as $d)
          {
          if($row["id"] == $d->booking_id)
          {
          if(count($row["links"]["payments"]) > 0)
          {
          $pay = $this->bookingSync->getApiData('payments/' . $row["links"]["payments"][0]);
          $d->transaction_id = $pay["payments"][0]["transaction_id"];
          $d->order_id = $pay["payments"][0]["order_id"];
          }
          $d->save();
          }
          }
          } */

        $data = $this->bookingSync->getApiData('bookings?status=booked&from=' . $formatFrom . '&until=' . $formatTo);
        $data = $data['bookings'];
        //valdebug($data);die;
        $results = array();
        foreach($data as $row)
        {
            $booking = array();
            $caution = (float) $row["damage_deposit"];
            $left_to_pay = (float) $row["payment_left_to_collect"];
            $rental = $this->bookingSync->getApiData('rentals/' . $row["links"]["rental"])["rentals"];
            //var_dump("");
            if(count($rental) == 0 || (float) $row["paid_amount"] == 0 || $caution == 0 || array_search($row["id"], $bookingIds) !== FALSE || array_search($row["id"], $paidBookingIds) !== FALSE)
            {
                continue;
            }
            echo $row["id"];
            $booking["booking_id"] = $row["id"];
            $property = Property::where("bookingsync_id", "=", $row["links"]["rental"])->first();
            if($property)
            {
                $booking["property_id"] = $property->id;
                $booking["client_id"] = $row["links"]["client"];
                $client = $this->bookingSync->getApiData('clients/' . $row["links"]["client"]);
                $client = $client['clients'][0];
                $booking["client"] = json_encode([
                    "id" => $client["id"],
                    "firstname" => $client["firstname"],
                    "lastname" => $client["lastname"],
                    "email" => count($client["emails"]) ? $client["emails"][0]["email"] : "",
                    "phone" => count($client["phones"]) ? $client["phones"][0]["number"] : "",
                    "lang" => $client["preferred_locale"],
                ]);
                if(count($row["links"]["payments"]) > 0)
                {
                    $pay = $this->bookingSync->getApiData('payments/' . $row["links"]["payments"][0]);
                    $booking["transaction_id"] = $pay["payments"][0]["transaction_id"];
                    $booking["order_id"] = $pay["payments"][0]["order_id"];
                }
                $booking["city"] = $rental[0]["city"];
                $booking["payment_url"] = $row["payment_url"] ?: "";
                $booking["reference"] = $row["reference"];
                $booking["amount"] = $caution;
                $booking["start_at"] = new Carbon($row["start_at"]);
                $booking["end_at"] = new Carbon($row["end_at"]);
                $booking["paid"] = false;
                $booking["token"] = bcrypt($booking["booking_id"] . $client["firstname"] . $client["lastname"] . $booking["amount"]);

                $deposit = Deposit::where("booking_id", "=", $row["id"])->first();
                if(!$deposit)
                {
                    $deposit = Deposit::create($booking);
                } else
                {
                    $deposit->fill($booking);
                }
                $deposit->save();
            }
        }
        
    }
}
