<?php

namespace App\Console\Commands;

use App\Booking;
use App\BookingSync\BookingSync;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Property;
use App\Helpers\Mailer;

class TestHash extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:hash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if contract has been uploaded and send mail if not';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = '{"account_id":11000,"event":"booking_updated","resource":{"links":{"bookings.account":"https://www.bookingsync.com/api/v3/accounts/{bookings.account}","bookings.rental":"https://www.bookingsync.com/api/v3/rentals/{bookings.rental}","bookings.client":"https://www.bookingsync.com/api/v3/clients/{bookings.client}","bookings.rental_agreement":"https://www.bookingsync.com/api/v3/rental_agreements/{bookings.rental_agreement}","bookings.source":"https://www.bookingsync.com/api/v3/sources/{bookings.source}","bookings.inquiry":"https://www.bookingsync.com/api/v3/inquiries/{bookings.inquiry}","bookings.user":"https://www.bookingsync.com/api/v3/users/{bookings.created_by.id}","bookings.booking":"https://www.bookingsync.com/api/v3/bookings/{bookings.created_by.id}","bookings.application":"https://www.bookingsync.com/api/v3/applications/{bookings.created_by.id}","bookings.booking_comments":"https://www.bookingsync.com/api/v3/booking_comments/{bookings.booking_comments}","bookings.bookings_fees":"https://www.bookingsync.com/api/v3/bookings_fees/{bookings.bookings_fees}","bookings.bookings_taxes":"https://www.bookingsync.com/api/v3/bookings_taxes/{bookings.bookings_taxes}","bookings.payments":"https://www.bookingsync.com/api/v3/payments/{bookings.payments}","bookings.bookings_payments":"https://www.bookingsync.com/api/v3/bookings_payments/{bookings.bookings_payments}","bookings.bookings_tags":"https://www.bookingsync.com/api/v3/bookings_tags/{bookings.bookings_tags}","bookings.rentals_tags":"https://www.bookingsync.com/api/v3/rentals_tags/{bookings.rentals_tags}"},"booking":{"links":{"account":11000,"booking_comments":[2029657],"bookings_fees":[1756545,1756546],"bookings_payments":[8562435],"bookings_tags":[],"bookings_taxes":[],"client":3411277,"created_by":{"id":65,"type":"Doorkeeper::Application"},"inquiry":null,"payments":[8562426],"rental":74942,"rental_agreement":60555,"rentals_tags":[2351,2504],"source":7441},"id":13884973,"start_at":"2020-08-03T15:00:00Z","end_at":"2020-08-07T11:00:00Z","status":Booking::STATUS_BOOKED,"updated_at":"2020-06-02T10:51:44Z","reference":"089LPP",Booking::STATUS_BOOKED:true,"booked_at":"2020-06-02T10:51:28Z",Booking::STATUS_UNAVAILABLE:false,"tentative_expires_at":null,"initial_price":"649.0","initial_rental_price":"649.0","channel_price":"764.47","discount":"","final_rental_price":"649.0","final_price":"764.47","downpayment":"382.24","paid_amount":"764.47","currency":"EUR","notes":null,"damage_deposit":"399.0","charge_damage_deposit_on_arrival":false,"adults":4,"children":0,"bookings_payments_count":1,"review_requests_count":0,"locked":"true","created_at":"2020-06-02T10:51:25Z","canceled_at":null,"cancelation_reason":null,"cancelation_message_to_guest":null,"cancelation_message_to_channel":null,"expected_checkin_time":null,"expected_checkout_time":null,"payment_url":"https://www.bookingsync.com/fr/mybookings/11000/bookings/13884973/payment","balance_due_at":"2020-07-04T15:00:00Z","rental_payback_to_owner":null,"final_payback_to_owner":"0.0","commission":"125.16","contract_updated_at":null,"door_key_code":null,"payment_left_to_collect":"399.0","external_reference":"HMRWYMAWA9","authorized_amount":"0.0","paid_or_authorized_amount":"764.47","imported":false,"owned_by_app":false}}}';

        $bookingsync = new BookingSync(
            config('bookingsync.client'),
            config('bookingsync.secret'),
            "", true
        );

        $scrt = config('bookingsync.secret');
        $sign = 'c8175d34de944fdc53760c652aa2b4fc3e8423c1';
        //$json = file_get_contents('php://input');
        $data = json_decode($json, true);
        $json = base64_encode($json);
        $json = chunk_split($json, 60, "\n");

        $hash = hash_hmac('sha1', $json, $scrt);

        if($hash === $sign){
            $bookingSync_id = $data["resource"]["booking"]["id"];
            $distBook = $data["resource"]["booking"];
            /* @var Booking $booking */
            switch($data["event"]){
                case 'booking_created':
                case 'booking_updated':
                    $booking = $bookingsync->registerBooking($distBook, true);
                    break;
                case 'booking_destroyed':
                    $booking = Booking::where('bookingsync_id', $bookingSync_id)->first();
                    $booking->delete();
                    break;
            }
            \Log::info('booking '. $bookingSync_id . ' : ' . $data["event"] . ' --- ' . Carbon::now());
        }else{
            echo 'can\'t read';
        }
    }

}
