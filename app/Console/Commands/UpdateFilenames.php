<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Property;

class UpdateFilenames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:filenames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update files default names';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $properties = Property::all();
        foreach($properties as $p){
            /* @var $p Property */
            echo "updating property " . $p->name . "\n";
            if($p->hasOldContract()){
                rename($p->getOldContractFile(), $p->getContractFile());
                $p->contract = $p->getUploadFolder().'/'.$p->getContractFileName();
                echo "\t-contract\n";
            }
            if($p->hasOldInventory()){
                rename($p->getOldInventoryFile(), $p->getInventoryFile());
                $p->inventory = $p->getUploadFolder().'/'.$p->getInventoryFileName();
                echo "\t-inventory\n";
            }
            if($p->hasOldAssess()){
                rename($p->getOldAssessmentFile(), $p->getAssessmentFile());
                $p->assessment = $p->getUploadFolder().'/'.$p->getAssessmentFileName();
                echo "\t-assessment\n";
            }
            if($p->hasOldKeysFile()){
                rename($p->getOldKeysFile(), $p->getKeysFile());
                $p->keys = $p->getUploadFolder().'/'.$p->getKeysFileName();
                echo "\t-keys\n";
            }
            echo "\n";
            $p->save();
        }
    }
}
