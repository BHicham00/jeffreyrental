<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Property;
use Carbon\Carbon;

class UpdateReportsFilenames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:reportsnames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update files default names';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $properties = Property::all();
        foreach($properties as $property){
            /* @var $p Property */
            echo "updating property " . $property->name . "\n";
            $reports = $property->getReports();
            $clientRef = $property->getUser()->client_id;
            foreach($reports as $r)
            {
                $from = new Carbon($r->start_at);
                $to = new Carbon($r->end_at);
                $time = ($from->month > 9 ? $from->month : "0" . $from->month) . substr($from->year, 2);
                if($property->hasOldInvoice($time)){
                    rename($property->getOldInvoiceFile($time), $property->getInvoiceFile($time));
                    echo "\t-invoice $time\n";
                }
                if($property->hasOldReport($time)){
                    rename($property->getOldReportFile($time), $property->getReportFile($time));
                    echo "\t-report $time\n";
                }
            }
        }
    }
}
