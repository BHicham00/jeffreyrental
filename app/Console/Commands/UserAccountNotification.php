<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Helpers\Mailer;
use Illuminate\Support\Facades\Hash;

class UserAccountNotification extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:users {send}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send a link to users and login informations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $send = $this->argument('send');
        $users = User::where("archived", "=", "0")->get();
        $passwd = bcrypt("XjeffreyX");
        foreach($users as $u)
        {
            $result = Hash::check("XjeffreyX", $u->password);
            if($result)
            {
                if($send == "yes")
                {
                    Mailer::sendAccountAccess($u);
                }
                echo "$u->name $u->email \n";
            }
        }
    }

}
