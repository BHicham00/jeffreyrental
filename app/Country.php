<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'iso', 'name', 'nicename', 'iso3', 'numcode', 'phonecode', 'vat_rate', 'allow_kyc', 'blocked', 'shipping_cost_id'
    ];

    protected $casts = [
      'blocked' => 'boolean',
      'allow_kyc' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shipping_cost()
    {
        return $this->belongsTo(ShippingCost::class);
    }
}
