<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronTaskEvent extends Model
{
    protected $casts = [
        'errors' =>'json',
    ];

    protected $fillable = [
        'name', 'state', 'frequency', 'errors', 'details'
    ];

}
