<?php


namespace App\Crons;


use App\Booking;
use App\CronTaskEvent;
use App\Helpers\Mailer;
use Carbon\Carbon;

class SendPrecheckinMails
{
    public const LABEL = 'Send precheckin Mails';
    public const FREQUENCY = 'Hourly';

    public function __invoke()
    {
        $cron_task = CronTaskEvent::create([
            'name' => self::LABEL,
            'frequency' => self::FREQUENCY,
        ]);

        $errors = [];
        $bookings = Booking::where('status', 'Booked')
            ->where('paid', true)
            ->whereNull('prechekin_sent')
            ->where('start_at', '<=', Carbon::now()->addDays(14))
            ->where('start_at', '>=', Carbon::now())
            ->where('end_at', '>', Carbon::now())
            ->get();
        $n = 0;
        foreach($bookings as $b){

            dump($b->reference);
            dump($b->start_at);
            try{
                Mailer::sendGuestInvite($b);
                $b->update(["prechekin_sent" => Carbon::now()]);
                $n++;
            }catch(\Exception $e){
                $errors[] = $e->getMessage() . ' - ' . json_decode($b->guest)->email;
            }
        }
        dump(count($bookings));
        $cron_task->state = !empty($errors) ? 2 : 1;
        $cron_task->errors = !empty($errors) ? json_encode($errors) : null;
        $cron_task->details = $n . " mails envoyés";
        $cron_task->save();
    }

}