<?php


namespace App\Crons;


use App\Booking;
use App\CronTaskEvent;
use App\Helpers\Mailer;
use Carbon\Carbon;

class SendPrecheckoutMails
{
    public const LABEL = 'Send precheckout Mails';
    public const FREQUENCY = 'Hourly';

    public function __invoke()
    {
        $cron_task = CronTaskEvent::create([
            'name' => self::LABEL,
            'frequency' => self::FREQUENCY,
        ]);

        $errors = [];
        $bookings = Booking::where('status', 'Booked')
            ->where('paid', true)
            ->whereNull('prechekout_sent')
            ->where('end_at', '<=', Carbon::now()->addDays(3))
            ->where('end_at', '>', Carbon::now())
            ->get();
        $n = 0;
        foreach($bookings as $b){

            dump($b->reference);
            dump($b->start_at);
            try{
                if($b->property->keynest_auto_checkout && $b->bookingguest){
                    Mailer::sendGuestAutoCheckout($b->bookingguest);
                }
                else {
                    Mailer::sendGuestCheckout($b);
                }
                $b->update(["prechekout_sent" => Carbon::now()]);
                $n++;
            }catch(\Exception $e){
                $errors[] = $e->getMessage();
            }
        }
        dump(count($bookings));
        $cron_task->state = !empty($errors) ? 2 : 1;
        $cron_task->errors = !empty($errors) ? json_encode($errors) : null;
        $cron_task->details = $n . " mails envoyés";
        $cron_task->save();
    }

}