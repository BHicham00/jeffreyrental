<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class Deposit extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id',
        'client',
        'reference',
        'property_id',
        'payment_url',
        'amount',
        'start_at',
        'end_at',
        'paid',
        'token',
        'transaction_id',
        'order_id',
        'pay_id',
        'card_brand',
        'card',
        'expire_date',
        'PM',
        'status',
        'mail_sent',
        'city',
        'blocked',
        'comment'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo("App\Property");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function booking(){
        return $this->hasOne(Booking::class, 'bookingsync_id', 'booking_id');
    }

    public function getLink(){
        return route('deposit', ['id' => $this->id]) . '?token='.$this->token;
    }

}
