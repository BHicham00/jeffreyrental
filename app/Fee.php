<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class Fee extends Model
{
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'property_id', 'info', 'value'
    ];
    
    public function property(){
        return $this->belongsTo("App\Property")->withTrashed();
    }
}
