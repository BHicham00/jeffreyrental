<?php

use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use App\City;
use App\Country;


function convertToPdf($file, $outdir){
    exec(config('convert.binary') . " --headless --invisible --convert-to pdf $file --outdir $outdir");
}

if(!function_exists("getArray"))
{

    function getArray($start, $end)
    {
        $array = [];
        for($i = $start; $i < $end + 1; $i++)
        {
            $array[$i] = $i;
        }
        return $array;
    }

}

if(!function_exists('getYear')){
    function getYear(){
        return (new Carbon())->format('Y');
    }
}

if(!function_exists("valdebug"))
{

    function valdebug($value)
    {
        echo "<pre>";
        var_dump($value);
        echo "</pre>";
    }

}

if(!function_exists("admin_path"))
{

    function admin_path($folder = "")
    {
        return base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . $folder;
    }

}

if(!function_exists("attachFile"))
{

    function attachFile($path, $fileName)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $fileName . ";");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        echo file_get_contents($path);
    }

}

if(!function_exists("cities"))
{
    function cities(){
        if(cache("cities")){
            return cache("cities");
        }
        $cities = [];
        $cities[null] = "Non française";
        foreach(City::all()->sortBy('nom_commune') as $c){
            $cities[$c->id] = $c->nom_commune . " " . $c->getPostal();
        }
        cache()->add("cities", $cities, 3600);
        return $cities;
    }
}
if(!function_exists("countries"))
{
    function countries(){
        if(cache("countries")){
            return cache("countries");
        }
        $countries = [];
        foreach(Country::all()->sortBy('nicename') as $c){
            $countries[$c->id] = $c->nicename;
        }
        cache()->add("countries", $countries, 3600);
        return $countries;
    }
    function country($id){
        $countries = [];
        if($c = Country::find($id)){
            $countries[$c->id] = $c->nicename;
        }
        return $countries;
    }
}
if(!function_exists("departements"))
{
    function departements()
    {
        $departements = [
            null => "Non français",
            '01' => 'Ain',
            '02' => 'Aisne',
            '03' => 'Allier',
            '04' => 'Alpes de Haute Provence',
            '05' => 'Hautes Alpes',
            '06' => 'Alpes Maritimes',
            '07' => 'Ardèche',
            '08' => 'Ardennes',
            '09' => 'Ariège',
            '10' => 'Aube',
            '11' => 'Aude',
            '12' => 'Aveyron',
            '13' => 'Bouches du Rhône',
            '14' => 'Calvados',
            '15' => 'Cantal',
            '16' => 'Charente',
            '17' => 'Charente Maritime',
            '18' => 'Cher',
            '19' => 'Corrèze',
            '2A' => 'Corse du Sud',
            '2B' => 'Haute Corse',
            '21' => 'Côte d\'Or',
            '22' => 'Côtes d\'Armor',
            '23' => 'Creuse',
            '24' => 'Dordogne',
            '25' => 'Doubs',
            '26' => 'Drôme',
            '27' => 'Eure',
            '28' => 'Eure et Loir',
            '29' => 'Finistère',
            '30' => 'Gard',
            '31' => 'Haute Garonne',
            '32' => 'Gers',
            '33' => 'Gironde',
            '34' => 'Hérault',
            '35' => 'Ille et Vilaine',
            '36' => 'Indre',
            '37' => 'Indre et Loire',
            '38' => 'Isère',
            '39' => 'Jura',
            '40' => 'Landes',
            '41' => 'Loir et Cher',
            '42' => 'Loire',
            '43' => 'Haute Loire',
            '44' => 'Loire Atlantique',
            '45' => 'Loiret',
            '46' => 'Lot',
            '47' => 'Lot et Garonne',
            '48' => 'Lozère',
            '49' => 'Maine et Loire',
            '50' => 'Manche',
            '51' => 'Marne',
            '52' => 'Haute Marne',
            '53' => 'Mayenne',
            '54' => 'Meurthe et Moselle',
            '55' => 'Meuse',
            '56' => 'Morbihan',
            '57' => 'Moselle',
            '58' => 'Nièvre',
            '59' => 'Nord',
            '60' => 'Oise',
            '61' => 'Orne',
            '62' => 'Pas de Calais',
            '63' => 'Puy de Dôme',
            '64' => 'Pyrénées Atlantiques',
            '65' => 'Hautes Pyrénées',
            '66' => 'Pyrénées Orientales',
            '67' => 'Bas Rhin',
            '68' => 'Haut Rhin',
            '69' => 'Rhône-Alpes',
            '70' => 'Haute Saône',
            '71' => 'Saône et Loire',
            '72' => 'Sarthe',
            '73' => 'Savoie',
            '74' => 'Haute Savoie',
            '75' => 'Paris',
            '76' => 'Seine Maritime',
            '77' => 'Seine et Marne',
            '78' => 'Yvelines',
            '79' => 'Deux Sèvres',
            '80' => 'Somme',
            '81' => 'Tarn',
            '82' => 'Tarn et Garonne',
            '83' => 'Var',
            '84' => 'Vaucluse',
            '85' => 'Vendée',
            '86' => 'Vienne',
            '87' => 'Haute Vienne',
            '88' => 'Vosges',
            '89' => 'Yonne',
            '90' => 'Territoire de Belfort',
            '91' => 'Essonne',
            '92' => 'Hauts de Seine',
            '93' => 'Seine St Denis',
            '94' => 'Val de Marne',
            '95' => 'Val d\'Oise',
            '97' => 'DOM',
            '971' => 'Guadeloupe',
            '972' => 'Martinique',
            '973' => 'Guyane',
            '974' => 'Réunion',
            '975' => 'Saint Pierre et Miquelon',
            '976' => 'Mayotte'
        ];
        return $departements;
    }

}
if(!function_exists("numericToText"))
{

    function numericToText($n, $cpt)
    {
        if(!is_numeric($n))
        {
            return false;
        }

        while(strlen($n) < 3)
        {
            $n = '0' . $n;
        } // On rajoute les 0 pour obtenir une chaine de 3 chiffres
        if(preg_match('#^[0-9]{3}$#', $n))
        {
            $C = substr($n, 0, 1);
            $D = substr($n, 1, 1);
            $U = substr($n, 2, 1);
        } else
        {
            return false;
        }
        //Initialisation
        $valeur = '';
        $chiffre = array('', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit', 'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize', 'dix-sept', 'dix-huit', 'dix-neuf');
        $nombre = array('', 'dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante-dix', 'quatre-vingt', 'quatre-vingt-dix');
        //Conversion
        if($D == 0)
        {
            $valeur = $chiffre[$U];
        }        // 1 à 10 (ne retourne pas "zéro" pour 0)
        elseif($D == 1)
        {
            $valeur = $chiffre[$U + 10];
        }      // 11 à 19
        elseif($D >= 2 && $D <= 6)
        {          // 20 à 69
            if($U == 0)
            {
                $valeur = $nombre[$D];
            }        // x0
            elseif($U == 1)
            {
                $valeur = $nombre[$D] . ' et un';
            }    // x1
            else
            {
                $valeur = $nombre[$D] . '-' . $chiffre[$U];
            }     // x2 à x9
        } elseif($D == 7)
        {
            $valeur = $nombre[6] . '-' . $chiffre[$U + 10];
        }  // 70 à 79
        elseif($D == 8)
        {            // 80 à 89
            if($U == 0)
            {
                if($cpt != 1)
                {
                    $valeur = $nombre[$D] . 's';
                }    // tous les 80 sauf ...
                else
                {
                    $valeur = $nombre[$D];
                }       // 80 000 => quatre-vingt mille (cas particulier)
            } else
            {
                $valeur = $nombre[$D] . '-' . $chiffre[$U];
            }    // 81 à 89
        } elseif($D == 9)
        {
            $valeur = $nombre[8] . '-' . $chiffre[$U + 10];
        }  // 90 à 99
        //centaines
        if($C == 1)
        {             //1xx
            if(strlen($valeur) == 0)
            {
                $valeur = 'cent';
            }      // 100
            else
            {
                $valeur = 'cent' . ' ' . $valeur;
            }        // 101 à 199
        } // 1xx				
        elseif($C != 0)
        {            // 2xx à 9xx
            if(strlen($valeur) == 0)
            {          // [2-9]00
                if($cpt == 1)
                {
                    $valeur = $chiffre[$C] . ' cent';
                }     // cas particulier (ex: 300 000 : trois cent mille)
                else
                {
                    $valeur = $chiffre[$C] . ' cents';
                }      // [2-9]00 ... le reste (ex: 300 000 000 : trois cents millions)
            } else
            {
                $valeur = $chiffre[$C] . ' cent ' . $valeur;
            }    // le reste
        } else
        {
            
        }              // 0xx
        //nettoyage
        unset($n, $C, $D, $U, $chiffre, $nombre, $esp);
        //renvoi
        return $valeur;
    }

}
if(!function_exists('curlGetContent'))
{

    function curlGetContent($url, $header = null, $data = null, $origin = null, $referer = null, $cookies = null)
    {
        $ch = curl_init($url);
        $query = null;
        if(FALSE === $ch)
            throw new Exception('failed to initialize');

        if(!empty($header))
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . DIRECTORY_SEPARATOR . "cookies.txt");
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . DIRECTORY_SEPARATOR . "cookies.txt");
        if(!empty($cookies))
        {
            curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        }
        $query = null;
        if(isset($data))
        {
            if(is_array($data))
                $query = http_build_query($data);
            else
                $query = $data;

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        }
        if($referer)
        {
            curl_setopt($ch, CURLOPT_REFERER, $referer);
        }


        $content = curl_exec($ch);

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $content, $matches);
        $cookies = array();
        foreach($matches[1] as $item)
        {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }

        if(FALSE === $content)
            throw new Exception(curl_error($ch), curl_errno($ch));

        //$info = curl_getinfo($ch);
        curl_close($ch);

        return [
            "content" => $content,
            //"info" => $info,
            "query" => $query,
            "cookies" => $cookies
        ];
    }

}
if(!function_exists('objectToObject'))
{

    function objectToObject($instance, $className)
    {
        return unserialize(sprintf(
                        'O:%d:"%s"%s', strlen($className), $className, strstr(strstr(serialize($instance), '"'), ':')
        ));
    }

}
if(!function_exists('arrayToObject'))
{

    function arrayToObject(array $array, $className)
    {
        return unserialize(sprintf(
                        'O:%d:"%s"%s', strlen($className), $className, strstr(serialize($array), ':')
        ));
    }

}


if(!function_exists('bookingsChart'))
{

    function bookingsChart(\Khill\Lavacharts\Lavacharts $lava, $type, $results, $title, $columns, $colors, $logScale = false, $fullyear = false, $height = 400)
    {
        $chartdata = $lava->DataTable();
        /* @var $chartdata \Khill\Lavacharts\DataTables\DataTable */
        /* @var $chartdata \Khill\Lavacharts\DataTables\Formats */
        if($fullyear)
        {
            $chartdata->addColumn('string', "Année");
        } else
        {
            $chartdata->addColumn('string', "Mois de l'année");
        }
        foreach($columns as $col)
        {
            $chartdata->addNumberColumn($col["title"], $lava->NumberFormat([
                        'suffix' => $col["suffix"]
            ]));
        }

        $globalOccupied = 0;
        $globalCount = 0;
        $globalNights = 0;

        foreach($results as $t => $v)
        {
            if(!isset($v["time"]))
            {
                continue;
            }
            if(!$fullyear){
                $date = new Carbon($t . "-01");
                $endDate = (new Carbon($t . "-01"))->endOfMonth();
            }else{
                $date = new Carbon($t . "01-01");
                $endDate = (new Carbon($t . "12-31"))->endOfMonth();
            }
            $diff = $date->diffInDays($endDate);
            //$v["occupied"] = ($v["days"] / $diff) * 100;
            Carbon::setLocale('fr');
            $row = [
                $fullyear ? substr($t, 0, 4) : $date->format('M Y'),
            ];
            if($v["count"] > 0)
            {
                $v["avgNights"] = floor(($v["nights"] / $v["count"]) * 100) / 100;
            }
            foreach($columns as $col)
            {
                array_push($row, $v[$col["name"]]);
            }
            $globalOccupied += $v["occupied"];
            $globalCount += $v["count"];
            $globalNights += $v["nights"];
            $chartdata->addRow($row);
        }
        $chart = $lava->$type($title, $chartdata, [
            'colors' => $colors,
            'height' => $height,
            'vAxis' => [
                "logScale" => $logScale,
            ],
            'seriesType' => 'bars',
        ]);
        $globalAvgNights = 0;
        if(count($results) && $globalCount)
        {
            $globalOccupied = floor(($globalOccupied / count($results)) * 100) / 100;
            $globalAvgNights = floor(($globalNights / $globalCount) * 100) / 100;
        }
        return [
            "occupied" => $globalOccupied,
            "avgNights" => $globalAvgNights,
        ];
    }

    function aggregateBookings($datas, $fullyear = false, $additionalsColumns = [])
    {
        $results = [];
        $columns = array_merge([
            "count",
            "days",
            "nights",
            "avgNights",
            "occupied",
            "amount",
            'paymenttocome',
            "paid_amount",
            "under_adar",
            "ota",
            "profit",
            "owner",
            "cleaning",
            "tax"
        ], $additionalsColumns);
        foreach($datas as $month => $data)
        {
            //$month = "01-" . $month;
            $results[$month] = [
                "time" => $month,
                "properties" => []
            ];
            foreach($columns as $col)
            {
                $results[$month][$col] = 0;
            }
            foreach($data as $key => $row)
            {
                if (!array_key_exists($row["property_id"], $results[$month]["properties"])) {
                    $results[$month]["properties"][$row["property_id"]] = [
                        "id" => $row["property_id"],
                    ];
                    foreach ($columns as $col) {
                        $results[$month]["properties"][$row["property_id"]][$col] = 0;
                    }
                }

                $start_at = new Carbon($row["start_at"]);
                $end_at = new Carbon($row["end_at"]);
                $results[$month]["count"] ++;
                $results[$month]["properties"][$row["property_id"]]["count"] ++;
                $nights = $start_at->hour(0)->minute(0)->diffInDays($end_at->hour(0)->minute(0));
                $results[$month]["nights"] += $nights;
                $results[$month]["properties"][$row["property_id"]]["nights"] += $nights;
                foreach($columns as $col)
                {
                    if(array_key_exists($col, $row))
                    {
                        if($col !== 'tax') {
                            $results[$month][$col] += $row[$col];
                            $results[$month]["properties"][$row["property_id"]][$col] += $row[$col];
                        }else{
                            $tax = strstr(strtolower($row['source']), "airbnb") ? 0 : $row[$col];
                            $results[$month][$col] += $tax;
                            $results[$month]["properties"][$row["property_id"]][$col] += $tax;
                        }
                    }
                }
                $moneyleft = $row['paid'] ? 0 : max(0, ($row['amount'] + $row['tax'] + $row['cleaning']) - $row['paid_amount']);
                $results[$month]['paymenttocome'] += $moneyleft;

                $startDiff = (new Carbon($row["start_at"]))->endOfMonth();

                if(!$fullyear && $start_at->month != $end_at->month)
                {
                    $daysPrev = $start_at->diffInDays($startDiff);
                    if(!isset($results[$start_at->format("Y-m")]))
                    {
                        $results[$start_at->format("Y-m")] = [
                            "time" => $start_at->format("Y-m"),
                            "properties" => []
                        ];
                        foreach($columns as $col)
                        {
                            $results[$start_at->format("Y-m")][$col] = 0;
                        }
                    }
                    if(!array_key_exists($row["property_id"], $results[$start_at->format("Y-m")]["properties"]))
                    {
                        $results[$start_at->format("Y-m")]["properties"][$row["property_id"]] = [
                            "id" => $row["property_id"],
                        ];
                        foreach($columns as $col)
                        {
                            $results[$start_at->format("Y-m")]["properties"][$row["property_id"]][$col] = 0;
                        }
                    }
                    $results[$start_at->format("Y-m")]["days"] += $daysPrev;
                    $results[$start_at->format("Y-m")]["properties"][$row["property_id"]]["days"] += $daysPrev;


                    $days = $startDiff->diffInDays($end_at);
                    if(!isset($results[$end_at->format("Y-m")]))
                    {
                        $results[$end_at->format("Y-m")] = [
                            "time" => $end_at->format("Y-m"),
                            "properties" => []
                        ];
                        foreach($columns as $col)
                        {
                            $results[$end_at->format("Y-m")][$col] = 0;
                        }
                    }
                    if(!array_key_exists($row["property_id"], $results[$end_at->format("Y-m")]["properties"]))
                    {
                        $results[$end_at->format("Y-m")]["properties"][$row["property_id"]] = [
                            "id" => $row["property_id"],
                        ];
                        foreach($columns as $col)
                        {
                            $results[$end_at->format("Y-m")]["properties"][$row["property_id"]][$col] = 0;
                        }
                    }
                    $results[$end_at->format("Y-m")]["days"] += $days;
                    $results[$end_at->format("Y-m")]["properties"][$row["property_id"]]["days"] += $days;
                } else
                {
                    $days = $start_at->diffInDays($end_at);
                    $results[$month]["days"] += $days;
                    $results[$month]["properties"][$row["property_id"]]["days"] += $days;
                }
            }
            //$time = ($endDate->month > 9 ? $endDate->month : "0" . $endDate->month) . substr($endDate->year, 2);
        }
        foreach($results as $month => $v)
        {

            $fullOccupied = 0;
            $countOccupation = 0;
            if(!$fullyear){
                $date = new Carbon($month . "-01");
                $endDate = (new Carbon($month . "-01"))->endOfMonth();
            }else{
                $date = new Carbon($month . "01-01");
                $endDate = new Carbon($month . "12-31");
            }
            
            $diff = $date->diffInDays($endDate);
            foreach($results[$month]["properties"] as $id => $p)
            {

                $p["occupied"] = ($p["days"] / $diff) * 100;
                if($p["count"])
                {
                    $p["avgNights"] = floor(($p["nights"] / $p["count"]) * 100) / 100;
                }
                //echo $id . " - " . $p["occupied"] . "<br/>";
                if($p["occupied"] > 0)
                {
                    $fullOccupied += $p["occupied"];
                    $countOccupation ++;
                }
            }
            if($results[$month]["count"])
            {
                $results[$month]["avgNights"] = floor(($results[$month]["nights"] / $results[$month]["count"]) * 100) / 100;
            }
            $results[$month]["occupied"] = ($results[$month]["days"] / $diff) * 100;
        }
        ksort($results);
        return $results;
    }

}
if(!function_exists('user_success'))
{

    /**
     * Flash a success message bag to session.
     *
     * @param string $title
     * @param string $message
     */
    function user_success($title, $message = '')
    {
        user_info($title, $message, 'success');
    }

}

if(!function_exists('user_error'))
{

    /**
     * Flash a error message bag to session.
     *
     * @param string $title
     * @param string $message
     */
    function user_error($title, $message = '')
    {
        user_info($title, $message, 'error');
    }

}

if(!function_exists('user_warning'))
{

    /**
     * Flash a warning message bag to session.
     *
     * @param string $title
     * @param string $message
     */
    function user_warning($title, $message = '')
    {
        user_info($title, $message, 'warning');
    }

}

if(!function_exists('user_info'))
{

    /**
     * Flash a message bag to session.
     *
     * @param string $title
     * @param string $message
     * @param string $type
     */
    function user_info($title, $message = '', $type = 'info')
    {
        $type .= "_user";
        $message = new MessageBag(get_defined_vars());

        session()->flash($type, $message);
    }

}

if(!function_exists('adminToolBtn'))
{
    function adminToolBtn($label, $link, $icon = 'fa-sign-in', $target = "_blank", $class = 'btn-primary'){
        return "<div class='btn-group' style='margin-right:5px;'>".
            "<a class='btn btn-sm " . $class . "' target='" . $target . "' href='" . $link . "'>".
            "<i class='fa ". $icon ."'></i><span class='hidden-xs'> $label</span></a></div>";
    }
}

if(!function_exists('daysOfWeek')){
    function daysOfWeek(){
        return [
            0 => _('Lundi'),
            1 => _('Mardi'),
            2 => _('Mercredi'),
            3 => _('Jeudi'),
            4 => _('Vendredi'),
            5 => _('Samedi'),
            6 => _('Dimanche'),
        ];
    }
    function formatMinutesTotime($minutes){
        $min = fmod($minutes, 60);
        return floor($minutes/60). ':' . ($min > 0 ? $min : '0'.$min);
    }
}
