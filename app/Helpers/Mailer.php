<?php

namespace App\Helpers;

use App\Booking;
use App\BookingGuest;
use App\Deposit;
use App\Property;
use App\User;
use Google\Cloud\Language\V1\PartOfSpeech\Proper;
use Mail;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mailer
 *
 * @author Administrateur
 */
class Mailer
{

    static public function sendDepositRequest(Deposit $deposit)
    {
        $view = "depositrequest_";
        $client = json_decode($deposit->client);
        $subject = "";
        switch($client->lang)
        {
            case "fr":
                $subject = "Paiement de la caution pour votre séjour à " . $deposit->city;
                $lang = "fr";
                break;
            default:
                $subject = "Damage Deposit for your trip in " . $deposit->city;
                $lang = "en";
                break;
        }
        $view .= $lang;
        Mail::send('emails.deposits.' . $view, [
            'deposit' => $deposit,
            'client' => $client,
                ], function ($message) use ($client, $subject)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $client->email;
            }
            $message->to($email)->subject($subject);
        });
    }

    static public function sendAccountAccess(User $user)
    {
        Mail::send("emails.accountaccess", ["user" => $user], function(\Illuminate\Mail\Message $message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $user->email;
            }
            $message->to($email)->subject("JeffreyHost - Accès compte en ligne")->bcc("developer@jeffreyhost.com");
        });
    }

    static public function sendDepositConfirmation(Deposit $deposit)
    {
        $view = "depositconfirm_";
        $client = json_decode($deposit->client);
        $subject = "";
        switch($client->lang)
        {
            case "fr":
                $subject = "Confirmation du paiement de votre caution pour votre séjour à " . $deposit->city;
                $lang = "fr";
                break;
            default:
                $subject = "Confirmation of your Damage Deposit payment";
                $lang = "en";
                break;
        }
        $view .= $lang;
        Mail::send('emails.deposits.' . $view, [
            'deposit' => $deposit,
            'client' => $client,
                ], function ($message) use ($client, $subject)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $test = (config("app.env") !== "production");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $client->email;
            }
            $message->to($email)->subject(($test ? "[TEST]" : "" ) . $subject);
        });
    }

    static public function sendDepositConfirmationToJeffrey(Deposit $deposit)
    {
        $client = json_decode($deposit->client);
        Mail::send('emails.deposits.jeffrey', [
            'deposit' => $deposit,
            'client' => $client,
                ], function ($message) use ($deposit, $client)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $test = (config("app.env") !== "production");
            $message->to("gery@jeffreyhost.com")->subject(($test ? "[TEST]" : "" ) . "Caution en traitement $deposit->amount € - " . $client->firstname . " " . $client->lastname);
        });
    }

    static public function sendUserPropertyRecall(User $user, Property $property)
    {
        $link = url()->action("\App\Http\Controllers\AccountController@documents", ["id" => $property->id]);
        Mail::send('emails.contractrecall', [
            'user' => $user,
            'property' => $property,
            'link' => $link,
                ], function ($message) use ($user, $property)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $user->email;
            }
            $message->to($email)->subject("Gestion de votre contrat - " . $property->name);
        });
    }

    static public function sendPropertyNotification(User $user, Property $property)
    {
        $link = url()->action("\App\Admin\Controllers\PropertyController@show", ["id" => $property->id]);
        Mail::send('emails.admin.propertynotification', [
            'user' => $user,
            'property' => $property,
            'link' => $link,
                ], function ($message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to("advertisement@jeffreyhost.com")->subject("Ajout d'une propriété par le client " . $user->name);
        });
    }

    static public function sendNotification(User $user)
    {
        $link = url()->action("\App\Admin\Controllers\UserController@show", ["id" => $user->id]);
        Mail::send('emails.admin.notification', [
            'user' => $user,
            'link' => $link,
                ], function ($message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to("advertisement@jeffreyhost.com")->subject("Creation du profil - " . $user->name);
        });
    }
    
    static public function sendBookingDemand(User $user, Property $property, $params)
    {
        $linkUser = url()->action("\App\Admin\Controllers\UserController@show", ["id" => $user->id]);
        $linkProperty = url()->action("\App\Admin\Controllers\PropertyController@show", ["id" => $property->id]);
        $linkBooking = "https://www.bookingsync.com/fr/rentals/" . $property->bookingsync_id . "/bookings";
        Mail::send('emails.admin.bookingdemand', [
            'user' => $user,
            'property' => $property,
            'linkUser' => $linkUser,
            'linkProperty' => $linkProperty,
            'linkBooking' => $linkBooking,
            'params' => $params,
        ], function ($message) use ($user, $property)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->replyTo($user->email);
            $subject = "[TEST ne pas prendre en compte]";
            if(config("app.env") === "production")
            {
                $subject = "";
            }
            $message->to("booking@jeffreyhost.com")->subject($subject . "!! Demande de réservation propriétaire - " . $property->name);
        });
    }

    static public function sendConfirmation(User $user)
    {
        $test = (config("app.env") !== "production");

        Mail::send('emails.welcome', [
            'user' => $user,
        ], function ($message) use ($user, $test) {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to($test ? 'developer@jeffreyhost.com' : $user->email)->subject("Bienvenue " . $user->name);
        });
    }

    static public function sendRentMail(User $user)
    {
        Mail::send('emails.rentnumber', [
            'user' => $user,
                ], function (\Illuminate\Mail\Message $message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to($user->email)->subject("Votre démarche loueur de meublés");
            $message->attach(__DIR__ . DIRECTORY_SEPARATOR . 'Memento-Loueur-de-Meubles.pdf');
        });
    }

    static public function sendContractSentMail(Property $property)
    {
        $test = (config("app.env") !== "production");
        if(!$test)
        {
            $link = url()->action("\App\Admin\Controllers\PropertyController@show", ["id" => $property->id]);
            Mail::send('emails.admin.contractsent', [
                'property' => $property,
                'link' => $link,
                    ], function ($message) use ($property)
            {
                $message->from("noreply@jeffreyhost.com", "JeffreyHost");
                $message->to("advertisement@jeffreyhost.com")->subject("Réception du contrat - " . $property->name);
            });
        }
    }

    static public function sendOneTimePass(BookingGuest $user, $pass)
    {
        $test = (config("app.env") !== "production");
        Mail::send('emails.guest.onetimepass_' . app()->getLocale(), [
            'user' => $user,
            'pass' => $pass,
        ], function ($message) use ($user, $test)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to($test ? 'developer@jeffreyhost.com' : $user->email)->subject("JeffreyHost - Votre accès unique");
        });
    }

    static public function alertDev($wording)
    {
        Mail::send('emails.alert', [
            'wording' => $wording
        ], function (\Illuminate\Mail\Message $message)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $message->to('developer@jeffreyhost.com')->subject("Alerte JeffreyAdmin");
        });
    }


    static public function sendGuestInvite(Booking $booking)
    {
        $guest = json_decode($booking->guest);
        if($guest) {
            $lang = $guest->lang !== "fr" ? "en" : "fr";
            Mail::send("emails.guest.invite_" . $lang, ["booking" => $booking, "guest" => $guest], function (\Illuminate\Mail\Message $message) use ($booking, $guest) {
                $message->from("noreply@jeffreyhost.com", "JeffreyHost");
                $email = "developer@jeffreyhost.com";
                if (config("app.env") === "production") {
                    try {
                        $email = $guest->email;
                    } catch (\Exception $e) {
                        throw new \Exception('la reservation ' . $booking->referenece . ' n\'a pas de mail de contact');
                    }
                }
                $message->to($email)->subject("JeffreyHost - Guest Precheckin");
            });
        }
    }

    static public function sendGuestCheckout(Booking $booking)
    {
        $guest = json_decode($booking->guest);
        $bookingguest = $booking->bookingguest;
        if($guest) {
            $lang = $guest->lang !== "fr" ? "en" : "fr";
            Mail::send("emails.guest.checkout_" . $lang, ["property" => $booking->property, "user" => $bookingguest ?: $guest], function (\Illuminate\Mail\Message $message) use ($bookingguest, $booking, $guest) {
                $message->from("noreply@jeffreyhost.com", "JeffreyHost");
                $email = "developer@jeffreyhost.com";
                if (config("app.env") === "production") {
                    try {
                        $email = $bookingguest ? $bookingguest->email : $guest->email;
                    } catch (\Exception $e) {
                        throw new \Exception('la reservation ' . $booking->referenece . ' n\'a pas de mail de contact');
                    }
                }
                $message->to($email)->subject("JeffreyHost - Guest Precheckout");
            });
        }
    }

    static public function sendGuestAccess(BookingGuest $user)
    {
        Mail::send("emails.guest.accountaccess_" . app()->getLocale(), ["user" => $user], function(\Illuminate\Mail\Message $message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $user->email;
            }
            $message->to($email)->subject("JeffreyHost - Guest login information")->bcc("developer@jeffreyhost.com");
        });
    }

    static public function sendGuestKeyCode(BookingGuest $user)
    {
        $guest = json_decode($user->booking->guest);
        $lang = null;
        if($guest){
            $lang = $guest->lang !== "fr" ? "en" : "fr";
        }
        Mail::send("emails.guest.autokey_" . $lang ?: app()->getLocale(), ["user" => $user, 'booking' => $user->booking, 'property' => $user->booking->property], function(\Illuminate\Mail\Message $message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $user->email;
            }
            $message->to($email)->subject("JeffreyHost - Guest keynest information")->bcc("developer@jeffreyhost.com");
        });
    }

    static public function sendGuestAutoCheckout(BookingGuest $user)
    {
        $guest = json_decode($user->booking->guest);
        $lang = null;
        if($guest){
            $lang = $guest->lang !== "fr" ? "en" : "fr";
        }
        Mail::send("emails.guest.autocheckout_" . $lang ?: app()->getLocale(), ["user" => $user, 'booking' => $user->booking, 'property' => $user->booking->property], function(\Illuminate\Mail\Message $message) use ($user)
        {
            $message->from("noreply@jeffreyhost.com", "JeffreyHost");
            $email = "developer@jeffreyhost.com";
            if(config("app.env") === "production")
            {
                $email = $user->email;
            }
            $message->to($email)->subject("JeffreyHost - Guest keynest information")->bcc("developer@jeffreyhost.com");
        });
    }

}
