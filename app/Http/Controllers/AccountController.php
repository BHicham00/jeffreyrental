<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Mail;
use ZipArchive;
use DOMDocument;
use App\Helpers\Mailer;
use App\User;
use App\Amenity;
use App\Property;
use App\PropertyAmenity;
use App\PropertyDetails;
use App\Report;
use Carbon\Carbon;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Admin\Extensions\Form\Field\Checkbox;
use Khill\Lavacharts\Lavacharts;
use App\BookingSync\BookingSync;
use Dompdf\Dompdf;

class AccountController extends Controller
{
    protected $bookingSync;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $date_du = session()->get('date_du');
        $date_au = session()->get('date_au');
        if($date_au==null) $date_au = (new Carbon())->endOfMonth();
        if($date_du==null) $date_du = $date_au->copy()->startOfYear();
        $user = auth()->user();
        $content = '';
        $propertyAlerts = [];
        $charts = [];
        $properties = $user->properties()->get();
        $i = 0;
        $lava = new Lavacharts();
        /** @var Property $p */
        foreach($properties as $p)
        {
            if(!$p->hasRanking() || !$p->hasAttestLoc())
            {
                $locale = session()->get('applocale');
                $files = [];
                if(!$p->hasRanking()){
                    $files[] = $locale=="fr" ? ' - Décision de classement' : 'Classification decision';
                }
                if(!$p->hasAttestLoc()){
                    $files[] = $locale=="fr" ? ' - Attestation de location meublé' : 'Furnished rental certificate';
                }
                $propertyAlerts[] = [
                    'message' => ($locale=="fr" ? 'Fichier(s) manquant(s) se rapportant à votre propriété "' : 'Missing file(s) related to your property "') . $p->name . '" :<br/>' . implode('<br/>',$files),
                    'link' => url('/property/' . $p->id . '/documents'),
                ];
            }
            $charts[$p->id] = $this->getCharts($lava, $p, $date_du, $date_au);
        }
        return view('account.home', [
            'content' => $content, 
            'user' => $user, 
            'propertyAlerts' => $propertyAlerts, 
            'properties' => $properties, 
            'charts' => $charts,
            'date_du' => $date_du,
            'date_au' => $date_au
        ]);
    }

    public function stats(Request $request)
    {
        $date_du = $request->input('date_du');
        $date_au = $request->input('date_au');
        return redirect('/account')->with( ['date_du' => $date_du, 'date_au' => $date_au]);

        // if($date_au==null) $date_au = (new Carbon())->startOfMonth()->seconds(-1);
        // if($date_du==null) $date_du = $date_au->copy()->startOfYear();

        // $user = auth()->user();
        // $content = '';
        // $propertyAlerts = [];
        // $charts = [];
        // $properties = $user->properties()->get();
        // $i = 0;
        // $lava = new Lavacharts();
        // /** @var Property $p */
        // foreach($properties as $p)
        // {
        //     if(!$p->hasRanking() || !$p->hasAttestLoc())
        //     {
        //         $locale = session()->get('applocale');
        //         $files = [];
        //         if(!$p->hasRanking()){
        //             $files[] = $locale=="fr" ? ' - Décision de classement' : 'Classification decision';
        //         }
        //         if(!$p->hasAttestLoc()){
        //             $files[] = $locale=="fr" ? ' - Attestation de location meublé' : 'Furnished rental certificate';
        //         }
        //         $propertyAlerts[] = [
        //             'message' => ($locale=="fr" ? 'Fichier(s) manquant(s) se rapportant à votre propriété "' : 'Missing file(s) related to your property "') . $p->name . '" :<br/>' . implode('<br/>',$files),
        //             'link' => url('/property/' . $p->id . '/documents'),
        //         ];
        //     }
        //     $charts[$p->id] = $this->getCharts($lava, $p, $date_du, $date_au);
        // }
        // return view('account.home', [
        //     'content' => $content, 
        //     'user' => $user, 
        //     'propertyAlerts' => $propertyAlerts, 
        //     'properties' => $properties, 
        //     'charts' => $charts,
        //     'date_du' => $date_du,
        //     'date_au' => $date_au
        // ]);
    }
    
    public function book(Request $request, $id){
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $id)->first();
        $params = $request->all();
        $dates = explode('-', $params['range']);
        $params['start_at'] = trim($dates[0]);
        $params['end_at'] = trim($dates[1]);
        Mailer::sendBookingDemand($user, $property, $params);
        user_success('Votre demande de réservation du '.$params['start_at'].' au '.$params['end_at'].' pour '.$property->name.' a bien été envoyé à notre équipe');
        return redirect('/calendars');
    }

    public function properties()
    {
        $user = auth()->user();
        $properties = $user->properties()->get();
        return view('account.properties', ['properties' => $properties, 'user' => $user]);
    }

    public function calendars()
    {
        $this->bookingSync = new BookingSync(
                config('bookingsync.client'),//'215b85af3abaed3151b4bc05c05db02f84f821966f36c9617a277b8cf3cdbc7a',
                config('bookingsync.secret'),//'c1f3912e16a72b6c4e017abec3a388b712839f72f84d61c20b2d16e87d8e23e8', 
                url('/calendars'), true
        );
        
        
        $user = auth()->user();
        $properties = $user->properties()->get();
        $pageData = [
            'properties' => $properties,
            'bookings' => [],
            'promises' => [],
        ];
        $dateStart = (new Carbon())->startOfMonth();
        $dateEnd = (new Carbon())->startOfMonth()->addMonth(1)->endOfMonth();
        foreach($properties as $property)
        {
            $bookings = $property->bookings()->where('status', '!=', 'Canceled')->orderBy('start_at', 'asc');
            if(!empty($property->vision)){
                $bookings = $bookings->where('end_at', '>=', $property->vision);
            }
            $bookings = $bookings->get();
            $bookings = $bookings->filter(function($b){
                return ($b->end_at < Carbon::now()->addDay() && $b->paid) || $b->end_at > Carbon::now();
            });
                            
                            /* ->where(function($query) use ($dateEnd, $dateStart){
                              $query->where(function($query) use ($dateEnd, $dateStart){
                              $query->where('start_at', '<=', $dateEnd)
                              ->where('start_at', '>=', $dateStart);
                              })->orWhere(function($query) use ($dateEnd, $dateStart){
                              $query->where('end_at', '<=', $dateEnd)
                              ->where('end_at', '>=', $dateStart);
                              });
                              }) */
            
            
            $data = $this->bookingSync->getApiData('bookings?status=booked,unavailable&rental_id=' . $property->bookingsync_id . '&from=' . $dateStart->format('Ymd'));
            $liveData = [];

            if($data && isset($data['bookings'])){
                foreach($data['bookings'] as $row){
                    $exists = $bookings->filter(function($value) use ($row){
                        return $value->bookingsync_id == $row['id'];
                    });
                    if(count($exists) == 0){
                        $liveData[] = [
                            'reference' => $row['reference'],
                            'guest' => '',
                            'start_at' => (new Carbon($row['start_at']))->format('Y-m-d'),
                            'end_at' => (new Carbon($row['end_at']))->format('Y-m-d'),
                            'status' => $row['status'],
                            'owner' => 0
                        ];
                    }
                }
            }
            //valdebug($liveData);die;
            
            $data = array_merge($bookings->toArray(), $liveData);
            $pageData['bookings'][$property->id] = $data;
            $groupBookings = $bookings->groupBy(function($val)
            {
                return Carbon::parse($val->end_at)->format('m-Y');
            });
            $pageData['promises'][$property->id] = [
                $dateStart->format('m-Y') => 0,
                $dateEnd->format('m-Y') => 0,
            ];
            foreach($groupBookings as $month => $bks)
            {
                if($month == $dateStart->format('m-Y') || $month == $dateEnd->format('m-Y'))
                {
                    $total = 0;
                    foreach($bks as $b)
                    {
                        $total += $b->owner;
                    }
                    $pageData['promises'][$property->id][$month] = $total;
                }
            }
        }
        return view('account.calendars', ['pageData' => $pageData, 'user' => $user]);
    }

    public function property(Request $request, $id)
    {
        $user = auth()->user();
        $property = $user->properties->where('id', '=', $id)->first();
        $propertyDetails = $property->detail;
        if(!$propertyDetails)
        {
            $propertyDetails = PropertyDetails::create(['property_id' => $property->id]);
        }
        if($request->method() == 'POST')
        {
            $rules = [
                'property.name' => 'required',
                //'property.address' => 'required',
                // 'property.iban' => 'required',
                // 'property.bic' => 'required',
            ];
            $request->validate($rules);
            $propertyInputs = $request->get('property');
            // dd($propertyInputs);
            foreach($propertyInputs as $key => $input){
                if(in_array($key, Property::$forbiddenKeys)){
                    unset($propertyInputs[$key]);
                }
            }

            $property->fill($propertyInputs);
            $property->save();
            $propertyDetails->fill($request->get('property_details'));
            $propertyDetails->save();
            $amenitiesValues = $request->get('amenities');
            foreach($property->amenities()->get() as $amen)
            {
                $amen->value = array_search($amen->amenity_id, $amenitiesValues) !== FALSE;
                $amen->save();
            }

            session(['status' => 'Enregistrement réussi']);
            return redirect(action('AccountController@property', ["id" => $id]));
        } else
        {
            session(['status' => null]);
        }
        $amenities = Amenity::all();
        if(count($property->amenities()->get()) < count($amenities))
        {
            foreach($amenities as $a)
            {
                $pa = PropertyAmenity::where('amenity_id', '=', $a->id)->where('property_id', '=', $property->id);
                if(!$pa->first())
                {
                    PropertyAmenity::create([
                        'property_id' => $property->id,
                        'amenity_id' => $a->id,
                        'value' => 0,
                    ]);
                }
            }
        }

        $amen = $property->amenities()->get();
        $options = [];
        $goodStuff = ['amenities' => []];

        foreach($amen as $pa)
        {
            $a = $pa->amenity;
            if($pa->value)
            {
                $goodStuff['amenities'][$a->id] = $a->id;
            }
            $options[$a->id] =  __($a->title);
        }
        //$form->checkbox('amenities', 'Equipement')->attribute(['class' => 'amenities'])->options($options);
        $listbox = new Checkbox('amenities', ['Equipements']);
        $listbox->options($options)->attribute(['class' => 'amenities'])->fill($goodStuff);


        /*         * * amenities ** */
        return view('account.property', [
            'property' => $property,
            'propertyDetails' => $propertyDetails,
            'user' => $user,
            'options1' => getArray(1, 6),
            'options2' => getArray(0, 6),
            'listbox' => $listbox,
        ]);
    }

    public function createProperty(Request $request)
    {
        $locale = session()->get('applocale');
        $user = auth()->user();
        $options = [];
        $amenities = Amenity::all();
        foreach($amenities as $a)
        {
            $options[$a->id] =  __($a->title);
        }
        //$form->checkbox('amenities', 'Equipement')->attribute(['class' => 'amenities'])->options($options);
        $listbox = new Checkbox('amenities', ['Equipements']);
        $listbox->options($options)->attribute(['class' => 'amenities']);

        if($request->method() == 'POST')
        {
            $rules = [
                'property.iban' => 'required',
                'property.bic' => 'required',
                'property.name' => 'required',
                'property.address' => 'required',
            ];
            // $request->validate($rules);
            $params = $request->get('property');
            if(empty($params['billing']))
            {
                $params['billing'] = '';
            }
            $fill = array_merge(['user_id' => $user->id], $params);
            $property = Property::create($fill);
            $amenitiesValues = $request->get('amenities');
            foreach($amenities as $a)
            {
                PropertyAmenity::create([
                    'property_id' => $property->id,
                    'amenity_id' => $a->id,
                    'value' => array_search($a->id, $amenitiesValues) !== FALSE,
                ]);
            }
            $propertyDetails = PropertyDetails::create(['property_id' => $property->id]);
            $propertyDetails->fill($request->get('property_details'));
            $propertyDetails->save();
            Mailer::sendPropertyNotification($user, $property);
            //Mailer::sendUserPropertyRecall($user, $property);
            session(['status' => 'Création réussi']);
            return redirect('/property/' . $property->id);
        } else
        {
            session(['status' => null]);
        }
        return view('account.property.create', [
            'user' => $user,
            'options1' => getArray(1, 6),
            'options2' => getArray(0, 6),
            'listbox' => $listbox
        ]);
    }

    public function editAccount()
    {
        $user = auth()->user();
        return view('account.user', [
            'user' => $user
        ]);
    }

    public function documents(Request $request, $id)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $id)->first();

        return view('account.property.documents', [
            'property' => $property,
            'user' => $user,
        ]);
    }

    public function reports(Request $request, $id)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $id)->first();
        $reports = $property->getReports();
        $reportsDisplay = [];
        setlocale(LC_ALL, 'fr');
        $vision = $property->vision;
        $dateVision = new Carbon($vision);
        foreach($reports as $r)
        {
            $ca = $r->values()->where('property_id', '=', $property->id)->where('label', '=', 'user_ca')->first()->profit;
            $from = new Carbon($r->start_at);
            $time = $from->format('my');
            if($property->hasReport($time) && (!$vision || $from >= $dateVision))
            {
                $reportsDisplay[] = [
                    'report' => $r,
                    'month' => utf8_encode(ucfirst($from->formatLocalized('%B'))),
                    'year' => utf8_encode($from->formatLocalized('%Y')),
                    'order' => $from->format('ym'),
                    'ca' => $ca
                ];
            }
        }
        uasort($reportsDisplay, function($a, $b)
        {
            return $a['order'] > $b['order'];
        });


        return view('account.property.reports', [
            'property' => $property,
            'user' => $user,
            'reports' => $reportsDisplay
        ]);
    }

    public function report(Request $request, $id, $reportId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $id)->first();
        $report = Report::where('id', '=', $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $clientRef = $user->client_id;
        $time = $from->format('my'); //($from->month > 9 ? $from->month : '0' . $from->month) . substr($from->year, 2);
        $filename = str_replace('.docx', '.pdf', $property->getReportFileName($time));
        $pdfPath = __DIR__ . DIRECTORY_SEPARATOR . $filename;
        //$filename = $property->getReportFileName($time); //'report_' . $clientRef . '_' . $time . '_' . urlencode($property->name) . '.docx';
        $path = $property->getReportFile($time);
        if(file_exists($path))
        {
            convertToPdf($path, __DIR__);
            attachFile($pdfPath, $filename);
            unlink($pdfPath);
        }
    }

    public function invoice(Request $request, $id, $reportId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $id)->first();
        $report = Report::where('id', '=', $reportId)->first();
        $from = new Carbon($report->start_at);
        $to = new Carbon($report->end_at);
        $clientRef = $user->client_id;
        $time = $from->format('my'); //($from->month > 9 ? $from->month : '0' . $from->month) . substr($from->year, 2);
        $filename = str_replace('.docx', '.pdf', $property->getInvoiceFileName($time));
        $pdfPath = __DIR__ . DIRECTORY_SEPARATOR . $filename;
        //$filename = $property->getReportFileName($time); //'report_' . $clientRef . '_' . $time . '_' . urlencode($property->name) . '.docx';
        $path = $property->getInvoiceFile($time);
        if(file_exists($path))
        {
            convertToPdf($path, __DIR__);
            attachFile($pdfPath, $filename);
            unlink($pdfPath);
        }
    }

    public function signedContract($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getContractFile();
        $filename = $property->getContractFileName();
        if($property->hasContract())
        {
            attachFile($path, $filename);
        }
    }

    public function ranking($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getRankingFile();
        $filename = $property->getRankingFileName();
        if($property->hasRanking())
        {
            attachFile($path, $filename);
        }
    }

    public function attestloc($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getAttestLocFile();
        $filename = $property->getAttestLocFileName();
        if($property->hasAttestLoc())
        {
            attachFile($path, $filename);
        }
    }

    public function annexe($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getAnnexeFile();
        $filename = $property->getAnnexeFileName();
        if($property->hasAnnexe())
        {
            attachFile($path, $filename);
        }
    }

    public function inventory($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getInventoryFile();
        $filename = $property->getInventoryFileName();
        if($property->hasInventory())
        {
            attachFile($path, $filename);
        }
    }

    public function assessment($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getAssessmentFile();
        $filename = $property->getAssessmentFileName();
        if($property->hasAssess())
        {
            attachFile($path, $filename);
        }
    }

    public function keys($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getKeysFile();
        $filename = $property->getKeysFileName();
        if($property->hasKeysFile())
        {
            attachFile($path, $filename);
        }
    }

    public function uploadContract(Request $request, $propertyId)
    {
        $user = auth()->user();
        /** @var Property $property */
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getContractFolder();
        $filename = $property->getContractFileName();
        if(!$property->hasContract())
        {
            //var_dump($request->file('contract'));die;
            $file = $request->file('contract');
            $file->move($path, $filename);
            $property->contract = $property->getUploadFolder() . '/' . $property->getContractFileName();
            $property->save();
            Mailer::sendContractSentMail($property);
        }
        return redirect('/property/' . $propertyId . '/documents');
    }

    public function uploadRanking(Request $request, $propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getContractFolder();
        $filename = $property->getRankingFileName();
        $file = $request->file('ranking');
        if(!$property->hasRanking() && $file)
        {
            $file->move($path, $filename);
            $property->ranking_file = $property->getUploadFolder() . '/' . $property->getRankingFileName();
            $property->save();
        }
        return redirect('/property/' . $propertyId . '/documents');
    }

    public function uploadAttestLoc(Request $request, $propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        $path = $property->getContractFolder();
        $filename = $property->getAttestLocFileName();
        $file = $request->file('attestloc');
        if(!$property->hasAttestLoc() && $file)
        {
            $file->move($path, $filename);
            $property->attest_loc = $property->getUploadFolder() . '/' . $property->getAttestLocFileName();
            $property->save();
        }
        return redirect('/property/' . $propertyId . '/documents');
    }

    public function createcontract($propertyId)
    {
        $user = auth()->user();
        $property = $user->properties()->where('id', '=', $propertyId)->first();
        if($property)
        {
            //header('Content-Disposition: attachment; filename=' . $filename . '.pdf; charset=iso-8859-1');
            $this->generateContract($user, $property);
            //echo file_get_contents(sys_get_temp_dir(). DIRECTORY_SEPARATOR . $filename . '.pdf')  ;
        }
    }

    protected function generateContract($user, $property)
    {
        //\PhpOffice\PhpWord\Settings::setPdfRenderer(\PhpOffice\PhpWord\Settings::PDF_RENDERER_TCPDF, base_path() . '/vendor/tecnickcom/tcpdf');
        set_time_limit(240);
        $date = new Carbon();
        $date = $date->formatLocalized('%d/%m/%Y');
        $address = empty($user->address) ? $property->billing : $user->address;
        /*
          $dynaReport = new DynaReport();
          $dynaReport->setApiKey('ab0861024e4fa63efab100d3ec22e190');
          $dynaReport->uploadTemplate(__DIR__ . DIRECTORY_SEPARATOR . 'ContratJeffreyV522.docx');
          var_dump($dynaReport->getUploadedTemplateId());die;
          $dynaReport->setData([
          'pourcentage' => numericToText($property->commission, 1) . ' pourcents',
          'commission' => $property->commission . '%',
          'date' => $date,
          'name' => $user->name,
          'address' => $address,
          'property_address' => $property->address,
          ]);

          $dynaReport->generateAndDownloadReport(
          $dynaReport->getUploadedTemplateId(), __DIR__ . DIRECTORY_SEPARATOR . $property->getContractFileName()
          );
         */
        $contractName = 'ContratJeffreyhost';
        if($property->lite)
        {
            $contractName = 'ContratJeffreyhost_YELD';
        }
        if($property->bnbkeys)
        {
            $contractName = 'ContratJeffreyhost_bnbkeys';
        }
        $company = $user->company;
        if($company && !empty($company->name)){
            $contract = new TemplateProcessor(__DIR__ . DIRECTORY_SEPARATOR . 'contracts' . DIRECTORY_SEPARATOR . $contractName.'_company.docx');
            $contract->setValue('company', $company->name);
            $contract->setValue('address', $company->address);
            $contract->setValue('number', $company->company_number);
        }else{
            $contract = new TemplateProcessor(__DIR__ . DIRECTORY_SEPARATOR . 'contracts' . DIRECTORY_SEPARATOR . $contractName.'.docx');
            $contract->setValue('name', $user->name);
            $contract->setValue('address', $address);
        }
        if($property->bnbkeys){
            $contract->setValue('all_inclusive', $property->commission == 23 ? 'X' : '_');
            $contract->setValue('standard', $property->commission == 18 ? 'X' : '_');
        }

        $contract->setValue('property_address', $property->address);
        $contract->setValue('pourcentage', numericToText($property->commission, 1) . ' pourcents');
        $contract->setValue('commission', $property->commission . '%');
        $contract->setValue('liteprice', floor(($property->commission / 100) * 20000) / 100);
        $contract->setValue('ville', 'Bray');
        $contract->setValue('date', $date);
        $fileName = str_replace('.pdf', '.docx', __DIR__ . DIRECTORY_SEPARATOR . $property->getContractFileName());
        $contract->saveAs($fileName);
        convertToPdf($fileName, __DIR__);
        unlink($fileName);
        //$apiUrl = 'https://sandbox.zamzar.com/v1/';
        /*$apiUrl = 'https://api.zamzar.com/v1/';

        $endpoint = $apiUrl . 'jobs';
        $apiKey = '129529149d375a25a72b8cee59c3afe0d9377118';
        $targetFormat = 'pdf';
        // Since PHP 5.5+ CURLFile is the preferred method for uploading files
        $sourceFile = curl_file_create($fileName);

        $postData = array(
            'source_file' => $sourceFile,
            'target_format' => $targetFormat
        );
        $ch = curl_init(); // Init curl
        curl_setopt($ch, CURLOPT_URL, $endpoint); // API endpoint
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        if(config('app.env') !== "production") {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        //curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false); // Enable the @ prefix for uploading files
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return response as a string
        curl_setopt($ch, CURLOPT_USERPWD, $apiKey . ':'); // Set the API key as the basic auth username
        $body = curl_exec($ch);
        curl_close($ch);

        $job = json_decode($body, true);
        unlink($fileName);

        if(isset($job['errors'])){
            Mailer::alertDev($job['errors']);
            die('Une erreur s\'est produite, une alerte a été envoyé, prenez contact avec JeffreyHost pour plus d\'informations');
        }
        $jobId = $job['id'];
        $endpoint = $apiUrl . 'jobs/' . $jobId;
        $status = $job['status'];
        while($status !== 'successful' || !array_key_exists('target_files', $job))
        {
            $ch = curl_init(); // Init curl
            curl_setopt($ch, CURLOPT_URL, $endpoint); // API endpoint
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return response as a string
            curl_setopt($ch, CURLOPT_USERPWD, $apiKey . ':'); // Set the API key as the basic auth username
            if(config('app.env') !== "production") {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            }
            $body = curl_exec($ch);
            curl_close($ch);

            $job = json_decode($body, true);
            $status = $job['status'];
            if($status !== 'successful')
            {
                sleep(2);
            }
        }
        $fileID = $job['target_files'][0]['id'];
        $localFilename = __DIR__ . DIRECTORY_SEPARATOR . $property->getContractFileName();
        $endpoint = $apiUrl . 'files/' . $fileID . '/content';

        $ch = curl_init(); // Init curl
        curl_setopt($ch, CURLOPT_URL, $endpoint); // API endpoint
        curl_setopt($ch, CURLOPT_USERPWD, $apiKey . ':'); // Set the API key as the basic auth username
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        if(config('app.env') !== "production") {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        $fh = fopen($localFilename, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fh);

        $body = curl_exec($ch);
        curl_close($ch);
        */
        attachFile(__DIR__ . DIRECTORY_SEPARATOR . $property->getContractFileName(), $property->getContractFileName());
        unlink(__DIR__ . DIRECTORY_SEPARATOR . $property->getContractFileName());
    }
    
    protected function getCharts($lava, $property, $date_du, $date_au)
    {
        $locale = session()->get('applocale');

        // STATS
        /* @var $property Property */
        $bookings = $property->bookings()
                    ->where('start_at', '>=', $date_du)
                    ->where('end_at', '<=', $date_au)
                    ->where('status', '=', 'Booked')
                    ->where('paid', '=', 1)
                    ->whereNotNull('bookingsync_id')
                    ->orderBy('end_at', 'asc');
            
        if(!empty($property->vision)){
            $bookings = $bookings->where('end_at', '>=', $property->vision);
        }
        $bookings = $bookings->get();
        /* @var $bookings \Illuminate\Database\Eloquent\Collection */

        $datas = [];
        $daysInMonth = [];
        if(count($bookings))
        {
            $dateStart = Carbon::parse($date_du);
            $dateEnd = Carbon::parse($date_au);
            // $dateStart = new Carbon($bookings[0]->end_at);
            // $dateEnd = (new Carbon())->startOfMonth()->seconds(-1);
            while($dateStart->diffInMonths($dateEnd) > 0)
            {
                $datas[$dateStart->format('Y-m')] = [];
                $dateStart->addMonth();
            }
            $datas[$dateStart->format('Y-m')] = [];
        }
        $groupBookings = $bookings->groupBy(function($val)
        {
            return Carbon::parse($val->end_at)->format('Y-m');
        });


        $datas = array_merge($datas, $groupBookings->toArray());
        $results = aggregateBookings($datas);
        //valdebug($datas);die;
        $columns = [
            [
                'title' => $locale == "fr" ? 'Revenues propio' : 'Owner income',
                'name' => 'owner',
                'suffix' => ' €'
            ],
        ];

        bookingsChart($lava, 'ColumnChart', $results, 'Stats-'.$property->id, $columns, ['#5A5'], FALSE, FALSE, 250);
        $columns = [
            [
                'title' => $locale == "fr" ? 'Nuités' : 'Overnight stays',
                'name' => 'days',
                'suffix' => $locale == "fr" ? ' nuités' : ' nights'
            ],
            [
                'title' => $locale == "fr" ? 'Réservations' : 'Reservations',
                'name' => 'count',
                'suffix' => $locale == "fr" ? ' réservations' : ' reservations'
            ],
        ];
        bookingsChart($lava, 'ColumnChart', $results, 'Nuités-'.$property->id, $columns, ['#08F', '#55A', '#cb5fd3', '#ff7f00'], true, FALSE, 250);
        $columns = [
            [
                'title' => $locale == "fr" ? 'Taux d\'occupations' : 'Average occupancy rate',
                'name' => 'occupied',
                'suffix' => '%'
            ],
        ];
        $global = bookingsChart($lava, 'ColumnChart', $results, 'Taux-'.$property->id, $columns, ['#ff7f00'], false, FALSE, 250);
        
        
        $statsGlobal = $locale == "fr" 
                            ? '<h4>Taux d\'occupation moyen : ' . $global['occupied'] . '%</h4>'
                            : '<h4>Average occupancy rate : ' . $global['occupied'] . '%</h4>';
        $statsGlobal .= $locale == "fr" 
                            ? '<h4>Durée moyenne de séjour : ' . $global['avgNights'] . '</h4>'
                            : '<h4>Average length of stay : ' . $global['avgNights'] . '</h4>';
        $statsGlobal .= '<div class="row">';
        $statsGlobal .= '<div class="col-md-12 min-chart" id="stats-chart-' . $property->id . '"></div>' . $lava->render('ColumnChart', 'Stats-'.$property->id, 'stats-chart-' . $property->id);
        $statsGlobal .= '<div class="col-md-12 min-chart" id="taux-chart-' . $property->id . '"></div>' . $lava->render('ColumnChart', 'Taux-'.$property->id, 'taux-chart-' . $property->id);
        $statsGlobal .= '<div class="col-md-12 min-chart" id="reservations-chart-' . $property->id . '"></div>' . $lava->render('ColumnChart', 'Nuités-'.$property->id, 'reservations-chart-' . $property->id);
        $statsGlobal .= '</div>';
        return $statsGlobal;
    }

    public function cgv(Request $request){
        if(auth()->user()->cgv){
            return redirect()->route('home');
        }

        if($request->method() === 'POST'){
            $this->validate($request, [
                'cgv' => 'required'
            ]);
            $user= auth()->user();
            $user->cgv = true;
            try {
                $user->save();
            } catch (\Exception $e) {
                throw new \Exception($e);
            }
            if($request->get('redirect')) {
                return redirect($request->get('redirect'));
            }else{
                return redirect()->route('home');
            }
        }
        return view('account.cgv',[
            'redirect' => $request->get('redirect')
        ]);
    }
}
