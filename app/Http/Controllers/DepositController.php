<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Checkout\Session as CheckoutSession;
use Stripe\PaymentIntent;
use Stripe\Customer;
use Exception;
use Mail;
use App\Helpers\Mailer;
use App\Deposit;
use Carbon\Carbon;

class DepositController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function depositConfirm(Request $request, $id)
    {
        $params = $request->all();
        $token = $request->get("token");
        $sessionId = $request->get("session");
        $deposit = Deposit::where("token", "=", $token)->first();
        if($deposit && $id == $deposit->id && $deposit->last_session)
        {
            Stripe::setApiKey(config("stripe.secretKey"));
            $session = CheckoutSession::retrieve($deposit->last_session);
            $payment = PaymentIntent::retrieve($session->payment_intent);
            if($payment && $payment->amount_received == $payment->amount && $this->stripeStatusAuthorized($payment->status) && count($payment->charges) > 0)
            {
                $deposit->paid = true;
                $deposit->pay_id = $payment->id;
                $charge = $payment->charges->getIterator()->current();
                if($charge && $charge->payment_method_details && isset($charge->payment_method_details->card)){
                    $card = $charge->payment_method_details->card;
                    $deposit->card_brand = $card->brand;
                    $deposit->card = "XXXXXXXXXXXX" . $card->last4;
                    $deposit->expire_date = $card->exp_month.'/'.$card->exp_year;
                    $deposit->PM = $card->funding;
                }
                $deposit->status = $payment->status;
                $deposit->pay_date = new Carbon();
                $deposit->save();
                Mailer::sendDepositConfirmation($deposit);
                Mailer::sendDepositConfirmationToJeffrey($deposit);
                return redirect("/deposit/" . $id . "?token=" . $token);
            }
        }
        echo "PAYMENT ERROR - your payment hasn't been received, you may reload this page if you think this is an error";
    }

    public function depositConfirmWithoutCapture(Request $request, $id)
    {
        $params = $request->all();
        $token = $request->get("token");
        $sessionId = $request->get("session");
        $deposit = Deposit::where("token", "=", $token)->first();
        $client = json_decode($deposit->client);
        $test = (config("app.env") !== "production");
        if($deposit && $id == $deposit->id && $deposit->last_session)
        {
            Stripe::setApiKey(config("stripe.secretKey"));
            $session = CheckoutSession::retrieve($deposit->last_session);

            $setupintent = \Stripe\SetupIntent::retrieve($session->setup_intent);
            $pm_id = $setupintent->payment_method;
            $pm = \Stripe\PaymentMethod::retrieve($pm_id);
            if($pm->customer){
                $customer = \Stripe\Customer::retrieve($pm->customer);
            }else{
                $customer = \Stripe\Customer::create([
                    'name' => $client->firstname . " " . $client->lastname
                ]);
                $pm->attach(['customer' => $customer->id]);
            }
            if($pm && $customer){
                $payment = PaymentIntent::create([
                    'amount' => $test ? 100 : $deposit->amount * 100,
                    'currency' => 'eur',
                    'payment_method_types' => ['card'],
                    'payment_method' => $pm_id,
                    'capture_method' => 'manual',
                    'customer' => $customer->id,
                    'confirm' => true
                ]);
                if($payment && $payment->amount_capturable == $payment->amount && $this->stripeStatusAuthorized($payment->status) && count($payment->charges) > 0)
                {
                    $deposit->paid = true;
                    $deposit->pay_id = $payment->id;
                    $charge = $payment->charges->getIterator()->current();
                    if($charge && $charge->payment_method_details && isset($charge->payment_method_details->card)){
                        $card = $charge->payment_method_details->card;
                        $deposit->card_brand = $card->brand;
                        $deposit->card = "XXXXXXXXXXXX" . $card->last4;
                        $deposit->expire_date = $card->exp_month.'/'.$card->exp_year;
                        $deposit->PM = $card->funding;
                    }
                    $deposit->status = $payment->status;
                    $deposit->pay_date = new Carbon();
                    $deposit->save();
                    try{Mailer::sendDepositConfirmation($deposit);}catch(Exception $e){}
                    try{Mailer::sendDepositConfirmationToJeffrey($deposit);}catch(Exception $e){}
                    return redirect("/deposit/" . $id . "?token=" . $token);
                }
            }
        }
        echo "PAYMENT ERROR - your payment hasn't been confirmed, try again <a href='".url("/deposit/" . $id . "?token=" . $token)."'>here</a>";
    }

    private function stripeStatusAuthorized($status)
    {
        return $status == "succeeded" ;//|| $status == 'requires_capture';
    }
    
    private function ingenicoStatusAuthorized($status)
    {
        switch($status)
        {
            case "5":
            case "51":
            case "4":
            case "41":
            case "9":
            case "91":
                return true;
            default:
                return false;
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function deposit(Request $request, $id)
    {
        $token = $request->get("token");
        $deposit = Deposit::where("token", "=", $token)->first();
        //var_dump(config("stripe.secretKey"));
        if($deposit && $id == $deposit->id)
        {
            /*$pspID = "JeffreyConciergerieLimited";
            $pspKey = "4267d106-ab78-43ad-b2f5-8878012d321c";*/
            $client = json_decode($deposit->client);

            $shain = null;
            $lang = $client->lang == "fr" ? "fr_FR" : "en_EN";
            $test = (config("app.env") !== "production");
            Stripe::setApiKey(config("stripe.secretKey"));
            $session = null;
            $params = [];
            if(!$deposit->paid)
            {
                if(!$test)
                {


                    $session = CheckoutSession::create([
                        'payment_method_types' => ['card'],
                        'customer_email' => $client->email,
                        'line_items' => [[
                            'name' => $client->lang != "fr" ? "Deposit" : "Caution",
                            'description' => $client->lang != "fr" ? "Damage deposit for your trip in " . $deposit->city : "Paiement de la caution pour votre séjour à " . $deposit->city,
                            'amount' => $deposit->amount * 100,
                            'currency' => 'eur',
                            'quantity' => 1,
                        ]],
                        'success_url' => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                        'cancel_url' => url("/deposit/" . $deposit->id) . "?token=" . $deposit->token,
                    ]);


                } else
                {

                    $session = CheckoutSession::create([
                        'payment_method_types' => ['card'],
                        'customer_email' => "vfran123@gmail.com",
                        'line_items' => [[
                            'name' => $client->lang != "fr" ? "Deposit" : "Caution",
                            'description' => $client->lang != "fr" ? "Damage deposit for your trip in " . $deposit->city : "Paiement de la caution pour votre séjour à " . $deposit->city,
                            'amount' => 1 * 100,
                            'currency' => 'eur',
                            'quantity' => 1,
                        ]],
                        'success_url' => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                        'cancel_url' => url("/deposit/" . $deposit->id) . "?token=" . $deposit->token,
                    ]);
                }
                $deposit->last_session = $session->id;
                $deposit->save();
            }

            return view('deposit', [
                "deposit" => $deposit,
                "client" => $client,
                "checkoutSessionId" => $deposit->last_session,
                "token" => $token,
                "params" => $params,
                //"shain" => $shain,
                "lang" => $lang,
                "startAt" => new Carbon($deposit->start_at),
                "endAt" => new Carbon($deposit->end_at)
            ]);
        }
        return view('welcome', []);
    }






    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function depositWithoutCapture(Request $request, $id)
    {
        $token = $request->get("token");
        $deposit = Deposit::where("token", "=", $token)->first();
        //var_dump(config("stripe.secretKey"));
        if($deposit && $id == $deposit->id)
        {
            /*$pspID = "JeffreyConciergerieLimited";
            $pspKey = "4267d106-ab78-43ad-b2f5-8878012d321c";*/
            $client = json_decode($deposit->client);

            $shain = null;
            $lang = $client->lang == "fr" ? "fr_FR" : "en_EN";
            $test = (config("app.env") !== "production");
            Stripe::setApiKey(config("stripe.secretKey"));
            $session = null;
            $params = [];
            if(!$deposit->paid)
            {
                if(!$test)
                {
                    $session = CheckoutSession::create([
                        'payment_method_types' => ['card'],
                        'customer_email' => $client->email,
                        'mode' => 'setup',
                        'success_url' => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                        'cancel_url' => url("/deposit/" . $deposit->id) . "?token=" . $deposit->token,
                    ]);
                } else
                {
                    $session = CheckoutSession::create([
                        'payment_method_types' => ['card'],
                        'customer_email' => "vfran123@gmail.com",
                        'mode' => 'setup',
                        'success_url' => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                        'cancel_url' => url("/deposit/" . $deposit->id) . "?token=" . $deposit->token,
                    ]);
                }
                $deposit->last_session = $session->id;
                $deposit->save();
            }

            return view('deposit', [
                "deposit" => $deposit,
                "client" => $client,
                "checkoutSessionId" => $deposit->last_session,
                "token" => $token,
                "params" => $params,
                //"shain" => $shain,
                "lang" => $lang,
                "startAt" => new Carbon($deposit->start_at),
                "endAt" => new Carbon($deposit->end_at)
            ]);
        }
        return view('welcome', []);
    }

}



/* "PSPID" => $pspID,
                              "ORDERID" => $deposit->booking_id . "-caution",
                              "AMOUNT" => 100,
                              "CURRENCY" => "EUR",
                              "LANGUAGE" => $lang,
                              "CN" => "Valentin Franck",
                              "EMAIL" => $client->email,
                              "TITLE" => $client->lang != "fr" ? "Damage deposit for your trip in " . $deposit->city : "Paiement de la caution pour votre séjour à " . $deposit->city,
                              "LOGO" => "logo.jpg",
                              "WIN3DS" => "MAINW",
                              "ACCEPTURL" => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                              "DECLINEURL" => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                              "EXCEPTIONURL" => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token,
                              "CANCELURL" => url("/deposit/" . $deposit->id) . "/confirm?token=" . $deposit->token, */


//$payment = PaymentIntent::retrieve($session->payment_intent);
            /* uksort($params, 'strnatcasecmp');
              unset($params["token"]);
              $shasign = $params["SHASIGN"];
              unset($params["SHASIGN"]);
              /* echo "<pre>";
              var_dump($params);
              echo "</pre>";
              echo "<br/>"; */
            /* $key = "6s6bua1iuS3QCImHiN5410LwexqTKVgZ";
              $values = [];
              foreach($params as $k => $p)
              {
              $values[] = strtoupper($k) . "=$p";
              }
              $shaout = strtoupper(hash('sha512', implode($key, $values) . $key));
              /* echo implode($key, $values).$key."<br/>";
              echo "<br/>";
              echo $shasign."<br/>";
              echo "<br/>";
              echo $shaout;
              echo "<br/>";
              echo "<br/>";
              echo "RESPONSE INTEGRITY :"; */
            //var_dump($shasign == $shaout);
            /* if($shasign === $shaout)
              { */



            /* if(array_key_exists("STATUS", $params) && $this->ingenicoStatusAuthorized($params["STATUS"]))
              {
              $deposit->paid = true;
              $deposit->pay_id = $params["PAYID"];
              $deposit->card_brand = $params["BRAND"];
              $deposit->card = $params["CARDNO"];
              $deposit->expire_date = $params["ED"];
              $deposit->PM = $params["PM"];
              $deposit->status = $params["STATUS"];
              $deposit->pay_date = new Carbon($params["TRXDATE"]);
              $deposit->save();
              Mailer::sendDepositConfirmation($deposit);
              Mailer::sendDepositConfirmationToJeffrey($deposit);
              return redirect("/deposit/" . $id . "?token=" . $token);
              } */
            //}


/*
             * ksort($params);
              $values = [];
              foreach($params as $k => $p)
              {
              $values[] = "$k=$p";
              }
              $shain = hash('sha512', implode($pspKey, $values) . $pspKey);
             */
