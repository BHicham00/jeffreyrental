<?php

namespace App\Http\Controllers\Guest;

use App\BookingGuest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\MessageBag;
use Mail;
use ZipArchive;
use DOMDocument;
use App\Helpers\Mailer;
use App\User;
use App\Amenity;
use App\Property;
use App\PropertyAmenity;
use App\PropertyDetails;
use App\Report;
use Carbon\Carbon;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Admin\Extensions\Form\Field\Checkbox;
use Khill\Lavacharts\Lavacharts;
use App\BookingSync\BookingSync;
use Dompdf\Dompdf;

class AccountController extends Controller
{
    protected $bookingSync;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        /** @var $user BookingGuest */
        $user = auth()->user();
        $booking = $user->booking;
        return view('guest.account.home', [
            'user' => $user,
            'booking' => $booking,
        ]);
    }


    public function identificationFile(){
        $user = auth()->user();
        if(file_exists($user->ident_file)) {
            $extension = explode('.', $user->ident_file);
            $extension = $extension[count($extension)-1];
            attachFile($user->ident_file, $user->firstname . '-' . $user->lastname . '.' . $extension);
        }
    }

    public function updateCheckin(Request $request){
        $user = auth()->user();
        if($request->validate(['arrival' => 'required','checkin' => 'required'])){
            $user->fill([
                'arrival' => Carbon::createFromFormat('d/m/Y H:i', $request->arrival),
                'checkin' => Carbon::createFromFormat('d/m/Y H:i', $request->checkin),
                'checkout' => Carbon::createFromFormat('d/m/Y H:i', $request->checkout),
            ]);
            try {
                $user->save();
            } catch (\Exception $e) {
                throw new \Exception($e);
            }
            
        }
        return redirect(route('guest.home'));
    }

    public function sendKeycode(){
        $user = auth()->user();
        if($user->booking->deposit && !$user->booking->deposit->paid) {
            return redirect(route('guest.home'))->withErrorUser(new MessageBag(['title' => __('Vous devez payer votre caution avant de pouvoir accéder à ces informations !')]));
        }else if($user->booking->property->keynest_auto){
            Mailer::sendGuestKeyCode($user);
        }
        return redirect(route('guest.home'))->withSuccessUser(new MessageBag(['title' =>'Mail envoyé']));
    }

    public function sendCheckout(){
        $user = auth()->user();
        if($user->booking->deposit && !$user->booking->deposit->paid){
            return redirect(route('guest.home'))->withErrorUser(new MessageBag(['title' => __('Vous devez payer votre caution avant de pouvoir accéder à ces informations !')]));
        }else if($user->booking->property->keynest_auto_checkout){
            Mailer::sendGuestAutoCheckout($user);
        }
        return redirect(route('guest.home'))->withSuccessUser(new MessageBag(['title' =>'Mail envoyé']));
    }

}
