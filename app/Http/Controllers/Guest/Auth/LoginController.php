<?php

namespace App\Http\Controllers\Guest\Auth;

use App\Booking;
use App\BookingGuest;
use App\Helpers\Mailer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:bookinguest')->except('logout');
    }


    public function showLoginForm()
    {
        if(session()->get('email_sent_to')){
            return view('guest.auth.login_step2');
        }
        return view('guest.auth.login');
    }

    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'booking_id' => 'required',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(\Illuminate\Http\Request $request)
    {
        return $request->only($this->username(), 'password', 'booking_id');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('bookinguest');
    }
    
    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if($request->get($this->username()) && (!session()->get('email_sent_to') || session()->get('email_sent_to') !== $request->get($this->username()))){
            $user = BookingGuest::where('email', $request->get($this->username()))
                ->selectRaw("booking_guests.*")
                ->leftJoin('bookings', 'bookings.id', '=', 'booking_guests.booking_id')
                ->where('bookings.reference', $request->get('reference'))
                ->first();
            if($user){
                session()->put('email_sent_to', $request->get($this->username()).'|'.$request->get('reference'));
                $pass = rand(1000,9999);
                $user->one_time_pass = bcrypt($pass);
                try {
                    $user->save();
                } catch (\Exception $e) {
                    throw new \Exception($e);
                }
                Mailer::sendOneTimePass($user, $pass);
            }else{
                return $this->sendFailedLoginResponse($request);
            }
            return view('guest.auth.login_step2');
        }else if($request->get('password') && session()->get('email_sent_to')) {
            $sessionInfo = explode('|', session()->get('email_sent_to'));
            $booking = Booking::where('reference', $sessionInfo[1])->first();
            if ($booking) {
                $request->merge([
                    $this->username() => $sessionInfo[0],
                    'booking_id' => $booking->id,
                ]);
            }
            //dd($request->all());
        }
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
