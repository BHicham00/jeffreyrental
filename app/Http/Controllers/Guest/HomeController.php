<?php

namespace App\Http\Controllers\Guest;

use App\Booking;
use App\BookingGuest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag;
use Log;
use App\Helpers\Mailer;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function setLocale(Request $request){
        $langs = config('app.all_langs');
        $lang = $request->get('lang');
        if(in_array($lang, $langs)){
            session()->put('lang', $lang);
        }
        return redirect()->back();
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function precheckin(Request $request, $reference)
    {
        $booking = Booking::where("reference", "=", $reference)->first();

        if($booking && !$booking->bookingguest)
        {
            $guest = json_decode($booking->guest);
            if($request->method() == "POST")
            {
                $rules = [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required',
                    'transport_type' => 'required',
                    'ident_type' => 'required',
                    'ident_number' => 'required',
                    'ident_file' => 'required',
                    'ident_expiration' => 'required',
                    'country_id' => 'required',
                    'arrival' => 'required',
                    'checkin' => 'required',
                    'checkout' => 'required',
                    'g-recaptcha-response' => 'required|recaptcha',
                ];
                if($booking->source !== "Airbnb"){
                    $rules = array_merge($rules, [
                        'card_type' => 'required',
                        'card_holder' => 'required',
                        'card_last_digits' => 'required',
                        'card_expiration' => 'required'
                    ]);
                }
                $request->validate($rules);
                $data = $request->all();
                $data["booking_id"] = $booking->id;

                //var_dump($request->file('contract'));die;
                $file = $request->file('ident_file');
                $path = $booking->getFileFolder();
                $filename = $booking->getIdentFileName($file->extension());
                $file->move($path, $filename);
                $file = $booking->getUploadFolder() . '/' . $filename;
                $data['ident_file'] = $file;
                $data['arrival'] = Carbon::createFromFormat('d/m/Y H:i', $data['arrival']);
                $data['checkin'] = Carbon::createFromFormat('d/m/Y H:i', $data['checkin']);
                $data['checkout'] = Carbon::createFromFormat('d/m/Y H:i', $data['checkout']);
                $data['ident_expiration'] = Carbon::createFromFormat('d/m/Y', $data['ident_expiration']);
                $user = BookingGuest::create($data);
                try
                {
                    if($user->id)
                    {
                        Mailer::sendGuestAccess($user);
                        return redirect(route('guest.login'))->withSuccessUser(new MessageBag(['title' =>'Registration successful, check your emails.']));
                    }
                } catch(Exception $e)
                {
                    return $e;
                }
            }

            return view('guest.precheckin', [
                "booking" => $booking,
                "guest" => $guest,
                "identTypes" => BookingGuest::getIdentTypes(),
                "transportTypes" => BookingGuest::getTransportTypes(),
                "cardTypes" => BookingGuest::getCardTypes(),
            ]);
        }
        return redirect(route('guest.welcome'));
    }

    public function index()
    {
        /*  @var $icalevents ICalEvents */
        $content = "";
        $user = auth('bookinguest')->user();
        return view('home', ["content" => $content, "user" => $user]);
    }
}

