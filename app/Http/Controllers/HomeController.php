<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Deposit;
use App\KeyLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Log;
use App\Helpers\Mailer;
use App\User;
use App\Company;
use App\Invitation;
use App\Property;
use App\PropertyDetails;
use App\Amenity;
use App\PropertyAmenity;
use Encore\Admin\Form\Field\Checkbox;
use Illuminate\Support\Facades\Auth;
use App\BookingSync\BookingSync;
use App\City;
use App\Country;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function timeouttest(){
        set_time_limit(240);
        sleep(120);
        echo "bravo, votre page s'est chargé en plus de 60sec";
        die;
    }

    // public function getBookings(){
    //     $bookingsToSync = [];
    //     $bookings = Booking::where("start_at",">=",date('y-m-d'))->get();
    //     $count = 0;
    //     foreach($bookings as $b){
    //         $b->property()->get()[0]->name;
    //         $count++;
    //         $bookingsToSync = [
    //             "name" => $b->property()->get()[0]->name,
    //         ];
    //     }
    //     echo '  ' .$count;
    // }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function invitation(Request $request)
    {
        $token = $request->get("entry");
        $invitation = Invitation::where("token", "=", $token)->first();
        if($invitation)
        {
            $clientId = $invitation->client_id;
            $email = $invitation->email;
            $options = [];
            $amenities = Amenity::all();
            foreach($amenities as $a)
            {
                $options[$a->id] =  __($a->title);
            }
            //$form->checkbox('amenities', 'Equipement')->attribute(["class" => "amenities"])->options($options);
            $listbox = new Checkbox('amenities', ["Equipements"]);
            $listbox->options($options)->attribute(["class" => "amenities"]);
            if($request->method() == "POST")
            {
                $rules = [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'email' => 'unique:users|required',
                    'phone' => 'required',
                    'address' => 'required',
                    'password' => 'required',
                    'password_confirm' => 'required',
                    'properties.0.iban' => 'required',
                    'properties.0.bic' => 'required',
                    'properties.0.name' => 'required',
                    'properties.0.address' => 'required',
                    'cgv' => 'required',
                    'g-recaptcha-response' => 'required|recaptcha',
                ];
                $request->validate($rules);
                $user = User::create([
                    'client_id' => $clientId,
                    'name' => $request->get('firstname') . ' ' . $request->get('lastname'),
                    'email' => $request->get('email'),
                    'phone' => $request->get('phone'),
                    'address' => $request->get('address'),
                    'tva_number' => empty($request->get('tva_number')) ? null : $request->get('tva_number'),
                    'bnbkeys' => $invitation->bnbkeys,
                    'password' => bcrypt($request->get('password')),
                    'cgv' => true
                ]);
                try
                {
                    if($user->id)
                    {
                        if($request->get('society') == "company")
                        {
                            Company::create([
                                "user_id" => $user->id,
                                "name" => $request->get('company'),
                                "capital" => $request->get('capital'),
                                "address" => $request->get('company_address'),
                                "company_number" => $request->get('company_number'),
                            ]);
                        }
                        $property = Property::create(array_merge([
                            'user_id' => $user->id,
                            'commission' => $invitation->commission,
                            'bnbkeys' => $invitation->bnbkeys,
                            'lite' => $invitation->lite
                        ], $request->get('properties')[0]));
                        if($property->id)
                        {
                            $amenitiesValues = $request->get('amenities');
                            foreach($amenities as $a)
                            {
                                PropertyAmenity::create([
                                    'property_id' => $property->id,
                                    'amenity_id' => $a->id,
                                    'value' => array_search($a->id, $amenitiesValues) !== FALSE,
                                ]);
                            }
                            $propertyDetails = PropertyDetails::create(array_merge(['property_id' => $property->id], $request->get('properties')[0]['property_details']));
                            if($propertyDetails->id)
                            {
                                if(Auth::attempt($request->only('email', 'password')))
                                {
                                    // Authentication passed...
                                    $invitation->delete();
                                    Mailer::sendConfirmation($user);
                                    Mailer::sendRentMail($user);
                                    //Mailer::sendUserPropertyRecall($user, $property);

                                    Mailer::sendNotification($user);
                                    return redirect()->action('AccountController@index');
                                }
                            }
                        }
                    }
                } catch(Exception $e)
                {
                    return $e;
                }
            }

            return view('invitation', [
                "clientId" => $clientId,
                "email" => $email,
                "token" => $token,
                "options1" => getArray(1, 6),
                "options2" => getArray(0, 6),
                "listbox" => $listbox
            ]);
        }
        return view('welcome', []);
    }
    public function syncRentals(Request $request){

        $this->bookingSync = new BookingSync(
            config('bookingsync.client'),
            config('bookingsync.secret'), 
            url('syncrentals')
        );

        $property = Property::where('id', '=', $request->sync)->first();
        $property_details = PropertyDetails::where('property_id','=',$request->sync)->first();
        $amen = $property->amenities()->get();
        $city = City::find($property->city_id);
        $country = Country::find($property->country_id);
        $options = [];
        $goodStuff = [];
        $amenities = [];
        $i = 0;
        foreach($amen as $pa)
        {
            $a = $pa->amenity;
            if($pa->value)
            {
                $goodStuff[$a->id] = $a->id;
            }
            //$options[$a->id] =  __($a->title);
        }
        foreach($goodStuff as $gs){
            $amenities[$i] =  $gs;
            $i++;
        }

        $data = [
            "name" => $property->name,
            "amenity_ids" => $amenities,
            "headline" => [
                "en" => $property_details->title,
                "fr" => $property_details->title,
                "it" => $property_details->title,
                "ru" => $property_details->title,
                "es" => $property_details->title,
                "de" => $property_details->title,
                "pt" => $property_details->title,
                "ar" => $property_details->title,
            ],
            
            "summary_en" =>$property_details->summary,
            "summary_fr" =>$property_details->summary,
            "summary_it" =>$property_details->summary,
            "summary_ru" =>$property_details->summary,
            "summary_es" =>$property_details->summary,
            "summary_de" =>$property_details->summary,
            "summary_pt" =>$property_details->summary,
            "summary_ar" =>$property_details->summary,

            "description" => [
                "en" => $property_details->description,
                "fr" => $property_details->description,
                "it" => $property_details->description,
                "ru" => $property_details->description,
                "es" => $property_details->description,
                "de" => $property_details->description,
                "pt" => $property_details->description,
                "ar" => $property_details->description,
            ],
            "rental_type" => $property_details->rental_type,
            "bedrooms_count" => $property_details->beds,
            "sleeps" => $property_details->sleeps,
            "sleeps_max" => $property_details->sleepsMax,
            "bathrooms_count" => $property_details->bathrooms,
            "surface" => $property_details->surface,
            "surface_unit" => "metric",
            "state" => $property->departement,
            "address1" => $property->address,
            "floor" => $property_details->floor,
            "checkin_time" => $property_details->arriving_time,
            "checkin_end_time" => $property_details->arriving_time_max,
            "checkout_time" => $property_details->begin_time,
        ];

            if(!is_null($city)){
                $data['city'] = $city->nom_commune;
                $data['zip'] = $city->code_postal;
            }if(!is_null($country)){
                $data['country_code'] = $country->iso;
            }if(strcmp($property_details->residence,"primary_residence") == 0 
                || strcmp($property_details->residence,"secondary_residence") == 0 
                || strcmp($property_details->residence,"non_residential") == 0){
                    $data['residency_category'] = $property_details->residence;
            }if(strcmp($property_details->Management_type,"professional") == 0 
                || strcmp($property_details->Management_type,"non_professional") == 0 ){
                    $data['management_type'] = $property_details->Management_type;
            }
            //synchronize
            if($property["bookingsync_id"] == 0){
                $res = $this->bookingSync->addRental($data,$request->sync);
            }else{
                $res = $this->bookingSync->updateRental($data,$property["bookingsync_id"]);
            }
            //check the result if there is any errors
            if(array_key_exists('error', $res)){
                admin_toastr("il y a une erreur lors de la synchronisation de la propriete","error");
                return redirect()->route('admin.properties.index');
            }
            else if(array_key_exists('errors', $res)){
                admin_toastr("il y a une erreur lors de la synchronisation de la propriete","error");
                return redirect()->route('admin.properties.index');
            }else{
                //add bookingsync id to database
                $bookingsync_id = $res['rentals'][0]['id'];
                Property::find($request->sync)->update(['bookingsync_id' => $bookingsync_id]);
                admin_toastr("La propriete est synchronisee avec succes","success");
                return redirect()->route('admin.properties.index');
            }
    }

    public function index()
    {
        /*  @var $icalevents ICalEvents */
        $content = "";
        $user = auth()->user();
        return view('home', ["content" => $content, "user" => $user]);
    }
    
    public function keynestWebhook(Request $request){
        if($request->id === "gfiuzhdikegfz541dqzd65") {
            $body = $request->getContent();
            Log::channel('keynest')->info($body);
            $body = json_decode($body);

            if (isset($body->KeyId)) {
                $property = Property::where('keynest_key_id', $body->KeyId)->first();
                if ($property) {
                    $date = new Carbon($body->WhenHappened);
                    KeyLog::create([
                        'property_id' => $property->id,
                        'date' => $date,
                        'event' => $body->EventName,
                        'status' => $body->CurrentStatus,
                        'previous_status' => $body->PreviousStatus,
                        'code_used' => $body->Code,
                        'store_id' => $body->StoreId
                    ]);
                }
            }
        }
        return null;
    }

    public function bsSync(Request $request){

        $bookingsync = new BookingSync(
            config('bookingsync.client'),
            config('bookingsync.secret'),
            "", true
        );
        
        $scrt = config('bookingsync.secret');
        $sign = $request->header('X-Content-Signature');
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);
        $json = base64_encode($json);
        $json = chunk_split($json, 60, "\n");

        $hash = hash_hmac('sha1', $json, $scrt);

        if($hash === $sign){
            $bookingSync_id = $data["resource"]["booking"]["id"];
            $distBook = $data["resource"]["booking"];
            /* @var Booking $booking */
            switch($data["event"]){
                case 'booking_created':
                case 'booking_updated':
                    $booking = $bookingsync->registerBooking($distBook, true);
                    break;
                case 'booking_destroyed':
                    $booking = Booking::where('bookingsync_id', $bookingSync_id)->first();
                    if($booking)
                        $booking->delete();
                    $deposit = Deposit::where('booking_id', $bookingSync_id)->first();
                    if($deposit)
                        $deposit->delete();
                    break;
            }
            \Log::info('booking '. $bookingSync_id . ' : ' . $data["event"] . ' --- ' . Carbon::now());
        }
    }

    public function jhCgv(){
        return response()->file(storage_path('app/jeffreyhost_cgv.pdf'));
    }

}

