<?php

namespace App\Http\Middleware;

use \Illuminate\Http\Request;
use Admin;
use Closure;
use Encore\Admin\Auth\Permission as Checker;

class AdminCityManager
{

    protected $checkArray = [
        '/admin/properties*',
        '/admin/clients*',
        '/admin/cminvoices*',
    ];

    public function handle(Request $request, Closure $next)
    {
        $user = Admin::user();
        if($user && $user->is_city_manager)
        {
            if($this->inCheckArray($request)){
                if(!empty($request->route()->parameters())){
                    foreach($request->route()->parameters() as $name => $value){
                        switch($name){
                            case 'cminvoice':
                                $invoice = $user->invoices()->where('id', $value)->first();
                                if(!$invoice){
                                    Checker::error();
                                }
                                break;
                            case 'property':
                                $property = $user->properties()->where('properties.id', $value)->first();
                                if(!$property){
                                    Checker::error();
                                }
                                break;
                            case 'client':
                                if(!$user->users()->where('users.id', $value)->first()){
                                    Checker::error();
                                }
                                break;
                        }
                    }
                }
            }
        }
        return $next($request);
    }

    protected function inCheckArray($request)
    {
        foreach ($this->checkArray as $check) {
            if ($check !== '/') {
                $check = trim($check, '/');
            }

            if ($request->fullUrlIs($check) || $request->is($check)) {
                return true;
            }
        }

        return false;
    }

}
