<?php


namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as ClassicAuthenticate;
use Illuminate\Support\Facades\Auth;

class Authenticate extends ClassicAuthenticate
{
    protected function redirectTo($request)
    {
        $routeName = $request->route()->getName();
        if(strpos($routeName, 'guest') === 0){
            return route('guest.login');
        }
    }
}