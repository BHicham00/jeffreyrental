<?php

namespace App\Http\Middleware;

use Closure;
use Encore\Admin\Facades\Admin;

class CheckCgvAccepted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(($user = auth()->user()) && !Admin::user())
        {
            if(!$user->cgv && $request->route()->getName() != 'cgv' && $request->route()->getName() != 'jhCgv'){
                return redirect()->route('cgv', ['redirect' => $request->fullUrl()]);
            }
        }
        return $next($request);
    }
}
