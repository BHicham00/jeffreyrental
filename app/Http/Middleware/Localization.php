<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (isset($_COOKIE['lang'])) {
            // Récupération de la 'lang' dans Cookie et activation
            App::setLocale($_COOKIE['lang']);
        }
        else if ( Session::has('lang')) {
            // Récupération de la 'lang' dans Session et activation
            App::setLocale(Session::get('lang'));
        }else{
            App::setLocale(config('app.fallback_locale'));
        }

        return $next($request);
    }
}
