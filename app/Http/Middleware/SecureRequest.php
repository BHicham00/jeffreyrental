<?php

namespace App\Http\Middleware;

use \Illuminate\Http\Request;
use Closure;

class SecureRequest
{

    public function handle(Request $request, Closure $next)
    {
        if($request->header('x-forwarded-proto') != 'https' && getenv("LARAVEL_ENV", 'http') === 'https' && !strpos($request->url(), "localhost"))
        {
            return redirect()->secure($request->path());
        }
        return $next($request);
    }

}
