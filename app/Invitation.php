<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class Invitation extends Model
{
    
    //public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'client_id', 'email', 'commission', 'lite', 'bnbkeys'
    ];
    
    
    public function getLink(){
        return url("/invitation?entry=".urlencode($this->token));
    }

    protected $casts = [
        'lite' => 'boolean',
        'bnbkeys' => 'boolean',
    ];
}
