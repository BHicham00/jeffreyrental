<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class InvoiceDebour extends Model
{
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'invoice_id', 'label', 'value'
    ];
    
    public function invoice(){
        return $this->belongsTo(CmInvoice::class, 'invoice_id');
    }
}
