<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceMission extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'property_id',
        'date',
        'checkin',
        'checkout',
        'cleaning'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(){
        return $this->belongsTo(Property::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function invoice(){
        return $this->hasOneThrough(CmInvoice::class, 'cm_invoice_has_missions');
    }

    /**
     * @return float
     */
    public function getTotal(){
        return $this->checkin + $this->checkout + $this->cleaning + $this->debours;
    }
}
