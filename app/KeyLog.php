<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyLog extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'property_id',
        'date',
        'event',
        'status',
        'previous_status',
        'code_used',
        'store_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(){
        return $this->belongsTo(Property::class);
    }

    public function booking(){
        return $this->hasOne(Booking::class, 'keynest_collection', 'code_used');
    }
}
