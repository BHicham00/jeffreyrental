<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeynestStore extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'time',
        'description',
        'address',
        'lon',
        'lat',
        'open_details',
        'close_details'
    ];

    protected $casts = [
        'open_details' => 'json',
        'close_details' => 'json',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties(){
        return $this->hasMany(Property::class, 'keynest_store_id');
    }
}
