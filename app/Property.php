<?php
namespace App;

use App\Admin\Extensions\Auth\Database\AdminHasProperty;
use App\Admin\Extensions\Auth\Database\Administrator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class Property extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'address', 'city_id', 'billing', 'commission', 'departement', 'bookingsync_id', 'iban', 'bic', 'airbnb', 'rent_number', 'class_number',
        'contract', 'inventory', 'assessment', 'keys', 'vision', 'country_id',
        'ranking_file', 'attest_loc',
        'lite',
        'bnbkeys',
        'report_tva','report_cleaning',
        'min_price',
        'keynest_key_id', 'keynest_store_id', 'keynest_dropoff', 'keynest_collection',
        'keynest_auto',
        'keynest_auto_checkout',
        'type', 'resiliated_at','isSync'
    ];

    static public $forbiddenKeys = [
        'commission',
        'created_at',
        'city_id',
        'departement',
        'keynest_key_id',
        'keynest_store_id',
        'keynest_dropoff',
        'keynest_collection',
        'bnbkeys',
        'address',
        'contract',
        'inventory',
        'assessment',
        'keys',
        'vision',
        'user_id',
        'name',
        'country_id',
        'lite',
        'report_tva',
        'report_cleaning',
        'ranking_file',
        'resiliated_at',
        'keynest_auto',
        'keynest_auto_checkout',
    ];

    static public function getTypes(){
        return [
            0 => 'résidence secondaire',
            1 => 'résidence principale',
        ];
    }

    protected $casts = [
        'lite' => 'boolean',
        'bnbkeys' => 'boolean',
        'report_tva' => 'boolean',
        'report_cleaning' => 'boolean',
    ];
    
    protected $owner = null;
    
    public function getUser(){
        if(!$this->owner){
            $this->owner = $this->user;
        }
        return $this->owner;
    }

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }
    public function city(){
        return $this->belongsTo(City::class);
    }
    
    public function fees(){
        return $this->hasMany(Fee::class);
    }
    
    public function bookings(){
        return $this->hasMany(Booking::class);
    }
    
    public function amenities(){
        return $this->hasMany(PropertyAmenity::class);
    }
    
    public function details(){
        return $this->hasMany(PropertyDetails::class);
    }
    public function detail(){
        return $this->hasOne(PropertyDetails::class);
    }
    
    public function reportValues(){
        return $this->hasMany(ReportHasValue::class);
    }
    public function reportCAValues(){
        return $this->hasMany(ReportHasValue::class)->where("profit", ">", "0")->where("label", "=", "user_ca");
    }
    public function getReports(){
        $values = $this->reportValues()->where("label", "=", "user_ca")->get();
        $reports = [];
        foreach($values as $value){
            $reports[] = $value->report;
        }
        return $reports;
    }
    
    public function getOldKeysFileName(){
        return "remise_cles_" . $this->user->client_id . "_" . urlencode(str_replace(" ", "", $this->name)) . ".docx";
    }
    public function getOldContractFileName(){
        return "contrat_" . $this->user->client_id . "_" . urlencode(str_replace(" ", "", $this->name)) . ".pdf";
    }
    public function getOldInventoryFileName(){
        return "inventaire_" . $this->user->client_id . "_" . urlencode(str_replace(" ", "", $this->name)) . ".pdf";
    }
    public function getOldAssessmentFileName(){
        return "etatlieux_" . $this->user->client_id . "_" . urlencode(str_replace(" ", "", $this->name)) . ".pdf";
    }
    
    public function getKeysFileName(){
        return "remise_cles_" . $this->getPropertyTag() . ".docx";
    }
    public function getAttestLocFileName(){
        return "attest_loc_" . $this->getPropertyTag() . ".pdf";
    }
    public function getRankingFileName(){
        return "ranking_" . $this->getPropertyTag() . ".pdf";
    }
    public function getContractFileName(){
        return "contrat_" . $this->getPropertyTag() . ".pdf";
    }
    public function getAnnexeFileName(){
        return "annexe_" . $this->getPropertyTag() . ".pdf";
    }
    public function getInventoryFileName(){
        return "inventaire_" . $this->getPropertyTag() . ".pdf";
    }
    public function getAssessmentFileName(){
        return "etatlieux_" . $this->getPropertyTag() . ".pdf";
    }
    public function getInvoiceFileName($time){
        return "facture_" . $this->user->client_id . "_" . $time . "_" . $this->id . ".docx";
    }
    public function getReportFileName($time){
        return "report_" . $this->user->client_id . "_" . $time . "_" . $this->id . ".docx";
    }
    public function getOldInvoiceFileName($time){
        return "facture_" . $this->user->client_id . "_" . $time . "_" . urlencode($this->name) . ".docx";
    }
    public function getOldReportFileName($time){
        return "report_" . $this->user->client_id . "_" . $time . "_" . urlencode($this->name) . ".docx";
    }
    
    public function getUploadFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "contrats" . DIRECTORY_SEPARATOR . $this->user->client_id . DIRECTORY_SEPARATOR;
        $directory = $this->user->client_id;
        if(!is_dir($path)){
            mkdir($path);
        }
        return $directory;
    }
    public function getContractFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "contrats" . DIRECTORY_SEPARATOR . $this->user->client_id . DIRECTORY_SEPARATOR;
        if(!is_dir($path)){
            mkdir($path);
        }
        return $path;
    }
    public function getInvoiceFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "factures" . DIRECTORY_SEPARATOR;
        if(!is_dir($path)){
            mkdir($path);
        }
        return $path;
    }
    public function getReportFolder(){
        $path = base_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Admin" . DIRECTORY_SEPARATOR . "Controllers" . DIRECTORY_SEPARATOR . "reports" . DIRECTORY_SEPARATOR;
        if(!is_dir($path)){
            mkdir($path);
        }
        return $path;
    }

    public function getPropertyTag(){
        return $this->user->client_id . '_' . $this->id;
    }

    
    
    public function getOldContractFile(){
        return $this->getContractFolder() . $this->getOldContractFileName();
    }
    public function getOldInventoryFile(){
        return $this->getContractFolder() . $this->getOldInventoryFileName();
    }
    public function getOldAssessmentFile(){
        return $this->getContractFolder() . $this->getOldAssessmentFileName();
    }
    public function getOldKeysFile(){
        return $this->getContractFolder() . $this->getOldKeysFileName();
    }
    
    public function getOldReportFile($time){
        return $this->getReportFolder() . $this->getOldReportFileName($time);
    }
    public function getOldInvoiceFile($time){
        return $this->getInvoiceFolder() . $this->getOldInvoiceFileName($time);
    }
    public function getReportFile($time){
        return $this->getReportFolder() . $this->getReportFileName($time);
    }
    public function getInvoiceFile($time){
        return $this->getInvoiceFolder() . $this->getInvoiceFileName($time);
    }
    
    public function getAttestLocFile(){
        return $this->getContractFolder() . $this->getAttestLocFileName();
    }
    public function getRankingFile(){
        return $this->getContractFolder() . $this->getRankingFileName();
    }
    public function getContractFile(){
        return $this->getContractFolder() . $this->getContractFileName();
    }
    public function getAnnexeFile(){
        return $this->getContractFolder() . $this->getAnnexeFileName();
    }
    public function getInventoryFile(){
        return $this->getContractFolder() . $this->getInventoryFileName();
    }
    public function getAssessmentFile(){
        return $this->getContractFolder() . $this->getAssessmentFileName();
    }
    public function getKeysFile(){
        return $this->getContractFolder() . $this->getKeysFileName();
    }
    
    public function hasReport($time){
        return file_exists($this->getReportFile($time));
    }
    public function hasInvoice($time){
        return file_exists($this->getInvoiceFile($time));
    }
    
    public function hasAttestLoc(){
        return file_exists($this->getAttestLocFile());
    }
    public function hasRanking(){
        return file_exists($this->getRankingFile());
    }
    public function hasContract(){
        return file_exists($this->getContractFile());
    }
    public function hasAnnexe(){
        return file_exists($this->getAnnexeFile());
    }
    public function hasAssess(){
        return file_exists($this->getAssessmentFile());
    }
    public function hasInventory(){
        return file_exists($this->getInventoryFile());
    }
    public function hasKeysFile(){
        return file_exists($this->getKeysFile());
    }
    
    public function hasOldReport($time){
        return file_exists($this->getOldReportFile($time));
    }
    public function hasOldInvoice($time){
        return file_exists($this->getOldInvoiceFile($time));
    }
    
    public function hasOldContract(){
        return file_exists($this->getOldContractFile());
    }
    public function hasOldAssess(){
        return file_exists($this->getOldAssessmentFile());
    }
    public function hasOldInventory(){
        return file_exists($this->getOldInventoryFile());
    }
    public function hasOldKeysFile(){
        return file_exists($this->getOldKeysFile());
    }

    public function ahp(){
        return $this->hasOne(AdminHasProperty::class);
    }
    public function manager(){
        return $this->hasOneThrough(Administrator::class, AdminHasProperty::class, 'property_id', 'id', 'id', 'admin_user_id');
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function keylogs(){
        return $this->hasMany(KeyLog::class);
    }

    public function keynest_store(){
        return $this->belongsTo(KeynestStore::class);
    }
}
