<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class PropertyAmenity extends Model
{
    
    public $timestamps = false;
    
    protected $table = 'property_has_amenities';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'amenity_id',
        'value'
    ];
    
    public function property(){
        return $this->belongsTo("App\Property")->withTrashed();
    }
    public function amenity(){
        return $this->belongsTo("App\Amenity");
    }
}
