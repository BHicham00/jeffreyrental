<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class PropertyDetails extends Model
{
    
    public $timestamps = false;
    
    protected $table = 'property_details';

    const ACCESS = [
        null => 'Aucun',
        'key' => 'Clé',
        'badge' => 'Badge',
        'code' => 'Code',
    ];

    const ACCESS_CAR = [
        null => 'Aucun',
        'key' => 'Clé',
        'badge' => 'Badge',
        'code' => 'Code',
    ];
    static public function getResidTypes(){
        return [
            'primary_residence' => 'Résidence principale',
            'secondary_residence' => 'Résidence secondaire',
            'non_residential' => 'Hébergement non résidentiel',
        ];
    }
    static public function getManagementTypes(){
        return [
            'professional' => 'Location professionnelle',
            'non_professional' => 'Location non professionnelle',
        ];
    }
    static public function getBeginTimeTypes(){
        return [
            '8' => '8h00',
            '9' => '9h00',
            '10' => '10h00',
            '11' => '11h00',
            '12' => '12h00',
            '13' => '13h00',
            '14' => '14h00',
        ];
    }
    static public function getArrivingTimeTypes(){
        return [
            '12' => '12h00',
            '13' => '13h00',
            '14' => '14h00',
            '15' => '15h00',
            '16' => '16h00',
            '17' => '17h00',
            '18' => '18h00',
        ];
    }
    static public function getArrivingTimeTypesMax(){
        return [
            '14' => '14h00',
            '15' => '15h00',
            '16' => '16h00',
            '17' => '17h00',
            '18' => '18h00',
            '19' => '19h00',
            '20' => '20h00',
            '21' => '21h00',
            '22' => '22h00',
            '23' => '23h00',
            '24' => '24h00',
            '0' => '00h00',
            '1' => '1h00',
            '2' => '2h00',
            '3' => '3h00',
        ];
    }
    static public function getRentalTypes(){
        return [
            'apartment' => 'Appartement',
            'condominium' => 'Appartement en résidence',
            'plane' => 'Avion',
            'bastide' => 'Bastide',
            'boat' => 'Bateau',
            'bungalow' => 'Bungalow',
            'treehouse' => 'Cabane dans les arbres',
            'recreational-vehicle' => 'Camping-carcaravane',
            'chalet' => 'Chalet',
            'bed-and-breakfast' => 'Chambre d\'hôtes',
            'private-room' => 'Chambre privée',
            'private-room-in-apartment' => 'Chambre privée dans un appartement',
            'private-room-in-house' => 'Chambre privée dans une maison',
            'castle' => 'Château',
            'cottage' => 'Cottage',
            'dormitory' => 'Dortoir',
            'farmhouse' => 'Ferme',
            'gite' => 'Gîte',
            'cave' => 'Habitation troglodyte',
            'hut' => 'Hutte',
            'igloo' => 'Igloo',
            'island' => 'Ile',
            'loft' => 'Loft',
            'longere' => 'Longère',
            'house' => 'Maison',
            'holiday-home' => 'Maison de vacances',
            'townhouse' => 'Maison de ville',
            'earth-house' => 'Maison écologique',
            'manor' => 'Manoir',
            'mas' => 'Mas',
            'mill' => 'Moulin',
            'lighthouse' => 'Phare',
            'cabin' =>' Refuge',
            'studio' => 'Studio',
            'tent' => 'Tente',
            'tipi' => 'Tipi',
            'train' => 'Train',
            'villa' => 'Villa',
            'yurt' => 'Yourteselect',
        ];
    }
    public static function accessTypes(){
        return [
            null => __('Aucun'),
            0 => __('Aucun'),
            'key' => __('Clé'),
            'badge' => __('Badge'),
            'code' => __('Code'),
        ];
    }
    public static function accessCarTypes(){
        return [
            0 => __('Aucun'),
            null => __('Aucun'),
            'key' => __('Clé'),
            'badge' => __('Badge'),
            'code' => __('Code'),
        ];
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'residence',
        'entry',
        'access',
        'access_code',
        'access_car',
        'access_car_code',
        'floor',
        'elevator',
        'door',
        'syndic',
        'capacity',
        'surface',
        'parking',
        'wifi_login',
        'wifi_pass',
        'water',
        'electricity',
        'rooms',
        'bathrooms',
        'showers',
        'bathtubs',
        'k140',
        'k160',
        'k180',
        'k200',
        'beds',
        'bunkbeds',
        'couchbeds',
        'covers',
        'square_pillows',
        'rect_pillows',
      	'wc',
        'sleeps',
        'sleepsMax',
        'floors',
        'rental_type',
        'permis',
        'Management_type',
        'arriving_time',
        'arriving_time_max',
        'begin_time',
        'delivery_time',
        'title',
        'summary',
        'description',
        'website_url'
    ];
    
    public function property(){
        return $this->belongsTo(Property::class)->withTrashed();
    }
}

