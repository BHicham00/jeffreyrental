<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class Report extends Model
{
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'start_at', 'end_at'
    ];
    
    public function brutvalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "brut");
    }
    public function touristvalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "tourist_tax");
    }
    public function comvalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "commission");
    }
    public function nightsvalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "nights");
    }
    public function cleaningvalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "cleaning");
    }
    public function uservalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "user_ca");
    }
    public function jeffreyvalues(){
        return $this->hasMany("App\ReportHasValue")->where("label", "=", "jeffrey");
    }
    public function values(){
        return $this->hasMany("App\ReportHasValue");
    }
    
    public function propertyValues($id){
        return $this->hasMany("App\ReportHasValue")->where("property_id",'=', $id);
    }
    
    public function getReportValuesByProperty(){
        $values = $this->hasMany("App\ReportHasValue")->get();
        $results = [];
        foreach($values as $v){
            /* @var App\ReportHasValue $v  */
            $property = $v->property;
            if(!array_key_exists($property->id, $results)){
                $results[$property->id] = [
                    "user_ca" => 0,
                    "cleaning" => 0,
                    "tourist_tax" => 0,
                    "nights" => 0,
                    "jeffrey" => 0,
                    "commission" => 0,
                    "brut" => 0
                ];
            }
            $results[$property->id][$v->label] = $v->profit;
        }
        //var_dump($results);die;
        return $results;
    }
}
