<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class ReportHasValue extends Model
{
    protected $table = 'report_has_values';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'report_id', 'property_id', 'profit', 'label'
    ];
    
    public function property(){
        return $this->belongsTo("App\Property")->withTrashed();
    }
    public function report(){
        return $this->belongsTo("App\Report");
    }
}
