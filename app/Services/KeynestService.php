<?php


namespace App\Services;


use Carbon\Carbon;
use Illuminate\Support\Arr;

class KeynestService
{

    private $apiUrl = 'https://api.keynest.com/api/v2/';
    private $privateKey = '6eb65adb7348467886661ea8ad3f25c3';

    public function __construct()
    {
        //$this->privateKey = $isPro ? config('synaps.pro_private_key') : config('synaps.private_key');
    }

    public function listAllKeys(){
        $request = (object)$this->restRequest("Key/KeyList");
        return json_decode($request->content);
    }

    public function listStores($city = null){
        $request = (object)$this->restRequest("KeyStore/StoreListByCountry", json_encode(["Country" => "France", "city" => $city]));
        return json_decode($request->content);
    }
    public function getStore($storeId){
        $request = (object)$this->restRequest("KeyStore/StoreListByCountry", json_encode(["Country" => "France", "storeId" => $storeId]));
        return json_decode($request->content);
    }
    public function nearestStores($property){
        $request = (object)$this->restRequest("KeyStore/NearestStoreList", json_encode(["Address" => $property->address]));
        return json_decode($request->content);
    }

    public function getKey($keyId){
        $request = (object)$this->restRequest("Key/GetKeyStatus", json_encode(["KeyId" => $keyId]));
        return json_decode($request->content);
    }

    public function getDropOff($keyId){
        $request = (object)$this->restRequest("Key/GetDropOffCode", json_encode(["KeyId" => $keyId]));
        return json_decode($request->content);
    }
    public function getValidCodes($keyId){
        $request = (object)$this->restRequest("Key/GetValidCodes", json_encode(["KeyId" => $keyId]));
        return json_decode($request->content);
    }

    public function newKey($property, $storeId){
        $params = [
            "KeyName" => $property->name . " Test",
            "PropertyId" => $property->bookingsync_id,
            "PropertyPostCode" => $property->city->getPostal(),
            "StoreId" => $storeId,
            "IsPermanentCode" => true
        ];
        $request = (object)$this->restRequest("Key/CreateKeyWithDropOffAndCollectionCode", json_encode($params));
        return json_decode($request->content);
    }

    public function createCollectionCode($property, $date = null, $isPermanent = false){
        $params = [
            "KeyId" => $property->keynest_key_id,
            "StoreId" => $property->keynest_store_id,
            "PropertyPostCode" => $property->city->getPostal(),
            "IsPermanentCode" => $isPermanent,
        ];
        if($date) {
            $date_start = new Carbon($date);
            $date_start->setTimezone(new \DateTimeZone('+0100'));
            $date_start->setTime(14, 0, 0);
            $date_end = new Carbon($date);
            $date_end->setTimezone(new \DateTimeZone('+0100'));
            $date_end->addDay();
            $params = array_merge($params, [
                "ValidFrom" => $date_start->toIso8601String(),
                "ValidTo" => $date_end->toIso8601String()
            ]);
        }
        $request = (object)$this->restRequest("Key/CreateKeyCollectionCode", json_encode($params));
        return json_decode($request->content);
    }

    public function shareCode($property, $email){
        $params = [
            "CodeType" => "Collection",
            "Code" => $property->keynest_collection,
            "email" => $email,
        ];
        $request = (object)$this->restRequest("key/ShareCode", json_encode($params));
        return json_decode($request->content);
    }


    protected function restRequest($request, $data = null, $headers = [])
    {
        $headers = array_merge([
            'ApiKey: ' . $this->privateKey,
            'Content-Type: application/json'
        ], $headers);
        $return = curlGetContent($this->apiUrl . $request, $headers, $data);
        if(!array_key_exists('ResponsePacket', json_decode($return['content'], true))){
            throw new \Exception('Keynest Error : ' . $return['content']);
        }
        return $return;
    }
}
