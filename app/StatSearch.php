<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Property
 *
 * @author Administrateur
 */
class StatSearch extends Model
{
    protected $table = 'statsearchs';
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label', 'filters'
    ];
    
    public function getFilters(){
        return json_decode($this->filters, true);
    }
}
