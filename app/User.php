<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'name', 'email', 'password', 'address', 'phone', 'bnbkeys', 'cgv', 'tva_number'
    ];

    protected $casts = [
        'bnbkeys' => 'boolean',
        'cgv' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function properties(){
        return $this->hasMany("App\Property");
    }
    public function companies(){
        return $this->hasMany("App\Company");
    }
    public function company(){
        return $this->hasOne("App\Company");
    }
}
