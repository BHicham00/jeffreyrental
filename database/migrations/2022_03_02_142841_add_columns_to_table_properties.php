<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTableProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->string('Hostenga_link');
            $table->string('Airbnb_link');
            $table->string('Booking_link');
            $table->string('VRBO_link');
            $table->string('VEEPEE_link');
            $table->string('autre1_link');
            $table->string('autre2_link');
            $table->string('autre3_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_properties', function (Blueprint $table) {
            //
        });
    }
}
