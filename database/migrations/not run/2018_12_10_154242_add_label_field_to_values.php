<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLabelFieldToValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_has_values', function (Blueprint $table) {
            $table->dropForeign('report_has_values_property_id_foreign');
            $table->dropForeign('report_has_values_report_id_foreign');
            $table->dropUnique('report_has_values_report_id_property_id_unique');
            /*
             * possible values :
             * user_ca : virement final du client
             * cleaning : frais de ménage
             * tourist_tax : taxe de séjour
             * nights : nombre de nuités
             * jeffrey : commission jeffrey
             * commission : commission jeffrey
             * brut : total brut
             */
            $table->string('label',255)->default('user_ca');
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_has_values', function (Blueprint $table) {
            $table->dropForeign('report_has_values_property_id_foreign');
            $table->dropForeign('report_has_values_report_id_foreign');
            $table->dropColumn('label');
            $table->unique(['report_id', 'property_id']);
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
        });
    }
}
