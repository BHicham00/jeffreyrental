<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned()->unique();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->string('access')->nullable();
            $table->string('access_code')->nullable();
            $table->string('access_car')->nullable();
            $table->string('access_car_code')->nullable();
            $table->string('floor')->nullable();
            $table->tinyInteger('elevator')->default(0);
            $table->string('door')->nullable();
            $table->tinyInteger('capacity')->nullable();
            $table->integer('surface')->nullable();
            $table->string('parking')->nullable();
            $table->string('wifi_login')->nullable();
            $table->string('wifi_pass')->nullable();
            $table->string('water')->nullable();
            $table->string('electricity')->nullable();
            $table->tinyInteger('rooms')->nullable();
            $table->tinyInteger('bathrooms')->nullable();
            $table->tinyInteger('waterrooms')->nullable();
            $table->tinyInteger('k140')->nullable();
            $table->tinyInteger('k160')->nullable();
            $table->tinyInteger('k180')->nullable();
            $table->tinyInteger('k200')->nullable();
            $table->tinyInteger('beds')->nullable();
            $table->tinyInteger('bunkbeds')->nullable();
            $table->tinyInteger('couchbeds')->nullable();
            $table->tinyInteger('covers')->nullable();
            $table->tinyInteger('square_pillows')->nullable();
            $table->tinyInteger('rect_pillows')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_details');
    }
}
