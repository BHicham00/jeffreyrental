<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->string("card_brand")->nullable();
            $table->string("card")->nullable();
            $table->string("expire_date")->nullable();
            $table->string("PM")->nullable();
            $table->string("status")->nullable();
            $table->string("pay_id")->unique()->nullable();
            $table->dateTime("pay_date")->nullable();
            $table->string("transaction_id")->unique()->nullable();
            $table->string("order_id")->unique()->nullable();
            $table->string("token")->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dropColumn("card_brand");
            $table->dropColumn("card");
            $table->dropColumn("expire_date");
            $table->dropColumn("PM");
            $table->dropColumn("status");
            $table->dropColumn("pay_id");
            $table->dropColumn("pay_date");
            $table->dropColumn("token");
            $table->dropColumn("transaction_id");
            $table->dropColumn("order_id");
        });
    }
}
