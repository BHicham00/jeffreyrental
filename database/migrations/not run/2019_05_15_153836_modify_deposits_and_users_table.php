<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDepositsAndUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->string("city")->nullable();
            $table->boolean("mail_sent")->default(false);
            $table->boolean("archived")->default(false);
        });
        Schema::table('users', function (Blueprint $table) {
            $table->boolean("archived")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dropColumn("city");
            $table->dropColumn("mail_sent");
            $table->dropColumn("archived");
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("archived");
        });
    }
}
