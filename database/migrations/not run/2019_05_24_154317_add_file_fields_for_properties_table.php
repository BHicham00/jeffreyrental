<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Property;

class AddFileFieldsForPropertiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table)
        {
            $table->string('contract')->nullable();
            $table->string('inventory')->nullable();
            $table->string('assessment')->nullable();
        });

        /*foreach(Property::all() as $p)
        {
            if($p->hasContract())
            {
                $p->contract = $p->getUploadFolder() . "/" .  $p->getContractFileName();
                $p->save();
            }
        }*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table)
        {
            $table->dropColumn('contract');
            $table->dropColumn('inventory');
            $table->dropColumn('assessment');
        });
    }

}
