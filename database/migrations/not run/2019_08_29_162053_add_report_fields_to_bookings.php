<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportFieldsToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->float('ota')->nullable();
            $table->float('tax')->nullable();
            $table->float('under_adar')->nullable();
            $table->float('cleaning')->nullable();
            $table->float('profit')->nullable();
            $table->float('owner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('ota');
            $table->dropColumn('tax');
            $table->dropColumn('under_adar');
            $table->dropColumn('cleaning');
            $table->dropColumn('profit');
            $table->dropColumn('owner');
        });
    }
}
