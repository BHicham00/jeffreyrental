<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBookingsDatetimeToDate extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table)
        {
            $table->date('created_at')->change();
            $table->date('start_at')->change();
            $table->date('end_at')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table)
        {
            $table->dateTime('created_at')->change();
            $table->dateTime('start_at')->change();
            $table->dateTime('end_at')->change();
        });
    }

}
