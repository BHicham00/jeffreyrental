<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_commune_INSEE',5);
            $table->string('nom_commune',191);
            $table->addColumn('integer', 'code_postal', ['length' => 5])->nullable();
            $table->string('libelle_acheminement',191);
            $table->string('ligne_5',191);
            $table->string('coordonnees_gps',191);
        });
        (new CitiesSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
