<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('iso');
            $table->string('name');
            $table->string('nicename');
            $table->string('iso3')->nullable();
            $table->unsignedInteger('numcode')->nullable();
            $table->unsignedInteger('phonecode');
            $table->decimal('vat_rate', 3, 1);
            $table->boolean('allow_kyc');
            $table->boolean('blocked');
            $table->unsignedInteger('shipping_cost_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
