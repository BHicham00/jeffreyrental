<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBookingsWebhookUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->float('paid_amount')->default(0);
            $table->float('downpayment')->default(0);
            $table->date('balance_due_at')->nullable();
            $table->boolean('need_sync')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('paid_amount');
            $table->dropColumn('balance_due_at');
            $table->dropColumn('downpayment');
            $table->dropColumn('need_sync');
        });
    }
}
