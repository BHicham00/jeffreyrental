<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityManagerHasPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_manager_has_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_user_id')->nullable();
            $table->foreign('admin_user_id')->references('id')->on('admin_users')->onDelete('cascade');
            $table->unsignedInteger('property_id')->unique();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->float('commission')->default(20);
            $table->boolean('owned')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_manager_has_properties');
    }
}
