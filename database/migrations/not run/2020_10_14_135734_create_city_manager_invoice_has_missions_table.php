<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityManagerInvoiceHasMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cm_invoice_has_missions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cm_invoice_id');
            $table->foreign('cm_invoice_id')->references('id')->on('cm_invoices')->onDelete('cascade');
            $table->unsignedInteger('invoice_mission_id');
            $table->foreign('invoice_mission_id')->references('id')->on('invoice_missions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cm_invoice_has_missions');
    }
}
