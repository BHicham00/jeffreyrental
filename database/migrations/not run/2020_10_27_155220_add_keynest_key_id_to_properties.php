<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeynestKeyIdToProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->string('keynest_key_id')->nullable();
            $table->unsignedInteger('keynest_store_id')->nullable();
            $table->string('keynest_dropoff')->nullable();
            $table->string('keynest_collection')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn(['keynest_key_id', 'keynest_store_id', 'keynest_dropoff', 'keynest_collection']);
        });
    }
}
