<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeyLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id')->default(73);
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->dateTime('date');
            $table->string('event')->nullable();
            $table->string('status')->nullable();
            $table->string('previous_status')->nullable();
            $table->string('code_used')->nullable();
            $table->string('store_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_status');
    }
}
