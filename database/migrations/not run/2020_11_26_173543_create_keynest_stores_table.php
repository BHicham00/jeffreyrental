<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeynestStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keynest_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('time')->nullable();
            $table->text('description')->nullable();
            $table->string('address');
            $table->decimal('lon', 10, 7);
            $table->decimal('lat', 10, 7);
            $table->json('open_details')->nullable();
            $table->json('close_details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keynest_stores');
    }
}
