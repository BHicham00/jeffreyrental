<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_guests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('booking_id');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('phone');
            $table->string('transport_type');
            $table->string('transport_number')->nullable();
            $table->dateTime('arrival');
            $table->string('ident_type');
            $table->string('ident_number');
            $table->string('ident_file');
            $table->date('ident_expiration');
            $table->unsignedInteger('country_id');
            $table->dateTime('checkin');
            $table->string('one_time_pass')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_holder')->nullable();
            $table->string('card_last_digits')->nullable();
            $table->string('card_expiration')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_guests');
    }
}
