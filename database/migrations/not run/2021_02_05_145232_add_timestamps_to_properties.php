<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->timestamps();
        });
        $properties = \App\Property::withTrashed()->get();
        foreach($properties as $p){
            if(!$p->created_at && ($user = $p->user()->withTrashed()->first())){
                $p->created_at = $user->created_at;
                $p->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropTimestamps();
        });
    }
}
