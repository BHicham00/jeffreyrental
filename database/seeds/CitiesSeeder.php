<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        set_time_limit(1200);
        DB::unprepared(file_get_contents('database/seeds/cities.sql'));
        DB::unprepared('DELETE
        FROM cities
        WHERE id NOT IN
        (
        SELECT cid FROM(
            SELECT MAX(id) as cid
            FROM cities
            GROUP BY nom_commune, code_postal
        ) as temp)');
    }
}
