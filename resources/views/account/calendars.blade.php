@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{  __("Mes Calendriers") }}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div>{{  __("Ici vous trouverez le calendrier de vos propriétés pour les 2 mois à venir") }}.</div>
                        @foreach($pageData["properties"] as $p)
                            <div id="property-{{$p->id}}" class="property-box">
                                <h3>{{$p->name}}</h3>
                                <p>{{$p->address}}</p>
                                <button class="book-btn btn btn-primary">{{  __("Réserver votre propriété") }}</button>
                                {{ Form::open(['class' => 'book-form', 'method'=>'POST', 'action' => ['AccountController@book', $p->id], "style" => "display:none;"]) }}
                                <label>{{  __("Vos dates") }} :</label>
                                <input type="text" name="range" class="dates" required/>
                                <br/>
                                <label for="guests-{{$p->id}}">{{  __("Nombre de personnes") }} :</label>
                                <input id="guests-{{$p->id}}" defaultValue="1" type="number" name="guests" required/>
                                <br/>
                                <label class="checkbox-box" for="checkin-{{$p->id}}">{{  __("Check-in") }} :
                                    <input id="checkin-{{$p->id}}" type="checkbox" name="checkin"/>
                                </label>
                                |
                                <label class="checkbox-box" for="checkout-{{$p->id}}">{{  __("Check-out") }} :
                                    <input id="checkout-{{$p->id}}" type="checkbox" name="checkout"/>
                                </label>
                                <br/>
                                <label class="checkbox-box" for="cleaning-{{$p->id}}">{{  __("Ménage") }} :
                                    <input id="cleaning-{{$p->id}}" type="checkbox" name="cleaning"/>
                                </label>
                                |
                                <label class="checkbox-box" for="sheets-{{$p->id}}">{{  __("Linge") }} :
                                    <input id="sheets-{{$p->id}}" type="checkbox" name="sheets"/>
                                </label>
                                <input type="submit" class="btn btn-primary">{{  __("Envoyer ma demande") }}</input>
                                {{ Form::close() }}
                            </div>
                            <div>
                                @foreach($pageData["promises"][$p->id] as $month => $prom)
                                {{  __("Revenu Prévisionnel") }} {{$month}}: {{$prom}}€<br/>
                                @endforeach
                            </div>
                            <div id="calendar-{{$p->id}}">

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script>
        /*import { Calendar, EventSource } from '@fullcalendar/core';
         import dayGridPlugin from '@fullcalendar/daygrid';*/
        $(function () {
            var data = {!! json_encode($pageData['bookings']) !!};
            var locale = '{{ config('app.locale') }}';
            for (var i in data) {

                var calendarEl = document.getElementById('calendar-' + i);
                var calendar = new Calendar(calendarEl, {
                    plugins: [dayGridPlugin],
                    displayEventTime: false,
                    events: data[i].map(function (el) {
                        var guest = el.reference;
                        try {
                            guest = JSON.parse(el.guest);
                        } catch (e) {
                        }
                        var dateStart = new Date(el.start_at);
                        var dateEnd = new Date(el.end_at);
                        var event = {
                            title: (locale=="fr" ? "Réservation " : "Reservation ") + el.reference + (locale=="fr" ? " du " : " from ") + dateStart.toLocaleDateString() + (locale=="fr" ? " au " : " to ") + dateEnd.toLocaleDateString(),
                            description: locale=="fr" ? "information à venir" : "information to come",
                            start: el.start_at + " 15:00:00",
                            end: el.end_at + " 12:00:00",
                            id: el.bookingsync_id,
                            color: "#0099ff"
                        };

                        if (el.status == "Unavailable") {
                            event.title = (locale=="fr" ? "résidence bloquée du " : "residence blocked from ") + dateStart.toLocaleDateString() + (locale=="fr" ? " au " : " to ") + dateEnd.toLocaleDateString();
                            event.color = "#777";
                            event.description = locale=="fr" ? "bloqué" : "bloqued"
                        } else if (guest.firstname) {
                            console.log(el);
                            event.title = guest.firstname + " " + guest.lastname + (locale=="fr" ? " du " : " from ") + dateStart.toLocaleDateString() + (locale=="fr" ? " au " : " to ") + dateEnd.toLocaleDateString();
                            event.description = (locale=="fr" ? " du " : " from ") + dateStart.toLocaleDateString() + (locale=="fr" ? " au " : " to ") + dateEnd.toLocaleDateString() + (locale=="fr" ? " Revenu net : " : " Net Revenue ") + el.owner + "€" + (!el.paid ? (locale=="fr" ? " en attente du paiement": " awaiting payment") : "");
                            event.color = "#3788d8";
                        }
                        return event;
                    }),
                    eventRender: function (info) {
                        var tooltip = $(info.el).tooltip({
                            title: info.event.extendedProps.description,
                            placement: 'top',
                            trigger: 'hover',
                            container: 'body'
                        });
                    }
                });
                calendar.render();
            }
            //moment.locale("fr");
            $('.dates').daterangepicker({
                locale: {
                    format: 'DD/M/Y'
                }
            });

            $(".book-btn").click(function () {
                $(this).next(".book-form").toggle();
            });
        });
    </script>
@append
