@extends('layouts.app')

@section('content')
    <div id="blurred-back">
        <div id="black-container">
            <div class="center-modal">
                @if($errors->any())
                    {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
                @endif
                <form id="cgv-form" method="POST">
                    {{ csrf_field() }}
                    <div>
                        {{  __("Pour utiliser les services de Jeffrey Host vous devez accepter les") }} <a target="_blank" href="jhCgv">{{  __("Conditions Générales de Ventes") }}</a>
                        <br/>
                        <br/>

                        <label style="cursor:pointer;" for="cgv-input"><input required id="cgv-input" type="checkbox" name="cgv" value="1"/> {{  __("J'ai lu et j'accepte les conditions générales de ventes et d'utilisation") }}</label>
                    </div>
                    <button id="submit" class="btn btn-success" type="submit">{{  __("Accepter") }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script>
        $(function () {

        });
    </script>
@append
