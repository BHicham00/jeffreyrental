@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{  __("Accueil") }}</div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach ($propertyAlerts as $alert)
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"> </i>
                        {!! $alert['message'] !!}<br/>
                        <a href="{{ $alert['link'] }}">{{  __("Voir ici") }}</a>
                    </div>
                    @endforeach

                    {{  __("Bienvenue sur Jeffrey Host") }} {{$user->name}}.<br/>
                    <br/>
                    {{  __("Dans le menu au-dessus vous pourrez retrouvez votre/vos propriétés que nous gérons avec leur documents respectifs (factures, rapports, contrats, ...)") }}.<br/>
                    <h2>{{  __("Tableau de bord") }}</h2>
                        {{ Form::open(['class' => 'property-form', 'id' => 'property-form', 'method'=>'POST', 'action' => ['AccountController@stats']]) }}
                            <div class="property-box">
                                <!-- <h5>Filtres:</h5> -->
                                <div class="container mt-6 float-right" style="max-width: 450px">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class='input-group date' id='datetimepickerdu'>
                                                    <input type='text' name="date_du" id="date_du" class="form-control" required="required" placeholder="From Date" value="{{ $date_du }}" {{ $errors->has('date_du') ? error : '' }} />
                                                    @if ($errors->has('date_du'))
                                                        <div class="error">
                                                            {{ $errors->first('date_du') }}
                                                        </div>
                                                    @endif
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <h4>{{  __("au") }}</h4>
                                            </div>
                                            <div class="col-md-5">
                                                <div class='input-group date' id='datetimepickerau'>
                                                    <input type='text' name="date_au" id="date_au" class="form-control" required="required" placeholder="To Date" value="{{ $date_au }}" {{ $errors->has('date_au') ? error : '' }} />
                                                    @if ($errors->has('date_au'))
                                                        <div class="error">
                                                            {{ $errors->first('date_au') }}
                                                        </div>
                                                    @endif
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="submit" name="send" class="btn-sm btn-primary" >{{  __("Filtrer") }}</button>
                                                <!-- <a name="filter" id="filter"  href="{{ route('home') }}" class="btn btn-primary" >{{  __("Filtrer") }}</a> -->
                                            </div>
                                        </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($properties as $p)
                            <li role="presentation" @if ($loop->first) class="active" @endif><a href="#property-{{$p->id}}" aria-controls="home" role="tab" data-toggle="tab">{{$p->name}}</a></li>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                            @foreach($properties as $p)
                            <div role="tabpanel" class="tab-pane active" id="property-{{$p->id}}">
                                {!! $charts[$p->id] !!}
                            </div>
                            @endforeach
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function () {
        setTimeout(function () {
            $(".nav-tabs li").each(function () {
                if (!$(this).hasClass("active")) {
                    $($(this).find("a").attr("href")).removeClass("active");
                }
            });
        }, 1000)
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
    $(function() {
        $('#datetimepickerdu').datetimepicker({
            format: 'YYYY-MM-DD',
            // autocolse: true,
            // todayHighlight:'TRUE',
        });
        $('#datetimepickerau').datetimepicker({
            format: 'YYYY-MM-DD',
            // todayHighlight:'TRUE',
            // autocolse: true,
        });

    });
</script>
@append
