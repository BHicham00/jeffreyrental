@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{  __("Mes Propriétés") }}</div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div>{{  __("Ici vous trouverez la liste de vos propriétés, en cliquant sur une vous obtiendrez les détails et rapports concernant cette propriété") }}.</div>
                    @foreach($properties as $p)
                    <div id="property-{{$p->id}}" class="property-box">
                        <h3>{{$p->name}}</h3>
                        <p>{{$p->address}}</p>
                        <a class="edit-property btn btn-primary" href="{{url("/property/".$p->id)}}"><i class="fa fa-info"></i> {{  __("Informations") }}</a>
                        <a class="documents-property btn btn-success" href="{{url("/property/".$p->id."/documents/")}}"><i class="fa fa-file"></i> {{  __("Documents") }}</a>
                        <a class="reports-property btn btn-warning" href="{{url("/property/".$p->id."/reports/")}}"><i class="fa fa-dollar"></i> {{  __("Rapports") }}</a>
                        <a class="contact-property btn btn-danger" contact-data="{{$p->manager ? json_encode($p->manager->info) : null}}" href="#"><i class="fa fa-phone"></i> {{  __("Contact") }}</a>
                    </div>
                    @endforeach
                    <a href="{{ url("/property/create") }}">
                        <h4>
                            <i class="fa fa-plus"></i>
                            {{  __("Ajouter une nouvelle propriété") }}
                        </h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="contactModalLabel">{{  __("Contact") }}</h3>
            </div>
            <div class="modal-body" style="font-size:16px">
                <div>
                    <b>{{  __("Manager") }} :</b> <span id="firstname"></span> <span id="lastname"></span>
                </div>
                <div>
                    <b>{{  __("Email") }} :</b> <span id="email"></span>
                </div>
                <div>
                    <b>{{  __("Téléphone") }} :</b> <span id="phone"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">{{  __("Ok") }}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function(){
       $('.contact-property').click(function(){
           $('#contact-modal').modal('toggle', {
               backdrop: true,
               keyboard: true
           });
           let manager = JSON.parse($(this).attr("contact-data"));
           if("firstname" in manager) {
               $('#firstname').text(manager.firstname);
               $('#lastname').text(manager.lastname);
               $('#email').text(!manager.email_pro ? manager.firstname.toLowerCase() + '@jeffreyhost.com' : manager.email_pro);
               $('#phone').text(manager.phone);
           }else{
               $('#firstname').text("Aucun manager attribué");
           }
       });
    });
</script>
@append
