@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url("properties")}}">{{  __("Mes Propriétés") }}</a>
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <a href="#">{{  __("Propriété") }} : {{$property->name}}</a>
                    <div id="property-tools">
                        <a class="documents-property btn btn-success" href="{{url("/property/".$property->id."/documents/")}}">{{  __("Documents") }}</a>
                        <a class="reports-property btn btn-warning" href="{{url("/property/".$property->id."/reports/")}}">{{  __("Rapports") }}</a>
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $name => $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div>{{  __("Ici vous pouvez éditer certaines informations de votre propriété") }}.</div>
                    <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">{{ __("Général")}}</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Description</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">{{__("Equipements")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">{{__("Emplacement")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-5" role="tab">{{__("Details du logement")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-6" role="tab">{{__("Liens")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-7" role="tab">{{__("Coordonnée bancaire")}}</a>
                    </li>
                </ul><!-- Tab panes -->

                    {{ Form::open(['class' => 'property-form', 'id' => 'property-form', 'method'=>'POST', 'action' => ['AccountController@property', $property->id]]) }}
                    <div>

                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <h2>{{  __("Informations du logement") }} :</h2>
                        <div id="property-0" class="fields-group">

                            <div class="fields-row">
                                <label for="property[name]">{{__("Libellé")}}:</label>
                                <input type="text" class="form-control" name="property[name]" id="property[name]" value="{{$property->name}}" readonly>
                            </div>
                            <div class="fields-row large">
                                {{ Form::label('', __("Commission Jeffrey")) }}
                                <div class="input">{{$property->commission}} %</div>
                            </div>
                            <div class="fields-row large">
                                {{ Form::label('', __("Date de création")) }}
                                <div class="input">{{(new \Carbon\Carbon($property->created_at))->format('d/m/Y')}}</div>
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[rental_type]', __("Type de location")) }}
                                {{ Form::select('property_details[rental_type]', $propertyDetails->getRentalTypes(), $propertyDetails->rental_type) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[residence]', __("Catégorie de résidence")) }}
                                {{ Form::select('property_details[residence]', $propertyDetails->getResidTypes(), $propertyDetails->residence) }}
                            </div>
                            <div class="fields-row">
                                <label for="property_details[sleeps]">{{__("Couchages")}}: </label>
                                <input type="number" name="property_details[sleeps]" id="property_details[sleeps]" value="{{$propertyDetails->sleeps}}">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[sleepsMax]">{{__("Couchages Max")}}: </label>
                                <input type="number" name="property_details[sleepsMax]" id="property_details[sleepsMax]" value="{{$propertyDetails->sleepsMax}}">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[surface]">{{__("Surface")}}: </label>
                                <input min="10" type="number" name="property_details[surface]" id="property_details[surface]" value="{{$propertyDetails->surface}}"> m²
                            </div>
                            <div class="fields-row">
                                <label for="property_details[floors]">{{__("Étages")}}: </label>
                                <input min="0" type="number" name="property_details[floors]" id="property_details[floors]" value="{{$propertyDetails->floors}}">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[floor]">{{__("Numéro d'étage")}}: </label>
                                <input min="0" type="number" name="property_details[floor]" id="property_details[floor]" value="{{$propertyDetails->floor}}">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[permis]">{{__("Permis/Licence/Numéro d'enregistrement")}}: </label>
                                <input type="text" name="property_details[permis]" id="property_details[permis]" value="{{$propertyDetails->permis}}">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[delivery_time]">{{__("Date de délivrance")}}: </label>
                                <input type="date" name="property_details[delivery_time]" id="property_details[delivery_time]" value="{{$propertyDetails->delivery_time}}">
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[Management_type]', __("Type de gestion de la location:")) }}
                                {{ Form::select('property_details[Management_type]', $propertyDetails->getManagementTypes(), $propertyDetails->Management_type) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[rooms]', __("Nombre de chambres")) }}
                                {{ Form::select('property_details[rooms]', $options2, $propertyDetails->rooms) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[bathrooms]', __("Nombre de salle de bains")) }}
                                {{ Form::select('property_details[bathrooms]', $options2, $propertyDetails->bathrooms) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[wc]', __("Nombre de toilettes")) }}
                                {{ Form::select('property_details[wc]', $options2, $propertyDetails->wc) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[bathtubs]', __("Nombre de baignoires")) }}
                                {{ Form::select('property_details[bathtubs]', $options2, $propertyDetails->bathtubs) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[showers]', __("Nombre de douches")) }}
                                {{ Form::select('property_details[showers]', $options2, $propertyDetails->showers) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('kingbeds', __("Lit(s) double(s)")) }}
                                <div id="beds">
                                    {{ Form::label('kingbeds', __("Lits 140 cm")) }}
                                    {{ Form::select('property_details[k140]', $options2, $propertyDetails->k140) }}
                                    {{ Form::label('kingbeds', __("Lits 160 cm")) }}
                                    {{ Form::select('property_details[k160]', $options2, $propertyDetails->k160) }}
                                    {{ Form::label('kingbeds', __("Lits 180 cm")) }}
                                    {{ Form::select('property_details[k180]', $options2, $propertyDetails->k180) }}
                                    {{ Form::label('kingbeds', __("Lits 200 cm")) }}
                                    {{ Form::select('property_details[k200]', $options2, $propertyDetails->k200) }}
                                </div>
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[beds]', __("Lit(s) simple(s)")) }}
                                {{ Form::select('property_details[beds]', $options2, $propertyDetails->beds) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[bunkbeds]', __("Lit(s) superposé(s)")) }}
                                {{ Form::select('property_details[bunkbeds]', $options2, $propertyDetails->bunkbeds) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[couchbeds]', __("Canapé(s) lit(s)")) }}
                                {{ Form::select('property_details[couchbeds]', $options2, $propertyDetails->couchbeds) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[covers]', __("Couette(s)")) }}
                                {{ Form::select('property_details[covers]', $options2, $propertyDetails->covers) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[square_pillows]', __("Oreiller(s) carré(s)")) }}
                                {{ Form::select('property_details[square_pillows]', $options2, $propertyDetails->square_pillows) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('property_details[rect_pillows]', __("Oreiller(s) rectangulaire(s)")) }}
                                {{ Form::select('property_details[rect_pillows]', $options2, $propertyDetails->rect_pillows) }}
                            </div>  

                        </div>
                    </div>
                    <!-- hidden -->
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <h2>Description</h2>
                        <div id="property-0-details" class="fields-group">
                            <div class="fields-row small-label">
                                <label for="property_details[title]">Titre: </label>
                                <input type="text" name="property_details[title]" id="property_details[title]" value="{{$propertyDetails->title}}">
                            </div>

                            <div class="fields-row small-label">
                                <label for="property_details[summary]">Résumé: </label>
                                <textarea style="height:70px!important;" rows="10" cols="50" name="property_details[summary]" id="property_details[summary]"">{{$propertyDetails->summary}}</textarea>
                            </div>

                            <div class="fields-row small-label">
                                <label for="property_details[description]">Description: </label>
                                <textarea style="height:170px!important;" rows="50" cols="50" name="property_details[description]" id="property_details[description]">{{$propertyDetails->description}}</textarea>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[website_url]">URL Site Web: </label>
                                <input type="text" name="property_details[website_url]" id="property_details[website_url]" value="{{$propertyDetails->website_url}}">
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                    <h2>{{__("Equipement du logement")}} :</h2>
                        <div id="property-0-equipment" class="fields-group">
                        </div>
                        {!! $listbox !!}

                    </div>
                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                    <h2>{{__("Emplacement")}} :</h2>
                        
                            <div class="fields-row epm">
                                {{ Form::label('property[country_id]', __("Pays *")) }}
                                {{ Form::select('property[country_id]', country($property->country_id), $property->country_id) }}
                            </div>
                            <div class="fields-row epm">
                                {{ Form::label('property[departement]', __("Département")) }}
                                {{ Form::select('property[departement]', [$property->departement => departements()[$property->departement]], $property->departement) }}
                            </div>
                            <div class="fields-row epm">
                                {{ Form::label('property[city_id]', __("Ville *")) }}
                                {{ Form::select('property[city_id]', [$property->city_id => cities()[$property->city_id]], $property->city_id) }}
                            </div>
                            <div class="fields-row epm">
                                {{ Form::label('property[address]', __("Adresse du logement")) }}
                                {{ Form::textarea('property[address]', $property->address, ['required' => 'required', 'disabled' => 'disabled']) }}
                            </div>
                    </div>
                    <div class="tab-pane" id="tabs-5" role="tabpanel">
                        <h2>{{__("Details du logement")}} :</h2>
                        <div class="fields-group">
                            <div class="fields-row" >
                                {{ Form::label('property_details[entry]', __("Entrée")) }}
                                {{ Form::text('property_details[entry]', $propertyDetails->entry) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[access]', __("Accès immeuble")) }}
                                {{ Form::select('property_details[access]', [
                                    'key' => __("Clé"),
                                    'badge' => __("Badge"),
                                    'code' => __("Code"),
                                ], $propertyDetails->access, ['class' => 'access']) }}
                                {{ Form::text('property_details[access_code]', $propertyDetails->access_code, ['class'=>'code-field access_code', 'placeholder'=>'code']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[access_car]', __("Accès garage ou résidence")) }}
                                {{ Form::select('property_details[access_car]', [
                                    0 => __("Aucun"),
                                    'key' => __("Clé"),
                                    'badge' => __("Badge"),
                                    'code' => __("Code"),
                                ], $propertyDetails->access_car, ['class' => 'access']) }}
                                {{ Form::text('property_details[access_car_code]', $propertyDetails->access_car_code, ['class'=>'code-field access_car_code', 'placeholder'=>'code']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[elevator]', __("Ascenseur")) }}
                                <div class="radio-field">
                                    <div class="custom-radio">
                                        {{ Form::label('elevator_yes', __("Oui")) }}
                                        {{ Form::radio('property_details[elevator]', '1', ($propertyDetails->elevator == 1), ['id' => 'elevator_yes']) }}
                                    </div>
                                    <div class="custom-radio">
                                        {{ Form::label('elevator_no', __("Non")) }}
                                        {{ Form::radio('property_details[elevator]', '0', ($propertyDetails->elevator == 0), ['id' => 'elevator_no',]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[door]', __("Indication du logement (n° de porte, autre)")) }}
                                {{ Form::text('property_details[door]', $propertyDetails->door) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[capacity]', __("Capacité d'accueil")) }}
                                {{ Form::number('property_details[capacity]', $propertyDetails->capacity) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[parking]', __("Place de parking ou garage")) }}
                                {{ Form::text('property_details[parking]', $propertyDetails->parking) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[wifi_login]', __("Wifi")) }}
                                {{ Form::text('property_details[wifi_login]', $propertyDetails->wifi_login, ['placeholder' => __("connexion")]) }}
                                {{ Form::text('property_details[wifi_pass]', $propertyDetails->wifi_pass, ['placeholder' => __("mot de passe")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[water]', __("Robinet d'arrivée d'eau générale")) }}
                                {{ Form::select('property_details[water]', [
                                    'outside' => __("Commun"),
                                    'inside' => __("Dans l'appartement"),
                                ], $propertyDetails->water) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[electricity]', __("Tableau électrique")) }}
                                {{ Form::textarea('property_details[electricity]',$propertyDetails->electricity, ['placeholder' => __("description de la localisation")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[syndic]', __("Coordonnées syndic. de Copro ou autre")) }}
                                {{ Form::textarea('property_details[syndic]', $propertyDetails->syndic) }}
                            </div>
                        </div>
                        
                    </div>
                    <div class="tab-pane" id="tabs-6" role="tabpanel">
                        <h2>{{__("Liens")}} :</h2>
                        <div class="fields-group">
                            <div class="fields-row epm">
                                {{ Form::label('property[Hostenga_link]', __("Hostenga Travel")) }}
                                {{ Form::text('property[Hostenga_link]', $property->Hostenga_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[Airbnb_link]', __("Airbnb")) }}
                                {{ Form::text('property[Airbnb_link]', $property->Airbnb_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[Booking_link]', __("Booking")) }}
                                {{ Form::text('property[Booking_link]', $property->Booking_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[VRBO_link]', __("VRBO/ABRITEL")) }}
                                {{ Form::text('property[VRBO_link]', $property->VRBO_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[VEEPEE_link]', __("VEEPEE")) }}
                                {{ Form::text('property[VEEPEE_link]', $property->VEEPEE_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[autre1_link]', __("Autre")) }}
                                {{ Form::text('property[autre1_link]', $property->autre1_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[autre2_link]', __("Autre")) }}
                                {{ Form::text('property[autre2_link]', $property->autre2_link, ['disabled' => 'disabled']) }}
                            </div>

                            <div class="fields-row epm">
                                {{ Form::label('property[autre3_link]', __("Autre")) }}
                                {{ Form::text('property[autre3_link]', $property->autre3_link, ['disabled' => 'disabled']) }}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-7" role="tabpanel">
                        <h2>{{__("Coordonnée bancaire")}} :</h2>
                        <div class="fields-group">
                        <div class="fields-row large">
                                {{ Form::label('property[iban]', __("IBAN *")) }}
                                {{ Form::text('property[iban]', $property->iban, ['required' => 'required']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property[bic]', __("BIC *")) }}
                                {{ Form::text('property[bic]', $property->bic, ['required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                    <div>
                        {{ Form::submit(__("Enregistrer"), ['class' => 'btn btn-primary']) }}
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function () {

    });
</script>
@append
