@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url("properties")}}">{{  __("Mes Propriétés") }}</a>
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <a href="#">{{  __("Enregistrement d'une nouvelle propriété") }}</a>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="alert alert-warning">
                        {{  __("Les informations suivi d'un * sont obligatoires") }}
                    </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">{{ __("Général")}}</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Description</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">{{__("Equipements")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">{{__("Emplacement")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-5" role="tab">{{__("Details du logement")}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-7" role="tab">{{__("Coordonnée bancaire")}}</a>
                    </li>
                </ul><!-- Tab panes -->
                {{ Form::open(['class' => 'property-form', 'id' => 'property-form', 'method'=>'POST', 'action' => ['AccountController@createProperty']]) }}
                <div>

                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                    <h2>{{  __("Informations du logement") }} :</h2>
                        <div id="property-0" class="fields-group">

                            <div class="fields-row">
                                <label for="property[name]">{{__("Libellé")}}:</label>
                                <input type="text" class="form-control" name="property[name]" id="property[name]">
                            </div>
                            <div class="fields-row">
                             <label for="property_details[rental_type]">{{__("Type de location")}}:</label>
                                <select name="property_details[rental_type]" id="property_details[rental_type]">
                                    <option value="" label=" "></option>
                                    <option value="apartment">Appartement</option>
                                    <option value="condominium">Appartement en résidence</option>
                                    <option value="plane">Avion</option>
                                    <option value="bastide">Bastide</option>
                                    <option value="boat">Bateau</option>
                                    <option value="bungalow">Bungalow</option>
                                    <option value="treehouse">Cabane dans les arbres</option>
                                    <option value="recreational-vehicle">Camping-car/caravane</option>
                                    <option value="chalet">Chalet</option>
                                    <option value="bed-and-breakfast">Chambre d'hôtes</option>
                                    <option value="private-room">Chambre privée</option>
                                    <option value="private-room-in-apartment">Chambre privée dans un appartement</option>
                                    <option value="private-room-in-house">Chambre privée dans une maison</option>
                                    <option value="castle">Château</option>
                                    <option value="cottage">Cottage</option>
                                    <option value="dormitory">Dortoir</option>
                                    <option value="farmhouse">Ferme</option>
                                    <option value="gite">Gîte</option>
                                    <option value="cave">Habitation troglodyte</option>
                                    <option value="hut">Hutte</option>
                                    <option value="igloo">Igloo</option>
                                    <option value="island">Ile</option>
                                    <option value="loft">Loft</option>
                                    <option value="longere">Longère</option>
                                    <option value="house">Maison</option>
                                    <option value="holiday-home">Maison de vacances</option>
                                    <option value="townhouse">Maison de ville</option>
                                    <option value="earth-house">Maison écologique</option>
                                    <option value="manor">Manoir</option>
                                    <option value="mas">Mas</option>
                                    <option value="mill">Moulin</option>
                                    <option value="lighthouse">Phare</option>
                                    <option value="cabin">Refuge</option>
                                    <option selected="selected" value="studio">Studio</option>
                                    <option value="tent">Tente</option>
                                    <option value="tipi">Tipi</option>
                                    <option value="train">Train</option>
                                    <option value="villa">Villa</option>
                                    <option value="yurt">Yourte</option>
                                </select>
                            </div>
                            <div class="fields-row">
                                <label for="property_details[residence]">{{__("Catégorie de résidence")}}: </label>
                                <select name="property_details[residence]" id="property_details[residence]">
                                    <option selected="selected" value="primary_residence">Résidence principale</option>
                                    <option value="secondary_residence">Résidence secondaire</option>
                                    <option value="non_residential">Hébergement non résidentiel</option>
                                </select>
                            </div>
                            <div class="fields-row">
                                <label for="property_details[sleeps]">{{__("Couchages")}}: </label>
                                <input type="number" name="property_details[sleeps]" id="property_details[sleeps]">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[sleepsMax]">{{__("Couchages Max")}}: </label>
                                <input type="number" name="property_details[sleepsMax]" id="property_details[sleepsMax]">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[surface]">{{__("Surface")}}: </label>
                                <input min="10" type="number" name="property_details[surface]" id="property_details[surface]"> m²
                            </div>
                            <div class="fields-row">
                                <label for="property_details[floors]">{{__("Étages")}}: </label>
                                <input min="0" type="number" name="property_details[floors]" id="property_details[floors]">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[floor]">{{__("Numéro d'étage")}}: </label>
                                <input min="0" type="number" name="property_details[floor]" id="property_details[floor]">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[permis]">{{__("Permis/Licence/Numéro d'enregistrement")}}: </label>
                                <input type="text" name="property_details[permis]" id="property_details[permis]">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[delivery_time]">{{__("Date de délivrance")}}: </label>
                                <input type="date" name="property_details[delivery_time]" id="property_details[delivery_time]">
                            </div>
                            <div class="fields-row">
                                <label for="property_details[Management_type]">{{__("Type de gestion de la location:")}}: </label>
                                <select name="property_details[Management_type]" id="property_details[Management_type]">
                                    <option selected="selected" value="professional">Location professionnelle</option>
                                    <option value="non_professional">Location non professionnelle</option>
                                </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[rooms]">{{__("Nombre de chambres")}}</label>
                                <select name="property_details[rooms]" id="property_details[rooms]">
                                    @foreach ($options2 as $option)
                                        <option value="{{$option}}">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[bathrooms]">{{__("Nombre de salle de bains")}}</label>
                                <select name="property_details[bathrooms]" id="property_details[bathrooms]">
                                    @foreach ($options2 as $option)
                                        <option value="{{$option}}">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[wc]">{{__("Nombre de toilettes")}}</label>
                                <select name="property_details[wc]" id="property_details[wc]">
                                    @foreach ($options2 as $option)
                                        <option value="{{$option}}">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[bathtubs]">{{__("Nombre de baignoires")}}</label>
                                <select name="property_details[bathtubs]" id="property_details[bathtubs]">
                                    @foreach ($options2 as $option)
                                        <option value="{{$option}}">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[showers]">{{__("Nombre de douches")}}</label>
                                <select name="property_details[showers]" id="property_details[showers]">
                                    @foreach ($options2 as $option)
                                        <option value="{{$option}}">{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="fields-row small-label">
                                <label>{{__("Lit(s) double(s)")}}</label>
                                <div id="beds">
                                    <label for="property_details[k140]">{{__("Lits 140 cm")}}</label>
                                    <select name="property_details[k140]" id="property_details[k140]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                                    
                                    <label for="property_details[k160]">{{__("Lits 160 cm")}}</label>
                                    <select name="property_details[k160]" id="property_details[k160]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>

                                    <label for="property_details[k180]">{{__("Lits 180 cm")}}</label>
                                    <select name="property_details[k180]" id="property_details[k180]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                                    
                                    <label for="property_details[k200]">{{__("Lits 200 cm")}}</label>
                                    <select name="property_details[k200]" id="property_details[k200]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="fields-row small-label">
                                    <label for="property_details[beds]">{{__("Lit(s) simple(s)")}}</label>
                                    <select name="property_details[beds]" id="property_details[beds]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[bunkbeds]">{{__("Lit(s) superposé(s)")}}</label>
                                    <select name="property_details[bunkbeds]" id="property_details[bunkbeds]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[couchbeds]">{{__("Canapé(s) lit(s)")}}</label>
                                    <select name="property_details[couchbeds]" id="property_details[couchbeds]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[covers]">{{__("Couette(s)")}}</label>
                                    <select name="property_details[covers]" id="property_details[covers]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[square_pillows]">{{__("Oreiller(s) carré(s)")}}</label>
                                    <select name="property_details[square_pillows]" id="property_details[square_pillows]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[rect_pillows]">{{__("Oreiller(s) rectangulaire(s)")}}</label>
                                    <select name="property_details[rect_pillows]" id="property_details[rect_pillows]">
                                        @foreach ($options2 as $option)
                                            <option value="{{$option}}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                            </div>                           

                        </div>
                    </div>
                    <!-- hidden -->
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <h2>Description</h2>
                        <div id="property-0-details" class="fields-group">
                            <div class="fields-row small-label">
                                <label for="property_details[title]">Titre: </label>
                                <input type="text" name="property_details[title]" id="property_details[title]">
                            </div>

                            <div class="fields-row small-label">
                                <label for="property_details[summary]">Résumé: </label>
                                <textarea style="height:70px!important;" rows="10" cols="50" name="property_details[summary]" id="property_details[summary]"></textarea>
                            </div>

                            <div class="fields-row small-label">
                                <label for="property_details[description]">Description: </label>
                                <textarea style="height:170px!important;" rows="50" cols="50" name="property_details[description]" id="property_details[description]"></textarea>
                            </div>
                            <div class="fields-row small-label">
                                <label for="property_details[website_url]">URL Site Web: </label>
                                <input type="text" name="property_details[website_url]" id="property_details[website_url]">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                    <h2>{{__("Equipement du logement")}} :</h2>
                        <div id="property-0-equipment" class="fields-group">
                        </div>
                        {!! $listbox !!}

                    </div>
                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                    <h2>{{__("Emplacement")}} :</h2>
                        <div id="property-0-equipment" class="fields-group">
                            <div class="fields-row epm">
                                {{ Form::label('property[country_id]', __("Pays") .' *') }}
                                {{ Form::select('property[country_id]', countries(), ['required' => 'required']) }}
                            </div>
                        </div>
                        <div class="fields-row epm">
                                {{ Form::label('property[departement]', __("Département") .' *') }}
                                {{ Form::select('property[departement]', departements(), ["required" => "required"]) }}
                        </div>
                        <div class="fields-row epm">
                                {{ Form::label('property[city_id]', __("Ville") .' *') }}
                                {{ Form::select('property[city_id]', cities(), ['required' => 'required']) }}
                        </div>
                        <div class="fields-row epm">
                                {{ Form::label('property[address]', __("Adresse du logement") .' *') }}
                                {{ Form::textarea('property[address]', '', ['required' => 'required', 'placeholder' => __("L'adresse de ma propriété complète (15 Avenue d'exemple, 95000, France)")]) }}
                        </div>

                    </div>

                    <div class="tab-pane" id="tabs-5" role="tabpanel">
                        <h2>{{__("Details du logement")}} :</h2>
                        <div class="fields-row">
                                {{ Form::label('property_details[residence]', __("Résidence")) }}
                                {{ Form::text('property_details[residence]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[entry]', __("Entrée")) }}
                                {{ Form::text('property_details[entry]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[access]', __("Accès immeuble")) }}
                                {{ Form::select('property_details[access]', [
                                    'key' => 'Clé',
                                    'badge' => 'Badge',
                                    'code' => 'Code',
                                ], null, ['class' => 'access']) }}
                                {{ Form::text('property_details[access_code]', null, ['class'=>'code-field access_code', 'placeholder'=>'code']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[access_car]', __("Accès garage ou résidence")) }}
                                {{ Form::select('property_details[access_car]', [
                                    0 => __("Aucun"),
                                    'key' => __("Clé"),
                                    'badge' => __("Badge"),
                                    'code' => __("Code"),
                                ], null, ['class' => 'access']) }}
                                {{ Form::text('property_details[access_car_code]', null, ['class'=>'code-field access_car_code', 'placeholder'=>'code']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[elevator]', __("Ascenseur")) }}
                                <div class="radio-field">
                                    <div class="custom-radio">
                                        {{ Form::label('elevator_yes', __("Oui")) }}
                                        {{ Form::radio('property_details[elevator]', '1', true, ['id' => 'elevator_yes']) }}
                                    </div>
                                    <div class="custom-radio">
                                        {{ Form::label('elevator_no', __("Non")) }}
                                        {{ Form::radio('property_details[elevator]', '0', false, ['id' => 'elevator_no',]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[door]', __("Indication du logement (n° de porte, autre)")) }}
                                {{ Form::text('property_details[door]', null) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[capacity]', __("Capacité d'accueil")) }}
                                {{ Form::number('property_details[capacity]', null) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[parking]', __("Place de parking ou garage")) }}
                                {{ Form::text('property_details[parking]', null) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[wifi_login]', __("Wifi")) }}
                                {{ Form::text('property_details[wifi_login]', null, ['placeholder' => __("login")]) }}
                                {{ Form::text('property_details[wifi_pass]', null, ['placeholder' => __("mot de passe")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[water]', __("Robinet d'arrivée d'eau générale")) }}
                                {{ Form::select('property_details[water]', [
                                    'outside' => __("Commun"),
                                    'inside' => __("dans l'appartement"),
                                ], null) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[electricity]', __("Tableau électrique")) }}
                                {{ Form::textarea('property_details[electricity]',null, ['placeholder' => __("description de la localisation")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property_details[syndic]', __("Coordonnées syndic. de Copro ou autre")) }}
                                {{ Form::textarea('property_details[syndic]', null) }}
                            </div>
                    </div>
                    <div class="tab-pane" id="tabs-7" role="tabpanel">
                        <h2>{{__("Coordonnée bancaire")}} :</h2>
                        <div class="fields-group">
                            <div class="fields-row large">
                                    {{ Form::label('property[iban]', __("IBAN *")) }}
                                    {{ Form::text('property[iban]', '', ['required' => 'required', "data-rule-iban" => "true"]) }}
                            </div>
                            <div class="fields-row">
                                    {{ Form::label('property[bic]', __("BIC *")) }}
                                    {{ Form::text('property[bic]', '', ['required' => 'required', "data-rule-bic" => "true"]) }}
                            </div>
                        </div>
                        
                    </div>
                </div>
                        
                        
                        
                    </div>
                    <div>
                        {{ Form::submit(__("Enregistrer"), ['class' => 'btn btn-primary']) }}
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function () {
        $("#property-form").validate({
            lang:"fr"
        });
    });
</script>
@append
