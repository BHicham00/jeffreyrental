@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url("properties")}}">{{  __("Mes Propriétés") }}</a>
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <a href="{{url("property/".$property->id)}}">{{  __("Propriété") }} : {{$property->name}}</a>
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <a href="#">{{  __("Documents") }}</a>

                    <div id="property-tools">
                        <a class="edit-property btn btn-primary" href="{{url("/property/".$property->id)}}">{{  __("Informations") }}</a>
                        <a class="reports-property btn btn-warning" href="{{url("/property/".$property->id."/reports/")}}">{{  __("Rapports") }}</a>
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    @if (!$property->hasRanking())
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"> </i>
                        {{  __("Envoyez votre décision de classement sur cette page") }}.
                    </div>
                    @endif
                    @if (!$property->hasAttestLoc())
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"> </i>
                        {{  __("Envoyez votre attestation de location sur cette page") }}.
                    </div>
                    @endif
                    <div>{{  __("Ici vous pouvez retrouver vos différents documents relatifs à votre propriété") }}.</div>
                    <!--<div id="contract" class="file col-sm-12 alert alert-{{$property->hasContract()?"success":"danger"}}">
                        <div class="file-top">
                            <h3>
                                Mon contrat avec Jeffrey
                            </h3>
                            @if(!$property->hasContract())
                            <a id="contract-gen-link" target="_blank" href="{{url("/contract/".$property->id)}}">Générer et télécharger mon contrat</a>
                        </div>
                        {{ Form::open(['method'=>'POST', 'enctype' =>'multipart/form-data', 'action' => ['AccountController@uploadRanking', $property->id]]) }}
                        <label>Envoyez votre contrat signé (au format pdf)</label>
                        <input id="contract-input" name="contract" type="file" accept=".pdf"/>
                        {{ Form::submit("Envoyer", ["class" => "btn btn-primary"]) }}
                        {{ Form::close() }}
                        @else
                            <a id="contract-link" target="about_blank" href="{{url("/signedContract/".$property->id)}}">Télécharger mon contrat</a>
                        </div>
                    @endif
                    </div>-->
                    <!-- ranking -->
                    <div id="contract" class="file col-sm-12 alert alert-{{$property->hasRanking() ? "success" : "danger"}}">
                        <div class="file-top">
                            <h3>
                            {{  __("Décision de classement") }}
                            </h3>
                    @if(!$property->hasRanking())
                        </div>
                        {{ Form::open(['method'=>'POST', 'enctype' =>'multipart/form-data', 'action' => ['AccountController@uploadRanking', $property->id]]) }}
                                <label>{{  __("Envoyez votre document de classement (au format pdf)") }}</label>
                                <input id="contract-input" name="ranking" type="file" accept=".pdf"/>
                        {{ Form::submit( __("Envoyer"), ["class" => "btn btn-primary"]) }}
                        {{ Form::close() }}
                    @else
                        <a id="ranking-link" target="about_blank" href="{{url("/ranking/".$property->id)}}">{{  __("Télécharger mon document de classement") }}</a>
                        </div>
                    @endif
                    </div>

                    <!-- attestation loc -->
                    <div id="contract" class="file col-sm-12 alert alert-{{$property->hasAttestLoc() ? "success" : "danger"}}">
                        <div class="file-top">
                            <h3>
                            {{  __("Mon attestation de location meublé") }}
                            </h3>
                            @if(!$property->hasAttestLoc())
                        </div>
                        {{ Form::open(['method'=>'POST', 'enctype' =>'multipart/form-data', 'action' => ['AccountController@uploadAttestLoc', $property->id]]) }}
                        <label>{{  __("Envoyez votre attestation de location meublé (au format pdf)") }}</label>
                        <input id="contract-input" name="attestloc" type="file" accept=".pdf"/>
                        {{ Form::submit("Envoyer", ["class" => "btn btn-primary"]) }}
                        {{ Form::close() }}
                        @else
                            <a id="ranking-link" target="about_blank" href="{{url("/attestloc/".$property->id)}}">{{  __("Télécharger mon attestation") }}</a>
                    </div>
                    @endif
                </div>

                    <!-- annexe -->
                    <div id="annexe" class="file col-sm-6 alert alert-{{$property->hasAnnexe()?"success":"warning"}}">
                        <div class="file-top">
                            <h3>
                            {{  __("Annexe de contrat (facultatif)") }}
                            </h3>
                            @if(!$property->hasAnnexe())
                                <a href="#">{{  __("Aucun fichier en ligne") }}</a>
                            @else
                                <a target="about_blank" href="{{url("/annexe/".$property->id)}}">{{  __("Télécharger l'annexe de contrat") }}</a>
                            @endif
                        </div>
                    </div>

                    <!-- inventaire -->
                    <div id="inventory" class="file col-sm-6 alert alert-{{$property->hasInventory()?"success":"danger"}}">
                        <div class="file-top">
                            <h3>
                            {{  __("L'inventaire de ma propriété") }}
                            </h3>
                            @if(!$property->hasInventory())
                            <a href="#">{{  __("Aucun fichier en ligne") }}</a>
                            @else
                            <a target="about_blank" href="{{url("/inventory/".$property->id)}}">{{  __("Télécharger l'inventaire") }}</a>
                            @endif
                        </div>
                    </div>
            
                    <!-- état des lieux -->
                    <div id="assessment" class="file col-sm-6 alert alert-{{$property->hasAssess()?"success":"danger"}}">
                        <div class="file-top">
                            <h3>
                                {{  __("L'état des lieux de ma propriété") }}
                            </h3>
                            @if(!$property->hasAssess())
                            <a href="#">{{  __("Aucun fichier en ligne") }}</a>
                            @else
                            <a target="about_blank" href="{{url("/assessment/".$property->id)}}">{{  __("Télécharger l'état des lieux") }}</a>
                            @endif
                        </div>
                    </div>
                    
                    <!-- clés -->
                    <div id="assessment" class="file col-sm-6 alert alert-{{$property->hasKeysFile()?"success":"danger"}}">
                        <div class="file-top">
                            <h3>
                                {{  __("L'attestation de remise des clés") }}
                            </h3>
                            @if(!$property->hasKeysFile())
                            <a href="#">{{  __("Aucun fichier en ligne") }}</a>
                            @else
                            <a target="about_blank" href="{{url("/keys/".$property->id)}}">{{  __("Télécharger l'attestation") }}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function () {
        var url = null;
        /*$("#contract-gen-link").click(function (e) {
            if (url)
                window.open(url);
            else {
                let link = $(this);
                $.get($(this).attr("href"), null, function (data) {
                    data = JSON.parse(data);
                    url = data.report_url;
                    var open = window.open(data.report_url);
                    link.html("Télécharger mon contrat");
                    if (open == null || typeof (open) == 'undefined')
                        alert("Contrat généré, cliquez à nouveau sur le lien SVP.");
                });
            }
            return false;
        });*/
        /*$("#contract-input").change(function(){
         $(this).parent().submit(); 
         });*/
    });
</script>
@append
