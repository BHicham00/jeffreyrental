@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{url("properties")}}">{{  __("Mes Propriétés") }}</a>
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <a href="{{url("property/".$property->id)}}">{{  __("Propriété") }} : {{$property->name}}</a>
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <a href="#">{{  __("Rapports") }}</a>
                    
                    <div id="property-tools">
                        <a class="edit-property btn btn-primary" href="{{url("/property/".$property->id)}}">{{  __("Informations") }}</a>
                        <a class="reports-property btn btn-success" href="{{url("/property/".$property->id."/documents/")}}">{{  __("Documents") }}</a>
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div>{{  __("Ici vous pouvez retrouver les rapports de location de votre propriété") }}.</div>
                    
                    @foreach($reports as $rd)
                    <div class="front-box">
                        <h3>Rapport de {{$rd["month"]}} {{$rd["year"]}}</h3>
                        <h4>CA : {{$rd["ca"]}} €</h4>
                        <a class="reports-property btn btn-primary" target="about_blank" href="{{url("/property/".$property->id."/report/".$rd["report"]->id)}}">{{  __("Rapport") }}</a>
                        <a class="reports-property btn btn-success" target="about_blank" href="{{url("/property/".$property->id."/invoice/".$rd["report"]->id)}}">{{  __("Facture") }}</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function () {

    });
</script>
@append
