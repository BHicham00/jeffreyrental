@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{  __("Formulaires d'inscription") }} : {{  __("client") }} {{$clientId}}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $name => $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="alert alert-warning">
                    {{  __("Les informations suivi d'un * sont obligatoires") }}
                </div>
                <div class="panel-content">
                    {{ Form::open(['id' => 'registration-form', 'method'=>'POST', 'action' => ['HomeController@invitation']]) }}
                    {{ Form::hidden('entry', $token) }}
                    <div>
                        <h2>{{  __("Informations de contact") }} :</h2>
                        <div class="fields-row">
                            {{ Form::label('', {{  __("Je suis :") }}) }}
                            <div class="radio-field">
                                <div class="custom-radio">
                                    {{ Form::label('user', {{  __("un particulier") }}, ['class' => "custom-control-label"]) }}
                                    {{ Form::radio('society', 'user', true, ['id' => 'user', 'class' => "user-radio"]) }}
                                </div>
                                <div class="custom-radio">
                                    {{ Form::label('company', {{  __("une société") }}, ['class' => "custom-control-label"]) }}
                                    {{ Form::radio('society', 'company', false, ['id' => 'company', 'class' => "user-radio"]) }}
                                </div>
                            </div>
                        </div>
                        <div id="user-info" class="field-group">
                            <div class="col-md-6">
                                <div class="fields-row">
                                    {{ Form::label('firstname', {{  __("Prénom *") }}) }}
                                    {{ Form::text('firstname', '', ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('lastname', {{  __("Nom *") }}) }}
                                    {{ Form::text('lastname', '',  ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('email', {{  __("Email *") }}) }}
                                    {{ Form::email('email', $email,  ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('phone', {{  __("Téléphone *") }}) }}
                                    {{ Form::text('phone', '', ['required' => 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="fields-row">
                                    {{ Form::label('password', {{  __("Mot de passe pour votre accès client *") }}) }}
                                    {{ Form::password('password', ['required' => 'required', 'pattern' => '.{6,}']) }} {{  __("(minimum 6 caractères)")
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('password', {{  __("Confirmation du mot de passe *") }}) }}
                                    {{ Form::password('password_confirm', ['required' => 'required', 'pattern' => '.{6,}']) }}
                                </div>
                            </div>

                            <div class="fields-row">
                                {{ Form::label('address', {{  __("Adresse de correspondance *") }}) }}
                                {{ Form::textarea('address', '', ['required' => 'required', 'placeholder' => {{  __("Mon adresse personnelle complète (15 Avenue d'exemple, 95000, France)") }}]) }}
                            </div>
                        </div>
                        <div id="company-info" class="field-group">
                            <div class="fields-row">
                                {{ Form::label('company', {{  __("Nom de la société") }}) }}
                                {{ Form::text('company') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('capital', {{  __("Capital (€)") }}) }}
                                {{ Form::number('capital') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('company_number', {{  __("Numéro d'immatriculation") }}) }}
                                {{ Form::text('company_number') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('company_address', {{  __("Adresse de l'immatriculation") }}) }}
                                {{ Form::textarea('company_address') }}
                            </div>
                        </div>
                    </div>
                    {{ Form::submit({{  __("Envoyer") }}, ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
