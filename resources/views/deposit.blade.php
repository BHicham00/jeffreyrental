@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Päiement de la caution pour séjour à : <b>{{$deposit->property()->first()->name}}</b></div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $name => $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="panel-content">
                    @if($deposit)
                    <div class="row">
                        <div id="booking-info" class="col-sm-6">
                            @if($client->lang == "fr")
                            <div>
                                Nom du client: <b>{{$client->firstname}} {{$client->lastname}}</b>
                            </div>
                            <div>
                                Séjour à : <b>{{$deposit->city}}</b>
                            </div>
                            <div>
                                Arrivé le : <b>{{$startAt->format("d/m/Y")}}</b>
                            </div>
                            <div>
                                Départ le : <b>{{$endAt->format("d/m/Y")}}</b>
                            </div>
                            @if(!$deposit->paid)
                            <div>
                                Caution à régler : <b>{{$deposit->amount}} €</b>
                            </div>
                            @endif
                            @else
                            <div>
                                Guest name: <b>{{$client->firstname}} {{$client->lastname}}</b>
                            </div>
                            <div>
                                Trip in : <b>{{$deposit->city}}</b>
                            </div>
                            <div>
                                From : <b>{{$startAt->format("d/m/Y")}}</b>
                            </div>
                            <div>
                                To : <b>{{$endAt->format("d/m/Y")}}</b>
                            </div>
                            @if(!$deposit->paid)
                            <div>
                                Damage deposit : <b>{{$deposit->amount}} €</b>
                            </div>
                            @endif
                            @endif
                        </div>
                        @if($deposit->paid)
                        <div class="col-sm-6" style="color:#292; font-weight: bold;font-size:20px;">
                            @if($client->lang == "fr")
                            Caution en traitement
                            @else
                            Damage deposit in processing
                            @endif
                            <i class="fa fa-check"></i>
                        </div>
                        @endif
                        @if(!$deposit->paid)
                        <!--action="https://secure.ogone.com/ncol/prod/orderstandard_utf8.asp"-->
                        <form  class="col-sm-6" method="post"  id="pay-form" name="pay-form">

                            <!-- paramètres généraux : voir Paramètres de formulaire -->
                            @foreach($params as $k => $p)
                            <!--<input type="hidden" name="{{$k}}" value="{{$p}}">-->
                            @endforeach
                            <!--<input type="hidden" name="SHASIGN" value="$shain">-->
                            
                            @if($client->lang == "fr")
                            <button id="payup" class="button btn btn-primary" type="submit">Payer la caution</button>
                            <div style="color:#f00">
                                Date limite : <b>{{$startAt->addDays(-1)->format("d/m/Y")}}</b>
                            </div>
                            @else
                            <button id="payup" class="button btn btn-primary" type="submit">Pay the damage deposit</button>
                            <div style="color:#f00">
                                Due date : <b>{{$startAt->addDays(-1)->format("d/m/Y")}}</b>
                            </div>
                            @endif
                        </form>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script src="https://js.stripe.com/v3/"></script>
<script>
$(function () {
    var stripe = Stripe('{{config("stripe.publicKey")}}');

    $("#payup").click(function (e) {
        e.preventDefault();
        stripe.redirectToCheckout({
            sessionId: '{{$checkoutSessionId}}'
        }).then(function (result) {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
            alert(result.error.message);
        });
    });

});
</script>
@append