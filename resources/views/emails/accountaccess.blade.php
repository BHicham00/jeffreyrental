@include('emails.header')
<br/>
Madame, Monsieur,<br/>
<br/>
Nous avons le plaisir de vous informer que nous avons travaillé avec notre développeur à la
création d’une application pour vous apporter un compte propriétaire en ligne.<br/>
<br/>
Veuillez trouver ci-joint vos identifiants pour vous connecter :<br/>
Lien : {{url('account')}}<br/>
Mail : {{$user->email}}<br/>
Mot de passe : XjeffreyX<br/>
<br/>
Nous vous conseillons de vous choisir un nouveau mot de passe en passant par le lien suivant :<br/>
{{url('auth/password/reset')}}
<br/>
<br/>
Vous pourrez retrouver dans votre espace, divers documents, comme l’état des lieux,
l’inventaire, l’attestation de remise de clés et vos relevés ainsi que le lien de votre calendrier.
<br/>

@include('emails.footer')