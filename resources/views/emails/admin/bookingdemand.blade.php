Le client 
<b><a href="{{$linkUser}}" target="_blank">{{$user->name}}</b>
vient de faire une demande pour réserver son logement 
<b><a href="{{$linkProperty}}" target="_blank">{{$property->name}}</a></b>
du {{$params["start_at"]}} au {{$params["end_at"]}}.<br/> 
<br/><br/>
Détails de la réservation :<br/>
 - Propriété : <b><a href="{{$linkProperty}}" target="_blank">{{$property->name}}</a></b><br/>
 - date d'arrivée : {{$params["start_at"]}}<br/>
 - date de départ : {{$params["end_at"]}}<br/>
 - nombre de personnes : {{$params["guests"]}}<br/>
@if(isset($params["checkin"]))
 - Check-in demandé<br/>
@endif
@if(isset($params["checkout"]))
 - Check-out demandé<br/>
@endif
@if(isset($params["cleaning"]))
 - Menage demandé<br/>
@endif
@if(isset($params["sheets"]))
 - Linge demandé<br/>
@endif
<br/><br/>
voici un lien pour accéder au calendrier BookingSync de la propriété "{{$property->name}}" : <a href="{{$linkBooking}}">{{$linkBooking}}</a>.
<br/><br/>
@include('emails.footer')