Le client <b>{{$property->getUser()->name}}</b> vient d'envoyer son contrat signé pour la propriété <b>{{$property->name}}</b><br/> 
<br/><br/>
ci-dessous voici un lien pour accéder à la fiche propriété et vérifier le contrat : {{$link}}.
<br/><br/>
@if($property->bookingsync_id)
<b>Attention, la propriété qu'il a enregistré n'a pas encore de lien avec la plateforme BookingSync, n'oubliez pas de renseigner le champ BookingSync ID de la propriété dès que possible.</b><br/>
@endif
@include('emails.footer')