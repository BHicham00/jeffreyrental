Le client <b>{{$user->name}}</b> vient de terminer son enregistrement sur le back-office<br/> 
<br/><br/>
ci-dessous voici un lien pour accéder à son profil et vérifier ses informations : {{$link}}.
<br/><br/>
<b>Attention, la propriété qu'il a enregistré n'a pas encore de lien avec la plateforme BookingSync, n'oubliez pas de renseigner le champ BookingSync ID de la propriété dès que possible.</b>
@include('emails.footer')