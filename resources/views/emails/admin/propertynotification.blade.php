Le client <b>{{$user->name}}</b> vient d'ajouter la propriété <b>{{$property->name}}</b> à son compte<br/> 
<br/><br/>
ci-dessous voici un lien pour accéder à ses informations et les vérifier : {{$link}}.
<br/><br/>
<b>Attention, la propriété qu'il a enregistré n'a pas encore de lien avec la plateforme BookingSync, n'oubliez pas de renseigner le champ BookingSync ID de la propriété dès que possible.</b>
@include('emails.footer')