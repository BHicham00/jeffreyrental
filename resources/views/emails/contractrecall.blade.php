@include('emails.header')
Bonjour {{$user->name}},<br/>
vous avez récemment ajouté la propriété <b>{{$property->name}}</b> à votre compte Jeffrey Host.
<br/><br/>
Le contrat se rapportant à votre propriété n'a pas encore été signé et renvoyé<br/>
voici un lien pour accéder à la génération de votre contrat qu'il vous faudra télécharger, signer et ensuite renvoyer en utilisant la même page : {{$link}}.<br/>
@include('emails.footer')
