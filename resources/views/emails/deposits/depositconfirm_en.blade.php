@include("emails.header")
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="padding-copy p">
      Dear {{ $client->firstname . " " . $client->lastname }},<br><br>
      We are processing the deposit for your accommodation in {{ $deposit->city }} It will be automatically credited to you within 14 days after your departure.
    </td>
  </tr>
  <tr>
    <td class="padding-copy p">
          </td>
  </tr>
  <tr>
    <td class="padding-copy p">
      Wishing you a great day,
    </td>
  </tr>
</table>
@include("emails.footer")