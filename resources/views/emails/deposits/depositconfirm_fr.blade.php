@include("emails.header")
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="padding-copy p">
      Bonjour {{ $client->firstname . " " . $client->lastname }},<br><br>
      la caution pour votre logement à {{ $deposit->city }} est en traitement, celle ci vous sera automatiquement créditée dans les 14 jours aprés votre départ .
    </td>
  </tr>
  <tr>
    <td class="padding-copy p">
          </td>
  </tr>
  <tr>
    <td class="padding-copy p">
      Nous vous souhaitons une agréable journée,
    </td>
  </tr>
</table>
@include("emails.footer")