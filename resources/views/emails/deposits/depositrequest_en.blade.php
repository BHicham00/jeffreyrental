@include("emails.header")
<table width="100%" border="0" cellspacing="0" cellpadding="0">    
    <tr>
        <td class="padding-copy p">
            Dear {{ $client->firstname . " " . $client->lastname }},<br><br>
            We are writing to you in connection with your accommodation booking.<br/>
            Please read this email carefully, as it contains important information about your stay in {{ $deposit->city }}.
        </td>
    </tr>
    <tr>
        <td class="padding-copy p">
            Please note that the full payment of the Refundable Security Deposit must be received by us at least 14 days
            before arrival in accordance with our Booking General Terms and Conditions.
        </td>
    </tr>
    <tr>
        <td class="padding-copy p">
            The security deposit for {{ $deposit->property()->first()->name }} will be refunded in full within 14 days after departure provided there is no loss or damage to the property.
        </td>  
    </tr>

    <tr>
        <td>
            Here is your payment link : <a href="{{ url("/deposit/".$deposit->id."?token=".$deposit->token) }}">{{ url("/deposit/".$deposit->id."?token=".$deposit->token) }}</a>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="padding-copy p">
            <b>As shown on your booking site, the security damage deposit is MANDATORY, if it is not paid under 48h your booking will be cancelled and your previous payments WILL NOT BE REFUNDED</b>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="padding-copy p">
            We wish you a pleasant stay in {{ $deposit->city }} We can't wait to welcome you soon!
        </td>
    </tr>
</table>
@include("emails.footer")