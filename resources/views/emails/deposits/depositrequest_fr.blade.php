@include("emails.header")
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="padding-copy p">
            Bonjour {{ $client->firstname . " " . $client->lastname }},<br><br>
            Nous vous adressons une information importante concernant votre séjour à {{ $deposit->city }}.
        </td>
    </tr>
    <tr>
        <td class="padding-copy p">
            En effet la caution de {{ $deposit->amount }} euros, doit selon les conditions générales de réservation être réglée dans les 14 jours qui précédent votre arrivée,
            celle-ci vous sera créditée via votre carte bancaire dans les 14 jours (délais maximum) après votre départ.
        </td>
    </tr>
    <tr>
        <td class="padding-copy p">
            Voici votre lien de paiement : <a href="{{ url("/deposit/".$deposit->id."?token=".$deposit->token) }}">{{ url("/deposit/".$deposit->id."?token=".$deposit->token) }}</a>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="padding-copy p">
            <b>Comme indiqué sur le site de votre réservation,le paiement de cette caution est OBLIGATOIRE  sans quoi votre réservation sera annulée et le(s) paiement(s) déjà effectué NE SERONT PAS REMBOURSE(S)</b>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="padding-copy p">
            Nous vous souhaitons une agréable journée, votre séjour à {{ $deposit->city }} vous attend !
        </td>
    </tr>
</table>
@include("emails.footer")