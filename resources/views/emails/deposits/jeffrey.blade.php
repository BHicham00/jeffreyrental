@include("emails.header")
Bonjour Géry,
<br/>
<br/>
La caution pour la réservation <a href="https://www.bookingsync.com/fr/bookings/{{$deposit->booking_id}}">{{$deposit->booking_id}}</a>,
<br/>
<br/>
au nom du client {{$client->firstname . " " . $client->lastname}}
<br/>
<br/>
d'une valeur de {{$deposit->amount}}€ est en traitement.
<br/>
<br/>
Vous pouvez retrouver les informations de paiement sur l'administration via cette <a href='{{url("/admin/deposits/".$deposit->id."/edit")}}'>page</a> ( bas de page, Pay ID de stripe ).
@include("emails.footer")
