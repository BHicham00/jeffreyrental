@include('emails.header')
<br/>
Dear guest,<br/>
<br/>
you will find in this email your login information to access your guest dashboard
<br/>
<br/>
Access Link : <a href="{{route('guest.login')}}">{{route('guest.login')}}</a><br/>
E-Mail : {{$user->email}}<br/>
Booking reference : {{$user->booking->reference}}<br/>
<br/>
<br/>
You will find there all you need for your arrival and stay with us.
<br/>
Best Regards.
@include('emails.guest.footer')
@include('emails.footer')