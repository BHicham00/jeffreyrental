@include('emails.header')
<br/>
Bonjour,<br/>
<br/>
vous trouverez dans ce mail les informations nécessaires pour vous connecter à notre espace voyageur
<br/>
<br/>
Lien de login : <a href="{{route('guest.login')}}">{{route('guest.login')}}</a><br/>
E-Mail : {{$user->email}}<br/>
Reférence réservation : {{$user->booking->reference}}<br/>
<br/>
<br/>
Vous trouverez toutes les informations dont vous aurez besoin pour votre séjour sur notre plateforme.
<br/>
Bien à vous.
@include('emails.guest.footer')
@include('emails.footer')