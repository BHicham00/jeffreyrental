@include('emails.header')
Hello {{$user->firstname}},<br/>
<Br/>
We hope your stay at "{{$property->name}}" is going well.<br/>
Here are some useful information about how to collect the keys of your accommodation from our nearby relay collection point:<br/>
<br/>
@if($property->keynest_store)
    Name of the relay point: {{$property->keynest_store->name}}<br/>
    Adress : {{$property->keynest_store->address}}<br/>
    Map Link : <a target="_blank" href="https://www.google.com/maps/?q={{$property->keynest_store->lat}},{{$property->keynest_store->lon}}">Google Maps</a><br/>
    Openings hours :<br/>
    <ul>
        @foreach($property->keynest_store->open_details as $day)
            <li>{{ daysOfWeek()[$day['DayOfWeek']] }} - from {{ formatMinutesTotime($day['StartMinuteOfDay']) }} to {{ formatMinutesTotime($day['EndMinuteOfDay']) }}</li>
        @endforeach
    </ul>
@endif
Check-out before 11:00am<br/>
<br/><br/>
<b>Important :</b><br/>
The keys must be returned at 12:00pm<br/>
If you do not return the property keys, if you return them at an incorrect location or if you lose them, you will be charged extra fees.<br/>
<br/>
<b>In the event of an early departure:</b><br/>
Check-out before 8:00 am or outside of the relay point's opening hours is possible with an additional charge of € 30 to be paid in cash directly to the welcomer when you check in.<br/>
<br/>
We wish you a pleasant day and enjoy the rest of your stay at "{{$property->name}}".<br/>
@include('emails.guest.footer')
@include('emails.footer')