@include('emails.header')
Bonjour {{$user->firstname}},<br/>
<Br/>
Nous espérons que votre séjour à {{$property->name}} se passe bien.<br/>
Informations utiles pour restituer les clés de votre logement en point relais :<br/>
@if($property->keynest_store)
    Nom du point relais : {{$property->keynest_store->name}}<br/>
    Adresse : {{$property->keynest_store->address}}<br/>
    Lien map : <a target="_blank" href="https://www.google.com/maps/?q={{$property->keynest_store->lat}},{{$property->keynest_store->lon}}">Google Maps</a><br/>
    Heure d’ouverture :<br/>
    <ul>
        @foreach($property->keynest_store->open_details as $day)
            <li>{{ daysOfWeek()[$day['DayOfWeek']] }} - de {{ formatMinutesTotime($day['StartMinuteOfDay']) }} à {{ formatMinutesTotime($day['EndMinuteOfDay']) }}</li>
        @endforeach
    </ul>
@endif
Check-out avant 11h00<br/>
<br/><br/>
<b>En cas de départ matinal :</b><br/>
Le check-out en dehors des heures d’ouverture du point relais sont possible avec un supplément de 30 € en espèces à payer directement au welcomer que vous verrez.<br/>
<br/>
<b>Important :</b><br/>
Le retour des clés doit se faire avant 12h00<br/>
Si vous ne restituez pas ou si vous venez à perdre les clés du logement celle-ci vous serons facturées.<br/>
<br/><br/>
Nous vous souhaitons une agréable journée et une bonne fin de séjour à {{$property->name}}.<br/>
@include('emails.guest.footer')
@include('emails.footer')