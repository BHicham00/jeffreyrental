@include('emails.header')
Hello {{$user->firstname}},<br/>
<br/>
Your stay at {{$property->name}} is approaching fast!<br/>
<br/>
Here are some useful information about how to collect the keys of your accommodation from our nearby relay collection point:<br/>
<br/>
@if($property->keynest_store)
Name of the relay point: {{$property->keynest_store->name}}<br/>
Adress : {{$property->keynest_store->address}}<br/>
Map Link : <a target="_blank" href="https://www.google.com/maps/?q={{$property->keynest_store->lat}},{{$property->keynest_store->lon}}">Google Maps</a><br/>
Openings hours :<br/>
<ul>
@foreach($property->keynest_store->open_details as $day)
    <li>{{ daysOfWeek()[$day['DayOfWeek']] }} - from {{ formatMinutesTotime($day['StartMinuteOfDay']) }} to {{ formatMinutesTotime($day['EndMinuteOfDay']) }}</li>
@endforeach
</ul>
@endif
<br/>
Check-in after 3:00 p.m. only<br/>
In case of late arrival:<br/>
Check-in outside of the relay point’s opening hours is possible with an additional charge of € 30 payable in cash to the welcomer who will welcome you. Please call us to book this service.<br/>
<br/>
Here is the code you will need to provide to the relay point in order to collect the keys: <b>{{$booking->keynest_collection}}</b><br/>
<br/>
Here are some useful information about how to find and access your accommodation:<br/>
<br/>
 : {{$property->name}}<br/>
Property name : {{$property->residence}}<br/>
Adress : {{$property->address}}<br/>
City : {{ $property->city->nom_commune . ' ' . $property->city->getPostal() }}<br/>
Access to the property : {{ \App\PropertyDetails::accessTypes()[$property->detail->access] }}<br/>
@if($booking->property->detail->access == 'code')
Code : {{ $property->detail->access_code }}
@endif
<br/>
Floor : {{ $property->detail->floor }}<br/>
Location of the entrance to the accommodation: {{ $property->detail->door ?: 'Aucune'}}<br/>
<br/>
Your accommodation benefits from an autonomous departure.<br/>
You will receive all the information about your check-out procedure 24 hours before your departure. You will need to follow the instructions provided in order to return the keys of the accommodation back to the collection point or to a designated location that will be indicated in our email or text message.<br/>
<br/>
Thank you for choosing us and enjoy your stay at {{$property->name}}.<br/>
@include('emails.guest.footer')
@include('emails.footer')
