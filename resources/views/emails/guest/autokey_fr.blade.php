@include('emails.header')
Bonjour {{$user->firstname}},<br/>
<br/>
Votre séjour à {{$property->name}} approche à grand pas !<br/>
<br/>
Informations utiles pour retirer les clés de votre logement en point relais :<br/>
<br/>
@if($property->keynest_store)
Nom du point relais : {{$property->keynest_store->name}}<br/>
Adresse : {{$property->keynest_store->address}}<br/>
Lien map : <a target="_blank" href="https://www.google.com/maps/?q={{$property->keynest_store->lat}},{{$property->keynest_store->lon}}">Google Maps</a><br/>
Heure d’ouverture :<br/>
<ul>
@foreach($property->keynest_store->open_details as $day)
    <li>{{ daysOfWeek()[$day['DayOfWeek']] }} - de {{ formatMinutesTotime($day['StartMinuteOfDay']) }} à {{ formatMinutesTotime($day['EndMinuteOfDay']) }}</li>
@endforeach
</ul>
@endif
<br/>
Check-in après 15h00<br/>
En cas d’arrivée tardive :<br/>
Le check-in en dehors des heures d’ouverture du point relais sont possible avec un supplément de 30 € en espèces à payer directement au welcomer qui vous accueillera.<br/>
<br/>
Voici le code à communiquer au point relais pour retirer les clés : <b>{{$booking->keynest_collection}}</b><br/>
<br/>
Informations utiles pour vous rendre à votre logement :<br/>
<br/>
Nom du logement : {{$property->name}}<br/>
Nom de la résidence : {{$property->residence}}<br/>
Adresse : {{$property->address}}<br/>
Ville : {{ $property->city->nom_commune . ' ' . $property->city->getPostal() }}<br/>
Accès à la résidence : {{ \App\PropertyDetails::accessTypes()[$property->detail->access] }}<br/>
@if($booking->property->detail->access == 'code')
Code : {{ $property->detail->access_code }}
@endif
<br/>
Étage : {{ $property->detail->floor }}<br/>
Localisation porte d’entrée du logement : {{ $property->detail->door ?: 'Aucune'}}<br/>
<br/>
Votre logement bénéficie d'un départ autonome.<br/>
24 heures avant votre départ, vous recevrez toutes les informations nécessaires pour restituer les clés du logement.<br/>
<br/>
Nous vous souhaitons une agréable journée, votre séjour à {{$property->name}} vous attendent.<br/>
@include('emails.guest.footer')
@include('emails.footer')
