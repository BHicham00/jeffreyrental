@include('emails.header')
Hello {{$user->firstname}},<br/>
<br/>
We hope your stay at "{{$property->name}}" is going well.<br/>
In order to make your departure easy here are a few informations :<br/>
<br/><br/>
<b>Your accommodation uses a self-check-out service:</b><br/>
Your accommodation benefits from a self-check-out service.<br/>
You will receive all the necessary information by email 48 hours before your departure including information about how to return the property keys back to the designated location.<br/>
<br/><br/>
<b>Your accommodation benefits from a regular check-out:</b><br/>
A Jeffrey Host welcomer will contact you 1 or 2 days before your departure to organize the return of the keys of your accommodation.<br/>
<br/><br/>
<b>Important :</b><br/>
If you do not return the property keys, if you return them at an incorrect location or if you lose them, you will be charged extra fees.<br/>
<br/>
<b>In the event of an early departure:</b><br/>
Check-out before 8:00 am or outside of the relay point's opening hours is possible with an additional charge of € 30 to be paid in cash directly to the welcomer when you check in.<br/>
We wish you a pleasant day and enjoy the rest of your stay at "{{$property->name}}".<br/>
@include('emails.guest.footer')
@include('emails.footer')
