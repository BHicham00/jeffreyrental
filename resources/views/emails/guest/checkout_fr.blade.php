@include('emails.header')
Bonjour {{$user->firstname}},<br/>
<br/>
Nous espérons que votre séjour à "{{$property->name}}" se passe bien.<br/>
Afin de préparer au mieux votre départ, voici quelques informations :<br/>
<br/><br/>
<b>Votre logement est en sortie autonome :</b><br/>
Votre logement bénéficie d'un départ autonome.<br/>
48 heures avant votre départ, vous recevrez toutes les informations nécessaires par mail pour restituer les clés de votre logement.<br/>
<br/><br/>
<b>Votre logement bénéficie d’un Check-out physique :</b><br/>
Un welcomer Jeffrey Host, vous contactera dans les 2 jours qui précédent votre départ afin d'organiser la restitution des clés de votre logement.<br/>
<br/><br/>
<b>Important :</b><br/>
Si vous ne restituez pas ou si vous venez à perdre les clés du logement celle-ci vous serons facturées.<br/>
<br/>
<b>En cas de départ matinal :</b><br/>
Le check-out avant 8h00 ou en dehors des heures d’ouverture du point relais sont possible avec un supplément de 30 € en espèces à payer directement au welcomer que vous verrez.<br/>
Nous vous souhaitons une agréable journée et une bonne fin de séjour à "{{$property->name}}".<br/>
@include('emails.guest.footer')
@include('emails.footer')
