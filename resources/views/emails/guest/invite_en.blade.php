@include('emails.header')
@if($guest)
    Hello {{$guest->firstname . " " . $guest->lastname}},<br/>
    <br/>
    You have booked a stay at "{{$booking->property->name}}"<br/>
    Please click the following self-check-in link to receive some information about your<br/>
    accommodation, how to access it and where to collect the keys.<br/>
    <br/><br/>
    Link: <a href="{{ route('guest.precheckin', ['reference' => $booking->reference]) }}">{{route('guest.precheckin', ['reference' => $booking->reference])}}</a><br/>
    <br/><br/>
    <b>Your accommodation uses a self-check-in service:</b><br/>
    Your accommodation uses a self-check-in and check-out service.<br/>
    You will receive detailed information by email 48 hours before your arrival to locate it easily.<br/>
    <br/><br/>
    <b>Your accommodation benefits from a regular check-in:</b><br/>
    A Jeffrey Host welcomer will contact you 1 or 2 days before your arrival to organize an<br/>
    appointment for the key collection and they will show you your accommodation.<br/>
    <br/><br/>
    <b>Important:</b><br/>
    If you do not return the property keys at the correct designated location or if you lose them,<br/>
    you will be charged extra fees.<br/>
    <br/><br/>
    <b>In case of late arrival:</b><br/>
    Check-in after 9:00 p.m. or outside the relay point's opening hours is possible with an additional charge of € 30 in cash to be paid directly to the welcomer when you check in.<br/>
    You can now concentrate on tourist guides and preparing for your trip.<br/>
    <br/><br/>
    We wish you a pleasant day, your stay at "{{$booking->property->name}}" awaits you.<br/>
@endif
@include('emails.guest.footer')
@include('emails.footer')
