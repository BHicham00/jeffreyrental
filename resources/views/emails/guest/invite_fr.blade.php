@include('emails.header')
@if($guest)
    Bonjour {{$guest->firstname . " " . $guest->lastname}},<br/>
    <br/>
    Vous avez réservé un séjour à "{{$booking->property->name}}"<br/>
    Afin d’échanger avec vous les informations pour récupérer les clés et l’adresse du logement,<br/>
    il est indispensable de faire votre pré check-in en cliquant sur le lien suivant<br/>
    <br/>
    Lien : <a href="{{ route('guest.precheckin', ['reference' => $booking->reference]) }}">{{route('guest.precheckin', ['reference' => $booking->reference])}}</a><br/>
    <br/>
    <br/>
    <b>Votre logement est en entrée autonome</b><br/>
    Votre logement bénéficie d'une entrée et d'un départ autonome.<br/>
    48 heures avant votre arrivée, vous recevrez toutes les informations nécessaires par mail<br/>
    pour le rejoindre facilement.<br/>
    <br/><br/>
    <b>Votre logement bénéficie d’un Check-in physique</b><br/>
    Un welcomer Jeffrey Host, vous contactera dans les 2 jours qui précédent votre arrivée afin d'organiser la remise des clés et vous présenter votre logement.<br/>
    <br/><br/>
    <b>Important</b><br/>
    Si vous ne restituez pas ou si vous venez à perdre les clés du logement celle-ci vous serons facturées.<br/>
    <br/><br/>
    <b>En cas d’arrivée tardive</b><br/>
    Le check-in après 21h00 ou en dehors des heures d’ouverture du point relais sont possible avec un supplément de 30 € en espèces à payer directement au welcomer que vous verrez.<br/>
    <br/><br/>
    Vous pouvez dès à présent vous concentrer sur les guides touristiques et la préparation de votre voyage.<br/>
    Nous vous souhaitons une agréable journée, votre séjour à "{{$booking->property->name}}" vous attendent.<br/>
@endif
@include('emails.guest.footer')
@include('emails.footer')
