@include('emails.header')
Hello {{$user->firstname}},<br/>
<br/>
here is your one time pass code : <strong>{{ $pass }}</strong>
<br/>
Best regards.<br/>
@include('emails.guest.footer')
@include('emails.footer')
