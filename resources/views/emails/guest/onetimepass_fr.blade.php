@include('emails.header')
Bonjour {{$user->firstname}},<br/>
<br/>
Voici votre mot de passe à usage unique : <strong>{{ $pass }}</strong>
<br/>
Bien cordialement.<br/>
@include('emails.guest.footer')
@include('emails.footer')
