@include('emails.header')
Cher(e) propriétaire,<br/>
<br/>
Vous avez fournis récemment un e-mail à Jeffrey Host pour vous inscrire à nos services.<br/>
<br/>
Veuillez trouver ci-dessous votre lien personnalisé pour créer votre compte propriétaire.
Il vous sera demandé d’entrer vos informations personnelles ainsi que les détails de votre logement que nous reprendrons pour créer votre annonce, il est important de parfaitement remplir les champs.<br/>
<br/>
Votre lien d'inscription: <a href="{{$link}}">{{ $link }}</a><br/>
<br/>
Une fois votre fiche propriétaire rempli, vous pourrez télécharger votre contrat pour le signer et le re-télécharger dans votre espace propriétaire.<br/>
<br/>
Également, vous retrouverez dans votre espace divers documents, comme l’état des lieux,
l’inventaire, l’attestation de remise de clés et vos relevés ainsi que le lien de votre calendrier.<br/>
<br/>
<br/>
@if($bnbkeys)
    En partenariat avec
    <img width="125" src="{{asset("images/bnbkeys.jpg")}}" />
@endif
@include('emails.footer')
