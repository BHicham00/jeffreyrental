@include('emails.header')
Bonjour {{ $user->name }},<br/>
<br/>
Nous vous informons que suite au vote de la loi pour une République Numérique, qui est applicable à l'ensemble du territoire à compter du 1er janvier 2019,<br/>
la loi prévoit que l'ensemble des loueurs de meublés de tourisme (résidence principale, résidence secondaire, pied-à-terre…) doivent faire une déclaration en ligne, qui vous permettra d’obtenir votre numéro d’enregistrement. Pour se faire, il suffit de cliquer sur les liens en bleu dans le corps du mail.<br/>
A l’issue de votre déclaration, il faudra nous communiquer votre numéro dans les meilleurs délais car celui-ci devra obligatoirement figurer sur vos annonces de location, quel que soit le statut de votre logement.<br/>
A défaut de numéro de loueur meublé, les sites ne pourront plus publier vos annonces et vous n’aurez donc plus de réservations. <br/>
<br/>
NB:  Toutes les communes n'utilisent pas encore le site DéclaLoc pour les déclarations en ligne pour le vérifier connecter-vous au site et rentrer la commune si celle-ci n'apparait pas vous devrez contacter directement les services de la mairie de votre commune.<br/>
<br/>
Lien du formulaire de déclaration :<br/>
<a href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14004.do">https://www.formulaires.modernisation.gouv.fr/gf/cerfa_14004.do</a><br/>
<br/>
Lien site Service public pour plus d'information :<br/>
<a href="https://www.service-public.fr/particuliers/vosdroits/R14321">https://www.service-public.fr/particuliers/vosdroits/R14321</a><br/>
<br/>
Vous trouverez aussi une fiche informative en pièce jointe.
<br/>
Cordialement,<br/>
@include('emails.footer')
