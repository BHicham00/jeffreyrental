@include('emails.header')
Bonjour {{$user->name}},<br/>
<br/>
Nous sommes heureux de vous compter comme nouveau client Jeffrey Host et nous vous remercions de la confiance que vous nous accordée.
<br/>
<br/>
Nos intérêts étant communs, sachez que nous mettrons tout en oeuvre pour que notre collaboration soit des plus fructueuse.
<br/>
<br/>
En tant que nouveau client, le Service Commercial passe la main à votre chargé de compte
qui est votre City Manager, c&#39;est pourquoi pour des raisons d&#39;organisation et de réactivité, nous
vous demandons de correspondre principalement par mail avec lui.<br/>
<br/>
Désormais votre interlocuteur est votre City Manager, il vous informera et veillera au bon
fonctionnement de votre logement et il répondra à vos questions sur le plan administratif.<br/>
<br/>
Vous êtes propriétaires à :<br/>
<ul>
    <li>
        Aix-les-Bains votre adresse mail de correspondance est cityaixlesbains@jeffreyhost.com
    </li>
    <li>
        Annecy et son agglomération et autour du lac votre adresse mail de correspondance est cityannecy@jeffreyhost.com
    </li>
    <li>
        Chamonix, Argentière et Les Houches votre adresse mail de correspondance est citychamonix@jeffreyhost.com
    </li>
    <li>
        Megève et Saint-Gervais-les-Bains votre adresse mail de correspondance est citymegeve@jeffreyhost.com
    </li>
    <li>
        Lyon votre adresse mail de correspondance est citylyon@jeffreyhost.com
    </li>
    <li>
        Lille votre adresse mail de correspondance est citylille@jeffreyhost.com
    </li>
    <li>
        Rennes votre adresse mail de correspondance est cityrennes@jeffreyhost.com
    </li>
    <li>
        Saint-Malo votre adresse mail de correspondance est citystmalo@jeffreyhost.com
    </li>
    <li>
        Bordeaux votre adresse mail de correspondance est citybordeaux@jeffreyhost.com
    </li>
    <li>
        Arcachon et Biscarosse votre adresse mail de correspondance est cityarcachon@jeffreyhost.com
    </li>
    <li>
        Aix-en-Provence, Meyreuil, Coudoux, Fuveau et Ventabren votre adresse mail de correspondance est
        cityaixenprovence@jeffreyhost.com
    </li>
    <li>
        Marseille, la Ciotat et Cassis votre adresse mail de correspondance est citymarseille@jeffreyhost.Com
    </li>
    <li>
        Cannes, Antibes et Grasse votre adresse mail de correspondance est citycannes@jeffreyhost.com
    </li>
    <li>
        Nice, Saint-Laurent-du -Var, Cagnes-sur-Mer, Beaulieu, Cap d’Ail et Beausoleil votre adresse mail de
        correspondance
        est citynice@jeffreyhost.com
    </li>
    <li>
        Strasbourg votre adresse mail de correspondance est citystrasbourg@jeffreyhost.com
    </li>
    <li>
        Genève votre adresse mail de correspondance est citygeneva@jeffreyhost.com
    </li>
</ul>
<br/>
Nous vous souhaitons la meilleur collaboration.<br/>
<br/>
Bien cordialement.<br/>

@include('emails.footer')
