@extends('guest.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __("Accueil") }}</div>
                <style>
                    .fields-row{
                        margin-bottom: 10px;
                    }
                </style>
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                        {{ __("Bienvenue sur Jeffrey Host") }} {{$user->firstname}}.
                    <br/>
                    <br/>
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ __("Votre réservation") }} : {{ $booking->property->name }} du {{ $booking->start_at }} au {{$booking->end_at}}</div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="fields-group">
                                    <div class="fields-row">
                                        <label>{{ __("Référence de réservation") }}</label>
                                        <div class="input-like">{{ $booking->reference }}</div>
                                    </div>
                                    <div class="fields-row">
                                        <label>{{ __("Date de séjour") }}</label>
                                        <div class="input-like">du {{ $booking->start_at }} au {{$booking->end_at}}</div>
                                    </div>
                                    <div class="fields-row">
                                        <label>{{ __("Adresse de la résidence") }}</label>
                                        <div class="input-like">{{ $booking->property->address }}</div>
                                    </div>
                                    @if($booking->property->city_id)
                                    <div class="fields-row">
                                        <label>{{ __("Ville de la résidence") }}</label>
                                        <div class="input-like">{{ $booking->property->city->nom_commune . ' ' . $booking->property->city->getPostal() }}</div>
                                    </div>
                                    @endif
                                    <div class="fields-row">
                                        <label>{{ __("Pays de la résidence") }}</label>
                                        <div class="input-like">{{ $booking->property->country->nicename }}</div>
                                    </div>
                                    @if($booking->property->keynest_auto)
                                        <a href="{{route('guest.sendkeycode')}}"><button class="btn btn-primary" {{($booking->deposit && !$booking->deposit->paid) ? "disabled" : ""}}>{{ _('Recevoir les instructions pour récupérer les clés') }}</button></a>
                                    @endif
                                    @if($booking->property->keynest_auto_checkout)
                                        <a href="{{route('guest.sendcheckout')}}"><button class="btn btn-primary" {{($booking->deposit && !$booking->deposit->paid) ? "disabled" : ""}}>{{ _('Recevoir les instructions pour déposer les clés') }}</button></a>
                                    @endif
                                    @if(($booking->property->keynest_auto || $booking->property->keynest_auto_checkout) && $booking->deposit && !$booking->deposit->paid)
                                    <div class="alert alert-danger">
                                        {{__('Vous devez payer votre caution avant de pouvoir accéder à ces informations !')}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 error">
                                <div class="fields-row">
                                    <label>{{ __("Nom de la résidence") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->residence ?: __('Aucun') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Entrée") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->entry ?: __('Aucune') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Accès") }}</label>
                                    {{$booking->property->detail->access}}
                                    <div class="input-like">{{ \App\PropertyDetails::accessTypes()[$booking->property->detail->access] }}</div>
                                </div>
                                @if($booking->property->detail->access == 'code')
                                    <div class="fields-row">
                                        <label>{{ __("Code") }}</label>
                                        <div class="input-like">{{ $booking->property->detail->access_code }}</div>
                                    </div>
                                @endif
                                <div class="fields-row">
                                    <label>{{ __("Etage") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->floor ?: __('Aucun') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Ascenseur") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->elevator ? _('Oui') : _('Non') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Indication du logement (n° de porte, autre)") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->door ?: _('Aucune') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Accès Parking") }}</label>
                                    <div class="input-like">{{ \App\PropertyDetails::accessCarTypes()[$booking->property->detail->access_car] }}</div>
                                </div>
                                @if($booking->property->detail->access_car == 'code')
                                    <div class="fields-row">
                                        <label>{{ __("Code") }}</label>
                                        <div class="input-like">{{ $booking->property->detail->access_car_code }}</div>
                                    </div>
                                @endif
                                <div class="fields-row">
                                    <label>{{ __("Indication de parking") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->parking ?: __('Aucune') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Wifi SSID") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->wifi_login ?: _('Aucun') }}</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Wifi password") }}</label>
                                    <div class="input-like">{{ $booking->property->detail->wifi_pass ?: __('Aucun') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($booking->deposit)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ __("Votre caution") }}</div>
                        <div class="panel-body">
                            <div class="fields-group">
                                @if(!$booking->deposit->paid)
                                <div class="fields-row">
                                    <label>{{ __("Caution à régler")}}</label>
                                    <div class="input-like">{{$booking->deposit->amount}} €</div>
                                </div>
                                <div class="fields-row">
                                    <label>{{ __("Date limite")}}</label>
                                    <div class="input-like"><b>{{\Carbon\Carbon::create($booking->start_at)->addDays(-1)->format("d/m/Y")}}</b></div>
                                </div>
                                <a href="{{$booking->deposit->getLink()}}"><button class="btn btn-danger">{{ __('Payer ma caution') }}</button></div></a>
                                @else
                                <a href="{{$booking->deposit->getLink()}}"><button class="btn btn-primary">{{ __('Voir ma caution') }}</button></div></a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="panel panel-default">
                        <div class="panel-heading">{{ __("Vos informations") }}</div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="fields-group">
                                    <div class="fields-row">
                                        <label>{{ __("Prénom")}}</label>
                                        <div class="input-like">{{ $user->firstname }}</div>
                                    </div>
                                    <div class="fields-row">
                                        <label>{{ __("Nom")}}</label>
                                        <div class="input-like">{{ $user->lastname }}</div>
                                    </div>
                                    <div class="fields-row">
                                        <label>{{ __("Téléphone")}}</label>
                                        <div class="input-like">{{ $user->phone }}</div>
                                    </div>
                                    <div class="fields-row">
                                        <label>{{ __("Email")}}</label>
                                        <div class="input-like">{{ $user->email }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                {{ Form::open(['class' => 'property-form', 'id' => 'update-form', 'method'=>'POST', 'action' => ['Guest\AccountController@updateCheckin', $booking->reference]]) }}
                                <div class="fields-row">
                                    {{ Form::label('arrival', __('Heure d\'arrivée').' *') }}
                                    {{ Form::text('arrival', (new \Carbon\Carbon($user->arrival))->format('d/m/Y H:i'), ["class" => "dates monthly"]) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('checkin', __('Heure de Checkin').' *') }}
                                    {{ Form::text('checkin', (new \Carbon\Carbon($user->checkin))->format('d/m/Y H:i'), ["class" => "dates monthly"]) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('checkout', __('Heure de Checkout').' *') }}
                                    {{ Form::text('checkout', (new \Carbon\Carbon($user->checkout))->format('d/m/Y H:i'), ["class" => "dates monthly"]) }}
                                </div>
                                {{ Form::submit(__('Mettre à jour'), ['class' => 'btn btn-primary']) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
    <style>
        .yearly table.table-condensed .monthselect,
        .yearly table.table-condensed thead tr:nth-child(2),
        .yearly table.table-condensed tbody,
        .monthly table.table-condensed thead tr:nth-child(2),
        .monthly table.table-condensed tbody {
            display: none;
        }

        .daterangepicker.monthly .drp-calendar,
        .daterangepicker.yearly .drp-calendar {
            width: 1000px !important;
        }

        .yearly table.table-condensed .yearselect {
            width: 100%;
        }
    </style>
@endsection

@section("scripts")
    <script>
        $(function () {
            let i = 0;
            $('.dates').each(function() {
                i++;
                $(this).daterangepicker({
                    timePicker: true,
                    timePicker24Hour: true,
                    singleDatePicker: true,
                    autoUpdateInput: true,

                    minDate: "{{(new \Carbon\Carbon($booking->start_at))->format('d/m/Y')}} "+ (i == 2 ? "15:00" : "00:00"),
                    maxDate: (i == 3 ? "{{(new \Carbon\Carbon($booking->end_at))->format('d/m/Y')}} 11:00" : "{{(new \Carbon\Carbon($booking->start_at))->format('d/m/Y')}} 23:59"),
                    locale: {
                        format: 'DD/MM/Y H:mm'
                    }
                }).data().daterangepicker.container.addClass('monthly');
            });

            $('.expiration').daterangepicker({
                singleDatePicker:true,
                autoUpdateInput: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/Y'
                }
            });


            var datepicker = $('.expiration_card');
            var pickerConfig = function(period) {
                var config = {};
                switch (period) {
                    case "daily":
                        config = {
                            showDropdowns: true,
                            singleDatePicker: true,
                            autoUpdateInput: true,
                            locale: {
                                format: "DD MMMM YYYY",
                                cancelLabel: 'Clear'
                            }
                        };
                        break;
                    case "monthly":
                        config = {
                            showDropdowns: true,
                            singleDatePicker: true,
                            autoUpdateInput: true,
                            locale: {
                                format: "MM/YYYY",
                                cancelLabel: 'Clear'
                            }
                        };
                        break;
                    case "yearly":
                        config = {
                            showDropdowns: true,
                            singleDatePicker: true,
                            autoUpdateInput: true,
                            locale: {
                                format: "YYYY",
                                cancelLabel: 'Clear'
                            }
                        };
                        break;
                    case "range":
                        config = {
                            showDropdowns: true,
                            autoUpdateInput: true,
                            locale: {
                                format: "DD MMMM YYYY",
                                cancelLabel: 'Clear'
                            }
                        };
                        break;
                    default:
                        config = null;
                        break;
                }
                return config;
            };
            var pickerEvent = function(picker, period) {
                switch (period) {
                    case "monthly":
                    case "yearly":
                        picker.element.on('hide.daterangepicker', function(ev, instance) {
                            /*
                             * i selected the third row because there was month
                             * that date 1 on second row, so i feel to keep it save
                             * with choosing the third row
                             */
                            var td = $(instance.container).find('.table-condensed tbody tr:nth-child(3) td:first-child');
                            /*
                             * the setTimeout have on purpose to delay calling trigger
                             * event when choosing date on third row, if you not provide
                             * the timeout, it will throw error maximum callstack
                             */
                            setTimeout(function() {
                                /*
                                 * on the newer version to pick some date was changed into event
                                 * mousedown
                                 */
                                td.trigger('mousedown');
                                /*
                                 * this was optional, because in my case i need send date with
                                 * starting day with 1 to keep backend neat
                                 */
                                instance.setStartDate(instance.startDate.date(1));
                                instance.setEndDate(instance.endDate.date(1));
                            }, 1);
                        })
                        break;
                    default:
                        break;
                }
            }
            var pickerInit = function(picker, period) {
                /*
                 * personally, i'm not using the jquery method,
                 * instead i'm using the constructor itself with purpose
                 * to detect if already initialized before, it will detached
                 * and destroy the picker element and reinitialize the picker
                 */
                if (picker instanceof daterangepicker) {
                    element = picker.element;
                    picker.element.off('.daterangepicker');
                    picker.element.removeData();
                    picker.container.remove();
                    picker = element;
                }
                datepicker = new daterangepicker(picker, pickerConfig(period));
                pickerEvent(datepicker, period);
                /* this was needed to make some change to what we see on the dom */
                datepicker.container.addClass(period);
            }
            pickerInit(datepicker, "monthly");
            /*$('.expiration_card').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                autoUpdateInput: true,
                locale: {
                    format: 'MM/Y'
                }
            });*/

            $("#registration-form").validate({
                lang:"fr",
                rules: {
                    phone:{
                        required: true,
                        phone: true
                    },
                    card_last_digits:{
                        digits:true,
                        minlength:4,
                        maxlength:4,
                        required: true
                    }
                }
            });
        })
    $(function () {
        setTimeout(function () {
            $(".nav-tabs li").each(function () {
                if (!$(this).hasClass("active")) {
                    $($(this).find("a").attr("href")).removeClass("active");
                }
            });
        }, 1000)
    });
</script>
@append
