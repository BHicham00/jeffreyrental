@extends('guest.layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                        {!!
                        __("Précheckin du séjour") . " : " . $booking->reference . " - " . $guest->firstname . " " . $guest->lastname . "<br/>" .
                        __("du") . " " . $booking->start_at . " " . __("to") . " " . $booking->end_at . " " . __("at") . " \"" . $booking->property->name . "\""
                        !!}
                    </h3>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $name => $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="alert alert-warning">
                    {{ __("Les informations suivi d'un * sont obligatoires") }}
                </div>
                <div class="panel-content">
                    {{ Form::open(['class' => 'property-form', 'id' => 'registration-form', 'method'=>'POST', 'enctype' =>'multipart/form-data', 'action' => ['Guest\HomeController@precheckin', $booking->reference]]) }}
                    <div>
                        <h2>{{ __("Informations :") }}</h2>
                        <div id="user-info" class="field-group clearfix">
                            <div class="col-md-6">
                                <div class="fields-row">
                                    {{ Form::label('firstname', __('Prénom').' *') }}
                                    {{ Form::text('firstname', $guest->firstname, ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('lastname', __('Nom').' *') }}
                                    {{ Form::text('lastname', $guest->lastname,  ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('email', __('E-mail').' *') }}
                                    {{ Form::email('email', '',  ['required' => 'required', 'palceholder' => "mail@mail.com"]) }}
                                    <br/>
                                    <span><i class="fa fa-info-circle"></i> {{ __("Cette email doit être consultable pour vous connecter à notre portail")  }}</span>
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('phone', __('Téléphone').' *') }}
                                    {{ Form::text('phone', $guest->phone, ['data-rule-mobile' => true, 'required' => 'required', 'placeholder' => '+33 1 08 08 08 08']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="fields-row">
                                    {{ Form::label('transport_type', __('Type de transport').' *') }}
                                    {{ Form::select('transport_type', $transportTypes, ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('transport_number', __('Numéro Transport (vol/train)')) }}
                                    {{ Form::text('transport_number', '', []) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('arrival', __('Heure d\'arrivée').' *') }}
                                    {{ Form::text('arrival', (new \Carbon\Carbon($booking->start_at))->format('d/m/Y') . ' 12:00', ["class" => "dates monthly"]) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('checkin', __('Heure de Checkin').' *') }}
                                    {{ Form::text('checkin', (new \Carbon\Carbon($booking->start_at))->format('d/m/Y') . ' 15:00', ["class" => "dates monthly"]) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('checkout', __('Heure de Checkout').' *') }}
                                    {{ Form::text('checkout', (new \Carbon\Carbon($booking->end_at))->format('d/m/Y') . ' 10:00', ["class" => "dates monthly"]) }}
                                </div>
                            </div>
                        </div>
                        <h2>{{ __("Identification :") }}</h2>
                        <div class="field-group clearfix">
                            <div class="col-md-12">
                                <div class="fields-row">
                                    {{ Form::label('country_id', __('Pays').' *') }}
                                    {{ Form::select('country_id', countries(), ['required' => 'required', 'default' => 73]) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('ident_type', __('Type de document d\'identité').' *') }}
                                    {{ Form::select('ident_type', $identTypes, ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('ident_number', __('Numéro du document d\'identité').' *') }}
                                    {{ Form::text('ident_number', '', ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('ident_file', __('Photo du document d\'identité').' *') }}
                                    {{ Form::file('ident_file', ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('ident_expiration', __('Expiration du document').' *') }}
                                    {{ Form::text('ident_expiration', '', ['required' => 'required', "class" => "expiration"]) }}
                                </div>
                            </div>
                        </div>
                        @if($booking->source !== "Airbnb")
                        <h2>{{ __("Information de paiement")." :" }}</h2>
                        <div class="field-group clearfix">
                            <div class="col-md-12">
                                <div class="fields-row">
                                    {{ Form::label('card_type', __('Type de carte').' *') }}
                                    {{ Form::select('card_type', $cardTypes, ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('card_holder', __('Nom du porteur').' *') }}
                                    {{ Form::text('card_holder', $guest->firstname . ' ' . $guest->lastname, ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('card_last_digits', __('4 derniers numéros de la carte').' *') }}
                                    {{ Form::text('card_last_digits', '', ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('card_expiration', __('Expiration de la carte').' *') }}
                                    {{ Form::text('card_expiration', '', ['required' => 'required', "class" => "expiration_card"]) }}
                                </div>
                            </div>
                        </div>
                        @endif

                    </div>

                    <div>
                        {!! Recaptcha::render() !!}
                        {{ Form::submit(__('Envoyer'), ['class' => 'btn btn-primary']) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
    <style>
        .yearly table.table-condensed .monthselect,
        .yearly table.table-condensed thead tr:nth-child(2),
        .yearly table.table-condensed tbody,
        .monthly table.table-condensed thead tr:nth-child(2),
        .monthly table.table-condensed tbody {
            display: none;
        }

        .daterangepicker.monthly .drp-calendar,
        .daterangepicker.yearly .drp-calendar {
            width: 1000px !important;
        }

        .yearly table.table-condensed .yearselect {
            width: 100%;
        }
    </style>
@endsection

@section("scripts")
<script>
    $(function () {
        let i = 0;
        $('.dates').each(function() {
            i++;
            $(this).daterangepicker({
                timePicker: true,
                timePicker24Hour: true,
                singleDatePicker: true,
                autoUpdateInput: true,

                minDate: "{{(new \Carbon\Carbon($booking->start_at))->format('d/m/Y')}} "+ (i == 2 ? "15:00" : "00:00"),
                maxDate: (i == 3 ? "{{(new \Carbon\Carbon($booking->end_at))->format('d/m/Y')}} 11:00" : "{{(new \Carbon\Carbon($booking->start_at))->format('d/m/Y')}} 23:59"),
                locale: {
                    format: 'DD/MM/Y H:mm'
                }
            }).data().daterangepicker.container.addClass('monthly');
        });

        $('.expiration').daterangepicker({
            singleDatePicker:true,
            autoUpdateInput: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/Y'
            }
        });


        var datepicker = $('.expiration_card');
        var pickerConfig = function(period) {
            var config = {};
            switch (period) {
                case "daily":
                    config = {
                        showDropdowns: true,
                        singleDatePicker: true,
                        autoUpdateInput: true,
                        locale: {
                            format: "DD MMMM YYYY",
                            cancelLabel: 'Clear'
                        }
                    };
                    break;
                case "monthly":
                    config = {
                        showDropdowns: true,
                        singleDatePicker: true,
                        autoUpdateInput: true,
                        locale: {
                            format: "MM/YYYY",
                            cancelLabel: 'Clear'
                        }
                    };
                    break;
                case "yearly":
                    config = {
                        showDropdowns: true,
                        singleDatePicker: true,
                        autoUpdateInput: true,
                        locale: {
                            format: "YYYY",
                            cancelLabel: 'Clear'
                        }
                    };
                    break;
                case "range":
                    config = {
                        showDropdowns: true,
                        autoUpdateInput: true,
                        locale: {
                            format: "DD MMMM YYYY",
                            cancelLabel: 'Clear'
                        }
                    };
                    break;
                default:
                    config = null;
                    break;
            }
            return config;
        };
        var pickerEvent = function(picker, period) {
            switch (period) {
                case "monthly":
                case "yearly":
                    picker.element.on('hide.daterangepicker', function(ev, instance) {
                        /*
                         * i selected the third row because there was month
                         * that date 1 on second row, so i feel to keep it save
                         * with choosing the third row
                         */
                        var td = $(instance.container).find('.table-condensed tbody tr:nth-child(3) td:first-child');
                        /*
                         * the setTimeout have on purpose to delay calling trigger
                         * event when choosing date on third row, if you not provide
                         * the timeout, it will throw error maximum callstack
                         */
                        setTimeout(function() {
                            /*
                             * on the newer version to pick some date was changed into event
                             * mousedown
                             */
                            td.trigger('mousedown');
                            /*
                             * this was optional, because in my case i need send date with
                             * starting day with 1 to keep backend neat
                             */
                            instance.setStartDate(instance.startDate.date(1));
                            instance.setEndDate(instance.endDate.date(1));
                        }, 1);
                    })
                    break;
                default:
                    break;
            }
        }
        var pickerInit = function(picker, period) {
            /*
             * personally, i'm not using the jquery method,
             * instead i'm using the constructor itself with purpose
             * to detect if already initialized before, it will detached
             * and destroy the picker element and reinitialize the picker
             */
            if (picker instanceof daterangepicker) {
                element = picker.element;
                picker.element.off('.daterangepicker');
                picker.element.removeData();
                picker.container.remove();
                picker = element;
            }
            datepicker = new daterangepicker(picker, pickerConfig(period));
            pickerEvent(datepicker, period);
            /* this was needed to make some change to what we see on the dom */
            datepicker.container.addClass(period);
        }
        pickerInit(datepicker, "monthly");
        /*$('.expiration_card').daterangepicker({
            showDropdowns: true,
            singleDatePicker: true,
            autoUpdateInput: true,
            locale: {
                format: 'MM/Y'
            }
        });*/
        
        $("#registration-form").validate({
            lang:"fr",
            rules: {
                phone:{
                    required: true,
                    phone: true
                },
                card_last_digits:{
                    digits:true,
                    minlength:4,
                    maxlength:4,
                    required: true
                }
            }
        });
    })
</script>
@append
