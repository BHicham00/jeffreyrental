@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __("Formulaires d'inscription") }} : {{  __("client") }} {{$clientId}}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $name => $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="alert alert-warning">
                {{  __("Les informations suivi d'un * sont obligatoires") }}
                </div>
                <div class="panel-content">
                    {{ Form::open(['class' => 'property-form', 'id' => 'registration-form', 'method'=>'POST', 'action' => ['HomeController@invitation']]) }}
                    {{ Form::hidden('entry', $token) }}
                    <div>
                        <h2>{{  __("Informations de contact") }} :</h2>
                        <div class="fields-row">
                            {{ Form::label('', __("Je suis :")) }}
                            <div class="radio-field">
                                <div class="custom-radio">
                                    {{ Form::label('user', __("un particulier"), ['class' => "custom-control-label"]) }}
                                    {{ Form::radio('society', 'user', true, ['id' => 'user', 'class' => "user-radio"]) }}
                                </div>
                                <div class="custom-radio">
                                    {{ Form::label('company', __("une société"), ['class' => "custom-control-label"]) }}
                                    {{ Form::radio('society', __("company"), false, ['id' => 'company', 'class' => "user-radio"]) }}
                                </div>
                            </div>
                        </div>
                        <div id="user-info" class="field-group">
                            <div class="col-md-6">
                                <div class="fields-row">
                                    {{ Form::label('firstname', __("Prénom *")) }}
                                    {{ Form::text('firstname', '', ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('lastname', __("Nom *")) }}
                                    {{ Form::text('lastname', '',  ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('email', __("E-mail *")) }}
                                    {{ Form::email('email', $email,  ['required' => 'required']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('phone', __("Téléphone *")) }}
                                    {{ Form::text('phone', '', ['required' => 'required', 'placeholder' => '+33 1 08 08 08 08']) }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('tva_number', __("Numéro de TVA")) }}
                                    {{ Form::text('tva_number', '', []) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="fields-row">
                                    {{ Form::label('password', __("Mot de passe pour votre accès client *")) }}
                                    {{ Form::password('password', ["id" => "password", 'required' => 'required', 'minlength' => '6']) }} {{ __("(minimum 6 caractères)") }}
                                </div>
                                <div class="fields-row">
                                    {{ Form::label('password', __("Confirmation du mot de passe *")) }}
                                    {{ Form::password('password_confirm', ['required' => 'required', 'minlength' => '6', "equalTo" => "#password"]) }}
                                </div>
                            </div>

                            <div class="fields-row">
                                {{ Form::label('address', __("Adresse de correspondance *")) }}
                                {{ Form::textarea('address', '', ['required' => 'required', 'placeholder' => __("Mon adresse personnelle complète (15 Avenue d'exemple, 95000, France)")]) }}
                            </div>
                        </div>
                        <div id="company-info" class="field-group">
                            <div class="fields-row">
                                {{ Form::label('company', __("Nom de la société")) }}
                                {{ Form::text('company') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('capital', __("Capital (€)")) }}
                                {{ Form::number('capital') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('company_number', __("Numéro d'immatriculation")) }}
                                {{ Form::text('company_number') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('company_address', __("Adresse de l'immatriculation")) }}
                                {{ Form::textarea('company_address') }}
                            </div>
                        </div>
                        <div class="fields-group">
                            <div class="fields-row large">
                                {{ Form::label('properties[0][iban]', __("IBAN *")) }}
                                {{ Form::text('properties[0][iban]', '', ['required' => 'required', "data-rule-iban" => "true"]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][bic]', __("BIC *")) }}
                                {{ Form::text('properties[0][bic]', '', ['required' => 'required', "data-rule-bic" => "true"]) }}
                            </div>
                        </div>
                    </div>
                    <div>
                        <h2>{{ __("Informations du logement") }} :</h2>
                        <div id="property-0" class="fields-group">

                            <div class="fields-row large">
                                {{ Form::label('properties[0][name]', __("Libellé *")) }}
                                {{ Form::text('properties[0][name]', '', ['placeholder' => __("dénomination pour l'affichage"), 'required' => 'required']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][country_id]', __("Pays *")) }}
                                {{ Form::select('properties[0][country_id]', countries(), 73, ['required' => 'required']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][departement]', __("Département *")) }}
                                {{ Form::select('properties[0][departement]', departements(), ["required" => "required"]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][city_id]', __("Ville *")) }}
                                {{ Form::select('properties[0][city_id]', cities(), ['required' => 'required']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('property[0][type]', __('Type de résidence') . ' *') }}
                                {{ Form::select('property[0][type]', \App\Property::getTypes(), ['required' => 'required']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][address]', __("Adresse du logement *")) }}
                                {{ Form::textarea('properties[0][address]', '', ['required' => 'required', 'placeholder' => __("L'adresse de ma propriété complète (15 Avenue d'exemple, 95000, France)")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][min_price]', __("Prix minimum/nuitée")) }}
                                {{ Form::number('properties[0][min_price]', 0) }}€
                            </div>
                            
                            <div class="fields-row">
                                {{ Form::label('properties[0][rent_number]', __("Numéro de loueur meublé")) }}
                                {{ Form::text('properties[0][rent_number]', '') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][class_number]', __("Numéro de classement")) }}
                                {{ Form::text('properties[0][class_number]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][airbnb]', __('Lien Airbnb')) }}
                                {{ Form::textarea('properties[0][airbnb]', '', ['placeholder' => __('Le lien de votre précédente annonce si existante')]) }}
                            </div>
                        </div>
                        <h2>{{ __("Détails du logement") }} :</h2>
                        <div id="property-0-details" class="fields-group">
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][residence]', __('Résidence')) }}
                                {{ Form::text('properties[0][property_details][residence]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][entry]', __("Entrée")) }}
                                {{ Form::text('properties[0][property_details][entry]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][access]', __('Accès immeuble')) }}
                                {{ Form::select('properties[0][property_details][access]', [
                                    'key' => __("Clé"),
                                    'badge' => __("Badge"),
                                    'code' => __("Code"),
                                ], 'key', ['class' => 'access']) }}
                                {{ Form::text('properties[0][property_details][access_code]', '', ['class'=>'code-field access_code', 'placeholder'=>'code']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][access_car]', __("Accès garage ou résidence")) }}
                                {{ Form::select('properties[0][property_details][access_car]', [
                                    0 => __("Aucun"),
                                    'key' => __("Clé"),
                                    'badge' => __("Badge"),
                                    'code' => __("Code"),
                                ], 0, ['class' => 'access']) }}
                                {{ Form::text('properties[0][property_details][access_car_code]', '', ['class'=>'code-field access_car_code', 'placeholder'=>'code']) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][floor]', __("Etage")) }}
                                {{ Form::text('properties[0][property_details][floor]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][elevator]', __('Ascenseur')) }}
                                <div class="radio-field">
                                    <div class="custom-radio">
                                        {{ Form::label('elevator_yes', __('Oui')) }}
                                        {{ Form::radio('properties[0][property_details][elevator]', '1', true, ['id' => 'elevator_yes']) }}
                                    </div>
                                    <div class="custom-radio">
                                        {{ Form::label('elevator_no', __('Non')) }}
                                        {{ Form::radio('properties[0][property_details][elevator]', '0', false, ['id' => 'elevator_no',]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][door]', __("Indication du logement (n° de porte, autre)")) }}
                                {{ Form::text('properties[0][property_details][door]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][capacity]', __("Capacité d'accueil")) }}
                                {{ Form::number('properties[0][property_details][capacity]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][surface]', 'Surface') }}
                                {{ Form::number('properties[0][property_details][surface]') }}m²
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][parking]', __("Place de parking ou garage")) }}
                                {{ Form::text('properties[0][property_details][parking]') }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][wifi_login]', __("Wifi")) }}
                                {{ Form::text('properties[0][property_details][wifi_login]', '', ['placeholder' => __("login")]) }}
                                {{ Form::text('properties[0][property_details][wifi_pass]', '', ['placeholder' => __("mot de passe")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][water]', __("Robinet d'arrivée d'eau générale")) }}
                                {{ Form::select('properties[0][property_details][water]', [
                                    'outside' => __("Commun"),
                                    'inside' => __("dans l'appartement"),
                                ]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][electricity]', __("Tableau électrique")) }}
                                {{ Form::textarea('properties[0][property_details][electricity]', '', ['placeholder' => __("description de la localisation")]) }}
                            </div>
                            <div class="fields-row">
                                {{ Form::label('properties[0][property_details][syndic]', __("Coordonnées syndic. de Copro ou autre")) }}
                                {{ Form::textarea('properties[0][property_details][syndic]') }}
                            </div>
                        </div>
                        <h2>{{ __("Equipement du logement") }} :</h2>
                        <div id="property-0-equipment" class="fields-group">
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][rooms]', __("Nombre de chambres")) }}
                                {{ Form::select('properties[0][property_details][rooms]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][bathrooms]', __("Nombre de salle de bains")) }}
                                {{ Form::select('properties[0][property_details][bathrooms]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][wc]', __("Nombre de toilettes")) }}
                                {{ Form::select('properties[0][property_details][wc]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][bathtubs]', __("Nombre de baignoires")) }}
                                {{ Form::select('properties[0][property_details][bathtubs]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][showers]', __("Nombre de douches")) }}
                                {{ Form::select('properties[0][property_details][showers]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('kingbeds', __("Lit(s) double(s)")) }}
                                {{ Form::select('kingbeds', [140 => __("140 cm"), 160 => __("160 cm"), 180 => __("180 cm"), 200 => __("200 cm")], ["id" => "kingbeds"]) }}
                                <button class="btn btn-success" id="add-beds">+</button>
                                <div id="beds">

                                </div>
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][beds]', __("Lit(s) simple(s)")) }}
                                {{ Form::select('properties[0][property_details][beds]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][bunkbeds]', __("Lit(s) superposé(s)")) }}
                                {{ Form::select('properties[0][property_details][bunkbeds]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][couchbeds]', __("Canapé(s) lit(s)")) }}
                                {{ Form::select('properties[0][property_details][couchbeds]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][covers]', __("Couette(s)")) }}
                                {{ Form::select('properties[0][property_details][covers]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][square_pillows]', __("Oreiller(s) carré(s)")) }}
                                {{ Form::select('properties[0][property_details][square_pillows]', $options2) }}
                            </div>
                            <div class="fields-row small-label">
                                {{ Form::label('properties[0][property_details][rect_pillows]', __("Oreiller(s) rectangulaire(s)")) }}
                                {{ Form::select('properties[0][property_details][rect_pillows]', $options2) }}
                            </div>
                        </div>
                        {!! $listbox !!}
                    </div>
                    <div class="fields-group" style="text-align: center">
                        {{ __("Pour utiliser les services de Jeffrey Host vous devez accepter les") }} <a target="_blank" href="jhCgv">{{ __("Conditions Générales de Ventes") }}</a>
                        <br/>
                        <label style="cursor:pointer;" for="cgv-input">*<input required id="cgv-input" type="checkbox" name="cgv" value="1"/> {{ __("J'ai lu et j'accepte les conditions générales de ventes et d'utilisation") }}</label>
                    </div>
                    <div>
                        {!! Recaptcha::render() !!}
                        {{ Form::submit(__('Envoyer'), ['class' => 'btn btn-primary']) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script>
    $(function () {
        $("#company-info").hide();
        $(".code-field").hide();
        $(".access").change(function () {
            if ($(this).val() == 'code') {
                $(this).next('.code-field').show();
            } else {
                $(this).next('.code-field').hide();
            }
        });
        $(".user-radio").change(function () {
            if ($(this).val() == 'user') {
                $("#company-info").hide();
            } else {
                $("#company-info").show();
            }
        });
        var bedSelect = '{{ Form::select('properties[0][property_details][kingbeds]', $options2) }}';
        $("#add-beds").click(function (e) {
            e.preventDefault();
            if (!$("#beds-" + $("#kingbeds").val()).length)
            {
                var bed = $(bedSelect);
                bed.attr("name", "properties[0][property_details][k" + $("#kingbeds").val() + "]");
                bed.attr("id", "beds-" + $("#kingbeds").val());
                let wrapper = $("<label>Lits " + $("#kingbeds").val() + " :</label>");
                wrapper.append(bed);
                wrapper.appendTo("#beds");
            }
            return false;
        });

        function validPassword() {
            return $("password").val() == $("password_confirm").val();
        }
        
        $("#registration-form").validate({
            lang:"fr"
        });
    })
</script>
@append
