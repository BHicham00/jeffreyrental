<style>
    .title {
        font-size: 50px;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        display: block;
        text-align: center;
        margin: 20px 0 10px 0px;
    }

    .links {
        text-align: center;
        margin-bottom: 20px;
    }

    .links > a {
        color: #FFF;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }
</style>

<div class="title">
    Jeffrey Administrator
</div>
<div class="links">
    <a href="{{url("/admin/invitations")}}" class="btn btn-primary">Invitations client</a>
    <a href="{{url("/admin/clients")}}" class="btn btn-primary">Les clients</a>
    <a href="{{url("/admin/properties")}}" class="btn btn-primary">Les propriétés</a>
    <!--<a href="https://github.com/z-song/laravel-admin" target="_blank">Github</a>
    <a href="http://laravel-admin.org/docs"  target="_blank">Documentation</a>
    <a href="http://laravel-admin.org/demo"  target="_blank">Demo</a>-->
</div>