<?php if(!isset($isUnique))$isUnique = false; ?>

<div class="row">
    @if (!$isUnique)
    <div class="{{$viewClass['label']}}"><h4 class="pull-right">{{ $label }}</h4></div>
    @endif
    <div class="{{$viewClass['field']}}"></div>
</div>

<hr style="margin-top: 0px;">
<div id="has-many-{{$column}}" class="has-many-{{$column}}">

    <div class="has-many-{{$column}}-forms">
        <?php $pos = 0; ?>
        @if ($column == "uservalues")
        <div class="form-group">
            <label class="{{$viewClass['label']}} control-label">Recherche par nom</label>
            <input id="property_search" type="text" name="search" value=""/>
        </div>
        @endif
        @foreach($forms as $pk => $form)
        <div class="has-many-{{$column}}-form fields-group">

            @foreach($form->fields() as $field)
            {!! $field->render() !!}
            @endforeach
            <div class="form-group">
                <!--<span>{{$pos}}</span>-->
                <?php $pos++; ?>
                @if ($column != "uservalues" && !$isUnique)
                <div class="{{$viewClass['field']}}">
                    <div class="remove btn btn-warning btn-sm pull-right"><i class="fa fa-trash">&nbsp;</i>{{ trans('admin.remove') }}</div>
                </div>
                @elseif($column == "uservalues")
                <div class="{{$viewClass['field']}}">
                    <div class="calculate btn btn-primary btn-sm pull-right"><i class="fa fa-calculator">&nbsp;</i>Calculate profit</div>
                    &nbsp;
                    <div class="report btn btn-success btn-sm pull-right"><i class="fa fa-file-pdf-o">&nbsp;</i>Get report</div>
                    <!--&nbsp;
                    <div class="report boss btn btn-danger btn-sm pull-right"><i class="fa fa-file-pdf-o">&nbsp;</i>Get Boss report</div>-->
                    &nbsp;
                    <div class="invoice_doc btn btn-warning btn-sm pull-right"><i class="fa fa-file-pdf-o">&nbsp;</i>Get invoice</div>
                    <i class="ajax-loader fa fa-circle-o-notch fa-spin" style="display:none;font-size:30px;"></i>
                </div>
                @endif
            </div>
        </div>

        @endforeach
    </div>
    @if ($column == "uservalues")
    <script>
        $(function () {

            $("#property_search").keyup(function (e) {
                var search = $("#property_search").val().toLowerCase();
                $(".has-many-{{$column}}-form").each(function () {
                    $(this).show();
                    if ($(this).find(".property_id option:selected").text().toLowerCase().indexOf(search) == -1) {
                        $(this).hide();
                    }
                });
            });

            $('#has-many-uservalues').on('click', '.report', function () {
                var propertyId = $(this).closest('.has-many-{{$column}}-form').find('select.property_id').val();
                var reportId = $("#report_id").val();
                //var reportId = 1;
                var url = "{{$app->make('url')->to('/admin/reports/export/report')}}";
                url += '?property=' + propertyId;
                url += '&report=' + reportId;
                if ($(this).hasClass("boss"))
                    url += '&boss=true';
                //url += '&type=report';
                window.open(url);
            });
            $('#has-many-uservalues').on('click', '.invoice_doc', function () {
                var propertyId = $(this).closest('.has-many-{{$column}}-form').find('select.property_id').val();
                var reportId = $("#report_id").val();
                //var reportId = 1;
                var url = "{{$app->make('url')->to('/admin/reports/export/invoice')}}";
                url += '?property=' + propertyId;
                url += '&report=' + reportId;
                //url += '&type=invoice';
                window.open(url);
            });
            $('#has-many-uservalues').on('click', '.calculate', function () {
                var propertyId = $(this).closest('.has-many-{{$column}}-form').find('select.property_id').val();
                var reportId = $("#report_id").val();
                //var reportId = 1;
                var url = "{{$app->make('url')->to('/admin/invoices/create')}}";
                url += '?property=' + propertyId;
                url += '&report=' + reportId;
                var startAt = $('#start_at').val();
                var endAt = $('#end_at').val();
                url += '&from=' + startAt;
                url += '&to=' + endAt;
                console.log(url);
                var profit = $(this).closest('.has-many-{{$column}}-form').find('input.profit');
                $(this).closest('.has-many-{{$column}}-form').find('.ajax-loader').show();
                $.get(url, (data) => {
                    $(this).closest('.has-many-{{$column}}-form').find('.ajax-loader').hide();
                    profit.val(data);
                    console.log(data);
                }).fail((error) => {
                    $(this).closest('.has-many-{{$column}}-form').find('.ajax-loader').hide();
                    profit.css("color", "#F00");
                    alert("Il y a eu une erreur, veuillez réessayer plus tard");
                });
                //$(this).closest('.has-many-{{$column}}-form').find('.fom-removed').val(1);
            });



            $('#calculate-all').on('click', function () {
                var inputs = $('#has-many-uservalues').find('.calculate');
                calculateAll(inputs);
            });
            $('#calculate-all-from').on('click', function () {
                var inputs = $('#has-many-uservalues').find('.calculate');
                var index = $("#calculate-from").val();
                calculateAllFrom(index, inputs);
            });
            $('#calculate-restart').on('click', function () {
                var inputs = $('#has-many-uservalues').find('.calculate');
                calculateAll(inputs, true);
            });
            $('#export-all').on('click', function () {
                var reportId = $("#report_id").val();
                var url = "{{$app->make('url')->to('/admin/reports/export?report=')}}" + reportId;
                window.open(url);
            });
            $('#export-adar').on('click', function () {
                var reportId = $("#report_id").val();
                var url = "{{$app->make('url')->to('/admin/reports/adar?report=')}}" + reportId;
                window.open(url);
            });
            $('#export-all-reports').on('click', function () {
                var reportId = $("#report_id").val();
                var url = "{{$app->make('url')->to('/admin/reports/export/allreports?report=')}}" + reportId;
                window.open(url);
            });
            $('#export-all-invoices').on('click', function () {
                var reportId = $("#report_id").val();
                var url = "{{$app->make('url')->to('/admin/reports/export/allinvoices?report=')}}" + reportId;
                window.open(url);
            });

            function calculateOne(input, restart) {
                return new Promise(function (resolve, error) {
                    var propertyId = $(input).closest('.has-many-{{$column}}-form').find('select.property_id').val();
                    var reportId = $("#report_id").val();
                    //var reportId = 1;
                    var url = "{{$app->make('url')->to('/admin/invoices/create')}}";
                    url += '?property=' + propertyId;
                    url += '&report=' + reportId;
                    var startAt = $('#start_at').val();
                    var endAt = $('#end_at').val();
                    url += '&from=' + startAt;
                    url += '&to=' + endAt;
                    var profit = $(input).closest('.has-many-{{$column}}-form').find('input.profit');
                    if ((restart && profit.val() == 0) || !restart) {
                        console.log(url);
                        $(input).closest('.has-many-{{$column}}-form').find('.ajax-loader').show();
                        $(input).closest('.has-many-{{$column}}-form').css("background", "#f39c12");
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $(input).closest('.has-many-{{$column}}-form').offset().top
                        }, 50);
                        $.get(url, (data) => {
                            $(input).closest('.has-many-{{$column}}-form').find('.ajax-loader').hide();
                            $(input).closest('.has-many-{{$column}}-form').css("background", "transparent");
                            profit.val(data);
                            resolve();
                        }).fail(function () {
                            $(input).closest('.has-many-{{$column}}-form').find('.ajax-loader').hide();
                            $(input).closest('.has-many-{{$column}}-form').css("background", "#F00");
                            alert("Limite de requêtes dépassé");
                            error();
                        });
                    } else {
                        resolve();
                    }
                });
            }

            function calculateAll(inputs, restart) {
                var i = 0;
                var cb = () => {
                    console.log(i);
                    i++;
                    if (i < inputs.length) {
                        calculateOne(inputs[i], restart).then(cb);
                    } else {
                        alert("Calcul complet ! :)");
                    }
                };

                calculateOne(inputs[i], restart).then(cb);
            }

            function calculateAllFrom(i, inputs, restart) {
                var cb = () => {
                    console.log(i);
                    i++;
                    if (i < inputs.length) {
                        calculateOne(inputs[i], restart).then(cb);
                    } else {
                        alert("Calcul complet ! :)");
                    }
                };

                calculateOne(inputs[i], restart).then(cb);
            }
        });
    </script>
    @endif
    <template class="{{$column}}-tpl">
        <div class="has-many-{{$column}}-form fields-group">

            {!! $template !!}

            <div class="form-group">
                <label class="{{$viewClass['label']}} control-label"></label>
                <div class="{{$viewClass['field']}}">
                    <div class="remove btn btn-warning btn-sm pull-right"><i class="fa fa-trash"></i>&nbsp;{{ trans('admin.remove') }}</div>
                </div>
            </div>
            <hr>
        </div>
    </template>
    @if ($column != "uservalues" && !$isUnique)
    <div class="form-group">
        <label class="{{$viewClass['label']}} control-label"></label>
        <div class="{{$viewClass['field']}}">
            <div class="add btn btn-success btn-sm"><i class="fa fa-save"></i>&nbsp;{{ trans('admin.new') }}</div>
        </div>
    </div>
    @elseif($column == "uservalues")
    <div class="form-group">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <label class="{{$viewClass['label']}} control-label"></label>
        <div class="{{$viewClass['field']}}">
            <div id="calculate-all" class="btn btn-danger btn-sm"><i class="fa fa-dollar"></i>&nbsp;Calculer tout (attention c'est long)</div>
            <div id="calculate-restart" class="btn btn-warning btn-sm"><i class="fa fa-dollar"></i>&nbsp;Reprendre le calcul</div>
        </div>
    </div>
    <div class="form-group">
        <label class="{{$viewClass['label']}} control-label"></label>
        <div class="{{$viewClass['field']}}">
            <div id="calculate-all-from" class="btn btn-warning btn-sm">
                <i class="fa fa-dollar"></i>&nbsp;Calculer tout depuis l'index
            </div>
            <input name="calculate-from" type="number" id="calculate-from" value="0"/>
        </div>
    </div>
    <div class="form-group">
        <label class="{{$viewClass['label']}} control-label"></label>
        <div id="export-all" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>&nbsp;Exporter</div>
        <div id="export-all-reports" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>&nbsp;Télécharger tous les rapports</div>
        <div id="export-all-invoices" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>&nbsp;Télécharger toutes les factures</div>
        <div id="export-adar" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>&nbsp;Export ADAR</div>
    </div>
    <style>
        /*.select2-container{display:none;}
        .select2-container--default{display:none;}*/
        .select2-hidden {
            display:none !important;
        }
        .select2-no-results {
            display: none !important;
        }
        .has-many-{{$column}}-form .btn{
            margin:0 10px;
        }
    </style>
    @endif

</div>