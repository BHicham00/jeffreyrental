
<div class="row">
    <div class="{{$viewClass['label']}}"><h4 class="pull-right">{{ $label }}</h4></div>
    <div class="{{$viewClass['field']}}"></div>
</div>

<hr style="margin-top: 0px;">

<div id="has-one-{{$relationName}}" class="has-one-{{$relationName}}">

    <div class="has-one-{{$relationName}}-forms">
        @foreach($forms as $pk => $form)
        <div class="has-one-{{$relationName}}-form fields-group">
            @foreach($form->fields() as $field)
            {!! $field->render() !!}
            @endforeach
        </div>
        @endforeach
    </div>
    <template class="{{$relationName}}-tpl">
        <div class="has-one-{{$relationName}}-form fields-group">

            {!! $template !!}

            <div class="form-group">
                <label class="{{$viewClass['label']}} control-label"></label>
                <div class="{{$viewClass['field']}}">
                    <div class="remove btn btn-warning btn-sm pull-right"><i class="fa fa-trash"></i>&nbsp;{{ trans('admin.remove') }}</div>
                </div>
            </div>
            <hr>
        </div>
    </template>
    <!--<div class="form-group">
        <label class="{{$viewClass['label']}} control-label"></label>
        <div class="{{$viewClass['field']}}">
            <div class="add btn btn-success btn-sm"><i class="fa fa-save"></i>&nbsp;{{ trans('admin.new') }}</div>
        </div>
    </div>-->
</div>