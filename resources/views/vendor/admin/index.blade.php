<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ Admin::title() }} @if($header) | {{ $header }}@endif</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    @if(!is_null($favicon = Admin::favicon()))
    <link rel="shortcut icon" href="{{$favicon}}">
    @endif

    {!! Admin::css() !!}
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

    <script src="{{ Admin::jQuery() }}"></script>
    {!! Admin::headerJs() !!}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition {{config('admin.skin')}} {{join(' ', config('admin.layout'))}}">

@if($alert = config('admin.top_alert'))
    <div style="text-align: center;padding: 5px;font-size: 12px;background-color: #ffffd5;color: #ff0000;">
        {!! $alert !!}
    </div>
@endif

<div class="wrapper">

    @include('admin::partials.header')

    @include('admin::partials.sidebar')

    <div class="content-wrapper" id="pjax-container">
        {!! Admin::style() !!}
        <div id="app">
        @yield('content')
        </div>
        {!! Admin::script() !!}
        {!! Admin::html() !!}
    </div>

    @include('admin::partials.footer')

</div>

<button id="totop" title="Go to top" style="display: none;"><i class="fa fa-chevron-up"></i></button>
<button id="todown" title="Go to bottom" style="display: block;"><i class="fa fa-chevron-down"></i></button>

<script>
    function LA() {}
    LA.token = "{{ csrf_token() }}";
        $(function () {
            $(".globalsync").click(function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                sendRequest(url);
                return false;
            });

            function sendRequest(url) {
                $.get(url, function (data) {
                    complete(data, url);
                });
            }

            function complete(data, url) {
                if (data != 'success') {
                    sendRequest(url);
                } else {
                    alert("Le informations sont à jour");
                }
            }
        });
    $(function(){
        if(document.body.scrollHeight <= window.innerHeight){
            $('#todown').fadeOut(500);
        }
        $(window).scroll(function() {
            if (document.body.scrollTop >= document.body.scrollHeight - window.innerHeight || document.documentElement.scrollTop >= document.body.scrollHeight - window.innerHeight) {
                $('#todown').fadeOut(500);
            } else {
                $('#todown').fadeIn(500);
            }
        });

        $('#todown').on('click', function (e) {
            e.preventDefault();
            console.log(document.body.scrollHeight);
            $('html,body').animate({scrollTop: document.body.scrollHeight}, 500);
        });
    });
</script>

<!-- REQUIRED JS SCRIPTS -->
{!! Admin::js() !!}

</body>
</html>
