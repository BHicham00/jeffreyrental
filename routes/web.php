<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["domain" => config("app.guest_sub").".".config("app.root"), "as" => "guest."], function() {
    Route::get('/', function () {
        return view('guest.welcome');
    })->name('welcome');
    Route::get('/locale', 'Guest\HomeController@setLocale')->name('setlocale');
    Route::get('/precheckin/{reference}', 'Guest\HomeController@precheckin')->name('precheckin');
    Route::post('/precheckin/{reference}', 'Guest\HomeController@precheckin')->name('precheckin');
    Route::group(['prefix' => 'auth'], function () {
        Route::get('login', 'Guest\Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Guest\Auth\LoginController@login');
        Route::post('logout', 'Guest\Auth\LoginController@logout')->name('logout');
        Route::get('logout', 'Guest\Auth\LoginController@logout')->name('logout');

        // Registration Routes...
        /*if ($options['register'] ?? true) {
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
            Route::post('register', 'Auth\RegisterController@register');
        }*/
    });

    Route::group(['middleware' => 'auth:bookinguest'], function () {
        Route::get('/account', 'Guest\AccountController@index')->name("home");
        Route::post('/update', 'Guest\AccountController@updateCheckin')->name("updatecheckin");
        Route::get('/account/identity', 'Guest\AccountController@identificationFile')->name("ident");
        Route::get('/sendkeycode', 'Guest\AccountController@sendKeycode')->name("sendkeycode");
        Route::get('/sendcheckout', 'Guest\AccountController@sendCheckout')->name("sendcheckout");
    });

});

Route::group(["domain" => config("app.owners_sub").".".config("app.root")], function() {
    Route::get('/', function () {
        if(auth()->user()){
            return redirect()->route('home');
        }
        return view('welcome');
    });
    Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);
    Route::get('/timeouttest', 'HomeController@timeouttest')->name('timeouttest');
    Route::get('/invitation', 'HomeController@invitation')->name('invitation');
    Route::post('/invitation', 'HomeController@invitation')->name('invitation');

    Route::post('/api/bsSync', 'HomeController@bsSync')->name('api.bsSync');
    Route::post('/api/keynest', 'HomeController@keynestWebhook')->name('api.keynest');

    Route::get('/deposit/{id}', 'DepositController@deposit')->name("deposit");
    Route::post('/deposit/{id}', 'DepositController@deposit')->name("deposit");
    Route::get('/deposit/{id}/confirm', 'DepositController@depositConfirm')->name("depositConfirm");
    Route::post('/deposit/{id}/confirm', 'DepositController@depositConfirm')->name("depositConfirm");

    Route::group(['prefix' => 'auth'], function () {
        Auth::routes(['register' => false]);
    });

    Route::get('/jhCgv', 'HomeController@jhCgv')->name("jhCgv");

    Route::group(['middleware' => ['cgv','auth']], function(){
        Route::get('/', 'AccountController@cgv')->name("cgv");
        Route::post('/', 'AccountController@cgv')->name("cgv");
        Route::get('/account', 'AccountController@index')->name("home");
        Route::get('/properties', 'AccountController@properties')->name("properties");
        Route::get('/calendars', 'AccountController@calendars')->name("calendars");
        Route::post('/book/{id}', 'AccountController@book')->name("book");
        Route::post('/stats', 'AccountController@stats')->name("stats");
        Route::get('/property/create', 'AccountController@createProperty')->name("createProperty");
        Route::post('/property/create', 'AccountController@createProperty')->name("createProperty");

        Route::get('/property/{propertyId}', 'AccountController@property')->name("property");
        Route::post('/property/{propertyId}', 'AccountController@property')->name("property");


        Route::get('/property/{propertyId}/documents', 'AccountController@documents')->name("documents");
        Route::get('/property/{propertyId}/reports', 'AccountController@reports')->name("reports");
        Route::get('/property/{propertyId}/report/{reportId}', 'AccountController@report')->name("report");
        Route::get('/property/{propertyId}/invoice/{reportId}', 'AccountController@invoice')->name("invoice");

        //Route::get('/contract/{propertyId}', 'AccountController@createContract')->name("genContract");
        //Route::get('/signedContract/{propertyId}', 'AccountController@signedContract')->name("getContract");
        Route::get('/attestloc/{propertyId}', 'AccountController@attestloc')->name("attestlocfile");
        Route::get('/ranking/{propertyId}', 'AccountController@ranking')->name("rankingfile");
        Route::get('/inventory/{propertyId}', 'AccountController@inventory')->name("inventoryfile");
        Route::get('/annexe/{propertyId}', 'AccountController@annexe')->name("annexefile");
        Route::get('/assessment/{propertyId}', 'AccountController@assessment')->name("assessmentfile");
        Route::get('/keys/{propertyId}', 'AccountController@keys')->name("keysfile");

        //Route::post('/uploadContract/{propertyId}', 'AccountController@uploadContract')->name("uploadContract");
        Route::post('/uploadRanking/{propertyId}', 'AccountController@uploadRanking')->name("uploadRanking");
        Route::post('/uploadAttest/{propertyId}', 'AccountController@uploadAttestLoc')->name("uploadAttest");

    });
});

Route::get('/syncrentals/{id?}', 'HomeController@syncRentals');
Route::get('/testsync', 'HomeController@testsyncrentals');
Route::get('/getbookings', 'HomeController@getBookings');
